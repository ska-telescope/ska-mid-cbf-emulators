# SKA Mid.CBF Emulators

Please refer to the documentation on the Developer's portal for detailed and up-to-date information on the emulator, including usage, configuration, APIs, etc.:
[ReadTheDocs](https://developer.skao.int/projects/ska-mid-cbf-emulators/en/latest/)

Code repository: [ska-mid-cbf-emulators](https://gitlab.com/ska-telescope/ska-mid-cbf-emulators)

## <a id="installation"></a> Installation
```bash
git clone https://gitlab.com/ska-telescope/ska-mid-cbf-emulators
cd ska-mid-cbf-emulators
git submodule init
git submodule update

# add certificate files, if needed
cp <my_cert_file(s)> ./certs
```
