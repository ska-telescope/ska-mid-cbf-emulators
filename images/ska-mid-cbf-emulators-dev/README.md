# ska-mid-cbf-emulators Development Image

## Docker setup

### Build image
```
docker build -t emulator_dev:<tag name> -f <path to Dockerfile> [--build-arg <arg1>=<value1> --build-arg <arg2>=<value2> ...] <path to root folder>
```
Notes:
* `<tag name>` should be a semantic version, e.g. `v0.0.1`.
* `<path to Dockerfile>` should refer to the Dockerfile in this folder, not the root-level Dockerfile.
* `--build-arg` arguments specify arguments:
  * `NEXUS_CONAN_USER_SKA` specifies the password used for logging into conan
  * `local_cert_path` (Optional) specifies the local directory containing all certificate files to copy into the image (default `"/images/ska-mid-cbf-emulators-dev/"`)
  * `img_cert_folder` (Optional) specifies the subdirectory name to use for local certificates (default `"own_certificates"`)
  * `primary_cert_name` (Optional) specifies the filename (without extension) of the primary certificate to use wherever one specific certificate file is required (default `"MDA-cert-bundle"`)
* `<path to root folder>` must refer to the root repository folder (i.e. the location of the `pyproject.toml` file), otherwise Docker will not be able to copy the necessary files.

Example (run from root folder):
```
docker build -t emulator_dev:v0.0.1 -f ./images/ska-mid-cbf-emulators-dev/Dockerfile .
```

Example (run from this folder):
```
docker build -t emulator_dev:v0.0.1 -f ./Dockerfile ../../
```

Example (with build args):
```
docker build -t emulator_dev:v0.0.1 -f ./images/ska-mid-cbf-emulators-dev/Dockerfile --build-arg local_cert_path="/certificates_folder" --build-arg img_cert_folder="custom_certificates" --build-arg primary_cert_name="my-certificate-bundle" .
```

### Create + start container (Run)
```
docker run --name emulator_dev -v <mount directory>:/emulator_dev -it emulator_dev:<tag name>
```
Notes:
* `<mount directory>` should refer to a folder on your local machine which will be mounted to the Docker image. *\*\*The mount directory must be an absolute path in Docker v4.23.x and earlier.*
* The command must be run from a Linux/Mac terminal (incl. WSL) or Windows PowerShell due to the `-it` option. Other terminal programs may or may not work correctly.
* To exit a running container, press `Ctrl+C` (or `Ctrl+D` in PowerShell).

Example:
```
docker run --name emulator_dev -v ~/emulator_dev_mount_folder/:/emulator_dev -it emulator_dev:v0.0.1
```

### Using container with EFK logging
In order to provide access between the containers running the Elasticsearch FluentD Kibana (EFK) stack, the container must gain access to the network via  `--network host`, or joining the network directly with `--network efk_network`.

Example:
```
docker run --name emulator_dev --network host -v ~/emulator_dev_mount_folder/:/emulator_dev -it emulator_dev:v0.0.1
```

### Start existing container
```
docker start -i emulator_dev
```

## Running Python with Poetry
To use poetry-installed packages, run Python scripts with:
```
poetry run python3.12 <python_script_file>
```
### Running with GUI from wsl
docker run -it --name emulator_dev -v /home/jturner/projects/ska-mid-cbf-emulators:/emulator_dev -v /tmp/.X11-unix:/tmp/.X11-unix -v /mnt/wslg:/mnt/wslg -e DISPLAY -e WAYLAND_DISPLAY -e XDG_RUNTIME_DIR -e PULSE_SERVER emulator_dev:v0.0.1

### Intellisense with VSCode
Run the following: 
  1. poetry config virtualenvs.in-project true
  2. poetry env list  # shows the name of the current environment
  3. poetry env remove <current environment>
  4. poetry install  # will create a new environment using your updated configuration