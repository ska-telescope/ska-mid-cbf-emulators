#!/bin/bash

# # ############################################
# # # Bitstream Emulator
# # ############################################
cd /emulator_dev && poetry install
git clone --depth=1 https://gitlab.com/ska-telescope/ska-mid-cbf-bitstreams /emulator_dev/ska-mid-cbf-bitstreams

bash
