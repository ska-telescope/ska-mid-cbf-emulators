import os
import sys

import click
from emulator_injector.features.injector.injector_controller import InjectorController

from ska_mid_cbf_emulators.common import EmulatorLogLevel, LoggerFactory


# help lines intentionally overindented for better option name readability
@click.command()
@click.option('-v', '--verbosity', default=-1, type=int, show_default=False,
                                help=('Verbosity level for messages printed to the console. '  # noqa: E127
                                      'May be provided as a string (case insensitive) or an integer. '
                                      'Available options are: 0 (NONE), 1 (ERROR), 2 (WARNING), '
                                      '3 (INFO), 4 (DEBUG), and 5 (TRACE). Default is 3 (INFO).'))
def run_injector(verbosity: int):
    if verbosity == -1:
        verbosity = int(os.getenv('EMULATOR_VERBOSITY', 3))
    if verbosity in EmulatorLogLevel:
        log_level = EmulatorLogLevel(verbosity)
    else:
        click.echo(f'Error: Provided verbosity level `{verbosity}` is not valid. Valid values are: '
                   '0 (NONE), 1 (ERROR), 2 (WARNING), 3 (INFO), 4 (DEBUG), and 5 (TRACE).')
        sys.exit(1)

    LoggerFactory.override_console_log_level(log_level)

    config_file_path = os.getenv('CONFIG_FILE', 'default_config.json')
    with open(config_file_path, 'r') as config_file:
        config_str = config_file.read()

    controller = InjectorController(config_str)

    try:
        controller.start()
        controller._uvicorn_server.join()
    except Exception as e:
        click.echo(f'An error occured: {e}')
    finally:
        if controller._uvicorn_server.is_alive():
            controller._uvicorn_server.terminate()
            controller._uvicorn_server.join()


if __name__ == '__main__':
    run_injector(max_content_width=150)
