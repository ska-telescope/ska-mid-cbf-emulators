from ska_mid_cbf_emulators.common import EventSeverity

injector_event_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Injected Emulator Event",
    "description": "An event sent to the emulator from the injector. It is a refined subset of the Emulator Event schema.",
    "type": "object",
    "properties": {
        "value": {
            "description": "Value of this event. Must contain an injection_type property, but the rest may be arbitrary.",
            "type": "object",
            "properties": {
                "injection_type": {
                    "description": "Type of the injection. This property must exist but its value is arbitrary.",
                    "type": "string"
                }
            },
            "required": ["injection_type"]
        },
        "severity": {
            "description": "Severity of this event. Must be a valid EventSeverity value.",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([s for s in EventSeverity]) + r")$"
        },
        "offset": {
            "description": ("Time offset of this event, in milliseconds, from start of injection. "
                            "Default value is 0 if not included. "
                            "This property must not be specified if the \"timestamp\" property is specified."),
            "type": "integer",
            "minimum": 0
        },
        "timestamp": {
            "description": ("Timestamp of this event, in milliseconds since the system epoch, regardless of injection time. "
                            "This property must not be specified if the \"offset\" property is specified."),
            "type": "integer",
            "minimum": 0
        },
    },
    "required": [
        "value",
        "severity"
    ],
    # "not required" here actually functions as "required not", i.e. if one property is provided, the other must not be.
    "dependentSchemas": {
        "offset": {
            "not": {
                "required": ["timestamp"]
            }
        },
        "timestamp": {
            "not": {
                "required": ["offset"]
            }
        }
    }
}
