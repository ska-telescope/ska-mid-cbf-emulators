injector_config_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Injector Configuration",
    "description": "Configuration object for the injector.",
    "type": "object",
    "properties": {
        "rabbitmq_host": {
            "description": "The full FQDN/hostname of the RabbitMQ server.",
            "type": "string"
        },
        "rabbitmq_port": {
            "description": "The port of the RabbitMQ server.",
            "type": "integer",
            "minimum": 0,
            "maximum": 99999
        },
        "bitstream_emulators": {
            "description": "List of bitstream emulators the injector can interact with.",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "bitstream_emulator_id": {
                        "description": "ID of the bitstream emulator to target.",
                        "type": "string"
                    },
                    "bitstream_id": {
                        "description": "ID of the bitstream this emulator uses.",
                        "type": "string"
                    },
                    "bitstream_ver": {
                        "description": "Semantic version of the bitstream this emulator uses.",
                        "type": "string",
                        "pattern": (
                            r"^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)"
                            r"(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))"
                            r"?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
                        )
                    }
                },
                "required": [
                    "bitstream_emulator_id",
                    "bitstream_id",
                    "bitstream_ver"
                ]
            },
            "minItems": 0,
            "uniqueItems": True
        }
    },
    "required": [
        "rabbitmq_host",
        "rabbitmq_port",
        "bitstream_emulators"
    ]
}
