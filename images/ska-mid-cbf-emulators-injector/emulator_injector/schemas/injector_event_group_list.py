from emulator_injector.schemas.injector_event_group import injector_event_group_schema

injector_event_group_list_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Injector Event Group List",
    "description": "Object containing a list of event groups for the injector.",
    "type": "object",
    "properties": {
        "injector_event_groups": {
            "description": "List of injector event groups.",
            "type": "array",
            "items": injector_event_group_schema
        }
    },
    "required": [
        "injector_event_groups"
    ]
}
