from emulator_injector.schemas.injector_event import injector_event_schema

injector_event_group_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Injector Event Group",
    "description": "Event group object for the injector.",
    "type": "object",
    "properties": {
        "bitstream_emulator_id": {
            "description": "ID of the bitstream emulator to send these events to.",
            "type": "string"
        },
        "ip_block_emulator_id": {
            "description": "ID of the IP block emulator within the specified bitstream emulator to send events to.",
            "type": "string"
        },
        "events": {
            "description": "List of events to send to the specified IP block.",
            "type": "array",
            "items": injector_event_schema,
            "minItems": 0,
            "uniqueItems": True
        }
    },
    "required": [
        "bitstream_emulator_id",
        "ip_block_emulator_id",
        "events"
    ]
}
