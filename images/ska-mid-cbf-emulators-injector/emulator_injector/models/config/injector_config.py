from __future__ import annotations

from typing import Self


class InjectorConfig():
    """Top-level injector configuration model.

    Contains connection information and a list of bitstream emulators.

    Args:
        rabbitmq_host (:obj:`str`): The RabbitMQ host to use.
        rabbitmq_port (:obj:`int`): The RabbitMQ port to use.
        bitstream_emulators (:obj:`dict[str, InjectorConfigEmulator]`): A dictionary of individual
            emulator configurations to use for this injector.

    Attributes:
        rabbitmq_host (:obj:`str`): The RabbitMQ host being used.
        rabbitmq_port (:obj:`int`): The RabbitMQ port being used.
        bitstream_emulators (:obj:`dict[str, InjectorConfigEmulator]`): A dictionary of the individual
            emulator configurations used for this injector.
    """

    def __init__(
        self: Self,
        rabbitmq_host: str,
        rabbitmq_port: int,
        bitstream_emulators: dict[str, InjectorConfigEmulator]
    ) -> None:
        self._rabbitmq_host = rabbitmq_host
        self._rabbitmq_port = rabbitmq_port
        self._bitstream_emulators = bitstream_emulators

    @property
    def rabbitmq_host(self: Self) -> str:
        """:obj:`str`: The RabbitMQ host for this config."""
        return self._rabbitmq_host

    @rabbitmq_host.setter
    def rabbitmq_host(self: Self, new_rabbitmq_host: str) -> None:
        self._rabbitmq_host = new_rabbitmq_host

    @property
    def rabbitmq_port(self: Self) -> int:
        """:obj:`int`: The RabbitMQ port for this config."""
        return self._rabbitmq_port

    @rabbitmq_port.setter
    def rabbitmq_port(self: Self, new_rabbitmq_port: int) -> None:
        self._rabbitmq_port = new_rabbitmq_port

    @property
    def bitstream_emulators(self: Self) -> dict[str, InjectorConfigEmulator]:
        """:obj:`dict[str, InjectorConfigEmulator]`: The dictionary of bitstream emulators for this config."""
        return self._bitstream_emulators

    @bitstream_emulators.setter
    def bitstream_emulators(self: Self, new_emulators: dict[str, InjectorConfigEmulator]) -> None:
        self._bitstream_emulators = new_emulators


class InjectorConfigEmulator():
    """Injector configuration sub-model for an individual emulators.

    This data is used to configure emulators.

    Args:
        bitstream_emulator_id (:obj:`str`): A unique identifier for the bitstream emulator.
        bitstream_id (:obj:`str`): ID of the bitstream the emulator is using (e.g. "agilex-vcc").
        bitstream_ver (:obj:`str`): Semantic version of the bitstream the emulator is using (e.g. "0.0.1").

    Attributes:
        bitstream_emulator_id (:obj:`str`): The unique identifier for this bitstream emulator.
        bitstream_id (:obj:`str`): The ID of the bitstream this emulator is using.
        bitstream_ver (:obj:`str`): The semantic version of the bitstream this emulator is using.
    """

    def __init__(self: Self, bitstream_emulator_id: str, bitstream_id: str, bitstream_ver: str) -> None:
        self._bitstream_emulator_id = bitstream_emulator_id
        self._bitstream_id = bitstream_id
        self._bitstream_ver = bitstream_ver

    @property
    def bitstream_emulator_id(self: Self) -> str:
        """:obj:`str`: The unique identifier for this bitstream emulator."""
        return self._bitstream_emulator_id

    @bitstream_emulator_id.setter
    def bitstream_emulator_id(self: Self, new_bitstream_emulator_id: str) -> None:
        self._bitstream_emulator_id = new_bitstream_emulator_id

    @property
    def bitstream_id(self: Self) -> str:
        """:obj:`str` The ID of the bitstream emulator config that will be loaded."""
        return self._bitstream_id

    @bitstream_id.setter
    def bitstream_id(self: Self, new_bitstream_id: str) -> None:
        self._bitstream_id = new_bitstream_id

    @property
    def bitstream_ver(self: Self) -> str:
        """:obj:`str` The semantic version of the bitstream emulator config that will be loaded."""
        return self._bitstream_ver

    @bitstream_ver.setter
    def bitstream_ver(self: Self, new_bitstream_ver: str) -> None:
        self._bitstream_ver = new_bitstream_ver
