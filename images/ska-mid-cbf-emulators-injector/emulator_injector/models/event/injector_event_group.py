"""Injector event interface."""

from typing import Self

from ska_mid_cbf_emulators.common import ManualEvent


class InjectorEventGroup():
    """Wrapper class containing a group of events to inject into a particular IP block of an emulator.

    Args:
        bitstream_emulator_id (:obj:`str`) The ID of the bitstream emulator to inject events into.
        ip_block_emulator_id (:obj:`str`) The ID of the IP block emulator to inject events into.
        events (:obj:`list[ManualEvent]`) A list of events to inject.

    Attributes:
        bitstream_emulator_id (:obj:`str`) The ID of the bitstream emulator that events will be injected into.
        ip_block_emulator_id (:obj:`str`) The ID of the IP block emulator that events will be injected into.
        events (:obj:`list[ManualEvent]`) The list of events that will be injected.
    """
    def __init__(
        self: Self,
        bitstream_emulator_id: str,
        ip_block_emulator_id: str,
        events: list[ManualEvent] = []
    ) -> None:
        self._bitstream_emulator_id = bitstream_emulator_id
        self._ip_block_emulator_id = ip_block_emulator_id
        self._events = events

    def __repr__(self: Self) -> str:
        return (f'<InjectorEvent with bitstream_emulator_id {self._bitstream_emulator_id}, '
                f'ip_block_emulator_id {self._ip_block_emulator_id}, events {self._events}>')

    @property
    def bitstream_emulator_id(self: Self) -> str:
        """:obj:`str`: The ID of the bitstream emulator that events will be injected into."""
        return self._bitstream_emulator_id

    @bitstream_emulator_id.setter
    def bitstream_emulator_id(self: Self, new_bitstream_emulator_id: str) -> None:
        self._bitstream_emulator_id = new_bitstream_emulator_id

    @property
    def ip_block_emulator_id(self: Self) -> str:
        """:obj:`str`: The ID of the IP block emulator that events will be injected into."""
        return self._ip_block_emulator_id

    @ip_block_emulator_id.setter
    def ip_block_emulator_id(self: Self, new_ip_block_emulator_id: str) -> None:
        self._ip_block_emulator_id = new_ip_block_emulator_id

    @property
    def events(self: Self) -> list[ManualEvent]:
        """:obj:`list[ManualEvent]`: The list of events that will be injected."""
        return self._events

    @events.setter
    def events(self: Self, new_events: list[ManualEvent]) -> None:
        self._events = new_events
