from multiprocessing import Process
from typing import Any, Self

import jsonschema
import uvicorn
from emulator_injector.models.config.injector_config import InjectorConfig
from emulator_injector.services.api.rest_api_middleware import RestAPIMiddleware
from emulator_injector.services.messaging.injector_service import InjectorService
from emulator_injector.services.parsing.injector_config_parser import InjectorConfigParser
from fastapi import FastAPI, Request, Response, status

from ska_mid_cbf_emulators.common import LoggingBase


class InjectorController(LoggingBase):
    """Main class for moving injection events into the emulators.

    A class for hooking up all of the connections needed for each emulator
    and send events to them.
    """

    def __init__(self: Self, config_str: str):
        super().__init__()
        self.fastapi_app = FastAPI()
        self._config: InjectorConfig = InjectorConfigParser.decode(config_str)
        self._configure()
        self._api_setup()

    def start(self: Self) -> None:
        """Start the injector and begin subscriptions."""
        self._uvicorn_server.start()
        self._controller_started = True

    def stop(self: Self) -> None:
        """Stop the injector and unsubscribe from any active subscriptions."""
        if self._uvicorn_server.is_alive():
            self._uvicorn_server.terminate()
            self._uvicorn_server.join()
        self._controller_started = False

    def _create_uvicorn_process(self: Self) -> Process:
        """creates a new uvicorn process"""

        return Process(
            target=uvicorn.run,
            args=[
                self.fastapi_app
            ],
            kwargs={
                'host': '0.0.0.0',
                'port': 5002
            }
        )

    def _configure(self: Self) -> None:
        """Set up services and the API server"""
        self._injector_service = InjectorService(self._config.rabbitmq_host, self._config.rabbitmq_port)
        self._uvicorn_server: Process = self._create_uvicorn_process()

    def _api_setup(self: Self) -> None:
        """Set up the API."""
        self.fastapi_app.add_middleware(RestAPIMiddleware)

        self.fastapi_app.add_api_route(
            '/server_healthcheck',
            self._get_server_healthcheck_response,
            methods=["GET"]
        )

        self.fastapi_app.add_api_route(
            '/inject',
            self._api_inject_events,
            methods=["POST"]
        )

        self.fastapi_app.openapi_schema = None
        self.fastapi_app.setup()

    async def _api_inject_events(self: Self, request: Request, response: Response):
        """Injects a JSON InjectorEvents request.

        Takes a posted JSON InjectorEvents request and injects them into the
        appropriate emulators.

        Args:
            request (:obj:`Request`): The Request object containing the posted data.
        """
        self.log_trace('inject_events called')
        injector_event_group_list_str = await request.body()
        try:
            self._injector_service.inject_event_group_list(injector_event_group_list_str)
            response.status_code = status.HTTP_200_OK
            return {}
        except jsonschema.ValidationError as ve:
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {'error_message': str(ve)}

    def _get_server_healthcheck_response(self: Self, response: Response) -> dict[str, Any]:
        """Internal API function to verify the API is accepting requests.
        For now, always returns 200 OK and an empty body.
        """
        response.status_code = status.HTTP_200_OK
        return {}
