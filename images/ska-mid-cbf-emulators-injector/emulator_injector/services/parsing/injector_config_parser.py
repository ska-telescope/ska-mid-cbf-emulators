"""Injector configuration parser/loader."""

import json

import jsonschema
from emulator_injector.models.config.injector_config import InjectorConfig, InjectorConfigEmulator
from emulator_injector.schemas.injector_config import injector_config_schema

from ska_mid_cbf_emulators.common import LoggerFactory


class InjectorConfigParser():
    """Configuration parser/loader for the injector.

    A static class providing functionality to extract config data
    from a JSON string into an InjectorConfig object.
    """

    @staticmethod
    def decode(config_str: str) -> InjectorConfig:
        """Convert JSON configuration into an InjectorConfig object.

        Takes in a JSON string containing the injector configuration
        and extracts the data into an InjectorConfig object.

        Args:
            config_str (:obj:`str`): The configuration JSON string to parse.

        Returns:
            :obj:`InjectorConfig`: The generated configuration object.
        """
        logger = LoggerFactory.get_logger()
        json_config: dict = json.loads(config_str)
        try:
            jsonschema.validate(json_config, injector_config_schema)
        except Exception as e:
            logger.error(str(e))
            raise e
        logger.debug('Config schema validation was successful.')

        config_emulators: list[dict] = json_config.get('bitstream_emulators')

        bitstream_emulators = {}
        for emulator in config_emulators:
            bitstream_emulator_id = emulator.get('bitstream_emulator_id')
            # add emulator
            bitstream_emulators[bitstream_emulator_id] = InjectorConfigEmulator(
                bitstream_emulator_id=bitstream_emulator_id,
                bitstream_id=emulator.get('bitstream_id'),
                bitstream_ver=emulator.get('bitstream_ver')
            )

        return InjectorConfig(
            rabbitmq_host=json_config.get('rabbitmq_host'),
            rabbitmq_port=json_config.get('rabbitmq_port'),
            bitstream_emulators=bitstream_emulators
        )
