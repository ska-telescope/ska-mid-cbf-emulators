import json

import jsonschema
from emulator_injector.models.event.injector_event_group import InjectorEventGroup
from emulator_injector.schemas.injector_event_group_list import injector_event_group_list_schema
from emulator_injector.services.time.time_service import TimeService

from ska_mid_cbf_emulators.common import EventSeverity, LoggerFactory, ManualEvent, ManualEventSubType


class InjectorEventGroupListParser():
    """JSON parser for a list of injector event groups."""

    @staticmethod
    def decode(json_event_group_list_str: str) -> list[InjectorEventGroup]:
        """Decode a list of JSON-encoded event groups.

        Args:
            json_events_str (:obj:`str`): The JSON string containing the injector events to decode

        Returns:
            :obj:`list[InjectorEventGroup]`: The decoded events
        """

        logger = LoggerFactory.get_logger()
        json_event_group_list: dict[str, list[dict]] = json.loads(json_event_group_list_str)
        try:
            jsonschema.validate(json_event_group_list, injector_event_group_list_schema)
        except jsonschema.ValidationError as ve:
            logger.error(str(ve))
            raise ve

        injector_events: list[InjectorEventGroup] = []
        for injector_event_group in json_event_group_list.get('injector_event_groups'):
            events = []
            for injector_event in injector_event_group.get('events'):
                if (provided_timestamp := injector_event.get('timestamp', None)) is not None:
                    timestamp = provided_timestamp
                else:
                    offset: int = injector_event.get('offset', 0)
                    timestamp = TimeService.get_timestamp(offset)

                events.append(
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value=injector_event.get('value'),
                        severity=EventSeverity(injector_event.get('severity').lower()),
                        source_timestamp=timestamp
                    )
                )

            injector_events.append(
                InjectorEventGroup(
                    bitstream_emulator_id=injector_event_group.get('bitstream_emulator_id'),
                    ip_block_emulator_id=injector_event_group.get('ip_block_emulator_id'),
                    events=events
                )
            )
        return injector_events
