from typing import Self

from emulator_injector.models.event.injector_event_group import InjectorEventGroup
from emulator_injector.services.messaging.message_service import InjectorMessageService
from emulator_injector.services.parsing.injector_event_group_list_parser import InjectorEventGroupListParser

from ska_mid_cbf_emulators.common import IdService, LoggingBase, ManualEvent


class InjectorService(LoggingBase):

    def __init__(self: Self, host: str, port: int) -> None:
        super().__init__()
        self._host: str = host
        self._port: int = port

    def inject_event_group_list(self: Self, injector_event_group_list_str: str) -> None:
        """Injects a list of injector events in string format.

        Takes a string object containing all of the JSON injector events
        and publishes them to the appropriate emulators.

        Args:
            injector_event_group_list_str (:obj:`str`): The injector events as a string.
        """

        injector_event_groups: list[InjectorEventGroup] = InjectorEventGroupListParser.decode(injector_event_group_list_str)
        for injector_event_group in injector_event_groups:
            self.inject_event_group(injector_event_group)

    def inject_event_group(self: Self, injector_event_group: InjectorEventGroup) -> None:
        """Injects an injector event group.

        Takes an injector event group, and publishes its events to the appropriate emulator/IP block.

        Args:
            injector_event_group (:obj:`InjectorEventGroup`): The injector event group to inject.
        """
        bitstream_emulator_id = injector_event_group.bitstream_emulator_id
        ip_block_emulator_id = injector_event_group.ip_block_emulator_id
        self.log_debug(f'Injecting event into: {bitstream_emulator_id}->{ip_block_emulator_id}')
        for event in injector_event_group.events:
            encoded_event = ManualEvent.encode(event)
            InjectorMessageService.publish(
                '',
                IdService.manual_queue_id(bitstream_emulator_id, ip_block_emulator_id),
                encoded_event,
                host=self._host,
                port=self._port
            )
