from pika import BlockingConnection, ConnectionParameters

from ska_mid_cbf_emulators.common import EmulatorLogger, LoggerFactory


class InjectorMessageService:
    """Message service for the Injector.

    A class for establishing RabbitMQ connections with emulators
    and publishing/injecting events into their queues.
    """
    @staticmethod
    def publish(
            exchange: str,
            queue: str,
            msg_body: str,
            host: str = 'localhost',
            port: int = 5672
    ) -> None:
        """Publishes a message to a specific exchange/queue.

        Takes a string message (assumed JSON) that will be published to
        the specified exchange/queue combination.

        Args:
            exchange (:obj:`str`): The exchange name.
            queue (:obj:`str`): The queue name.
            body (:obj:`str`): The body of the message to send (e.g. JSON data).
        """
        logger: EmulatorLogger = LoggerFactory.get_logger()
        connection = BlockingConnection(ConnectionParameters(host=host, port=port))
        channel = connection.channel()

        if exchange != '':
            channel.exchange_declare(exchange=exchange)
        if queue != '':
            channel.queue_declare(queue=queue)
        channel.basic_publish(exchange=exchange, routing_key=queue, body=msg_body)
        logger.trace(f'Published message on exchange={exchange}, queue={queue}')

        connection.close()
