from typing import Awaitable, Callable, Self

from fastapi import Request, Response, status
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware

from ska_mid_cbf_emulators.common import LoggerFactory


class RestAPIMiddleware(BaseHTTPMiddleware):
    """Handles middleware for the REST API.

    A middleware class for catching and handling internal errors
    within the REST API.
    """

    async def dispatch(self: Self, request: Request, call_next: Callable[[Request], Awaitable[Response]]) -> Response:
        """Implements the dispatch functionality for BaseHTTPMiddleware.

        Intercepts HTTP calls in order to catch exceptions and handle them.

        Args:
            request :obj:`Request` the request.
            call_next :obj:`Callable[[Request], Awaitable[Response]]` the callable function.

        Returns:
            :obj:`Response` the response.
        """
        logger = LoggerFactory.get_logger()

        try:
            return await call_next(request)
        except Exception as e:
            # log the error
            logger.error(f'Internal Rest API error: {str(e)}')

            # return error response
            return JSONResponse(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                content=f'{str(e)}'
            )
