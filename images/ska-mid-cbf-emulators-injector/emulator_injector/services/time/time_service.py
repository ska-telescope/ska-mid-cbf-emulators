import time


class TimeService:
    """Time service for the Injector.

    A class for getting fetching synchronized timestamps with
    the emulator.
    """
    @staticmethod
    def get_timestamp(offset_milliseconds: int = 0):
        """Gets a synchronized timestamp.

        Gets a timestamp that is synchronized with the emulator.

        Args:
            offset_milliseconds (:obj:`str`): Optional parameter in
            milliseconds to offset the timestamp by.
        """
        return round(time.time() * 1000 + offset_milliseconds)
