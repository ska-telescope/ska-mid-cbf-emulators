import os
import sys
from pathlib import Path

import click
from emulator_engine.features.controller import EmulatorController

from ska_mid_cbf_emulators.common import EmulatorLogLevel, LoggerFactory


# help lines intentionally overindented for better option name readability
@click.command()
@click.option('-c', '--config-file', default=None, type=str, show_default=False,
                                help=('Path to a custom emulator configuration file, if desired. '))  # noqa: E127
@click.option('-p', '--pulse-interval', default=-1, type=float, show_default=False,
                                help=('Interval in seconds between pulses. Minimum is 0.1 '  # noqa: E127
                                      '(lower will be capped). Default is 1.0. '))
@click.option('-s', '--signal-config-file', default=None, type=str, show_default=False,
                                help=('Path to a custom initial signal configuration file, if desired. '))  # noqa: E127
@click.option('-v', '--verbosity', default=-1, type=int, show_default=False,
                                help=('Verbosity level for messages printed to the console. '  # noqa: E127
                                      'May be provided as a string (case insensitive) or an integer. '
                                      'Available options are: 0 (NONE), 1 (ERROR), 2 (WARNING), '
                                      '3 (INFO), 4 (DEBUG), and 5 (TRACE). Default is 3 (INFO).'))
@click.option('--cwd', default=None, type=str, show_default=False,
                                help=('Working directory path used to resolve relative paths. '))  # noqa: E127
def run_emulator(
    config_file: str | None,
    pulse_interval: float,
    signal_config_file: str | None,
    verbosity: int,
    cwd: str | None
):
    if verbosity == -1:
        verbosity = int(os.getenv('EMULATOR_VERBOSITY', 3))
    if verbosity in EmulatorLogLevel:
        log_level = EmulatorLogLevel(verbosity)
    else:
        click.echo(f'Error: Provided verbosity level `{verbosity}` is not valid. Valid values are: '
                   '0 (NONE), 1 (ERROR), 2 (WARNING), 3 (INFO), 4 (DEBUG), and 5 (TRACE).')
        sys.exit(1)

    LoggerFactory.override_console_log_level(log_level)

    click.echo('Initializing emulator data...')

    emulators_path = os.getenv('EMULATORS_PATH', None)
    bitstream_root_path = os.getenv('BITSTREAM_ROOT_PATH', None)
    bitstream_emulator_id = os.getenv('EMULATOR_ID', None)

    if ((emulators_path or bitstream_root_path) and bitstream_emulator_id) is None:
        click.echo('Error: One or more of the required environment variables '
                   'BITSTREAM_ROOT_PATH/EMULATORS_PATH, EMULATOR_ID could not be found.')
        sys.exit(1)

    bitstream_path = emulators_path or os.path.join(bitstream_root_path, "emulators")

    if config_file is not None:
        config_file_path = Path(config_file)
        if cwd is not None and not Path.is_absolute(config_file_path):
            config_file_path = Path.joinpath(Path(cwd), config_file_path)
        if not os.path.isfile(config_file_path):
            click.echo(f'{config_file_path} could not be loaded. Please check that the path is correct.')
            sys.exit(1)
    else:
        if (
            (persona := os.getenv('PERSONA', None)) is not None
            and os.path.isfile(persona_path := os.path.join(bitstream_path, persona, 'emulator', 'config.json'))
        ):
            config_file_path = persona_path
        else:
            config_file_path = os.path.join(bitstream_path, 'config.json')

    if signal_config_file is not None:
        signal_config_file_path = Path(signal_config_file)
        if cwd is not None and not Path.is_absolute(signal_config_file_path):
            signal_config_file_path = Path.joinpath(Path(cwd), signal_config_file_path)
        if not os.path.isfile(signal_config_file_path):
            click.echo(f'{signal_config_file_path} could not be loaded. Please check that the path is correct.')
            sys.exit(1)
    elif os.path.exists(cm_path := os.path.join(os.path.dirname(os.path.abspath(__file__)), 'configmap', 'initial_signal.json')):
        signal_config_file_path = cm_path
    else:
        signal_config_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'initial_signal.json')

    with open(config_file_path, 'r') as open_config_file:
        config_str = open_config_file.read()
    with open(signal_config_file_path, 'r') as open_signal_file:
        signal_str = open_signal_file.read()

    if pulse_interval == -1:
        pulse_interval = os.getenv('PULSE_INTERVAL', 1.0)
    pulse_interval = max(pulse_interval, 0.1)

    controller = EmulatorController(config_str, signal_str, bitstream_path, bitstream_emulator_id, pulse_interval)
    controller.flush_all()
    controller.start()

    # await termination and ensure remaining threads exit cleanly
    controller.await_api_cleanup()


if __name__ == '__main__':
    run_emulator(max_content_width=150)
