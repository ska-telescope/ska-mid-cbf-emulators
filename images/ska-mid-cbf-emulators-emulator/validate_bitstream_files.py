import json
import os
import sys
import textwrap
from typing import Iterable

import click
from emulator_engine.services.ip_block_emulator.ip_block_emulator_validator import (
    IPBlockEmulatorValidator,
    IPBlockEmulatorValidatorResult,
)

from ska_mid_cbf_emulators.common import EmulatorConfigValidator, EmulatorConfigValidatorResult, ValidatorMessage, ValidatorStatus


# help lines intentionally overindented for better option name readability
@click.command()
@click.argument('ip-block-folder', type=str)
@click.option('-i', '--ip-block', default=[], show_default=True, multiple=True, type=str,
                                help=('Name of a specific IP block to validate. May be provided multiple times. '  # noqa: E127
                                      'Should match the name of a subfolder within the IP block emulator parent folder. '
                                      'If none specified, will attempt to validate every subfolder '
                                      'in the IP block emulator folder (excluding directories starting with underscores).'))
@click.option('-e', '--exclude-ip-block', default=[], show_default=True, multiple=True, type=str,
                                help=('Name of a specific IP block to NOT include in validation '  # noqa: E127
                                      'if validating the entire folder. May be provided multiple times. '
                                      'Should match the name of a subfolder within the IP block emulator parent folder. '
                                      'Will be ignored if the -i/--ip-block option is also provided.'))
@click.option('-n', '--no-ip-block', is_flag=True,
                                help=('Do not validate any IP block emulators. '  # noqa: E127
                                      'If, additionally, no config files are provided, the script will do nothing and exit.'))
@click.option('-c', '--config-file', default=[], show_default=True, multiple=True, type=str,
                                help=('Path to a config file to validate. May be provided multiple times. '  # noqa: E127
                                      'If none specified, will not attempt to validate any configurations. '
                                      '(Note that this path is relative to the working directory, NOT the IP_BLOCK_FOLDER.)'))
@click.option('-o', '--output-file', default='', show_default=False, type=str,
                                help=('A file to which validation output will be written.'  # noqa: E127
                                      'If not specified, all output will be printed to the console.'))
@click.option('-g', '--graph', is_flag=True,
                                help=('Generate a graph PNG for each config file validated, '  # noqa: E127
                                      'showing the connections between IP blocks.'))
@click.option('--graph-folder', default='./', show_default=True, type=str,
                                help=('Folder in which to put graph PNG images, if they are generated. '  # noqa: E127
                                      'Ignored if -g/--graph is not provided.'))
@click.option('-s', '--state-diagram', is_flag=True,
                                help=('Generate a state diagram PNG for each IP block emulator validated, '  # noqa: E127
                                      'provided that the validation passes for that emulator.'))
@click.option('--state-diagram-folder', default='./', show_default=True, type=str,
                                help=('Folder in which to put state diagram PNG images, if they are generated. '  # noqa: E127
                                      'Ignored if -s/--state-diagram is not provided.'))
@click.option('-p', '--pretty-print', is_flag=True,
                                help=('Convert validation output into a more readable format. '  # noqa: E127
                                      'If not enabled, the raw JSON will be printed instead.'))
@click.option('-w', '--pretty-print-width', default=150, show_default=True, type=int,
                                help='Maximum width (in # characters) of the pretty-printed output, if using.')  # noqa: E127
def validate_bitstream_files(
    ip_block_folder: str,
    ip_block: list[str],
    exclude_ip_block: list[str],
    no_ip_block: bool,
    config_file: list[str],
    output_file: str,
    graph: bool,
    graph_folder: str,
    state_diagram: bool,
    state_diagram_folder: str,
    pretty_print: bool,
    pretty_print_width: int
) -> None:
    """Validate emulator configurations and IP block emulator code within IP_BLOCK_FOLDER without running the main emulator.

    IP_BLOCK_FOLDER must be the folder containing `config.json` and the individual IP block emulator folders,
    for example: `/app/mnt/bitstream/<id>/<ver>/emulators`.

    Example usage: `validate_bitstream_files.py /app/mnt/bitstream/<id>/<ver>/emulators
    -p -i ethernet_200g_r4 -i vcc_fs_selection -c /app/mnt/bitstream/<id>/<ver>/emulators/config.json
    -o ./validation_result.txt`
    """
    click.echo('Starting validation...')

    if not no_ip_block:
        if not os.path.exists(ip_block_folder):
            click.echo(f'Error: The IP block folder {ip_block_folder} could not be found.')
            sys.exit(1)

        sys.path.append(ip_block_folder)

        if len(exclude_ip_block) and len(ip_block):
            click.echo('Warning: -e/--exclude-ip-block was provided at least once at the same time as -i/--ip-block. '
                       'All instances of -e are ignored when -i is provided.')

    out_str = 'Result: \n\n'
    final_result = ValidatorStatus.OK

    config_results: dict[str, EmulatorConfigValidatorResult] = {}
    ip_results: dict[str, IPBlockEmulatorValidatorResult] = {}

    for f in config_file:
        if not os.path.exists(f):
            click.echo(f'Warning: The specified config file `{f}` could not be found; skipping.')
            continue
        with open(f, 'r') as open_file:
            config_dict = json.load(open_file)
        click.echo(f'Validating config file: {f}...')
        config_results[f], config = EmulatorConfigValidator.validate_and_get_config(config_dict)
        out_str += f'{f}: \n' + str(config_results[f]) + '\n\n'
        final_result = max(final_result, config_results[f].overall_status)

        if graph:
            if config is None:
                click.echo(f'Warning: A graph was not generated for {f}, as validation of the config file failed.')
            elif not os.path.exists(graph_folder):
                click.echo(f'Warning: The graph folder {graph_folder} could not be found; skipping graph generation.')
            else:
                try:
                    graph_path = os.path.normpath(os.path.join(graph_folder, f'{os.path.splitext(f)[0]}.png'))
                    with open(graph_path, 'wb') as graph_file:
                        graph_file.write(config.get_graph_img())
                        click.echo(f'Successfully generated graph for {f} into {graph_path}.')
                except Exception as e:
                    click.echo(f'Failed to generate graph for {f}; exception: {e}')

    if not no_ip_block:
        ip_blocks_to_validate: Iterable[str] = (
            ip_block
            or [d for d in next(os.walk(ip_block_folder))[1] if d not in exclude_ip_block and not d.startswith('_')]
        )
        for block in ip_blocks_to_validate:
            if not os.path.exists(os.path.join(ip_block_folder, block)):
                click.echo(f'Warning: The specified IP block emulator `{block}` '
                           f'could not be found in `{ip_block_folder}`; skipping.')
                continue
            click.echo(f'Validating IP block emulator: {block}...')

            if state_diagram:
                ip_results[block], _, _, _, fsm = IPBlockEmulatorValidator.validate_and_get_components(ip_block_folder, block)
                if fsm is None:
                    click.echo(f'Warning: A state diagram was not generated for {block}, '
                               'as validation of the state machine failed.')
                elif not os.path.exists(state_diagram_folder):
                    click.echo(f'Warning: The state diagram folder {state_diagram_folder} '
                               'could not be found; skipping generation.')
                else:
                    try:
                        sd_path = os.path.normpath(os.path.join(state_diagram_folder, f'{block}.png'))
                        fsm().export_diagram_full(
                            sd_path,
                            title=f'{block} - State Machine',
                            format='png'
                        )
                        click.echo(f'Successfully generated state diagram for {block} into {sd_path}.')
                    except Exception as e:
                        click.echo(f'Failed to generate state diagram for {block}; exception: {e}')
            else:
                ip_results[block] = IPBlockEmulatorValidator.validate(ip_block_folder, block)

            out_str += f'{block}: \n' + str(ip_results[block]) + '\n\n'
            final_result = max(final_result, ip_results[block].overall_status)
    else:
        ip_blocks_to_validate = []

    if pretty_print:
        out_str = 'Result: \n\n'
        name_max = min(pretty_print_width // 5, max(map(len, list(config_file) + list(ip_blocks_to_validate))))
        name_width = name_max + 2
        status_width = max(6, 0, *map(
            len,  # 0 guarantees max() uses multi-arg impl if no msgs
            [msg.status.name for result in config_results.values() for msg in result.result]
            + [msg.status.name for result in ip_results.values() for msg_list in result.result.values() for msg in msg_list]
        )) + 2
        subcat_width = 15
        msg_width = pretty_print_width - name_width - status_width - subcat_width - 5
        msg_max = msg_width - 2

        widths = (name_width, status_width, subcat_width, msg_width)

        def _append_msg(name: str = '', status: str = '', subcat: str = '', msg: str = ''):
            nonlocal out_str
            trunc_name = name if len(name) <= name_max else f'{name[:name_max - 3]}...'
            out_str += (
                '|' + f' {trunc_name}'.ljust(name_width)
                + '|' + f' {status}'.ljust(status_width)
                + '|' + f' {subcat}'.ljust(subcat_width)
                + '|' + f' {msg}'.ljust(msg_width)
                + '|\n')

        def _append_wrapped(name: str = '', status: str = '', subcat: str = '', msg: str = ''):
            for line in textwrap.wrap(msg, width=msg_max, initial_indent='', subsequent_indent='      '):
                _append_msg(name=name, status=status, subcat=subcat, msg=line)

        def _append_divider(fill: str = '-', start_col: int = 0, left_char: str = '+', right_char: str = '+'):
            nonlocal out_str
            left_join = '|'.join([' ' * w for w in widths[: start_col]])
            right_join = '+'.join([fill * w for w in widths[start_col :]])  # noqa: E203
            main_section = f'{"|" + left_join + left_char if left_join else left_char}{right_join}'
            out_str += (main_section + right_char + '\n')

        # HEADER
        _append_divider(fill='-', left_char='/', right_char='\\')
        _append_msg(name='ITEM NAME', status='STATUS', subcat='SUBCATEGORY', msg='MESSAGE(s)')
        _append_divider(fill='=')
        footer_written = False

        # CONFIG
        for c_idx, (config_name, config_result) in enumerate(config_results.items()):
            if not len(config_result.result):
                _append_msg(name=config_name, status=config_result.overall_status.name, subcat='n/a', msg='n/a')
            else:
                for m_idx, msg in enumerate(config_result.result):
                    if m_idx == 0:
                        _append_msg(
                            name=config_name,
                            status=config_result.overall_status.name,
                            subcat='n/a',
                            msg=f'[{msg.status.name}]')
                    else:
                        _append_msg(msg=f'[{msg.status.name}]')

                    if isinstance(msg.message, str):
                        _append_wrapped(msg=str(msg.message))
                    else:
                        lines = [
                            f'Schema validation failed with error: {msg.message.get("error_message")}',
                            'Caused by instance:',
                            *(json.dumps(msg.message.get('failed_instance'), indent=4).splitlines()),
                            'Checked against schema:',
                            *(json.dumps(msg.message.get('against_schema'), indent=4).splitlines()),
                            f'at path: `{msg.message.get("schema_path")}`.'
                        ]
                        for line in lines:
                            _append_wrapped(msg=line)

                    if m_idx < len(config_result.result) - 1:
                        _append_msg()
            if c_idx == len(config_results.items()) - 1 and not len(ip_results.items()):
                _append_divider(left_char='\\', right_char='/')  # FOOTER
                footer_written = True
            else:
                _append_divider()

        # IP BLOCK EMULATORS
        for ip_idx, (ip_name, ip_result) in enumerate(ip_results.items()):
            api_results: list[ValidatorMessage] = ip_result.result.get(IPBlockEmulatorValidatorResult.API_KEY)
            if not len(api_results):
                _append_msg(name=ip_name, status=ip_result.overall_status.name, subcat='API', msg='n/a')
            else:
                for m_idx, msg in enumerate(api_results):
                    if m_idx == 0:
                        _append_msg(
                            name=ip_name,
                            status=ip_result.overall_status.name,
                            subcat='API',
                            msg=f'[{msg.status.name}]')
                    else:
                        _append_msg(msg=f'[{msg.status.name}]')

                    _append_wrapped(msg=str(msg.message))

                    if m_idx < len(api_results) - 1:
                        _append_msg()

            _append_divider(start_col=2)

            eh_results: list[ValidatorMessage] = ip_result.result.get(IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY)
            if not len(eh_results):
                _append_msg(subcat='Event Handler', msg='n/a')
            else:
                for m_idx, msg in enumerate(eh_results):
                    if m_idx == 0:
                        _append_msg(subcat='Event Handler', msg=f'[{msg.status.name}]')
                    else:
                        _append_msg(msg=f'[{msg.status.name}]')

                    _append_wrapped(msg=str(msg.message))

                    if m_idx < len(eh_results) - 1:
                        _append_msg()

            _append_divider(start_col=2)

            ipb_results: list[ValidatorMessage] = ip_result.result.get(IPBlockEmulatorValidatorResult.IP_BLOCK_KEY)
            if not len(ipb_results):
                _append_msg(subcat='IP Block', msg='n/a')
            else:
                for m_idx, msg in enumerate(ipb_results):
                    if m_idx == 0:
                        _append_msg(subcat='IP Block', msg=f'[{msg.status.name}]')
                    else:
                        _append_msg(msg=f'[{msg.status.name}]')

                    _append_wrapped(msg=str(msg.message))

                    if m_idx < len(ipb_results) - 1:
                        _append_msg()

            _append_divider(start_col=2)

            fsm_results: list[ValidatorMessage] = ip_result.result.get(IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY)
            if not len(fsm_results):
                _append_msg(subcat='State Machine', msg='n/a')
            else:
                for m_idx, msg in enumerate(fsm_results):
                    if m_idx == 0:
                        _append_msg(subcat='State Machine', msg=f'[{msg.status.name}]')
                    else:
                        _append_msg(msg=f'[{msg.status.name}]')

                    _append_wrapped(msg=str(msg.message))

                    if m_idx < len(fsm_results) - 1:
                        _append_msg()

            if ip_idx < len(ip_results.items()) - 1:
                _append_divider()

        # FOOTER
        if not footer_written:
            _append_divider(left_char='\\', right_char='/')
        out_str += '\n'

    final_status_msg = 'Final status of this run: ' + final_result.name + '\n'
    out_str += final_status_msg

    click.echo('\n----------------------------\n')

    if len(output_file):
        with open(output_file, 'wt') as open_output_file:
            open_output_file.write(out_str)
            click.echo(f'Result saved to {output_file}.\n')
        click.echo(final_status_msg)
    else:
        click.echo(out_str)


if __name__ == '__main__':
    validate_bitstream_files(max_content_width=150)
