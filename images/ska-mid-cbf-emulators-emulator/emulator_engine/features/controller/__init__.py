"""Controller which configures subcontrollers and their messaging setups."""

from emulator_engine.features.controller.emulator_controller import EmulatorController

__all__ = ['EmulatorController']
