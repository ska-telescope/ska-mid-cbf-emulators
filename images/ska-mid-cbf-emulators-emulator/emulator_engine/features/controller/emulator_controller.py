"""Bitstream emulator controller which configures IP block emulators and sends a pulse downstream."""

import os
import sys
import threading
import time
from multiprocessing import Process
from signal import SIGTERM
from threading import Thread
from typing import Self

import psutil
import uvicorn
from emulator_engine import RABBITMQ_HOST
from emulator_engine.features.dish.signal import DishSignal
from emulator_engine.features.dish.subcontroller import DishSubcontroller
from emulator_engine.models.controller_command import ControllerCommand
from emulator_engine.models.controller_command_event import ControllerCommandEvent
from emulator_engine.services.controller import controller_router_impl
from emulator_engine.services.controller.controller_router_client import ControllerRouterClient
from emulator_engine.services.controller.controller_state_machine import ControllerState, ControllerStateMachine
from emulator_engine.services.controller.pulse_generator import PulseGenerator
from emulator_engine.services.ip_block_emulator.ip_block_emulator_factory import IPBlockEmulatorFactory
from emulator_engine.services.messaging.api_queue_service import ApiQueueService
from emulator_engine.services.messaging.event_queue_service import EventQueueService
from fastapi import FastAPI
from pika import BlockingConnection, ConnectionParameters

from ska_mid_cbf_emulators.common import (
    BaseEvent,
    EmulatorConfig,
    EmulatorConfigParser,
    EmulatorError,
    EmulatorSubcontroller,
    IdService,
    LoggingBase,
)

UVICORN_HOST = '0.0.0.0'
UVICORN_PORT = 5001


class EmulatorController(LoggingBase):
    """Controller which configures IP block emulators and sends a pulse downstream.

    Takes in a config JSON string which it uses to set up
    all the emulators and connections between them,
    among other configurations.

    Args:
        config_str (:obj:`str`): A serialized JSON string containing
            the configuration for this emulator.
        bitstream_emulator_path (:obj:`str`): The path to the directory containing individual IP block emulator folders.
        bitstream_emulator_id (:obj:`str`, optional): ID of this bitstream emulator.
    """

    def __init__(
        self: Self,
        config_str: str,
        initial_signal_str: str,
        bitstream_emulator_path: str,
        bitstream_emulator_id: str,
        pulse_interval: float = 1.0,
        **kwargs
    ) -> None:
        super().__init__()
        self.state_machine = ControllerStateMachine()
        self.fastapi_app = FastAPI()
        self._bitstream_emulator_path = bitstream_emulator_path
        self._bitstream_emulator_id = bitstream_emulator_id
        self._pulse_interval = pulse_interval
        self._config_str = config_str
        self._config: EmulatorConfig = EmulatorConfigParser.decode(config_str)
        self._initial_signal_str = initial_signal_str
        self._initial_signal: DishSignal = DishSignal.decode(initial_signal_str)
        self._subcontrollers: dict[str, EmulatorSubcontroller] = {}
        self._event_service: EventQueueService = None
        self._api_service: ApiQueueService = None
        self._pulse_generator: PulseGenerator = None
        self._pulse_started_ok = False
        self._pulse_queue_id: str = ''
        self._uvicorn_server: Process = None
        self._command_queue = IdService.manual_queue_id(self._bitstream_emulator_id)
        self._init_connection(**kwargs)
        self._setup(**kwargs)
        self._configure(**kwargs)
        self._api_setup()
        self._start_controller_api()
        self._init_pulse_thread()
        self._command_listener_t = Thread(target=self._listen_to_commands)
        self._command_listener_t.start()
        if self.state_machine.may_finish_initializing():
            self.state_machine.finish_initializing()

    def __del__(self: Self) -> None:  # pragma: no cover
        try:
            if self.state_machine.may_terminate():
                self._terminate_internal()
            del self.fastapi_app
        except Exception:
            pass

    def _create_uvicorn_process(self: Self) -> Process:
        """creates a new uvicorn process"""

        return Process(
            target=uvicorn.run,
            args=[
                self.fastapi_app
            ],
            kwargs={
                'host': UVICORN_HOST,
                'port': UVICORN_PORT
            }
        )

    def start(self: Self) -> None:
        """Start the emulator and begin subscriptions."""
        if self.state_machine.may_start():
            self._start_internal()
        else:
            self.log_error(f'Could not start the emulator while in state {self.state_machine.state}.')

    def stop(self: Self) -> None:
        """Stop the emulator and unsubscribe from any active subscriptions. Does not stop the API server."""
        if self.state_machine.may_stop():
            self._stop_internal()
        else:
            self.log_error(f'Could not stop the emulator while in state {self.state_machine.state}.')

    def terminate(self: Self) -> None:
        """Terminate the emulator, unsubscribe from any active subscriptions, and kill the API server."""
        if self.state_machine.may_terminate():
            self._terminate_internal()
        else:
            self.log_error(f'Could not terminate the emulator while in state {self.state_machine.state}.')

    def flush_all(self: Self) -> None:
        """Flush all queues.

        Attempts to flush all queues
        of all emulators the controller has configured.
        """
        self._api_service.flush()
        for subcontroller in self._subcontrollers.values():
            subcontroller.event_service.flush()
            subcontroller.api_service.flush()

    def await_api_cleanup(self: Self) -> None:
        """Wait for the API server to finish shutting down and halt the main API listener thread."""
        if self._uvicorn_server.is_alive():
            self._uvicorn_server.join()
        self._api_service.force_stop()

    def _init_connection(self: Self, **kwargs) -> None:
        """Initialize any controller-level RabbitMQ dependencies (run before any other setup)."""
        try:
            connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            self.state_machine.error()
            raise EmulatorError('Could not connect to RabbitMQ broker after 5 attempts.')
        channel = connection.channel()
        channel.queue_declare(queue=self._command_queue)
        connection.close()

    def _setup(self: Self, **kwargs) -> None:
        """Set up controller-specific data (run before configuring anything)."""
        self._pulse_generator: PulseGenerator = PulseGenerator()
        self._pulse_queue_id: str = IdService.pulse_queue_id(self._bitstream_emulator_id, 'dish')
        self._event_service: EventQueueService = EventQueueService(
            self._pulse_interval,
            name=f'{self._bitstream_emulator_id}_controller_event_service'
        )
        self._api_service: ApiQueueService = ApiQueueService(
            api_request_queue=IdService.api_request_queue_id(self._bitstream_emulator_id),
            api_callback_queue=IdService.api_callback_queue_id(self._bitstream_emulator_id),
            router_impl=controller_router_impl.ControllerRouterImpl(controller=self),
            name=f'{self._bitstream_emulator_id}_controller_api_service'
        )

    def _configure(self: Self, **kwargs) -> None:
        """Process the top-level emulator configuration
        and initialize all IP block emulator subcontrollers. Must be run after setup.
        """
        sys.path.append(self._bitstream_emulator_path)
        self._create_dish()
        ip_block_emulators = IPBlockEmulatorFactory.create_many(
            self._bitstream_emulator_path,
            self._bitstream_emulator_id,
            self._config.ip_blocks,
            self._pulse_interval
        )
        for subcontroller, client in ip_block_emulators:
            self._subcontrollers[subcontroller.ip_block_id] = subcontroller
            if client.router is not None:
                self.fastapi_app.include_router(client.router)

        self.log_debug(f'Bitstream emulator configured with IP block emulators: {self._subcontrollers}')

    def _create_dish(self: Self) -> None:
        """Create the DISH subcontroller."""
        event_service = EventQueueService.init_for_subcontroller(
            bitstream_emulator_id=self._bitstream_emulator_id,
            ip_block_id='dish',
            downstream_ip_block_ids=self._config.first,
            pulse_interval=self._pulse_interval,
            expected_input_pulses=1
        )
        api_service = ApiQueueService.init_for_subcontroller(
            bitstream_emulator_id=self._bitstream_emulator_id,
            ip_block_id='dish'
        )
        self._subcontrollers['dish'] = DishSubcontroller(
            bitstream_emulator_id=self._bitstream_emulator_id,
            event_service=event_service,
            api_service=api_service,
            state_machine=None,
            ip_block_id='dish',
            display_name='DISH',
            type='dish',
            downstream_block_ids=self._config.first,
            initial_signal=self._initial_signal
        )

    def _api_setup(self: Self) -> None:
        """Set up the API for the controller and each IP block emulator. Must be run after configuration."""
        self.client = ControllerRouterClient(self._bitstream_emulator_id, self._pulse_interval)
        self.fastapi_app.include_router(self.client.router)
        self.fastapi_app.openapi_schema = None
        self.fastapi_app.setup()
        self._uvicorn_server: Process = self._create_uvicorn_process()

    def _start_controller_api(self: Self) -> None:
        """Start the Uvicorn server and controller-level API. Must be run after API setup."""
        self._uvicorn_server.start()
        self._api_service.subscribe()

    def _init_pulse_thread(self: Self) -> None:
        self._pulse_t = Thread(target=self._generate_pulse)

    def _start_internal(self: Self) -> None:
        """Start the emulator and begin subscriptions."""
        self.log_info('Emulator starting...')
        self.state_machine.start()
        self._subscribe_subcontrollers()
        if self._pulse_t is None:
            self._init_pulse_thread()
        self._pulse_t.start()
        while not self._pulse_started_ok:
            if self.state_machine.state == ControllerState.ERROR:
                self._terminate_internal()
                raise EmulatorError('Failed to start pulse generation.')
            time.sleep(0.3)
        self.state_machine.finish_starting()
        self.log_info('Emulator has started successfully.')

    def _stop_internal(self: Self) -> None:
        """Stop the emulator and unsubscribe from any active subscriptions. Does not stop the API server."""
        self.log_info('Emulator stopping (this may take a few seconds)...')
        self.state_machine.stop()
        self._stop_emulator_only()
        self.state_machine.finish_stopping()
        self.log_info('Emulator has stopped successfully.')

    def _terminate_internal(self: Self) -> None:
        """Terminate the emulator, unsubscribe from any active subscriptions, and kill the API server."""
        self.log_info('Emulator terminating (this may take a few seconds)...')
        self.state_machine.terminate()
        self._stop_emulator_only()
        self._kill_uvicorn()
        if self._command_listener_t.is_alive():
            if self._command_listener_t.ident != threading.get_ident():
                self._command_listener_t.join()
        self._api_service.unsubscribe()
        self.state_machine.finish_terminating()
        self.log_info('Emulator has terminated successfully.')

    def _stop_emulator_only(self: Self) -> None:
        """Stop the emulator (but not the API server).

        Stops generating pulses, and then
        triggers all subcontrollers to unsubscribe from events.
        """
        self._pulse_generator.stop()
        if self._pulse_t is not None and self._pulse_t.is_alive():
            self._pulse_t.join()
            self.log_info('Pulse generation has stopped. '
                          '(It may take a few seconds for remaining pulses in the system to finish processing.)')
        self._pulse_t = None
        self._unsubscribe_subcontrollers()

    def _kill_uvicorn(self: Self) -> None:
        """Kill the uvicorn API server."""
        pid = self._uvicorn_server.pid
        parent = psutil.Process(pid)
        for child in parent.children(recursive=True):
            child.kill()
        os.kill(pid, SIGTERM)

    def _subscribe_subcontrollers(self: Self) -> None:
        """Subscribe to events in all IP block emulators."""
        for subcontroller in self._subcontrollers.values():
            subcontroller.subscribe()

    def _unsubscribe_subcontrollers(self: Self) -> None:
        """Unsubscribe from events in all IP block emulators."""
        for subcontroller in self._subcontrollers.values():
            subcontroller.unsubscribe()
            self.log_info(f'IP block emulator "{subcontroller.display_name}" unsubscribed successfully.')

    def _generate_pulse(self: Self) -> None:
        """Internal thread function to get pulses and publish them."""
        try:
            connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            self.state_machine.error()
            return
        channel = connection.channel()
        last_heartbeat = time.time()
        self._pulse_started_ok = True
        for pulse in self._pulse_generator.start(interval=self._pulse_interval):
            if (time.time() - last_heartbeat >= 10):
                connection.process_data_events()
                last_heartbeat = time.time()
            event_encoded = BaseEvent.encode(pulse)
            channel.basic_publish(exchange='', routing_key=self._pulse_queue_id, body=event_encoded)
            self.log_debug(f'Published pulse to DISH at timestamp: {round(time.time() * 1000)}')
        if connection is not None:
            connection.close()

    def _handle_command(self: Self, event: ControllerCommandEvent) -> None:
        """Internal function which handles [asynchronous] command events."""
        match event.command:
            case ControllerCommand.START:
                self.start()
            case ControllerCommand.STOP:
                self.stop()
            case ControllerCommand.TERMINATE:
                self.terminate()

    def _listen_to_commands(self: Self):
        """Internal thread function which listens for [asynchronous] command events."""
        try:
            connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            self.state_machine.error()
            return
        channel = connection.channel()

        if self._command_queue is None or self._command_queue == '':
            return

        self.log_trace(f'Subscribing to {self._command_queue}')
        heartbeat_counter = 0
        for _method, _props, body in channel.consume(
            self._command_queue,
            inactivity_timeout=1.0,
            auto_ack=True
        ):
            heartbeat_counter += 1
            if heartbeat_counter >= 10:
                connection.process_data_events()
                heartbeat_counter = 0
            if self.state_machine.state in [ControllerState.TERMINATING, ControllerState.TERMINATED]:
                self.log_trace(f'Unsubscribing from {self._command_queue}')
                break
            if body is not None:
                body_decoded: ControllerCommandEvent = ControllerCommandEvent.decode(body)
                self.log_trace(f'Consumed {str(body_decoded.subtype).upper()} event from {self._command_queue}')
                self._handle_command(body_decoded)

        channel.cancel()
        self.log_trace(f'Unsubscribed from {self._command_queue}')

        channel.close()
        connection.close()
