"""Specialized emulator for the DISH.

DISH is not an IP block, but is a necessary part of the system to emulate,
thus it is treated in some ways like an IP block emulator but has no API/IP interface.
It acts as the head of the signal chain and the source of all signal information.
"""

from emulator_engine.features.dish.event_handler import DishEventHandler
from emulator_engine.features.dish.signal import DishSignal
from emulator_engine.features.dish.subcontroller import DishSubcontroller

__all__ = ['DishEventHandler', 'DishSignal', 'DishSubcontroller']
