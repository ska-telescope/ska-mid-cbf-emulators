"""Emulator subcontroller for the DISH."""

from typing import Self, override

from emulator_engine.features.dish.event_handler import DishEventHandler
from emulator_engine.features.dish.signal import DishSignal
from emulator_engine.services.messaging.event_queue_service import EventQueueService

from ska_mid_cbf_emulators.common import EmulatorSubcontroller


class DishSubcontroller(EmulatorSubcontroller):
    """Emulator subcontroller for the DISH.

    Subclass of :obj:`EmulatorSubcontroller`.
    """
    event_service: EventQueueService

    @override
    def subcontroller_init(self: Self, initial_signal: DishSignal) -> None:
        self.ip_block = initial_signal
        self.event_service.event_handler = DishEventHandler(self)
