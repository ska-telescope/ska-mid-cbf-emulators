"""Specialized event handler for the DISH."""

from typing import Any, Self, override

import jsonschema
from emulator_engine.features.dish.signal import DishSignal
from emulator_engine.schemas.dish_injection import dish_injection_schemas

from ska_mid_cbf_emulators.common import (
    BaseEventHandler,
    IdService,
    ManualEvent,
    ManualEventSubType,
    PulseEvent,
    SignalUpdateEvent,
    SignalUpdateEventList,
)


class DishEventHandler(BaseEventHandler):
    """Specialized event handler for the DISH.
    Ensures that a signal update is sent on the first pulse,
    and handles injected DISH events.

    Extends :obj:`BaseEventHandler`.
    """
    ip_block: DishSignal

    def __init__(self: Self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._is_first_pulse: bool = True

    @override
    def handle_pulse_event(self: Self, event: PulseEvent, **kwargs) -> None:
        """Handle an incoming pulse event.

        Args:
            event (:obj:`PulseEvent`): The event to handle.
            **kwargs: Arbitrary keyword arguments.
        """
        self.log_trace(f'DISH Pulse event handler called for {event}')
        if self._is_first_pulse:
            signal_update = self._generate_signal_update(event.source_timestamp)
            self.subcontroller.publish(
                signal_update,
                exchange=IdService.signal_update_exchange_id(
                    self.subcontroller.bitstream_emulator_id,
                    self.subcontroller.ip_block_id
                ),
            )
            self.log_debug('Initial signal published.')
            self._is_first_pulse = False

    @override
    def handle_signal_update_events(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
        """Create new signal update event(s) to be sent downstream.

        Args:
            event_list (:obj:`SignalUpdateEventList`): An arbitrary list with a timestamp. Any contained events are ignored.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            :obj:`SignalUpdateEventList` The signal update event list to send to the next block.
        """
        self.log_trace('DISH is generating a new Signal Update.')
        output_event_list = SignalUpdateEventList(
            events=[self._generate_signal_update(event_list.source_timestamp)],
            source_timestamp=event_list.source_timestamp
        )
        return output_event_list

    @override
    def handle_manual_event(self: Self, event: ManualEvent, **kwargs) -> None:
        """Handle an incoming manual event.

        Args:
            event (:obj:`ManualEvent`): The manual event to handle.
            **kwargs: Arbitrary keyword arguments.
        """
        self.log_trace(f'DISH manual event handler called for {event}')

        match event.subtype:

            case ManualEventSubType.INJECTION:
                injection_type = event.value.get('injection_type')
                try:
                    if injection_type is not None:
                        if dish_injection_schemas.get(injection_type) is not None:
                            jsonschema.validate(event.value, dish_injection_schemas[injection_type])

                        match injection_type:
                            case 'update_signal_properties':
                                updated_props: dict[str, Any] = event.value.get('updated_properties')
                                for prop in updated_props:
                                    if hasattr(self.ip_block, prop):
                                        setattr(self.ip_block, prop, updated_props[prop])
                                        self.subcontroller.force_signal_update()
                                    else:
                                        self.log_warning(f'DISH Signal has no property named `{prop}`; '
                                                         'ignoring and continuing.')

                            case 'update_signal':
                                self.ip_block = DishSignal(**event.value['signal'])
                                self.subcontroller.force_signal_update()

                            case _:
                                self.subcontroller.log_warning(f'injection_type not implemented for DISH: {injection_type}')

                    else:
                        self.log_error(f'Injected DISH event with value `{event.value}` missing required injection_type.')

                except jsonschema.ValidationError as e:
                    self.log_error(f'Injected DISH event value failed schema validation: {str(e)}')

            case _:
                self.log_debug(f'Unhandled event subtype {event.subtype}')

    def _generate_signal_update(self: Self, source_timestamp: int) -> SignalUpdateEvent:
        return SignalUpdateEvent(
            source_timestamp=source_timestamp,
            value={
                'packet_rate': self.ip_block.packet_rate,
                'sample_rate': self.ip_block.sample_rate,
                'dish_id': self.ip_block.dish_id,
                'band_id': self.ip_block.band_id,
                'regular_sky_power': self.ip_block.regular_sky_power,
                'noise_diode_on_power': self.ip_block.noise_diode_on_power,
                'duty_cycle': self.ip_block.duty_cycle,
            },
        )
