"""Model "IP Block" for the DISH signal."""

from __future__ import annotations

import json
from typing import Self

import jsonschema
from emulator_engine.schemas.dish_signal import dish_signal_schema

from ska_mid_cbf_emulators.common import LoggerFactory


class DishSignal:
    """DISH "IP Block" for storing source signal information.

    Args:
        packet_rate (:obj:`float`): Packet rate
        sample_rate (:obj:`int`): Sample rate
        dish_id (:obj:`str`): DISH ID
        band_id (:obj:`str`): Band ID
        regular_sky_power (:obj:`list[float]`): Regular sky power
        noise_diode_on_power (:obj:`list[float]`): Noise diode ON power
        duty_cycle (:obj:`float`): Duty cycle

    Attributes:
        packet_rate (:obj:`float`): Packet rate
        sample_rate (:obj:`int`): Sample rate
        dish_id (:obj:`str`): DISH ID
        band_id (:obj:`str`): Band ID
        regular_sky_power (:obj:`list[float]`): Regular sky power
        noise_diode_on_power (:obj:`list[float]`): Noise diode ON power
        duty_cycle (:obj:`float`): Duty cycle
    """
    def __init__(
        self: Self,
        packet_rate: float,
        sample_rate: int,
        dish_id: str,
        band_id: str,
        regular_sky_power: list[float],
        noise_diode_on_power: list[float],
        duty_cycle: float,
    ) -> None:
        self.packet_rate: float = packet_rate
        self.sample_rate: int = sample_rate
        self.dish_id: str = dish_id
        self.band_id: str = band_id
        self.regular_sky_power: list[float] = regular_sky_power
        self.noise_diode_on_power: list[float] = noise_diode_on_power
        self.duty_cycle: float = duty_cycle

    @staticmethod
    def decode(json_signal_str: str) -> DishSignal:
        """Decode a JSON-encoded DishSignal.

        Args:
            json_signal_str (:obj:`str`): The JSON string containing the signal data to decode

        Returns:
            :obj:`DishSignal`: The decoded signal data
        """
        logger = LoggerFactory.get_logger()
        json_signal = json.loads(json_signal_str)
        try:
            jsonschema.validate(json_signal, dish_signal_schema)
        except Exception as e:
            logger.error(str(e))
            raise e
        logger.trace('DISH Signal schema validation was successful.')

        return DishSignal(**json_signal)
