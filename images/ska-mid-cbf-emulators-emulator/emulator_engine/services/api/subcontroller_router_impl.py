"""API class which contains implementations of common endpoints for all IP block emulators."""

from io import BytesIO
from typing import Self

from emulator_engine.services.api.router_impl import RouterImpl

from ska_mid_cbf_emulators.common import EmulatorSubcontroller, InternalRestResponse


class SubcontrollerRouterImpl(RouterImpl):
    """API class which contains implementations of common endpoints for all IP block emulators.

    Subclass of :obj:`RouterImpl`.

    Args:
        subcontroller (:obj:`EmulatorSubcontroller`): The emulator subcontroller to process API calls for.
    """

    def __init__(self: Self, subcontroller: EmulatorSubcontroller) -> None:
        super().__init__()
        self.subcontroller = subcontroller
        self.ip_block = subcontroller.ip_block

    @RouterImpl.error_catcher
    def _get_state_diagram(self: Self, context_only: bool) -> InternalRestResponse:
        """Get a state transition diagram for the current state of the IP block emulator.

        Args:
            context_only (:obj:`bool`): If False, the full emulator context (all known states and transitions)
                will be included. If True, only the context of the current state (the current state, the previous state,
                and any states accessible from the current state) will be included.

        Returns:
            :obj:`InternalRestResponse` the response, containing PNG data in `bytes` format.
        """
        strm = BytesIO()
        if context_only:
            self.subcontroller.export_state_diagram_current_context(strm, format='png')
        else:
            self.subcontroller.export_state_diagram_full(strm, format='png')
        return InternalRestResponse.ok(strm.getvalue())

    @RouterImpl.error_catcher
    def _get_state(self: Self) -> InternalRestResponse:
        """Get the current state of the IP block emulator."""
        return InternalRestResponse.ok({'current_state': self.subcontroller.get_state()})
