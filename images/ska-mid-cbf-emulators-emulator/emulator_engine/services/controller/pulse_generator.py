"""Pulse generator."""

import time
from typing import Any, Generator, Self

from ska_mid_cbf_emulators.common import EventSeverity, LoggingBase, PulseEvent


class PulseGenerator(LoggingBase):
    """Class to generate pulse events at regular intervals.

    Args:
        default_interval_sec (:obj:`float`, optional): Default pulse interval
            in seconds (decimal values OK) to use for this instance
            if an interval is not provided to the `start` method.
            Default value is 1 second.
    """

    def __init__(self: Self, default_interval: float = 1.0) -> None:
        super().__init__()
        self._default_interval: float = default_interval
        self._running: bool = False
        self._kill: bool = False
        self._pulse_id: int = 0

    def __del__(self: Self) -> None:
        self.stop()

    def start(self: Self, interval: float = None) -> Generator[PulseEvent, Any, Any]:
        """Start generating pulse events at a given interval.

        Returns a generator that will yield a new :obj:`PulseEvent`
        every `interval_sec` seconds. The generator will only stop
        when the `stop` method is called.

        Args:
            interval_sec (:obj:`float`, optional): The number of seconds (decimal values OK)
                between pulses. If not provided, the default interval set in the constructor is used.

        Returns:
            :obj:`Generator[PulseEvent]`: The pulse generator object.
        """
        interval = interval or self._default_interval
        self._running = True
        while not self._kill:
            original_time = self._get_time()
            yield PulseEvent(
                value={'id': self._pulse_id},
                severity=EventSeverity.GENERAL,
                source_timestamp=original_time
            )
            self._pulse_id += 1
            time.sleep(interval)
        self._pulse_id = 0
        self._kill = False
        self._running = False

    def stop(self: Self) -> None:
        """Stop generating pulse events.

        If the pulse generator is active, sets a kill flag so that it will terminate.
        """
        if self._running:
            self._kill = True

    def _get_time(self: Self) -> int:
        """Gets the current timestamp in milliseconds."""
        return round(time.time() * 1000)
