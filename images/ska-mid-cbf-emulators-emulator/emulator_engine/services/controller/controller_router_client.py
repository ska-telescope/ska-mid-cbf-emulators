"""Controller API router client."""
from __future__ import annotations

from typing import Any, Self, override

from emulator_engine import RABBITMQ_HOST
from emulator_engine.models.controller_command import ControllerCommand
from emulator_engine.models.controller_command_event import ControllerCommandEvent
from emulator_engine.services.api.router_client import RouterClient
from emulator_engine.services.messaging.event_queue_service import EventQueueService
from fastapi import Response, status
from pika import BlockingConnection, ConnectionParameters

from ska_mid_cbf_emulators.common import IdService, InternalRestRequest


class ControllerRouterClient(RouterClient):
    """API router client used by the emulator controller for top-level routes.

    Subclass of :obj:`RouterClient`.
    """

    def __init__(self: Self, bitstream_emulator_id: str, pulse_interval: float, *args, **kwargs):
        super().__init__(bitstream_emulator_id, '', *args, **kwargs)
        self._event_service = EventQueueService(
            pulse_interval,
            name='controller_client_event_service'
        )
        self._command_queue = IdService.manual_queue_id(bitstream_emulator_id)

    def _get_graph(self: Self) -> Response:
        """Plot the IP block graph as a PNG image.

        HTTP Method:
            `GET`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_get_graph',
            http_method='GET',
            kwargs={}
        ))

        return Response(
            content=rpc_response.body,
            status_code=rpc_response.status,
            media_type=rpc_response.media_type
        )

    def _get_server_healthcheck(self: Self, response: Response) -> dict[str, Any]:
        """Verify the API is accepting requests. For now, always returns 200 OK and an empty body.

        HTTP Method:
            `GET`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_get_server_healthcheck',
            http_method='GET',
            kwargs={}
        ))

        response.status_code = rpc_response.status

        return rpc_response.body

    def _get_state(self: Self, response: Response) -> dict[str, Any]:
        """Get the current state of the top-level emulator.

        HTTP Method:
            `GET`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_get_state',
            http_method='GET',
            kwargs={}
        ))
        response.status_code = rpc_response.status

        return rpc_response.body

    def _get_config(self: Self, response: Response) -> dict[str, Any]:
        """Get the emulator configuration from this emulator's bitstream.

        HTTP Method:
            `GET`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_get_config',
            http_method='GET',
            kwargs={}
        ))
        response.status_code = rpc_response.status

        return rpc_response.body

    def _get_initial_signal(self: Self, response: Response) -> dict[str, Any]:
        """Get the initial dish signal used to instantiate this emulator.

        HTTP Method:
            `GET`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_get_initial_signal',
            http_method='GET',
            kwargs={}
        ))
        response.status_code = rpc_response.status

        return rpc_response.body

    def _start(self: Self, response: Response) -> dict[str, Any]:
        """Start the emulator if it is stopped. Pulses and event listeners will reactivate.

        HTTP Method:
            `POST`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_start',
            http_method='POST',
            kwargs={}
        ))
        if rpc_response.status == status.HTTP_202_ACCEPTED:
            self._send_command(ControllerCommand.START)

        response.status_code = rpc_response.status

        return rpc_response.body

    def _stop(self: Self, response: Response) -> dict[str, Any]:
        """Stop the emulator if it is started. The API server will remain active but pulses and event listeners will be halted.

        HTTP Method:
            `POST`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_stop',
            http_method='POST',
            kwargs={}
        ))
        if rpc_response.status == status.HTTP_202_ACCEPTED:
            self._send_command(ControllerCommand.STOP)

        response.status_code = rpc_response.status

        return rpc_response.body

    def _terminate(self: Self, response: Response) -> dict[str, Any]:
        """Terminate the emulator.

        Stops all pulses and listeners, and tears down the API server, completely terminating execution.
        If used, the emulator script will have to be re-run to start again.

        HTTP Method:
            `POST`
        """
        rpc_response = self.get_rpc_response(InternalRestRequest(
            method_name='_terminate',
            http_method='POST',
            kwargs={}
        ))
        if rpc_response.status == status.HTTP_202_ACCEPTED:
            self._send_command(ControllerCommand.TERMINATE)

        response.status_code = rpc_response.status

        return rpc_response.body

    def _send_command(self: Self, command: ControllerCommand, command_value: dict[str, Any] = {}) -> None:
        try:
            connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            return
        channel = connection.channel()
        command_event = ControllerCommandEvent(
            command=command,
            value=command_value
        )
        channel.basic_publish(exchange='', routing_key=self._command_queue, body=ControllerCommandEvent.encode(command_event))
        self.log_trace(f'Published {str(command).upper()} command to {self._command_queue}')
        connection.close()

    @override
    def _setup(self: Self, *args, **kwargs):
        return

    @override
    def _initialize_common_routes(self: Self) -> None:
        """Initializes common routes."""
        self.router.add_api_route('/graph', self._get_graph, methods=['GET'])
        self.router.add_api_route('/server_healthcheck', self._get_server_healthcheck, methods=['GET'])
        self.router.add_api_route('/state', self._get_state, methods=['GET'])
        self.router.add_api_route('/config', self._get_config, methods=['GET'])
        self.router.add_api_route('/initial_signal', self._get_initial_signal, methods=['GET'])
        self.router.add_api_route('/start', self._start, methods=['POST'])
        self.router.add_api_route('/stop', self._stop, methods=['POST'])
        self.router.add_api_route('/terminate', self._terminate, methods=['POST'])
