"""Controller API router implementation."""
from __future__ import annotations

import json
from typing import TYPE_CHECKING, Self

from emulator_engine.services.api.router_impl import RouterImpl
from emulator_engine.services.controller.controller_state_machine import ControllerState

from ska_mid_cbf_emulators.common import InternalRestResponse

if TYPE_CHECKING:
    from emulator_engine.features.controller.emulator_controller import EmulatorController


class ControllerRouterImpl(RouterImpl):
    """Router implementation for the emulator controller.

    Subclass of :obj:`RouterImpl`.

    Args:
        controller (:obj:`EmulatorController`): A reference to the controller using this implementation.
    """

    def _setup(self: Self, controller: EmulatorController) -> None:
        self.controller = controller

    @RouterImpl.error_catcher
    def _get_graph(self: Self) -> InternalRestResponse:
        """Plot the IP block graph as a PNG image."""
        return InternalRestResponse.ok(
            body=self.controller._config.get_graph_img(),
            media_type='image/png'
        )

    @RouterImpl.error_catcher
    def _get_server_healthcheck(self: Self) -> InternalRestResponse:
        """Verify the emulator is healthy (not in ERROR state) and that the API is accepting requests."""
        if self.controller.state_machine.state == ControllerState.ERROR:
            return InternalRestResponse.internal_server_error('Emulator is currently in an ERROR state.')
        return InternalRestResponse.ok()

    @RouterImpl.error_catcher
    def _get_state(self: Self) -> InternalRestResponse:
        """Get the controller state."""
        return InternalRestResponse.ok({'current_state': self.controller.state_machine.state})

    @RouterImpl.error_catcher
    def _get_config(self: Self) -> InternalRestResponse:
        """Get the emulator configuration from this emulator's bitstream."""
        return InternalRestResponse.ok(json.loads(self.controller._config_str))

    @RouterImpl.error_catcher
    def _get_initial_signal(self: Self) -> InternalRestResponse:
        """Get the initial dish signal used to instantiate this emulator."""
        return InternalRestResponse.ok(json.loads(self.controller._initial_signal_str))

    @RouterImpl.error_catcher
    def _start(self: Self) -> InternalRestResponse:
        """Start the emulator."""
        if self.controller.state_machine.may_start():
            return InternalRestResponse.accepted()
        return InternalRestResponse.conflict(
            f'Cannot start the emulator while in state {self.controller.state_machine.state}'
        )

    @RouterImpl.error_catcher
    def _stop(self: Self) -> InternalRestResponse:
        """Stop the emulator."""
        if self.controller.state_machine.may_stop():
            return InternalRestResponse.accepted()
        return InternalRestResponse.conflict(
            f'Cannot stop the emulator while in state {self.controller.state_machine.state}'
        )

    @RouterImpl.error_catcher
    def _terminate(self: Self) -> InternalRestResponse:
        """Terminate the emulator."""
        if self.controller.state_machine.may_terminate():
            return InternalRestResponse.accepted()
        return InternalRestResponse.conflict(
            f'Cannot terminate the emulator while in state {self.controller.state_machine.state}'
        )
