from enum import auto
from typing import Any, Self, override

from ska_mid_cbf_emulators.common import BaseState, BaseTransitionTrigger, FiniteStateMachine, RoutingState


class ControllerState(BaseState):
    """Enum containing possible states for the emulator controller.

    Implements :obj:`BaseState`.
    """

    INITIALIZING = 'INITIALIZING'
    """The emulator is being initialized (but not starting)."""

    IDLE = 'IDLE'
    """The emulator has been initialized but is dormant. No pulses are being sent and no events are being handled."""

    STARTING = 'STARTING'
    """The emulator is in the process of starting up."""

    RUNNING = 'RUNNING'
    """The emulator is fully started and running."""

    STOPPING = 'STOPPING'
    """The emulator is in the process of stopping."""

    TERMINATING = 'TERMINATING'
    """The emulator is in the process of terminating."""

    TERMINATED = 'TERMINATED'
    """The emulator, including its API server, has been completely terminated."""

    ERROR = 'ERROR'
    """The emulator has experienced an error it has not recovered from."""


class ControllerTransitionTrigger(BaseTransitionTrigger):
    """Enum containing transitions for the emulator controller.

    Implements :obj:`BaseTransitionTrigger`.
    """

    FINISH_INITIALIZING = auto()
    """The emulator finishes initialization."""

    START = auto()
    """The emulator is told to start."""

    FINISH_STARTING = auto()
    """The emulator finishes starting."""

    STOP = auto()
    """The emulator is told to stop."""

    FINISH_STOPPING = auto()
    """The emulator finishes stopping."""

    TERMINATE = auto()
    """The emulator is told to terminate."""

    FINISH_TERMINATING = auto()
    """The emulator finishes terminating."""

    ERROR = auto()
    """The emulator experiences a critical error and does not recover."""


class ControllerStateMachine(FiniteStateMachine):
    """State machine for the emulator controller.

    Implements :obj:`FiniteStateMachine`.
    """

    @override
    @property
    def _states(self: Self) -> list[ControllerState]:
        return [
            ControllerState.INITIALIZING,
            ControllerState.IDLE,
            ControllerState.RUNNING,
            ControllerState.STARTING,
            ControllerState.STOPPING,
            ControllerState.TERMINATED,
            ControllerState.TERMINATING,
            ControllerState.ERROR
        ]

    @override
    @property
    def _initial_state(self: Self) -> ControllerState:
        return ControllerState.INITIALIZING

    @override
    @property
    def _transitions(self) -> list[dict[str, Any]]:
        return [
            {
                'source': ControllerState.INITIALIZING,
                'dest': ControllerState.IDLE,
                'trigger': ControllerTransitionTrigger.FINISH_INITIALIZING
            },
            {
                'source': ControllerState.IDLE,
                'dest': ControllerState.STARTING,
                'trigger': ControllerTransitionTrigger.START
            },
            {
                'source': ControllerState.STARTING,
                'dest': ControllerState.RUNNING,
                'trigger': ControllerTransitionTrigger.FINISH_STARTING
            },
            {
                'source': ControllerState.RUNNING,
                'dest': ControllerState.STOPPING,
                'trigger': ControllerTransitionTrigger.STOP
            },
            {
                'source': ControllerState.STOPPING,
                'dest': ControllerState.IDLE,
                'trigger': ControllerTransitionTrigger.FINISH_STOPPING
            },
            {
                'source': [
                    ControllerState.INITIALIZING,
                    ControllerState.IDLE,
                    ControllerState.RUNNING,
                    ControllerState.STARTING,
                    ControllerState.STOPPING,
                    ControllerState.ERROR
                ],
                'dest': ControllerState.TERMINATING,
                'trigger': ControllerTransitionTrigger.TERMINATE
            },
            {
                'source': ControllerState.TERMINATING,
                'dest': ControllerState.TERMINATED,
                'trigger': ControllerTransitionTrigger.FINISH_TERMINATING
            },
            {
                'source': RoutingState.FROM_ANY,
                'dest': ControllerState.ERROR,
                'trigger': ControllerTransitionTrigger.ERROR
            }
        ]
