"""Message service implementation."""

from __future__ import annotations

import copy
import time
from threading import Thread
from typing import Self, override

from emulator_engine import RABBITMQ_HOST
from pika import BlockingConnection, ConnectionParameters
from pika.adapters.blocking_connection import BlockingChannel

from ska_mid_cbf_emulators.common import (
    BaseEvent,
    BaseEventHandler,
    BaseMessageService,
    EmulatorError,
    EventSeverity,
    IdService,
    ManualEvent,
    PulseEvent,
    SignalUpdateEvent,
    SignalUpdateEventList,
    UnimplementedEventHandler,
)


class EventQueueService(BaseMessageService):
    """Message service which listens for RabbitMQ events, and processes and/or forwards them as necessary.

    Implements :obj:`BaseMessageService`.

    Args:
        **kwargs: Keyword arguments passed to the setup function.

    Keyword Args:
        pulse_interval (:obj:`float`): The interval in seconds between pulses being used by the system. \
            Used to calibrate certain internal timings.
        pulse_queue (:obj:`str`): A unique identifier to give the pulse queue.
        signal_update_queue (:obj:`str`): A unique identifier to give the signal update queue.
        manual_queue (:obj:`str`): A unique identifier to give the manual queue.
        publishable_pulse_queues (:obj:`list[str]`): A list of identifiers \
            of pulse queues this service can publish to.
        publishable_signal_update_queues (:obj:`list[str]`): A list of identifiers \
            of signal update queues this service can publish to.
        publishable_manual_queues (:obj:`list[str]`): A list of identifiers \
            of manual queues this service can publish to.
        pulse_exchange (:obj:`str`): A unique identifier to give the pulse exchange.
        signal_update_exchange (:obj:`str`): A unique identifier to give the signal update exchange.
        manual_exchange (:obj:`str`): A unique identifier to give the manual exchange.
        subscriber_timeout (:obj:`int`): If specified, a timeout value in seconds after which \
            the service will automatically unsubscribe from events. Will never timeout if not specified.
        expected_input_pulses (:obj:`int`, optional): The number of parallel input pulses this block should listen for \
            before processing other events. Default is 1.
        name (:obj:`str`): A name to give this service (primarily for debugging purposes).
    """

    @staticmethod
    def init_for_subcontroller(
            bitstream_emulator_id: str,
            ip_block_id: str,
            downstream_ip_block_ids: list[str],
            pulse_interval: float,
            expected_input_pulses: int = 1,
            *args,
            **kwargs
    ) -> EventQueueService:
        """Construct an instance of this service for a specific IP block subcontroller.

        This will automatically populate queue and exchange names based on the subcontroller data.

        Args:
            bitstream_emulator_id (:obj:`str`): The (unique) bitstream emulator ID.
            ip_block_id (:obj:`str`): The (unique) IP block ID associated with the subcontroller.
            downstream_ip_block_ids (:obj:`list[str]`): A list of the downstream IP block IDs \
                which are directly connected to this one.
            pulse_interval (:obj:`float`): The interval in seconds between pulses being used by the system. \
                Used to calibrate certain internal timings.
            expected_input_pulses (:obj:`int`, optional): The number of parallel input pulses this block should listen for \
                before processing other events. Default is 1.
            **kwargs: Other keyword arguments to pass to the service constructor/setup function.

        Returns:
            :obj:`EventQueueService`: The created service instance for the subcontroller.
        """
        return EventQueueService(
            pulse_interval,
            pulse_queue=IdService.pulse_queue_id(bitstream_emulator_id, ip_block_id),
            signal_update_queue=IdService.signal_update_queue_id(bitstream_emulator_id, ip_block_id),
            manual_queue=IdService.manual_queue_id(bitstream_emulator_id, ip_block_id),
            publishable_pulse_queues=[
                IdService.pulse_queue_id(bitstream_emulator_id, ds_block_id) for ds_block_id in downstream_ip_block_ids
            ],
            publishable_signal_update_queues=[
                IdService.signal_update_queue_id(bitstream_emulator_id, ds_block_id) for ds_block_id in downstream_ip_block_ids
            ],
            publishable_manual_queues=[
                IdService.manual_queue_id(bitstream_emulator_id, ds_block_id) for ds_block_id in downstream_ip_block_ids
            ],
            pulse_exchange=IdService.pulse_exchange_id(bitstream_emulator_id, ip_block_id),
            signal_update_exchange=IdService.signal_update_exchange_id(bitstream_emulator_id, ip_block_id),
            manual_exchange=IdService.manual_exchange_id(bitstream_emulator_id, ip_block_id),
            expected_input_pulses=expected_input_pulses,
            name=f'{ip_block_id}_event_service',
            *args,
            **kwargs
        )

    @override
    def setup(
            self: Self,
            pulse_interval: float,
            pulse_queue: str = '',
            signal_update_queue: str = '',
            manual_queue: str = '',
            publishable_pulse_queues: list[str] = [],
            publishable_signal_update_queues: list[str] = [],
            publishable_manual_queues: list[str] = [],
            pulse_exchange: str = '',
            signal_update_exchange: str = '',
            manual_exchange: str = '',
            subscriber_timeout: int = None,
            expected_input_pulses: int = 1,
            *args,
            **kwargs
    ) -> None:
        """Set up the service.

        Args:
            pulse_interval (:obj:`float`): The interval in seconds between pulses being used by the system. \
                Used to calibrate certain internal timings.
            pulse_queue (:obj:`str`, optional): A unique identifier to give the pulse queue. \
                Default is the empty string.
            signal_update_queue (:obj:`str`, optional): A unique identifier to give the signal update queue. \
                Default is the empty string.
            manual_queue (:obj:`str`, optional): A unique identifier to give the manual queue. \
                Default is the empty string.
            publishable_pulse_queues (:obj:`list[str]`, optional): A list of identifiers \
                of pulse queues this service can publish to. \
                Default is an empty list.
            publishable_signal_update_queues (:obj:`list[str]`, optional): A list of identifiers \
                of signal update queues this service can publish to. \
            publishable_manual_queues (:obj:`list[str]`, optional): A list of identifiers \
                of manual queues this service can publish to. \
                Default is an empty list.
            pulse_exchange (:obj:`str`, optional): A unique identifier to give the pulse exchange. \
                Default is the empty string.
            signal_update_exchange (:obj:`str`, optional): A unique identifier to give the signal update exchange. \
                Default is the empty string.
            manual_exchange (:obj:`str`, optional): A unique identifier to give the manual exchange. \
                Default is the empty string.
            subscriber_timeout (:obj:`int`, optional): If specified, a timeout value in seconds after which \
                the service will automatically unsubscribe from events. Default is `None` (no timeout).
            expected_input_pulses (:obj:`int`, optional): The number of parallel input pulses this block should listen for \
                before processing other events. Default is 1.
            **kwargs: Arbitrary keyword arguments.

        Keyword Args:
            name (:obj:`str`): A name to give this service (primarily for debugging purposes).
        """
        self._log_prefix: str | None = kwargs.get('name')
        self._debug: bool = True
        self._pulse_interval: float = pulse_interval
        self._subscriber_timeout: int = subscriber_timeout
        self._expected_input_pulses: int = expected_input_pulses
        self._listener_t: Thread = None
        self._current_signal: SignalUpdateEventList = SignalUpdateEventList()

        # Must be set externally (i.e. by the IP block emulator factory).
        self.event_handler: BaseEventHandler = UnimplementedEventHandler()
        self.force_signal_update: bool = False

        try:
            producer_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            raise EmulatorError('Could not connect to RabbitMQ broker after 5 attempts.')
        producer_channel = producer_connection.channel()

        self.pulse_queue: str = pulse_queue
        self.signal_update_queue: str = signal_update_queue
        self.manual_queue: str = manual_queue
        self.publishable_pulse_queues: list[str] = publishable_pulse_queues
        self.publishable_signal_update_queues: list[str] = publishable_signal_update_queues
        self.publishable_manual_queues: list[str] = publishable_manual_queues
        self.pulse_exchange: str = pulse_exchange
        self.signal_update_exchange: str = signal_update_exchange
        self.manual_exchange: str = manual_exchange

        for exchange in [self.pulse_exchange, self.signal_update_exchange, self.manual_exchange]:
            if exchange != '':
                producer_channel.exchange_delete(exchange=exchange)
                producer_channel.exchange_declare(exchange=exchange, exchange_type='fanout')

        producer_channel.queue_declare(queue=self.pulse_queue)
        producer_channel.queue_declare(queue=self.signal_update_queue)
        producer_channel.queue_declare(queue=self.manual_queue)
        for pulse_q in self.publishable_pulse_queues:
            producer_channel.queue_declare(queue=pulse_q)
            if self.pulse_exchange != '':
                producer_channel.queue_bind(exchange=self.pulse_exchange, queue=pulse_q)
        for signal_update_q in self.publishable_signal_update_queues:
            producer_channel.queue_declare(queue=signal_update_q)
            if self.signal_update_exchange != '':
                producer_channel.queue_bind(exchange=self.signal_update_exchange, queue=signal_update_q)
        for manual_q in self.publishable_manual_queues:
            producer_channel.queue_declare(queue=manual_q)
            if self.manual_exchange != '':
                producer_channel.queue_bind(exchange=self.manual_exchange, queue=manual_q)
        self._kill: bool = False

        self._init_event_thread()

        producer_channel.close()
        producer_connection.close()

    @override
    def subscribe(self: Self, **kwargs) -> None:
        """Subscribe to events.

        Starts the main listener thread.

        Args:
            event_handler (:obj:`Callable[[Any], Any]`): A callback function to handle an event.
            **kwargs: Arbitrary keyword arguments.
        """
        if self._listener_t is not None and self._listener_t.is_alive():
            self.log_debug('Cannot subscribe to events while already subscribed.')
            return
        if self._listener_t is None:
            self._init_event_thread()
        self._listener_t.start()

    @override
    def publish(
        self: Self,
        event: BaseEvent,
        exchange: str,
        routing_key: str = '',
        channel: BlockingChannel | None = None,
        **kwargs
    ) -> None:
        """Publish an event.

        Encodes an event and publishes it using the provided exchange and/or routing key.

        Args:
            event (:obj:`BaseEvent`): The event to publish.
            exchange (:obj:`str`): The exchange to publish to.
            routing_key (:obj:`str`, optional): The routing key to use for publishing.
                Default is the empty string.
            channel (:obj:`str`, optional): The channel to use for publishing.
                Default is None (will create a new connection and channel).
            **kwargs: Arbitrary keyword arguments.
        """
        new_connection = None
        if channel is None:
            try:
                new_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
            except Exception:
                self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
                return
            channel = new_connection.channel()
        event_encoded = BaseEvent.encode(event)
        channel.basic_publish(exchange=exchange, routing_key=routing_key, body=event_encoded)
        self.log_trace(f'Published {str(event.type).upper()} event to {exchange if len(exchange) else routing_key}')
        if new_connection is not None:
            new_connection.close()

    @override
    def unsubscribe(self: Self) -> None:
        """Unsubscribe from events.

        Signals the consumer thread to exit its main loop
        and waits for the thread to join.
        """
        if self._listener_t is None or not self._listener_t.is_alive():
            self.log_debug('Cannot unsubscribe as the service is not currently subscribed to events.')
            return
        self._kill = True
        if self._listener_t is not None and self._listener_t.is_alive():
            self._listener_t.join()
            self.log_debug('Unsubscribed from events.')
        self._kill = False
        self._listener_t = None

    def flush(self: Self) -> None:
        """Flush all queues that this service is aware of.

        Loops through every queue name available and attempts to flush it.
        Any queue names that do not exist or cannot be flushed are ignored.
        """
        try:
            flush_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            return
        flush_channel = flush_connection.channel()
        for q in [
            self.pulse_queue,
            self.signal_update_queue,
            self.manual_queue,
            *self.publishable_pulse_queues,
            *self.publishable_signal_update_queues,
            *self.publishable_manual_queues
        ]:
            try:
                flush_channel.queue_purge(q.strip())
                self.log_trace(f'Flushed {q}')
            except Exception:
                continue
        flush_channel.close()
        flush_connection.close()

    def _init_event_thread(self: Self) -> None:
        self._listener_t = Thread(target=self._listen_to_pulse)

    def _get_updated_pulse_event(self: Self, prev_pulse: PulseEvent) -> PulseEvent:
        """Generate a new pulse event based on a received pulse event,
        updated with the current time.
        """
        new_timestamp = round(time.time() * 1000)
        return PulseEvent(
            severity=EventSeverity.GENERAL,
            source_timestamp=prev_pulse.source_timestamp,
            update_timestamp=new_timestamp,
            value=prev_pulse.value,
        )

    def _get_signal_update_from_current(self: Self, pulse_timestamp: int) -> SignalUpdateEventList:
        return SignalUpdateEventList([
            SignalUpdateEvent(
                copy.deepcopy(event.value),
                event.severity,
                pulse_timestamp
            ) for event in self._current_signal.events
        ], pulse_timestamp)

    def _listen_to_pulse(self: Self, **kwargs):
        """Internal thread function which listens on the pulse queue for pulse events
        and triggers manual of events in the manual queue
        when a pulse event is received.
        """
        if self.pulse_queue is None or self.pulse_queue == '':
            self.log_error('No pulse queue could be found.')
            return

        if self.signal_update_queue is None or self.signal_update_queue == '':
            self.log_error('No signal update queue could be found.')
            return

        try:
            pulse_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
            internal_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            raise EmulatorError('Could not connect to RabbitMQ broker after 5 attempts.')
        pulse_channel = pulse_connection.channel()
        signal_update_channel = internal_connection.channel()
        manual_channel = internal_connection.channel()

        self.log_trace(f'Subscribing to {self.pulse_queue}')
        subscriber_timeout_count = 0
        pulse_timeout_interval = min(self._pulse_interval, 1.0) * 0.5
        pulse_heartbeat_counter = 0
        last_heartbeat = time.time()
        expected_pulse_counter = 0
        expected_pulse_id = -1
        pulse_to_handle = None
        # Listen to the pulse queue. When a pulse is received, send it downstream and trigger manual for this subcontroller.
        for _pulse_method, _pulse_props, pulse_body in pulse_channel.consume(
            self.pulse_queue,
            inactivity_timeout=pulse_timeout_interval,
            auto_ack=True
        ):
            pulse_heartbeat_counter += 1
            # send heartbeat every 10 messages, or at an absolute minimum every 15 seconds (in case execution is abnormally slow)
            if (pulse_heartbeat_counter >= 10) or (time.time() - last_heartbeat >= 15):
                pulse_connection.process_data_events()
                internal_connection.process_data_events()
                pulse_heartbeat_counter = 0
                last_heartbeat = time.time()
            if self._kill:
                self.log_trace(f'Unsubscribing from {self.pulse_queue}')
                break
            if pulse_body is None:
                if self._subscriber_timeout is not None:
                    subscriber_timeout_count += pulse_timeout_interval
                    if subscriber_timeout_count > self._subscriber_timeout:
                        self.log_warning(f'Subscription to {self.pulse_queue} timed out:\
                                          no messages in {self._subscriber_timeout} seconds.')
                        break
                continue
            subscriber_timeout_count = 0
            pulse_body_decoded: PulseEvent = PulseEvent.decode(pulse_body)
            pulse_id = pulse_body_decoded.value.get('id')
            self.log_trace(f'Consumed pulse event from {self.pulse_queue}')
            self.log_debug(f'Got pulse #{pulse_id} with timestamps src={pulse_body_decoded.source_timestamp}, '
                           f'upd={pulse_body_decoded.update_timestamp}')

            pulse_src_timestamp = pulse_body_decoded.source_timestamp
            next_pulse_to_handle = None
            if expected_pulse_counter == 0 or pulse_id == expected_pulse_id:
                if expected_pulse_counter == 0:
                    pulse_to_handle = pulse_body_decoded
                expected_pulse_counter += 1
                if expected_pulse_counter < self._expected_input_pulses:
                    expected_pulse_id = pulse_id
                    continue
                else:
                    self.log_debug(f'All {self._expected_input_pulses} expected pulse(s) '
                                   f'with ID {pulse_id} received successfully.')
                    expected_pulse_counter = 0
                    expected_pulse_id = -1
            else:
                self.log_error(f'Failed to receive the expected number ({self._expected_input_pulses}) '
                               f'of input pulses with ID #{expected_pulse_id}. The system will attempt '
                               'to discard and continue without the missing events.')
                # note: this condition can only possibly occur if expecting at least 2 pulses,
                # so it is always safe to set these values here (instead of re-queueing the new pulse).
                expected_pulse_counter = 1
                expected_pulse_id = pulse_id
                next_pulse_to_handle = pulse_body_decoded
            if pulse_to_handle is not None:
                self.event_handler.handle_pulse_event(pulse_to_handle)
                pulse_to_handle = next_pulse_to_handle

            requeue_tags: list[int] = []
            current_su_events: list[SignalUpdateEvent] = []

            # Handle Signal Update events
            self.log_trace(f'Consuming events from {self.signal_update_queue}')
            signal_update_heartbeat_counter = 0
            for signal_update_method, _signal_update_props, signal_update_body in signal_update_channel.consume(
                self.signal_update_queue,
                inactivity_timeout=min(self._pulse_interval, 1.0) * 0.1
            ):
                signal_update_heartbeat_counter += 1
                if signal_update_heartbeat_counter >= 10:
                    pulse_connection.process_data_events()
                    internal_connection.process_data_events()
                    signal_update_heartbeat_counter = 0
                if signal_update_body is None:
                    self.log_trace(f'Finished consuming events from {self.signal_update_queue}')
                    break
                signal_update_body_decoded: SignalUpdateEvent = SignalUpdateEvent.decode(signal_update_body)
                if signal_update_body_decoded.update_timestamp == pulse_src_timestamp:
                    self.log_trace(f'Consumed signal update event from {self.signal_update_queue}')
                    signal_update_channel.basic_ack(delivery_tag=signal_update_method.delivery_tag)

                    current_su_events.append(signal_update_body_decoded)
                else:
                    requeue_tags.append(signal_update_method.delivery_tag)
                    self.log_trace(f'Ignoring/requeuing invalid signal update event\
                                        from {self.signal_update_queue} with tag {signal_update_method.delivery_tag}')
                    self.log_debug(f'Requeuing signal update event with timestamps '
                                   f'src={signal_update_body_decoded.source_timestamp}, '
                                   f'upd={signal_update_body_decoded.update_timestamp} '
                                   f'as {signal_update_body_decoded.update_timestamp} '
                                   f'!= {pulse_body_decoded.source_timestamp}')
                    break

            output_event_list = None
            if len(current_su_events):
                self.log_debug(f'Processing signal update event(s): {current_su_events}')
                event_list = SignalUpdateEventList(current_su_events, signal_update_body_decoded.source_timestamp)
                self._current_signal = copy.deepcopy(event_list)
                output_event_list = self.event_handler.handle_signal_update_events(event_list)
            elif self.force_signal_update:
                updated_current = self._get_signal_update_from_current(pulse_timestamp=pulse_body_decoded.source_timestamp)
                self.log_debug(f'Forcing new signal update based on current signal: {updated_current}')
                output_event_list = self.event_handler.handle_signal_update_events(updated_current)
            self.force_signal_update = False

            if output_event_list is not None:
                # push SU events downstream (if possible)
                if not len(self.publishable_signal_update_queues):
                    self.log_trace('No more downstream blocks to publish signal updates to.')
                else:
                    for event in output_event_list.events:
                        self.publish(
                            event=event,
                            exchange=self.signal_update_exchange,
                            channel=signal_update_channel
                        )
                    self.log_debug(f'Published Signal Update events downstream: {output_event_list.events}.')

            # requeue any ignored events
            for tag in requeue_tags:
                signal_update_channel.basic_nack(delivery_tag=tag, requeue=True)
            signal_update_channel.cancel()

            requeue_tags = []

            # handle any manual events after pulse received and signal updates processed
            if self.manual_queue != '':
                self.log_trace(f'Consuming events from {self.manual_queue}')
                manual_heartbeat_counter = 0
                for manual_method, _manual_props, manual_body in manual_channel.consume(
                    self.manual_queue,
                    inactivity_timeout=min(self._pulse_interval, 1.0) * 0.1
                ):
                    manual_heartbeat_counter += 1
                    if manual_heartbeat_counter >= 10:
                        pulse_connection.process_data_events()
                        internal_connection.process_data_events()
                        manual_heartbeat_counter = 0
                    if manual_body is None:
                        self.log_trace(f'Finished consuming events from {self.manual_queue}')
                        break
                    manual_body_decoded: ManualEvent = ManualEvent.decode(manual_body)
                    if manual_body_decoded.update_timestamp <= pulse_src_timestamp:
                        self.log_trace(f'Consumed {str(manual_body_decoded.subtype).upper()} '
                                       f'manual event from {self.manual_queue}')
                        self.log_debug(f'Processing manual event: {manual_body_decoded}')
                        manual_channel.basic_ack(delivery_tag=manual_method.delivery_tag)

                        output_events = self.event_handler.handle_manual_event(manual_body_decoded)

                        if output_events is not None:
                            # push any new manual events downstream (if possible)
                            if not len(self.publishable_manual_queues):
                                self.log_trace('No more downstream blocks to publish manual events to.')
                            else:
                                for event in output_events:
                                    self.publish(
                                        event=event,
                                        exchange=self.manual_exchange,
                                        channel=manual_channel
                                    )
                    else:
                        requeue_tags.append(manual_method.delivery_tag)
                        self.log_trace(f'Ignoring/requeuing invalid {str(manual_body_decoded.subtype).upper()} manual event\
                                         from {self.manual_queue} with tag {manual_method.delivery_tag}')
                        self.log_debug(f'Requeuing manual event with timestamps src={manual_body_decoded.source_timestamp}, '
                                       f'upd={manual_body_decoded.update_timestamp} as {manual_body_decoded.update_timestamp} '
                                       f'> {pulse_body_decoded.source_timestamp}')
                        break

            # requeue any ignored events
            for tag in requeue_tags:
                manual_channel.basic_nack(delivery_tag=tag, requeue=True)
            manual_channel.cancel()

            # once up to date, forward the pulse downstream (if possible)
            if not len(self.publishable_pulse_queues):
                self.log_trace('Reached end of signal chain.')
            else:
                self.publish(
                    event=self._get_updated_pulse_event(pulse_body_decoded),
                    exchange=self.pulse_exchange,
                    channel=pulse_channel
                )

        pulse_channel.cancel()
        self.log_trace(f'Unsubscribed from {self.pulse_queue}')

        pulse_channel.close()
        manual_channel.close()
        pulse_connection.close()
        internal_connection.close()
