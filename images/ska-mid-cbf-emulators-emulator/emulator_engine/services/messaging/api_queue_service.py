"""Message service implementation."""

from __future__ import annotations

import threading
from threading import Thread
from typing import Self, override
from uuid import uuid4

from emulator_engine import RABBITMQ_HOST
from emulator_engine.services.api.router_impl import RouterImpl
from pika import BasicProperties, BlockingConnection, ConnectionParameters

from ska_mid_cbf_emulators.common import (
    BaseMessageService,
    EmulatorError,
    IdService,
    InternalRestRequest,
    InternalRestResponse,
    LoggerFactory,
)


class ApiQueueService(BaseMessageService):
    """Message service which listens for API calls and processes them.

    Implements :obj:`BaseMessageService`.

    Args:
        **kwargs: Keyword arguments passed to the setup function.

    Keyword Args:
        api_request_queue (:obj:`str`): A unique identifier to give the API request queue.
        api_callback_queue (:obj:`str`): A unique identifier to give the API callback queue.
        subscriber_timeout (:obj:`int`): If specified, a timeout value in seconds after which \
            the service will automatically unsubscribe from events. Will never timeout if not specified.
        router_impl (:obj:`RouterImpl`): If specified, a router implementation to supply on initialization.
        name (:obj:`str`): A name to give this service (primarily for debugging purposes).
    """

    @staticmethod
    def init_for_subcontroller(
            bitstream_emulator_id: str,
            ip_block_id: str,
            **kwargs
    ) -> ApiQueueService:
        """Construct an instance of this service for a specific subcontroller.

        This will automatically populate queue names based on the controller and subcontroller IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The (unique) bitstream emulator ID.
            ip_block_id (:obj:`str`): The (unique) IP block ID associated with this subcontroller.
            **kwargs: Other keyword arguments to pass to the service constructor/setup function.

        Returns:
            :obj:`ApiQueueService`: The created service instance for the subcontroller.
        """
        return ApiQueueService(
            api_request_queue=IdService.api_request_queue_id(bitstream_emulator_id, ip_block_id),
            api_callback_queue=IdService.api_callback_queue_id(bitstream_emulator_id, ip_block_id),
            name=f'{ip_block_id}_api_service',
            **kwargs
        )

    @override
    def setup(
            self: Self,
            api_request_queue: str = '',
            api_callback_queue: str = '',
            subscriber_timeout: int = None,
            router_impl: RouterImpl = None,
            **kwargs
    ) -> None:
        """Set up the service.

        Args:
            api_request_queue (:obj:`str`): A unique identifier to give the API request queue. \
                Default is the empty string.
            api_callback_queue (:obj:`str`): A unique identifier to give the API callback queue. \
                Default is the empty string.
            subscriber_timeout (:obj:`int`, optional): If specified, a timeout value in seconds after which \
                the service will automatically unsubscribe from events. Default is `None` (no timeout).
            router_impl (:obj:`RouterImpl`, optional): If specified, a router implementation to supply on initialization. \
                Default is None.
            **kwargs: Arbitrary keyword arguments.

        Keyword Args:
            name (:obj:`str`): A name to give this service (primarily for debugging purposes).
        """
        self._log_prefix: str | None = kwargs.get('name')
        self._debug: bool = True
        self._subscriber_timeout: int | None = subscriber_timeout
        self._api_listener_t: Thread | None = None
        self.router_impl: RouterImpl = router_impl
        try:
            producer_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            raise EmulatorError('Could not connect to RabbitMQ broker after 5 attempts.')
        producer_channel = producer_connection.channel()

        self.api_request_queue: str = api_request_queue
        self.api_callback_queue: str = api_callback_queue
        producer_channel.queue_declare(queue=self.api_request_queue)
        producer_channel.queue_declare(queue=self.api_callback_queue)

        self._kill: bool = False

        self._init_api_thread()

        producer_channel.close()
        producer_connection.close()

    @override
    def subscribe(self: Self, **kwargs) -> None:
        """Subscribe to API calls.

        Starts the main listener thread.
        """
        if self._api_listener_t is not None and self._api_listener_t.is_alive():
            self.log_debug('Cannot subscribe to API calls while already subscribed.')
            return
        if self._api_listener_t is None:
            self._init_api_thread()
        self._api_listener_t.start()

    @override
    def publish(self: Self, **kwargs) -> None:
        return

    @override
    def unsubscribe(self: Self) -> None:
        """Unsubscribe from events.

        Signals the consumer thread to exit its main loop
        and waits for the thread to join.
        """
        if self._api_listener_t is None or not self._api_listener_t.is_alive():
            self.log_debug('Cannot unsubscribe as the service is not currently subscribed to API calls.')
            return
        self._kill = True
        if self._api_listener_t is not None and self._api_listener_t.is_alive():
            if self._api_listener_t.ident != threading.get_ident():
                self._api_listener_t.join()
                self.log_debug('Unsubscribed from API calls.')
        self._kill = False
        self._api_listener_t = None

    def flush(self: Self) -> None:
        """Flush all queues that this service is aware of.

        Loops through every queue name available and attempts to flush it.
        Any queue names that do not exist or cannot be flushed are ignored.
        """
        try:
            flush_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            return
        flush_channel = flush_connection.channel()
        for q in [
            self.api_request_queue,
            self.api_callback_queue
        ]:
            try:
                flush_channel.queue_purge(q.strip())
                self.log_trace(f'Flushed {q}')
            except Exception:
                continue
        flush_channel.close()
        flush_connection.close()

    def force_stop(self: Self) -> None:
        """Force stop the API listener."""
        self._kill = True

    @staticmethod
    def get_rpc_response(
            request_queue: str,
            callback_queue: str,
            request_body: InternalRestRequest
    ) -> InternalRestResponse:
        """Sends an RPC request via RabbitMQ, then waits for and returns the response."""
        logger = LoggerFactory.get_logger()
        try:
            request_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            logger.error('Could not connect to RabbitMQ broker after 5 attempts.')
            return InternalRestResponse.internal_server_error(
                'Could not connect to RabbitMQ broker after 5 attempts.'
            )
        request_channel = request_connection.channel()
        corr_id = str(uuid4())
        request_channel.basic_publish(
            exchange='',
            routing_key=request_queue,
            properties=BasicProperties(
                reply_to=callback_queue,
                correlation_id=corr_id,
            ),
            body=InternalRestRequest.to_json(request_body)
        )

        response = None
        timeout_sec = 30.0
        for _method, props, body in request_channel.consume(
            queue=callback_queue,
            auto_ack=True,
            inactivity_timeout=timeout_sec
        ):
            if body is None:
                response = InternalRestResponse.request_timeout(
                    f'Timeout: a response was not produced within {timeout_sec} seconds.'
                )
                break
            if props.correlation_id == corr_id:
                response: InternalRestResponse = InternalRestResponse.deserialize(body)
                break
        request_channel.cancel()
        request_connection.close()
        return response

    def _init_api_thread(self: Self) -> None:
        self._api_listener_t = Thread(target=self._listen_to_api)

    def _listen_to_api(self: Self) -> None:
        """Internal thread function which listens on the API RPC queue for requests,
        then calls the respective implementation and sends back an appropriate response.
        """
        try:
            api_connection = BlockingConnection(ConnectionParameters(host=RABBITMQ_HOST, connection_attempts=5))
        except Exception:
            self.log_error('Could not connect to RabbitMQ broker after 5 attempts.')
            raise EmulatorError('Could not connect to RabbitMQ broker after 5 attempts.')
        api_channel = api_connection.channel()
        self.log_trace(f'Subscribing to {self.api_request_queue}')
        api_heartbeat_counter = 0

        for api_method, api_props, api_body in api_channel.consume(self.api_request_queue, inactivity_timeout=1.0):
            api_heartbeat_counter += 1
            if api_heartbeat_counter >= 10:
                api_connection.process_data_events()
                api_heartbeat_counter = 0
            if api_body is None:
                if self._kill:
                    self.log_trace(f'Unsubscribing from {self.api_request_queue}')
                    break
                continue
            api_body_decoded: InternalRestRequest = InternalRestRequest.from_json(api_body)
            self.log_debug(f'Got API request: {api_body}')
            response = InternalRestResponse.not_found()
            if self.router_impl is not None:
                try:
                    response = self.router_impl.call(api_body_decoded.method_name, **api_body_decoded.kwargs)
                except Exception as e:
                    self.log_error(f'Failed to complete API call, error: {e}')
                    response = InternalRestResponse.internal_server_error(str(e))

            api_channel.basic_publish(
                exchange='',
                routing_key=api_props.reply_to,
                properties=BasicProperties(correlation_id=api_props.correlation_id),
                body=InternalRestResponse.serialize(response)
            )
            api_channel.basic_ack(delivery_tag=api_method.delivery_tag)

        api_channel.cancel()
        self._kill = False
