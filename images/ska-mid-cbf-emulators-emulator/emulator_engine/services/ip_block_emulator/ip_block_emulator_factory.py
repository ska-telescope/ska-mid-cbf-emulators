"""Factory class which creates IP block emulators based on imported emulator component specs."""

from typing import Any, Type

from emulator_engine.services.api.router_client import RouterClient
from emulator_engine.services.ip_block_emulator.ip_block_emulator_validator import IPBlockEmulatorValidator
from emulator_engine.services.ip_block_emulator.router_factory import RouterFactory
from emulator_engine.services.messaging.api_queue_service import ApiQueueService
from emulator_engine.services.messaging.event_queue_service import EventQueueService

from ska_mid_cbf_emulators.common import (
    BaseEmulatorApi,
    BaseEventHandler,
    EmulatorConfigIPBlock,
    EmulatorSubcontroller,
    FiniteStateMachine,
    LoggerFactory,
    ValidationError,
    ValidatorStatus,
)


class IPBlockEmulatorFactory():
    """Factory class which creates IP block emulators based on imported emulator component specs."""

    @staticmethod
    def create_many(
        root_path: str,
        bitstream_emulator_id: str,
        configs: list[EmulatorConfigIPBlock],
        pulse_interval: float
    ) -> list[tuple[EmulatorSubcontroller, RouterClient]]:
        """Create subcontrollers and API routers from a list of IP block emulator specs and configurations.

        NOTE: the bitstream emulator path must be present in the python search path, via `sys.path.append`.

        Args:
            root_path (:obj:`str`): The root path which contains the individual IP block emulator folders, \
                e.g. `/app/mnt/bitstream/<id>/<ver>/emulators`.
            bitstream_emulator_id (:obj:`str`): The ID of the bitstream emulator this subcontroller will belong to.
            configs (:obj:`list[EmulatorConfigIPBlock]`): The list of IP block configurations.
            pulse_interval (:obj:`float`): The interval in seconds between pulses being used by the system. \
                Used to calibrate certain internal timings.

        Returns:
            :obj:`list[tuple[EmulatorSubcontroller, RouterClient]]` The list of generated subcontrollers \
                and their API routers.
        """
        already_processed = {}
        return [IPBlockEmulatorFactory.create(
            root_path,
            bitstream_emulator_id,
            config,
            pulse_interval,
            already_processed
        ) for config in configs]

    @staticmethod
    def create(
        root_path: str,
        bitstream_emulator_id: str,
        config: EmulatorConfigIPBlock,
        pulse_interval: float,
        already_processed: dict[str, tuple[
            Type[BaseEmulatorApi] | None,
            Type[BaseEventHandler] | None,
            Type[Any] | None,
            Type[FiniteStateMachine] | None
        ]] | None = None
    ) -> tuple[EmulatorSubcontroller, RouterClient]:
        """Create a subcontroller and API router from an IP block emulator spec and configuration.

        NOTE: the bitstream emulator path must be present in the python search path, via `sys.path.append`.

        Args:
            root_path (:obj:`str`): The root path which contains the individual IP block emulator folders, \
                e.g. `/app/mnt/bitstream/<id>/<ver>/emulators`.
            bitstream_emulator_id (:obj:`str`): The ID of the bitstream emulator this subcontroller will belong to.
            config (:obj:`EmulatorConfigIPBlock`): The IP block configuration.
            pulse_interval (:obj:`float`): The interval in seconds between pulses being used by the system. \
                Used to calibrate certain internal timings.
            already_processed (:obj:`dict[str, tuple[Type[BaseEmulatorApi] | None, \
                Type[BaseEventHandler] | None, Type[Any] | None, \
                Type[FiniteStateMachine] | None]]`, optional): Dict of component classes that have already been processed. \
                Default is an empty dict.

        Returns:
            :obj:`tuple[EmulatorSubcontroller, RouterClient]` The generated subcontroller and its API router.
        """
        logger = LoggerFactory.get_logger()

        event_service = EventQueueService.init_for_subcontroller(
            bitstream_emulator_id=bitstream_emulator_id,
            ip_block_id=config.id,
            downstream_ip_block_ids=config.downstream_block_ids,
            pulse_interval=pulse_interval,
            expected_input_pulses=max(len(config.upstream_block_ids), 1)
        )
        api_service = ApiQueueService.init_for_subcontroller(
            bitstream_emulator_id=bitstream_emulator_id,
            ip_block_id=config.id
        )

        if already_processed is not None and (existing_classes := already_processed.get(config.type)) is not None:
            api_cls, eh_cls, ip_cls, fsm_cls = existing_classes
        else:
            validation_result, api_cls, eh_cls, ip_cls, fsm_cls = IPBlockEmulatorValidator.validate_and_get_components(
                root_path,
                config.type
            )
            match validation_result.overall_status:
                case ValidatorStatus.OK:
                    logger.info(f'Validation for block {config.type} was successful. Result: \n{str(validation_result)}')
                case ValidatorStatus.WARNING:
                    logger.warning(f'Validation for block {config.type} passed with warnings. Result: \n{str(validation_result)}')
                case ValidatorStatus.ERROR:
                    raise ValidationError(f'Validation for block {config.type} failed. Result: \n{str(validation_result)}')

        state_machine: FiniteStateMachine = fsm_cls()

        new_subcontroller = EmulatorSubcontroller(
            bitstream_emulator_id=bitstream_emulator_id,
            event_service=event_service,
            api_service=api_service,
            state_machine=state_machine,
            ip_block_id=config.id,
            display_name=config.display_name,
            type=config.type,
            downstream_block_ids=config.downstream_block_ids,
            logger=logger
        )

        new_subcontroller.ip_block = ip_cls()
        for (const_name, const_value) in config.constants.items():
            setattr(new_subcontroller.ip_block, const_name, const_value)

        api: BaseEmulatorApi = api_cls(new_subcontroller)
        event_handler: BaseEventHandler = eh_cls(new_subcontroller)

        router_client, router_impl = RouterFactory.create(bitstream_emulator_id, new_subcontroller, api)
        new_subcontroller.api_service.router_impl = router_impl
        new_subcontroller.event_service.event_handler = event_handler

        if already_processed is not None and existing_classes is None:
            already_processed[config.type] = (api_cls, eh_cls, ip_cls, fsm_cls)

        return new_subcontroller, router_client
