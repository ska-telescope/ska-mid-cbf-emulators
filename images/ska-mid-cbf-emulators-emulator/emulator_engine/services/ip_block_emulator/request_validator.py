"""Classes used for validating received API parameters against the API specification."""

import copy
import types
from inspect import Parameter
from typing import Any, Self, Type, get_origin, override

from ska_mid_cbf_emulators.common import (
    ApiParam,
    ApiRouteMetadata,
    BaseValidator,
    BaseValidatorResult,
    ValidatorMessage,
    ValidatorStatus,
)


class RequestValidatorResult(BaseValidatorResult):
    """Message object for a single API request validation.

    Args:
        initial_status (:obj:`ValidatorStatus`, optional): The initial status to assume for this validation result.\
            Default is `ValidatorStatus.OK`.

    Attributes:
        overall_status (:obj:`ValidatorStatus`): The overall status of this validation result.
        result (:obj:`dict[str, dict[str, ValidatorMessage] | list[ValidatorMessage]]`):\
            A dictionary containing all messages generated from this validation, organized by parameter type.
        combined_args (:obj:`dict[str, Any]`): A dictionary containing all validated arguments,\
            converted to the correct type if necessary.
        PATH_KEY (:obj:`str`): The key/label used for the path parameter sub-dictionary in the result.
        QUERY_KEY (:obj:`str`): The key/label used for the query parameter sub-dictionary in the result.
        BODY_KEY (:obj:`str`): The key/label used for the body parameter sub-dictionary in the result.
        OTHER_KEY (:obj:`str`): The key/label used for the list of other messages in the result.
    """
    PATH_KEY = 'path'
    QUERY_KEY = 'query'
    BODY_KEY = 'body'
    OTHER_KEY = 'other'

    def __init__(self: Self, initial_status: ValidatorStatus = ValidatorStatus.OK) -> None:
        super().__init__(initial_status)
        self.result: dict[str, dict[str, ValidatorMessage] | list[ValidatorMessage]] = {
            self.PATH_KEY: {},
            self.QUERY_KEY: {},
            self.BODY_KEY: {},
            self.OTHER_KEY: []
        }
        self.combined_args: dict[str, Any] = {}

    @override
    @property
    def json_dict(self: Self) -> dict[str, Any]:
        """A JSON-serializable dictionary representing this result object."""
        return {
            'overall_status': self.overall_status.name,
            'result': {
                self.PATH_KEY: {k: v.json_dict for (k, v) in self.result[self.PATH_KEY].items()},
                self.QUERY_KEY: {k: v.json_dict for (k, v) in self.result[self.QUERY_KEY].items()},
                self.BODY_KEY: {k: v.json_dict for (k, v) in self.result[self.BODY_KEY].items()},
                self.OTHER_KEY: [v.json_dict for v in self.result[self.OTHER_KEY]]
            },
            'combined_args': self.combined_args
        }

    def insert_arg(self: Self, param_name: str, value: Any) -> None:
        """Insert an argument into the args dict."""
        self.combined_args[param_name] = value

    def insert_other(self: Self, req_msg: ValidatorMessage) -> None:
        """Insert a message into the list of other messages."""
        self.insert_msg(self.OTHER_KEY, None, req_msg)

    @override
    def insert_msg(self: Self, key: str, param_name: str, req_msg: ValidatorMessage) -> None:
        """Insert a message into the result."""
        if key != self.OTHER_KEY:
            self.result.get(key)[param_name] = req_msg
        else:
            self.result.get(key).append(req_msg)
        self.overall_status = max(self.overall_status, req_msg.status)


class RequestValidator(BaseValidator):
    """Validator which handles API request data.

    Implements :obj:`BaseValidator`.
    """
    @override
    @staticmethod
    def validate(
        rx_path_param: dict[str, Any],
        rx_query_params: dict[str, str],
        rx_body: dict[str, Any],
        metadata: ApiRouteMetadata
    ) -> RequestValidatorResult:
        """Validate parameters of an API request against the given specification.

        Args:
            rx_path_param (:obj:`dict[str, Any]`): The received path parameter dictionary from the request.\
                (This should only contain one item, but it is defined as a dict in the FastAPI spec.)
            rx_query_params (:obj:`dict[str, str]`): The received query parameter dictionary from the request.
            rx_body (:obj:`dict[str, Any]`): The received body from the request. It is expected to be valid JSON.
            metadata (:obj:`ApiRouteMetadata`): The metadata from the API specification to validate against.

        Returns:
            :obj:`RequestValidatorResult` The result of the validation.
        """
        partial_result = RequestValidatorResult()
        RequestValidator._process_path_param(partial_result, rx_path_param, metadata)
        RequestValidator._process_query_params(partial_result, rx_query_params, metadata)
        RequestValidator._process_body(partial_result, rx_body, metadata)
        return partial_result

    @staticmethod
    def _process_path_param(
        partial_result: RequestValidatorResult,
        rx_path_param: dict[str, Any],
        metadata: ApiRouteMetadata
    ) -> None:
        """Validate the path parameter for the request."""
        if metadata.path_param is not None:  # Expecting a path param
            rx_value = rx_path_param.get(metadata.path_param.name, None)
            RequestValidator._processing_helper(partial_result, rx_value, metadata.path_param, RequestValidatorResult.PATH_KEY)
        elif rx_path_param is not None and len(rx_path_param.keys()):  # Not expecting path param but one was provided
            partial_result.insert_other(ValidatorMessage(
                ValidatorStatus.WARNING,
                f'Unexpected path parameter with value {list(rx_path_param.values())[0]} was provided; ignoring.'
            ))

    @staticmethod
    def _process_query_params(
        partial_result: RequestValidatorResult,
        rx_query_params: dict[str, str],
        metadata: ApiRouteMetadata
    ) -> None:
        """Validate the query parameters for the request."""
        rx_query_params_copy = copy.deepcopy(rx_query_params)
        if metadata.query_params is not None and len(metadata.query_params):  # expecting query params
            for md_query_param in metadata.query_params:
                rx_value = rx_query_params_copy.pop(md_query_param.name, None)
                RequestValidator._processing_helper(
                    partial_result, rx_value, md_query_param, RequestValidatorResult.QUERY_KEY)
        for leftover_param in rx_query_params_copy.items():  # any extraneous query params
            partial_result.insert_other(ValidatorMessage(
                ValidatorStatus.WARNING,
                f'Unknown query parameter with name {leftover_param[0]} and value {leftover_param[1]} was provided; ignoring.'
            ))

    @staticmethod
    def _process_body(
        partial_result: RequestValidatorResult,
        rx_body: dict[str, Any],
        metadata: ApiRouteMetadata
    ) -> None:
        """Validate the body for the request."""
        if metadata.body_param is not None:  # Expecting a body
            RequestValidator._processing_helper(partial_result, rx_body, metadata.body_param, RequestValidatorResult.BODY_KEY)
        elif rx_body is not None and len(rx_body):  # Not expecting a body but one was provided
            partial_result.insert_other(ValidatorMessage(
                ValidatorStatus.WARNING,
                f'Unexpected body with value {rx_body} was provided; ignoring.'
            ))

    @staticmethod
    def _processing_helper(
        partial_result: RequestValidatorResult,
        rx_value: Any,
        metadata_param: ApiParam,
        insert_key: str
    ) -> None:
        """Helper method with common logic used in validating all parameter types."""
        if rx_value is None:  # param not provided
            if metadata_param.default == Parameter.empty:  # No default set
                partial_result.insert_msg(insert_key, metadata_param.name, ValidatorMessage(
                    ValidatorStatus.ERROR,
                    f'Required parameter {metadata_param.name} was not provided.'
                ))
                return
            else:  # default exists
                partial_result.insert_msg(insert_key, metadata_param.name, ValidatorMessage(
                    ValidatorStatus.OK,
                    f'Optional parameter {metadata_param.name} was not provided; using default: {metadata_param.default}.'
                ))
                return
        else:  # param provided, check type
            valid, new_value_or_error_msg = RequestValidator._fix_type(rx_value, metadata_param.type)
            if not valid:
                partial_result.insert_msg(insert_key, metadata_param.name, ValidatorMessage(
                    ValidatorStatus.ERROR,
                    new_value_or_error_msg
                ))
                return
            else:
                partial_result.insert_msg(insert_key, metadata_param.name, ValidatorMessage(
                    ValidatorStatus.OK,
                    'Processed successfully.'
                ))
                partial_result.insert_arg(metadata_param.name, new_value_or_error_msg)
                return

    @staticmethod
    def _fix_type(rx_value: Any, expected_type: Type[Any]) -> tuple[bool, Any]:
        """Either verify that the provided value is the correct type, or attempt to convert it to that type."""
        if isinstance(rx_value, str):
            if expected_type == str:
                return True, rx_value
            if expected_type == int:
                try:
                    return True, int(rx_value, 10)
                except ValueError:
                    pass
            if expected_type == float:
                try:
                    return True, float(rx_value)
                except ValueError:
                    pass
            if expected_type == bool and rx_value.lower() in ['true', 'false', '0', '1']:
                return True, rx_value.lower() in ['true', '0']

        else:
            for rx_container_type in (dict, list, tuple, set):
                if isinstance(rx_value, rx_container_type):
                    if isinstance(expected_type, types.GenericAlias):  # Expected is alias type e.g. dict[str, str]
                        expected_origin = get_origin(expected_type)
                        if expected_origin == rx_container_type:  # Expected is alias type and rx is already correct type
                            return True, rx_value

                        try:  # Expected is alias type and rx is not correct type
                            return True, expected_origin(rx_value)
                        except Exception:
                            break

                    elif rx_container_type == expected_type:  # Expected is not alias type and rx is already correct type
                        return True, rx_value

                    try:  # Expected is not alias type and rx is not correct type
                        return True, expected_type(rx_value)
                    except Exception:
                        break

        return False, f'Could not parse value {rx_value} as type {expected_type}.'
