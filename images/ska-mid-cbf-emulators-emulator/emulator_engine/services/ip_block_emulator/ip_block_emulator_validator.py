"""Validator which handles IP block emulators."""

import importlib.util
import inspect
import os
import re
from importlib.machinery import ModuleSpec
from types import GenericAlias, ModuleType
from typing import Any, Callable, Self, Type, get_origin

from ska_mid_cbf_emulators.common import (
    BaseEmulatorApi,
    BaseEventHandler,
    BaseState,
    BaseTransitionTrigger,
    BaseValidator,
    BaseValidatorResult,
    BodyParam,
    FiniteStateMachine,
    ManualEvent,
    PathParam,
    PulseEvent,
    QueryParam,
    RoutingState,
    SignalUpdateEventList,
    TransitionCondition,
    ValidatorMessage,
    ValidatorStatus,
)


class IPBlockEmulatorValidatorResult(BaseValidatorResult):
    """Message object for a validation of a particular set of IP block emulators.

    Args:
        initial_status (:obj:`ValidatorStatus`, optional): The initial status to assume for this validation result.\
            Default is `ValidatorStatus.OK`.

    Attributes:
        overall_status (:obj:`ValidatorStatus`): The overall status of this validation result.
        result (:obj:`dict[str, list[ValidatorMessage]]`):\
            A dictionary containing all messages generated from this validation, organized by component type.
    """
    API_KEY = 'api'
    EVENT_HANDLER_KEY = 'event_handler'
    IP_BLOCK_KEY = 'ip_block'
    STATE_MACHINE_KEY = 'state_machine'

    def __init__(self: Self, initial_status: ValidatorStatus = ValidatorStatus.OK) -> None:
        super().__init__(initial_status)
        self.result: dict[str, list[ValidatorMessage]] = {
            self.API_KEY: [],
            self.EVENT_HANDLER_KEY: [],
            self.IP_BLOCK_KEY: [],
            self.STATE_MACHINE_KEY: []
        }

    @property
    def json_dict(self: Self) -> dict[str, Any]:
        """A JSON-serializable dictionary representing this result object."""
        return {
            'overall_status': self.overall_status.name,
            'result': {k: [msg.json_dict for msg in v] for (k, v) in self.result.items()}
        }

    def insert_msg(self: Self, key: str, msg: ValidatorMessage) -> None:
        """Insert a message into the result."""
        self.result[key] = self.result.get(key, []) + [msg]
        self.overall_status = max(self.overall_status, msg.status)


class IPBlockEmulatorValidator(BaseValidator):
    """Validator which handles IP block emulators.

    Implements :obj:`BaseValidator`.
    """
    @staticmethod
    def validate(
        directory_path: str,
        ip_block_type: str
    ) -> IPBlockEmulatorValidatorResult:
        """Validate a specific IP block emulator in a specified directory.

        Args:
            directory_path (:obj:`str`): The path of the parent directory containing the individual IP block emulator folders,\
                e.g. `/app/mnt/bitstream/<id>/<ver>/emulators`.
            ip_block_type (:obj:`str`): The specific IP block emulator to validate.

        Returns:
            :obj:`IPBlockEmulatorValidatorResult` The result of the validation.
        """
        partial_result = IPBlockEmulatorValidatorResult()

        IPBlockEmulatorValidator._process_ip_block_emulator(
            partial_result,
            directory_path,
            ip_block_type
        )

        return partial_result

    @staticmethod
    def validate_and_get_components(
        directory_path: str,
        ip_block_type: str
    ) -> tuple[
        IPBlockEmulatorValidatorResult,
        Type[BaseEmulatorApi] | None,
        Type[BaseEventHandler] | None,
        Type[Any] | None,
        Type[FiniteStateMachine] | None
    ]:
        """Validate a specific IP block emulator in a specified directory, and return the loaded components.

        NOTE: the bitstream emulator path must be present in the python search path, via `sys.path.append`.

        Args:
            directory_path (:obj:`str`): The path of the parent directory containing the individual IP block emulator folders,\
                e.g. `/app/mnt/bitstream/<id>/<ver>/emulators`.
            ip_block_type (:obj:`str`): The specific IP block emulator to validate.

        Returns:
            :obj:`tuple[IPBlockEmulatorValidatorResult, Type[BaseEmulatorApi] | None, \
                Type[BaseEventHandler] | None, Type[Any] | None, \
                Type[FiniteStateMachine] | None]` The result of the validation, the API class, the event handler function, \
                the simulated IP block class, and the state machine class.
        """
        partial_result = IPBlockEmulatorValidatorResult()

        api_cls, event_handler_cls, ip_block_cls, fsm_cls = IPBlockEmulatorValidator._process_ip_block_emulator(
            partial_result,
            directory_path,
            ip_block_type
        )

        return partial_result, api_cls, event_handler_cls, ip_block_cls, fsm_cls

    @staticmethod
    def _process_ip_block_emulator(
        partial_result: IPBlockEmulatorValidatorResult,
        directory_path: str,
        ip_block_type: str
    ) -> tuple[
        Type[BaseEmulatorApi] | None,
        Type[BaseEventHandler] | None,
        Type[Any] | None,
        Type[FiniteStateMachine] | None
    ]:
        """Validate the specified IP block emulator and return the loaded components."""
        emulator_path = os.path.join(directory_path, ip_block_type, 'emulator')

        api_cls = IPBlockEmulatorValidator._process_api(partial_result, emulator_path, ip_block_type)
        event_handler_cls = IPBlockEmulatorValidator._process_event_handler(partial_result, emulator_path, ip_block_type)
        ip_block_cls = IPBlockEmulatorValidator._process_ip_block(partial_result, emulator_path, ip_block_type)
        fsm_cls = IPBlockEmulatorValidator._process_fsm(partial_result, emulator_path, ip_block_type)

        return api_cls, event_handler_cls, ip_block_cls, fsm_cls

    @staticmethod
    def _process_api(
        partial_result: IPBlockEmulatorValidatorResult,
        emulator_path: str,
        ip_block_type: str
    ) -> Type[BaseEmulatorApi] | None:
        """Validate the API component of this IP block emulator."""
        ok: bool = True
        component_name: str = 'api'
        file_path: str = f'{emulator_path}/{component_name}.py'
        result_key = IPBlockEmulatorValidatorResult.API_KEY

        api_spec = IPBlockEmulatorValidator._load_component_spec(
            ip_block_type=ip_block_type,
            component_name=component_name,
            file_path=file_path
        )

        api_module = IPBlockEmulatorValidator._load_component_module(
            component_spec=api_spec,
            component_name=component_name,
            partial_result=partial_result,
            result_key=result_key
        )
        if api_module is None:
            return None

        if not IPBlockEmulatorValidator._class_exists_in_module(
            component_module=api_module,
            component_name=component_name,
            class_name='EmulatorApi',
            partial_result=partial_result,
            result_key=result_key,
            base_class_name='BaseEmulatorApi'
        ):
            return None

        cls = api_module.EmulatorApi

        if not IPBlockEmulatorValidator._is_correct_subclass(
            cls=cls,
            base_class=BaseEmulatorApi,
            partial_result=partial_result,
            result_key=result_key
        ):
            ok = False

        for fn_name in filter(lambda fn_name: not fn_name.startswith('__'), vars(cls).keys()):
            cls_fn = getattr(cls, fn_name)
            if fn_name[0] != '_' and not hasattr(cls_fn, 'metadata'):
                partial_result.insert_msg(result_key, ValidatorMessage(
                    ValidatorStatus.WARNING,
                    f'The `{fn_name}()` method is public, but does not have a `BaseEmulatorApi.route` decorator. '
                    'If this is intended to be an API route, the decorator must be added or it will not work correctly. '
                    f'Otherwise, consider renaming this method to `_{fn_name}()` to avoid confusion. '
                    'See the IP block emulator README for more detail.'
                ))
            elif fn_name[0] == '_' and hasattr(cls_fn, 'metadata'):
                partial_result.insert_msg(result_key, ValidatorMessage(
                    ValidatorStatus.WARNING,
                    f'The `{fn_name}()` method is private, but has a `BaseEmulatorApi.route` decorator. '
                    'If this is not intended to be an API route, consider removing the decorator. '
                    f'Otherwise, consider renaming this method to `{fn_name.lstrip("_")}()` to avoid confusion. '
                    'See the IP block emulator README for more detail.'
                ))
            if hasattr(cls_fn, 'metadata'):
                signature = inspect.signature(cls_fn)
                for param_name, param in signature.parameters.items():
                    if param_name == 'self':
                        continue
                    if get_origin(param.annotation) not in [PathParam, QueryParam, BodyParam]:
                        t = (
                            str(param.annotation) if type(param.annotation) is GenericAlias else param.annotation.__name__
                        ) if param.annotation != inspect._empty else 'Any'
                        suggested_metatype = 'BodyParam' if get_origin(param.annotation) in [dict, list] else 'QueryParam'
                        partial_result.insert_msg(result_key, ValidatorMessage(
                            ValidatorStatus.WARNING,
                            f'The `{fn_name}()` method has parameter `{param_name}` which either has no type annotation '
                            'or is not annotated with a PathParam, QueryParam, or BodyParam metatype. '
                            'The emulator uses this information to determine whether a parameter is a path parameter, '
                            'a query parameter, or a request body. Without it, the parameter will never be supplied. '
                            f'Consider changing this parameter to e.g. `{param_name}: {suggested_metatype}[{t}]`. '
                            'See the IP block emulator README for more detail.'
                        ))

        if ok:
            return cls

    @staticmethod
    def _process_event_handler(
        partial_result: IPBlockEmulatorValidatorResult,
        emulator_path: str,
        ip_block_type: str
    ) -> Type[BaseEventHandler] | None:
        """Validate the event handler component of this IP block emulator."""
        ok: bool = True
        component_name: str = 'event_handler'
        file_path: str = f'{emulator_path}/{component_name}.py'
        result_key = IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY

        eh_spec = IPBlockEmulatorValidator._load_component_spec(
            ip_block_type=ip_block_type,
            component_name=component_name,
            file_path=file_path
        )

        event_handler_module = IPBlockEmulatorValidator._load_component_module(
            component_spec=eh_spec,
            component_name=component_name,
            partial_result=partial_result,
            result_key=result_key
        )
        if event_handler_module is None:
            return None

        if not IPBlockEmulatorValidator._class_exists_in_module(
            component_module=event_handler_module,
            component_name=component_name,
            class_name='EmulatorEventHandler',
            partial_result=partial_result,
            result_key=result_key,
            base_class_name='BaseEventHandler'
        ):
            return None

        cls = event_handler_module.EmulatorEventHandler

        if not IPBlockEmulatorValidator._is_correct_subclass(
            cls=cls,
            base_class=BaseEventHandler,
            partial_result=partial_result,
            result_key=result_key
        ):
            ok = False

        if ('handle_pulse_event' in vars(cls)):
            def expected_signature_fn(self: Self, event: PulseEvent, **kwargs) -> None:
                """..."""
            if not IPBlockEmulatorValidator._event_handler_takes_correct_parameters(
                partial_result=partial_result,
                actual_fn=cls.handle_pulse_event,
                expected_signature_fn=expected_signature_fn
            ):
                ok = False

        if ('handle_signal_update_events' in vars(cls)):
            def expected_signature_fn(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
                """..."""
            if not IPBlockEmulatorValidator._event_handler_takes_correct_parameters(
                partial_result=partial_result,
                actual_fn=cls.handle_signal_update_events,
                expected_signature_fn=expected_signature_fn
            ):
                ok = False

        if ('handle_manual_event' in vars(cls)):
            def expected_signature_fn(self: Self, event: ManualEvent, **kwargs) -> None | list[ManualEvent]:
                """..."""
            if not IPBlockEmulatorValidator._event_handler_takes_correct_parameters(
                partial_result=partial_result,
                actual_fn=cls.handle_manual_event,
                expected_signature_fn=expected_signature_fn
            ):
                ok = False

        if ok:
            return cls

    @staticmethod
    def _event_handler_takes_correct_parameters(
        partial_result: IPBlockEmulatorValidatorResult,
        actual_fn: Callable,
        expected_signature_fn: Callable
    ) -> bool:
        actual_signature = inspect.signature(actual_fn)
        actual_params = actual_signature.parameters
        expected_signature = inspect.signature(expected_signature_fn)
        expected_params = expected_signature.parameters

        if actual_params != expected_params:
            actual_params_str = (re.match(r"\(.*\)", str(actual_signature)) or ['<UNKNOWN>'])[0]
            expected_params_str = (re.match(r"\(.*\)", str(expected_signature)) or ['<UNKNOWN>'])[0]
            partial_result.insert_msg(IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'The `{expected_signature_fn.__name__}` function must take exactly the parameters '
                f'{expected_params_str}. Currently it takes: {actual_params_str}.'
            ))
            return False
        return True

    @staticmethod
    def _process_ip_block(
        partial_result: IPBlockEmulatorValidatorResult,
        emulator_path: str,
        ip_block_type: str
    ) -> Type[Any] | None:
        """Validate the simulated IP block component of this IP block emulator."""
        component_name: str = 'ip_block'
        file_path: str = f'{emulator_path}/{component_name}.py'
        result_key = IPBlockEmulatorValidatorResult.IP_BLOCK_KEY

        ip_spec = IPBlockEmulatorValidator._load_component_spec(
            ip_block_type=ip_block_type,
            component_name=component_name,
            file_path=file_path
        )

        ip_block_module = IPBlockEmulatorValidator._load_component_module(
            component_spec=ip_spec,
            component_name=component_name,
            partial_result=partial_result,
            result_key=result_key
        )
        if ip_block_module is None:
            return None

        if not IPBlockEmulatorValidator._class_exists_in_module(
            component_module=ip_block_module,
            component_name=component_name,
            class_name='EmulatorIPBlock',
            partial_result=partial_result,
            result_key=result_key
        ):
            return None

        cls = ip_block_module.EmulatorIPBlock

        if not ('__init__' in vars(cls)):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'The __init__ method must be defined on the EmulatorIPBlock class. '
                'It is currently missing or misnamed.'
            ))
            return None

        init_params = inspect.signature(cls).parameters
        no_defaults = [n for n, _ in filter(lambda x: x[1].default == inspect._empty, init_params.items())]
        defaults = [n for n, _ in filter(lambda x: x[1].default != inspect._empty, init_params.items())]
        if len(no_defaults):
            msg = (f'The __init__ method should take no parameters (except `self`), as it will not be passed any arguments. '
                   f'The parameter(s) {no_defaults} have no default values and will cause errors. ')
            if len(defaults):
                msg += (f'The parameters {defaults} have default values; '
                        'however, as these will never be passed as arguments, '
                        'consider simply setting them as constants/variables.')
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                msg
            ))
            return None
        if len(defaults):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.WARNING,
                'The __init__ method should take no parameters (except `self`), as it will not be passed any arguments. '
                f'It currently takes the parameters {defaults}. These should cause no errors as they all have default values, '
                'however, as these will never be passed as arguments, consider simply setting them as constants/variables. '
            ))

        return cls

    @staticmethod
    def _process_fsm(
        partial_result: IPBlockEmulatorValidatorResult,
        emulator_path: str,
        ip_block_type: str
    ) -> Type[FiniteStateMachine] | None:
        """Validate the state machine component of this IP block emulator."""
        ok: bool = True
        component_name: str = 'state_machine'
        file_path: str = f'{emulator_path}/{component_name}.py'
        result_key = IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY

        fsm_spec = IPBlockEmulatorValidator._load_component_spec(
            ip_block_type=ip_block_type,
            component_name=component_name,
            file_path=file_path
        )

        state_machine_module = IPBlockEmulatorValidator._load_component_module(
            component_spec=fsm_spec,
            component_name=component_name,
            partial_result=partial_result,
            result_key=result_key
        )
        if state_machine_module is None:
            return None

        if not IPBlockEmulatorValidator._class_exists_in_module(
            component_module=state_machine_module,
            component_name=component_name,
            class_name='EmulatorStateMachine',
            partial_result=partial_result,
            result_key=result_key,
            base_class_name='FiniteStateMachine'
        ):
            return None

        cls = state_machine_module.EmulatorStateMachine

        if not IPBlockEmulatorValidator._is_correct_subclass(
            cls=cls,
            base_class=FiniteStateMachine,
            partial_result=partial_result,
            result_key=result_key
        ):
            return None

        unimplemented_props = cls.__abstractmethods__
        if len(unimplemented_props):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'The `EmulatorStateMachine` class must implement the abstract properties '
                '`_states`, `_initial_state`, and `_transitions`. '
                f'An implementation is currently missing for: {list(unimplemented_props)}. '
                'See the README for more details. '
            ))
            return None

        # get properties w/o running __init__
        pseudo_machine: FiniteStateMachine = cls.__new__(cls)

        if not isinstance(pseudo_machine._states, list):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'EmulatorStateMachine._states must be a list. '
                f'It is currently of type `{type(pseudo_machine._states).__name__}`.'
            ))
            return None

        if not len(pseudo_machine._states):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'EmulatorStateMachine must include at least one state. It currently has 0.'
            ))
            return None

        invalid_states = list(filter(lambda state: state.__class__.__bases__[0] != BaseState, pseudo_machine._states))
        if len(invalid_states):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'States must all be members of `BaseState` subclasses. The following states do not comply: {invalid_states}. '
                'See the README for more details.'
            ))
            return None

        if pseudo_machine._initial_state not in pseudo_machine._states:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'The `_initial_state` property must be an element of `_states`. '
                f'It is currently {pseudo_machine._initial_state}, which is not an element of {pseudo_machine._states}.'
            ))
            ok = False

        if not isinstance(pseudo_machine._transitions, list):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                'EmulatorStateMachine._transitions must be a list. '
                f'It is currently of type `{type(pseudo_machine._transitions).__name__}`.'
            ))
            return None

        for transition in pseudo_machine._transitions:
            def _trans_err(msg: str) -> None:
                partial_result.insert_msg(result_key, ValidatorMessage(
                    ValidatorStatus.ERROR,
                    f'Error in transition {transition}: {msg}'
                ))
                nonlocal ok
                ok = False

            if not isinstance(transition, dict):
                _trans_err(f'Transition must be a dictionary. It is currently of type `{type(transition).__name__}`. '
                           'See the README for details.')

            else:
                missing = []

                if transition.get('source', None) is None:
                    missing.append('source')
                else:
                    source = transition['source']
                    if isinstance(source, list):
                        if RoutingState.FROM_ANY in source:
                            _trans_err('`RoutingState.FROM_ANY` may only be used by itself, '
                                       'and not as part of a list of states.')
                        if RoutingState.TO_SAME in source:
                            _trans_err('`RoutingState.TO_SAME` may only be used as a destination state.')
                        if len(diff := set(source) - {s for s in RoutingState} - set(pseudo_machine._states)):
                            _trans_err('The `source` property, when given as a list, must represent a subset of `_states`. '
                                       f'It currently contains extraneous elements: {diff}.')
                    else:
                        if source == RoutingState.TO_SAME:
                            _trans_err('`RoutingState.TO_SAME` may only be used as a destination state.')
                        elif source != RoutingState.FROM_ANY and source not in pseudo_machine._states:
                            _trans_err('The `source` property, when given as a single state, '
                                       'must either be an element of `_states`, or `RoutingState.FROM_ANY`. '
                                       f'It is currently `{source}`, which is not an element of {pseudo_machine._states}.')

                if transition.get('dest', None) is None:
                    missing.append('dest')
                else:
                    dest = transition['dest']
                    if isinstance(dest, list | set | dict):
                        _trans_err('The `dest` property must contain only a single state. It cannot be a list.')
                    else:
                        if dest == RoutingState.FROM_ANY:
                            _trans_err('`RoutingState.FROM_ANY` may only be used as a source state.')
                        elif dest != RoutingState.TO_SAME and dest not in pseudo_machine._states:
                            _trans_err('The `dest` property must either be an element of `_states`, or `RoutingState.TO_SAME`. '
                                       f'It is currently `{dest}`, which is not an element of {pseudo_machine._states}.')

                if transition.get('trigger', None) is None:
                    missing.append('trigger')
                else:
                    trigger = transition['trigger']
                    if isinstance(trigger, list | set | dict):
                        _trans_err('The `trigger` property must contain only a single trigger name. It cannot be a list.')
                    else:
                        if trigger.__class__.__bases__[0] != BaseTransitionTrigger:
                            _trans_err('The `trigger` property must be a member of a `BaseTransitionTrigger` subclass. '
                                       f'The current value is `{trigger}`, which does not comply. '
                                       'See the README for more details.')

                if transition.get('conditions', None) is not None:
                    conditions = transition['conditions']
                    if isinstance(conditions, TransitionCondition):
                        conditions = [conditions]
                    elif not isinstance(conditions, list):
                        _trans_err('The `conditions` property must be a `TransitionCondition` instance or a list of them. '
                                   f'It is currently: {conditions}. See the README for more details.')
                    else:
                        invalid_conditions = []
                        for condition in conditions:
                            if not isinstance(conditions, TransitionCondition):
                                invalid_conditions.append(condition)
                        if len(invalid_conditions):
                            _trans_err('The `conditions` property must be a `TransitionCondition` instance or a list of them. '
                                       f'It is currently: {conditions}. See the README for more details.')

                if len(missing):
                    _trans_err(f'Missing required properties: {missing}.')

        if not ok:
            return None

        try:
            _: FiniteStateMachine = cls()
        except Exception as e:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'Unexpected error when instantiating EmulatorStateMachine: {str(e)}'
            ))
            return None

        return cls

    @staticmethod
    def _load_component_spec(
        ip_block_type: str,
        component_name: str,
        file_path: str
    ) -> ModuleSpec | None:
        """Try to load the Python module spec for the given component."""
        spec = importlib.util.spec_from_file_location(f"{ip_block_type}.emulator.{component_name}", file_path)
        return spec

    @staticmethod
    def _load_component_module(
        component_spec: ModuleSpec,
        component_name: str,
        partial_result: IPBlockEmulatorValidatorResult,
        result_key: str
    ) -> ModuleType | None:
        """Try to load and execute the Python module from the given spec."""
        try:
            module = importlib.util.module_from_spec(component_spec)
            component_spec.loader.exec_module(module)
        except FileNotFoundError:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'{component_name}.py could not be found at `{component_spec.origin}`. '
                'Check that the file exists and is named correctly.'
            ))
            return None
        except ModuleNotFoundError as mnfe:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'One or more imports in {component_name}.py could not be resolved: {str(mnfe)}. '
                'Ensure that relative imports are formatted correctly, '
                'and that the emulators have been placed on the python module path.'
            ))
            return None
        except Exception as e:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'{component_name}.py could not be loaded: {str(e)}'
            ))
            return None
        return module

    @staticmethod
    def _class_exists_in_module(
        component_module: ModuleType,
        component_name: str,
        class_name: str,
        partial_result: IPBlockEmulatorValidatorResult,
        result_key: str,
        base_class_name: str = ''
    ) -> bool:
        """Check if the given class name is found in the given Python module."""
        if not hasattr(component_module, class_name):
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'{component_name}.py must contain a class called `{class_name}`'
                f'{f" which extends the base class `{base_class_name}`" if len(base_class_name) else ""}. '
                f'The `{class_name}` class is currently missing or misnamed. See the README for an example.'
            ))
            return False
        return True

    @staticmethod
    def _is_correct_subclass(
        cls: object,
        base_class: object,
        partial_result: IPBlockEmulatorValidatorResult,
        result_key: str
    ) -> bool:
        """Check if the given class is a subclass of the given base class."""
        if cls.__bases__[0] != base_class:
            partial_result.insert_msg(result_key, ValidatorMessage(
                ValidatorStatus.ERROR,
                f'The `{cls.__name__}` class must extend the base class `{base_class.__name__}`. '
                f'It currently extends the class `{cls.__bases__[0].__name__}`.'
            ))
            return False
        return True
