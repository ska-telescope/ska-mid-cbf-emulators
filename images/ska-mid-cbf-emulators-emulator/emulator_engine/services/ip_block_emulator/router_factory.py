"""Factory class which creates internal router clients and implementations based on imported API specs."""

import json
import json.scanner
import traceback
from functools import wraps
from typing import Any, Callable, Self

from emulator_engine.services.api.router_client import RouterClient
from emulator_engine.services.api.subcontroller_router_impl import SubcontrollerRouterImpl
from emulator_engine.services.ip_block_emulator.request_validator import RequestValidator
from fastapi import Request, Response, status

from ska_mid_cbf_emulators.common import (
    ApiRouteMetadata,
    BaseEmulatorApi,
    EmulatorSubcontroller,
    InternalRestRequest,
    InternalRestResponse,
    ValidatorStatus,
)


class RouterFactory():
    """Factory class which creates internal router clients and implementations based on imported API specs."""

    @staticmethod
    def create(
        bitstream_emulator_id: str,
        subcontroller: EmulatorSubcontroller,
        api_def: BaseEmulatorApi
    ) -> tuple[RouterClient, SubcontrollerRouterImpl]:
        """Create a router client and implementation from a given subcontroller and API specification.

        Args:
            bitstream_emulator_id (:obj:`str`): The ID of the bitstream emulator this router belongs to.
            subcontroller (:obj:`EmulatorSubcontroller`): The subcontroller for which to generate this router.
            api_def (:obj:`BaseEmulatorApi`): The API specification to base this router on.

        Returns:
            :obj:`tuple[RouterClient, EmulatorSubcontrollerRouterImpl]` The generated router client and implementation.
        """
        router_client = RouterClient(bitstream_emulator_id, subcontroller.ip_block_id)
        router_impl = SubcontrollerRouterImpl(subcontroller)
        for fn_name in list(filter(lambda s: s is not None and (len(s) < 2 or s[0:2] != '__'), dir(api_def.__class__))):
            original_fn: Callable = getattr(api_def, fn_name)
            metadata: ApiRouteMetadata = getattr(original_fn, 'metadata', None)
            if metadata is not None:
                RouterFactory._apply_client(router_client, metadata, fn_name)
            RouterFactory._apply_impl(router_impl, original_fn, metadata, fn_name)

        return (router_client, router_impl)

    @staticmethod
    def _apply_client(router_client: RouterClient, metadata: ApiRouteMetadata, fn_name: str) -> None:
        """Inject a new client function called `fn_name` into the given base client."""
        async def client_fn(request: Request, response: Response) -> dict[str, Any]:
            path_params = request.path_params
            query_params = request.query_params._dict
            body = await request.body()

            try:
                body = json.loads(body)
            except Exception as e:
                if len(body):
                    intl_response = InternalRestResponse.bad_request(f'Request body is not valid JSON: {str(e)}.')
                    response.status_code = intl_response.status
                    return intl_response.body

            validator_result = RequestValidator.validate(path_params, query_params, body, metadata)

            if validator_result.overall_status == ValidatorStatus.ERROR:
                response.status_code = status.HTTP_400_BAD_REQUEST
                return {
                    'error_message': 'Request validation failed.',
                    'request_validation_result': validator_result.json_dict
                }

            rpc_response = router_client.get_rpc_response(InternalRestRequest(
                method_name=fn_name,
                http_method=metadata.http_method,
                kwargs=validator_result.combined_args
            ))
            response.status_code = rpc_response.status
            return {**rpc_response.body, 'request_validation_result': validator_result.json_dict}

        setattr(client_fn, '__name__', fn_name)
        setattr(router_client, fn_name, client_fn)
        route_head = f'/{metadata.route_name if metadata.route_name is not None else fn_name}'
        route_tail = f'/{{{metadata.path_param.name}}}' if metadata.path_param is not None else ''
        router_client.router.add_api_route(
            route_head + route_tail,
            getattr(router_client, fn_name),
            methods=[metadata.http_method]
        )

    @staticmethod
    def _apply_impl(
        router_impl: SubcontrollerRouterImpl,
        original_fn: Callable,
        metadata: ApiRouteMetadata | None,
        fn_name: str
    ) -> None:
        """Inject a wrapped copy of `original_fn`, called `fn_name`, into the given base implementation."""
        def impl_fn(*args, **kwargs) -> Any:
            if metadata is not None:
                try:
                    return original_fn(router_impl, *args, **kwargs)
                except Exception as e:
                    router_impl.log_error(traceback.format_exc())
                    return InternalRestResponse.internal_server_error(str(e))
            wrapped_fn = RouterFactory._internal(original_fn)
            return wrapped_fn(router_impl, *args, **kwargs)
        setattr(impl_fn, '__name__', fn_name)
        setattr(router_impl, fn_name, impl_fn)

    @staticmethod
    def _internal(fn: Callable) -> Callable:
        """Wrap `fn` to accept and ignore a provided `self` argument."""
        @wraps(fn)
        def inner(self: Self, *args, **kwargs):
            return fn(*args, **kwargs)
        return inner
