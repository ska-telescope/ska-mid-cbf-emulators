"""Emulator engine for the SKA Mid.CBF system."""

import os

RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', 'rabbitmq')

__all__ = ['RABBITMQ_HOST']
