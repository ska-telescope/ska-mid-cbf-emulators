from emulator_engine.models.controller_command import ControllerCommand

from ska_mid_cbf_emulators.common import EventSeverity

controller_command_event_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Emulator Controller Command Event",
    "description": "A command event sent to the emulator controller.",
    "type": "object",
    "properties": {
        "type": {
            "description": "Type of this event. Must be 'manual' (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(manual)$"
        },
        "subtype": {
            "description": "Subtype of this event. Must be 'general' (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(general)$"
        },
        "command": {
            "description": "Command type of this event. Must be a valid ControllerCommand value (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([c for c in ControllerCommand]) + r")$"
        },
        "value": {
            "description": "Value of this event. Can be anything as long as it is an object (i.e. an empty value is {}).",
            "type": "object"
        },
        "severity": {
            "description": "Severity of this event. Must be a valid EventSeverity value (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([s for s in EventSeverity]) + r")$"
        },
        "source_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was originally created.",
            "type": "number",
            "minimum": 0
        },
        "update_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was created or last updated.",
            "type": "number",
            "minimum": 0
        },
    },
    "required": [
        "command",
        "value",
        "severity",
        "source_timestamp",
    ]
}
