dish_signal_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "DISH Signal",
    "description": "A set of signal properties to propagate down from the DISH.",
    "type": "object",
    "properties": {
        "packet_rate": {
            "description": "Packet rate of the signal.",
            "type": "number",
            "minimum": 0
        },
        "sample_rate": {
            "description": "Sample rate of the signal.",
            "type": "integer",
            "minimum": 0
        },
        "dish_id": {
            "description": "ID of the DISH generating this signal.",
            "type": "string"
        },
        "band_id": {
            "description": "Band ID of the signal.",
            "type": "string",
            "enum": ["1", "2", "3", "4", "5a", "5b"]
        },
        "regular_sky_power": {
            "description": "Power of the signal for the regular sky.",
            "type": "array",
            "items": {
                "type": "number",
                "minimum": 0.0,
                "maximum": 1.0,
            }
        },
        "noise_diode_on_power": {
            "description": "Power of the signal when the noise diode is ON.",
            "type": "array",
            "items": {
                "type": "number",
                "minimum": 0.0,
                "maximum": 1.0,
            }
        },
        "duty_cycle": {
            "description": "Duty cycle of the signal.",
            "type": "number",
            "minimum": 0.0,
            "maximum": 1.0,
        },
    },
    "required": [
        "packet_rate",
        "sample_rate",
        "dish_id",
        "band_id",
        "regular_sky_power",
        "noise_diode_on_power",
        "duty_cycle",
    ]
}
