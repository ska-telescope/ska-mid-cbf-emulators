from emulator_engine.schemas.dish_signal import dish_signal_schema

dish_injection_schemas = {
    "update_signal_properties": {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "title": "\"Update DISH Signal Properties\" event value",
        "description": ("The value of an \"Update DISH Signal Properties\" event sent to the emulator from the injector. "
                        "It is used to update specific properties of the DISH Signal."),
        "type": "object",
        "properties": {
            "injection_type": {
                "const": "update_signal_properties"
            },
            "updated_properties": {
                "description": "A dictionary of property names and values to update.",
                "type": "object"
            }
        },
        "additionalProperties": True,
        "required": [
            "injection_type",
            "updated_properties"
        ]
    },
    "update_signal": {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        "title": "\"Update DISH Signal\" event value",
        "description": ("The value of an \"Update DISH Signal\" event sent to the emulator from the injector. "
                        "It is used to update the entire DISH Signal from a new JSON file/object."),
        "type": "object",
        "properties": {
            "injection_type": {
                "const": "update_signal"
            },
            "signal": dish_signal_schema
        },
        "additionalProperties": True,
        "required": [
            "injection_type",
            "signal",
        ]
    }
}
