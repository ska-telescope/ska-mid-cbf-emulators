"""Enum containing controller commands."""
from enum import StrEnum, auto


class ControllerCommand(StrEnum):
    """Enum containing controller commands.

    Implements :obj:`StrEnum`.
    """
    START = auto()
    STOP = auto()
    TERMINATE = auto()
