FROM python:3.12-slim

LABEL \
    author="Joey Arthur <Joey.Arthur@mda.space>" \
    description="Mid CBF Emulator."

ARG local_cert_path="/certs"
ARG img_cert_folder="own-certificates"

# Since the docker build context is at the root project
# level so the build can access the project dependencies
# (pyproject.toml, poetry.lock)
# we need to set the default directory to be used at
# container launch time to avoid having to access
# scripts through the directories each time
WORKDIR /app/images/ska-mid-cbf-emulators-emulator

RUN mkdir -p /app/images/ska-mid-cbf-emulators-emulator
USER root

ENV PATH /usr/local/bin:$PATH

ENV LANG C.UTF-8

COPY /images/ska-mid-cbf-emulators-emulator/pip.conf /etc/pip.conf

# ###########################################
# # Certificate setup
# ###########################################
RUN mkdir -p /tmp/tmp_certs /usr/local/share/ca-certificates/${img_cert_folder}
COPY ${local_cert_path} /tmp/tmp_certs
# recursively copy over all local certificate files (and ignore all other files)
RUN find /tmp/tmp_certs -type f \( -iname \*.cer -o -iname \*.crt -o -iname \*.pem \) -execdir cp {} /usr/local/share/ca-certificates/${img_cert_folder}/ \;
# update all cert file extensions to .crt
RUN (cd /usr/local/share/ca-certificates/${img_cert_folder} ; for f in *.cer; do mv -- "$f" "${f%.cer}.crt" 2>/dev/null || true ; done ; for f in *.pem; do mv -- "$f" "${f%.pem}.crt" 2>/dev/null || true ; done)
RUN rm -rf /tmp/tmp_certs

RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates netbase tzdata wget; rm -rf /var/lib/apt/lists/*
RUN update-ca-certificates

RUN apt-get update && apt-get -y install curl build-essential graphviz graphviz-dev jq
RUN pip install --upgrade pip

ENV REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt
ENV SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt

# # ############################################
# # # Poetry
# # ############################################
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python3.12 -
RUN ln -s /opt/poetry/bin/poetry /usr/local/bin/poetry
RUN ls -la ./

# Put the project dependency files where they can be found
COPY pyproject.toml poetry.lock* ./

# Get dependencies
ENV PIP_REQUESTS_TIMEOUT 30
ENV POETRY_REQUESTS_TIMEOUT 30
RUN poetry lock && poetry install --only emulator  --no-root

EXPOSE 5001

# move application files over
COPY ./images/ska-mid-cbf-emulators-emulator/emulator_engine /app/images/ska-mid-cbf-emulators-emulator/emulator_engine
COPY ./src/ska_mid_cbf_emulators /app/images/ska-mid-cbf-emulators-emulator/ska_mid_cbf_emulators
COPY ./images/ska-mid-cbf-emulators-emulator/app.py \
     ./images/ska-mid-cbf-emulators-emulator/default_config.json \
     ./images/ska-mid-cbf-emulators-emulator/initial_signal.json \
     ./images/ska-mid-cbf-emulators-emulator/.init_container \
     /app/images/ska-mid-cbf-emulators-emulator/

# start the app immediately when image is run
ENTRYPOINT ["poetry", "run", "python", "app.py"]