run-emulator() {
  export RE_CWD=$(pwd);
  (cd /ska-mid-cbf-emulators/images/ska-mid-cbf-emulators-emulator; poetry install; poetry run python app.py --cwd="$RE_CWD" "$@");
}

poetry install --no-root
eval $(poetry env activate)
(cd /ska-mid-cbf-emulators/images/ska-mid-cbf-emulators-emulator; poetry install)
cd /workspace