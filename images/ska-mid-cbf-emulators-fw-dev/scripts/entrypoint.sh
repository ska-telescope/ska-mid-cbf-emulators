#!/bin/bash

# # #################################################################################
# # Can change this user to be anything as its GID / UID will match your host systems
# # #################################################################################
addgroup --gid $2 fwdev 
adduser --uid $1 --gid $2 --disabled-password --gecos "" fwdev 
echo 'fwdev ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

git clone --depth=1 https://gitlab.com/ska-telescope/ska-mid-cbf-emulators /ska-mid-cbf-emulators
chown -R fwdev:fwdev /ska-mid-cbf-emulators/ /pyproject.toml /poetry.lock /tasks.py /etc/invoke.yaml

cat /opt/._bashrc >> /root/.bashrc
cat /opt/._bashrc >> /home/fwdev/.bashrc

su - fwdev <<EOF
# # ############################################
# # # Bitstream Emulator
# # ############################################
poetry install --no-root
cd /ska-mid-cbf-emulators && poetry install
cd /workspace
EOF

bash
