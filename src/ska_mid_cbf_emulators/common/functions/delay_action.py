from functools import wraps
from threading import Timer
from typing import Callable

from ska_mid_cbf_emulators.common.services.logging.logger_factory import LoggerFactory


def delay_action(delay_ms: int, action_fn: Callable, *fn_args, **fn_kwargs) -> Timer:
    """Delay an action asynchronously by the specified number of milliseconds.

    This function wraps the built-in :obj:`Timer` object
    and additionally logs an error message if an exception occurs inside `action_fn`.

    Args:
        delay_ms (:obj:`int`): The number of milliseconds to wait before calling `action_fn`.
        action_fn (:obj:`Callable`): The action function to run after the delay. Any return value is ignored.
        *fn_args: Arbitrary positional arguments to pass to `action_fn` when it is called.
        **fn_kwargs: Arbitrary keyword arguments to pass to `action_fn` when it is called.

    Returns:
        :obj:`Timer`: The running thread created to execute the action, \
            returned immediately after the thread is started. \
            This is made available in case manual joining or other manipulation is necessary.
    """
    logger = LoggerFactory.get_logger()

    @wraps(action_fn)
    def inner() -> None:
        try:
            action_fn(*fn_args, **fn_kwargs)
        except Exception as e:
            logger.error(str(e))

    t = Timer(delay_ms * 0.001, inner)
    t.start()
    return t
