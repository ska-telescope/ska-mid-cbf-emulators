"""Base message service interface."""

from abc import abstractmethod
from typing import Any, Self

from ska_mid_cbf_emulators.common.interfaces.event.base_event import BaseEvent
from ska_mid_cbf_emulators.common.interfaces.logging.logging_base import LoggingBase


class BaseMessageService(LoggingBase):
    """Base event interface used for all events in the system.

    Args:
        **kwargs: Arbitrary keyword arguments passed to the setup function.
    """

    def __init__(self: Self, *args, **kwargs) -> None:
        super().__init__()
        self.setup(*args, **kwargs)

    @abstractmethod
    def setup(self: Self, *args, **kwargs) -> None:
        """Set up the service.

        Args:
            **kwargs: Arbitrary keyword arguments
                which may be refined by individual subclass implementations.
        """

    @abstractmethod
    def subscribe(self: Self, **kwargs) -> Any:
        """Subscribe to events.

        Args:
            **kwargs: Arbitrary keyword arguments
                which may be refined by individual subclass implementations.
        """

    @abstractmethod
    def publish(self: Self, event: BaseEvent, **kwargs) -> Any:
        """Publish an event.

        Args:
            event (:obj:`BaseEvent`): The event to publish.
            **kwargs: Arbitrary keyword arguments
                which may be refined by individual subclass implementations.
        """

    def unsubscribe(self: Self, **kwargs) -> Any:
        """Unsubscribe from events.

        Args:
            **kwargs: Arbitrary keyword arguments
                which may be refined by individual subclass implementations.
        """
        return
