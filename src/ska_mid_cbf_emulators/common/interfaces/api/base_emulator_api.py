"""Base class and related models for IP block emulator API definitions."""

from enum import StrEnum
from functools import wraps
from inspect import Parameter, signature
from typing import Any, Callable, Self

from ska_mid_cbf_emulators.common.interfaces.logging.logging_base import LoggingBase
from ska_mid_cbf_emulators.common.models.api.param_models import ApiParam, ApiParamType
from ska_mid_cbf_emulators.common.services.subcontroller import emulator_subcontroller


class HttpMethod(StrEnum):
    """Enum containing standard HTTP methods."""
    GET = 'GET'
    POST = 'POST'
    PUT = 'PUT'
    DELETE = 'DELETE'


class ApiRouteMetadata():
    """Model for storing information about a route definition.

    Args:
        http_method (:obj:`HttpMethod`): The (only) HTTP method to allow for this route.
        path_param (:obj:`ApiParam[Any]`, optional): A path parameter (if any) expected by this route.\
            Default is `None`.
        query_params (:obj:`list[ApiParam[Any]]`, optional): A list of query parameter(s) (if any) expected by this route.\
            Default is `[]`.
        body_param (:obj:`ApiParam[Any]`, optional): A body parameter (if any) expected by this route.\
            Default is `None`.
        route_name (:obj:`str`, optional): A name to use for this route (i.e. what will be used in the URI).\
            Default is `None`.

    Attributes:
        http_method (:obj:`HttpMethod`): The (only) HTTP method allowed by this route.
        path_param (:obj:`ApiParam[Any]`): The path parameter (if any) expected by this route.
        query_params (:obj:`list[ApiParam[Any]]`): The query parameter(s) (if any) expected by this route.
        body_param (:obj:`ApiParam[Any]`): The body parameter (if any) expected by this route.
        route_name (:obj:`str`): The name of this route (i.e. what is used in the URI).
    """
    def __init__(
            self: Self,
            http_method: HttpMethod,
            path_param: ApiParam[Any] = None,
            query_params: list[ApiParam[Any]] = [],
            body_param: ApiParam[Any] = None,
            route_name: str = None
    ) -> None:
        self.http_method = http_method
        self.path_param = path_param
        self.query_params = query_params
        self.body_param = body_param
        self.route_name = route_name


class BaseEmulatorApi(LoggingBase):
    """Base class for IP block emulator API definitions.

    Args:
        subcontroller (:obj:`EmulatorSubcontroller`): The subcontroller to associate with this IP block emulator.

    Attributes:
        subcontroller (:obj:`EmulatorSubcontroller`): The subcontroller associated with this IP block emulator.
        ip_block (:obj:`EmulatorIPBlock`): The simulated IP block component used by this emulator.
    """
    def __init__(self: Self, subcontroller: emulator_subcontroller.EmulatorSubcontroller) -> None:
        super().__init__(log_prefix=f'{subcontroller.display_name} API', extra_stacklevel=1)
        self.subcontroller = subcontroller
        self.ip_block = subcontroller.ip_block

    @staticmethod
    def route(
            http_method: HttpMethod = HttpMethod.GET
    ) -> Callable:
        """Decorator which adds context metadata to API call implementations for emulator use.

        It takes the HTTP method passed to the decorator, and uses the special
        `PathParam[T]`, `QueryParam[T]`, `BodyParam[T]` type annotations
        to determine internally which parameters to the decorated function
        are path parameters, query parameters, and the request body, respectively.

        Example:
            For a route e.g. `host:8000/some_api/insert_data/some_target?index=3`,
            to which one sends a body like `{"the_value_is": 314159265}` via POST request,
            the code might look like:
            ```
            @BaseEmulatorApi.route(http_method=HttpMethod.POST)
            def insert_data(
                target_key: PathParam[str],
                index: QueryParam[int],
                data: BodyParam[dict]
            ) -> InternalRestResponse:
                self.targets[target_key][index] = data
                return InternalRestResponse.ok()
            ```

        Args:
            http_method (:obj:`HttpMethod`): The HTTP method for this API call.

        Returns:
            :obj:`Callable` The updated method.
        """
        def decorator(fn: Callable) -> Callable:
            params = signature(fn).parameters
            path_param: ApiParam = None
            query_params: list[ApiParam] = []
            body_param: ApiParam = None
            for param_name in params.keys():
                if param_name == 'self':
                    continue
                try:
                    param: Parameter = params[param_name]
                    api_param: ApiParam = ApiParam(param_name, param.annotation.__args__[0], param.default)
                    param_type: ApiParamType = param.annotation.__value__.__metadata__[0]
                    match param_type:
                        case ApiParamType.PATH:
                            path_param = api_param
                        case ApiParamType.QUERY:
                            query_params.append(api_param)
                        case ApiParamType.BODY:
                            body_param = api_param
                        case _:
                            continue

                except Exception:
                    continue

            @wraps(fn)
            def inner(self: Self, *args, **kwargs):
                return fn(*args, **kwargs)
            setattr(inner, 'metadata', ApiRouteMetadata(
                http_method,
                path_param,
                query_params,
                body_param,
                route_name=inner.__name__
            ))
            return inner
        return decorator
