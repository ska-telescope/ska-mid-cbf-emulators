"""Base class with logger wrapper methods."""
from abc import ABC
from typing import Self

from ska_mid_cbf_emulators.common.services.logging.logger_factory import EmulatorLogger, LoggerFactory


class LoggingBase(ABC):
    """Base class with logger wrapper methods."""
    base_stacklevel = 2

    def __init__(
        self: Self,
        log_prefix: str = None,
        logger: EmulatorLogger = None,
        extra_stacklevel: int = 0,
        *args,
        **kwargs
    ) -> None:
        self._log_prefix = log_prefix
        self._logger: EmulatorLogger = logger if logger is not None else LoggerFactory.get_logger()
        self._stacklevel = self.base_stacklevel + extra_stacklevel

    def log_critical(self: Self, msg: str) -> str:
        """Log a critical error message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.critical(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def log_error(self: Self, msg: str) -> str:
        """Log an error message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.error(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def log_warning(self: Self, msg: str) -> str:
        """Log a warning message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.warning(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def log_info(self: Self, msg: str) -> str:
        """Log an info message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.info(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def log_debug(self: Self, msg: str) -> str:
        """Log a debug message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.debug(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def log_trace(self: Self, msg: str) -> str:
        """Log a trace message using the class logger.
        Prepends the log prefix if it exists."""
        if self._logger is not None:
            self._logger.trace(self._msg_with_prefix(msg), stacklevel=self._stacklevel)

    def _msg_with_prefix(self: Self, msg: str) -> str:
        """Prepend the log prefix to a log message if it exists."""
        return msg if self._log_prefix is None else f'{self._log_prefix} | {msg}'
