"""Base classes for validators."""

import json
from abc import ABC, abstractmethod
from enum import IntEnum
from typing import Any, Self


class ValidationError(Exception):
    pass


class ValidatorStatus(IntEnum):
    """Status of a validation."""

    OK = 0
    """Validation passed with no concerns."""

    WARNING = 1
    """Validation passed, but there are message(s) that may be of concern."""

    ERROR = 2
    """Validation failed."""


class ValidatorMessage():
    """Message object for a single validation.

    Args:
        status (:obj:`ValidatorStatus`): The status to provide for this validation.
        message (:obj:`str`): The message to provide for this validation.

    Attributes:
        status (:obj:`ValidatorStatus`): The status of this validation.
        message (:obj:`str`): The message for this validation.
    """
    def __init__(self: Self, status: ValidatorStatus, message: str | dict) -> None:
        self.status = status
        self.message = message

    def __repr__(self: Self) -> str:
        return json.dumps(self.json_dict, indent=4)

    @property
    def json_dict(self: Self) -> dict[str, Any]:
        """A JSON-serializable dictionary representing this object."""
        return {
            'status': self.status.name,
            'message': self.message
        }


class BaseValidatorResult(ABC):
    """Base message object class for a single validation.

    Args:
        initial_status (:obj:`ValidatorStatus`, optional): The initial status to assume for this validation result.\
            Default is `ValidatorStatus.OK`.

    Attributes:
        overall_status (:obj:`ValidatorStatus`): The overall status of this validation result.
        result (:obj:`dict[str, ValidatorMessage] | dict[str, dict[str, ValidatorMessage] | list[ValidatorMessage]]`):\
            A dictionary containing all messages generated from this validation.
    """

    def __init__(self: Self, initial_status: ValidatorStatus = ValidatorStatus.OK) -> None:
        self.overall_status: ValidatorStatus = initial_status
        self.result: dict[str, ValidatorMessage] | dict[str, dict[str, ValidatorMessage] | list[ValidatorMessage]] = {}

    def __repr__(self: Self) -> str:
        return json.dumps(self.json_dict, indent=4)

    @property
    @abstractmethod
    def json_dict(self: Self) -> dict[str, Any]:
        """A JSON-serializable dictionary representing this object."""

    @abstractmethod
    def insert_msg(self: Self, key: str, msg: ValidatorMessage, *args, **kwargs) -> None:
        """Insert a message into the result."""


class BaseValidator(ABC):
    """Base class for validators."""
    @staticmethod
    @abstractmethod
    def validate(*args, **kwargs) -> BaseValidatorResult:
        """Perform a validation.

        Returns:
            :obj:`BaseValidatorResult` The result of the validation.
        """
