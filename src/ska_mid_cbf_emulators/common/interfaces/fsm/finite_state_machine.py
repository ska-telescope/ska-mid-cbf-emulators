"""Base state machine interfaces."""

from abc import ABC, abstractmethod
from enum import StrEnum
from io import BytesIO
from typing import Any, Callable, Self, override

from transitions.extensions import LockedGraphMachine


class BaseState(StrEnum):
    """Base enum over which subclasses will define their states."""


class RoutingState(BaseState):
    """States with special routing functionality used only for transition definitions.

    Implements :obj:`BaseState`.
    """

    FROM_ANY = '*'
    """Transition from any defined state. Can only be used as a source state."""

    TO_SAME = '='
    """Reflexive transition / transition to self (e.g. State.X -> State.X). Can only be used as a destination state."""


class BaseTransitionTrigger(StrEnum):
    """Base enum over which subclasses will define their transition names."""
    pass


class TransitionCondition():
    """Wrapper class for transition conditional functions
    to provide display names which can be used in diagrams.

    Args:
        display_name (:obj:`str`): Display name that represents the True state of this condition.
            For example, if the condition is `return (val > 0)`, a display name may be `'Value > 0'`.
        fn (:obj:`Callable[[Any], bool]`): The condition function to execute. Must return a boolean value.
    """
    def __init__(self: Self, display_name: str, fn: Callable[[Any], bool]) -> None:
        self.display_name = display_name
        self.fn = fn

    def __call__(self: Self, *args, **kwargs):
        return self.fn(*args, **kwargs)


class FiniteStateMachine(ABC):
    """Abstract base class used by all state machines.

    All subclasses/implementations must override the `_states`, `_initial_state`, and `_transitions` properties.
    """

    class StrEnumLockedGraphMachine(LockedGraphMachine):
        """Extension of the LockedGraphMachine class with support for StrEnum states and transitions.

        A state or trigger enum member is represented internally as its value.

        For example:

            `MyState.TEST = "custom_test"` becomes the state `"custom_test"`

            `MyTransitionTrigger.DO_THING = "do_a_thing"` becomes the trigger `"do_a_thing"`
        """
        @override
        @staticmethod
        def format_references(func):
            """ Creates a string representation of referenced callbacks.
            Returns:
                str that represents a callback reference.
            """
            if isinstance(func, TransitionCondition):
                return func.display_name
            return super(LockedGraphMachine, LockedGraphMachine).format_references(func)

        @override
        def add_states(self: Self, states: list[BaseState], *args, **kwargs):
            super().add_states([state.value for state in states], *args, **kwargs)

        @override
        def add_transition(self: Self, trigger: BaseTransitionTrigger | str, *args, **kwargs):
            super().add_transition(trigger.value if hasattr(trigger, 'value') else trigger, *args, **kwargs)

    def __init__(self: Self) -> None:
        self._machine = self.StrEnumLockedGraphMachine(
            model=self,
            states=self._states,
            transitions=self._transitions,
            initial=self._initial_state,
            show_conditions=True
        )

    def export_diagram_full(self: Self, file: str | BytesIO, title: str = None, format: str | None = 'png') -> None:
        """Export the full state machine context to a state-transition diagram image.

        The context includes all states and transitions in the entire machine model,
        highlighting both the current state and the previous state (i.e. the most recent transition).

        Args:
            file (:obj:`str | BytesIO`): Either a filename (e.g. `'diagram.png'`),
                or a `BytesIO` stream, to export the image to.
            title (:obj:`str`, optional): The title of the diagram to display at the bottom of the image.
                Default is `None`.
            format (:obj:`str | None`, optional): The format of the image export.
                Default is `'png'`.
        """
        self._machine.get_combined_graph(
            title=title
        ).draw(
            file,
            prog='dot',
            format=format
        )

    def export_diagram_current_context(self: Self, file: str | BytesIO, title: str = None, format: str | None = 'png') -> None:
        """Export the current state context to a state-transition diagram image.

        The state context includes the current state, the previous state,
        and all other states to which the current state may transition.

        Args:
            file (:obj:`str | BytesIO`): Either a filename (e.g. `'diagram.png'`),
                or a `BytesIO` stream, to export the image to.
            title (:obj:`str`, optional): The title of the diagram to display at the bottom of the image.
                Default is `None`.
            format (:obj:`str | None`, optional): The format of the image export.
                Default is `'png'`.
        """
        self._machine.get_combined_graph(
            show_roi=True,
            title=title
        ).draw(
            file,
            prog='dot',
            format=format
        )

    @property
    @abstractmethod
    def _states(self: Self) -> list[BaseState]:
        """:obj:`list[BaseState]` The list of possible states in which this state machine can exist."""

    @property
    @abstractmethod
    def _initial_state(self: Self) -> BaseState:
        """:obj:`BaseState` The initial state of this state machine."""

    @property
    @abstractmethod
    def _transitions(self: Self) -> list[dict[str, Any]]:
        """:obj:`list[dict[str, Any]]` A list of possible transitions between states for this state machine.

        Each transition should be a dict of the form:
            ```
            {
                'source': [MyState.SOURCE_STATE],
                'dest': MyState.DESTINATION_STATE,
                'trigger': MyTransitionTrigger.TRIGGER,
                'conditions': [
                    TransitionCondition(
                        'Positive',
                        lambda num: num > 0
                    )
                ]
            }
            ```
        in which:

        * `source` (:obj:`BaseState | list[BaseState]`) is the state (or list of states) to transition from.
        * `dest` (:obj:`BaseState`) is the state to transition to.
        * `trigger` (:obj:`BaseTransitionTrigger`) is the trigger name that will execute this transition.
        * `conditions` (:obj:`Callable[[Any], bool] | list[Callable[[Any], bool]] | None`) is an optional list
            of conditions that will be checked at transition execution.
            Any included arguments should be passed to the trigger function when called (e.g.
            `trigger(MyTransitionTrigger.TRIGGER, True)` or
            `trigger(MyTransitionTrigger.TRIGGER, my_arg=False)`)

        For more details, see the `pytransitions` library documentation.
        """
