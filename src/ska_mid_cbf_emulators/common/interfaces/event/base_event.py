"""Base event interface."""

from __future__ import annotations

import copy
import json
import time
from abc import ABC, abstractmethod
from enum import StrEnum
from typing import Any, Self

from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity
from ska_mid_cbf_emulators.common.models.event.event_type import EventType


class BaseEventSubType(StrEnum):
    """Base enum over which subclasses may define event subtypes."""
    pass


class BaseEvent(ABC):
    """Base event interface used for all events in the system.

    Args:
        type (:obj:`EventType`, optional): The primary type of this event (i.e. "PULSE", "MANUAL", \
            or "SIGNAL_UPDATE"). Defaults to `None`.
        subtype (:obj:`BaseEventSubType`, optional): The subtype of this event. Defaults to `None`.
        value (:obj:`dict[Any, Any]`, optional): The value of this event,\
            which can be any arbitrary dictionary. Default is `{}`.
        severity (:obj:`EventSeverity`, optional): The severity of this event.\
            Defaults to :obj:`EventSeverity.GENERAL`.
        source_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was originally created. \
            Defaults to the current system time.
        update_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was created or last updated. \
            Defaults to the value of source_timestamp.

    Attributes:
        type (:obj:`EventType`): The primary type of the event.
        subtype (:obj:`BaseEventSubType | None`): The subtype of the event, if available.
        value (:obj:`dict[Any, Any]`): The value of the event.
        severity (:obj:`EventSeverity`): The severity of the event.
        source_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was originally created.
        update_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was created or last updated. \
            For newly created events, update_timestamp == source_timestamp.
    """

    def __init__(
        self: Self,
        type: EventType = None,
        subtype: BaseEventSubType | None = None,
        value: dict[Any, Any] = {},
        severity: EventSeverity = EventSeverity.GENERAL,
        source_timestamp: int = None,
        update_timestamp: int = None
    ) -> None:
        self._type = type
        self._subtype = subtype
        self._value = copy.deepcopy(value)
        self._severity = severity
        self._source_timestamp = source_timestamp if source_timestamp is not None else round(time.time() * 1000)
        self._update_timestamp = update_timestamp if update_timestamp is not None else self._source_timestamp

    def __repr__(self: Self) -> str:
        return (f'<{self._type} event with severity {self._severity}, '
                f'source timestamp {self._source_timestamp}, update timestamp {self._update_timestamp}, value {self._value}>')

    @staticmethod
    @abstractmethod
    def decode(json_event_str: str) -> BaseEvent:
        """Decode a JSON-encoded event.

        Args:
            json_event_str (:obj:`str`): The JSON string containing the event to decode

        Returns:
            :obj:`BaseEvent`: The decoded event
        """

    @staticmethod
    def encode(event: BaseEvent) -> str:
        """Encode an event to a JSON string.

        Args:
            event (:obj:`BaseEvent`): The event to encode

        Returns:
            :obj:`str`: The JSON-encoded event string
        """
        e_dict = {
            'type': event.type,
            'subtype': event.subtype,
            'severity': event.severity,
            'source_timestamp': event.source_timestamp,
            'update_timestamp': event.update_timestamp,
            'value': event.value
        }
        return json.dumps(e_dict)

    @property
    def type(self: Self) -> EventType:
        """:obj:`EventType`: The primary type of the event."""
        return self._type

    @type.setter
    def type(self: Self, new_type: EventType) -> None:
        self._type = new_type

    @property
    def subtype(self: Self) -> BaseEventSubType | None:
        """:obj:`BaseEventSubType | None`: The subtype of the event, if available."""
        return self._subtype

    @subtype.setter
    def subtype(self: Self, new_subtype: BaseEventSubType | None) -> None:
        self._subtype = new_subtype

    @property
    def value(self: Self) -> dict[Any, Any]:
        """:obj:`dict[Any, Any]`: The value of the event."""
        return self._value

    @value.setter
    def value(self: Self, new_value: dict[Any, Any]) -> None:
        self._value = new_value

    @property
    def severity(self: Self) -> EventSeverity:
        """:obj:`EventSeverity`: The severity of the event."""
        return self._severity

    @severity.setter
    def severity(self: Self, new_severity: EventSeverity) -> None:
        self._severity = new_severity

    @property
    def source_timestamp(self: Self) -> int:
        """:obj:`int`: The timestamp, in milliseconds, for when the event was originally created."""
        return self._source_timestamp

    @source_timestamp.setter
    def source_timestamp(self: Self, new_source_timestamp: int) -> None:
        self._source_timestamp = new_source_timestamp

    @property
    def update_timestamp(self: Self) -> int:
        """:obj:`int`: The timestamp, in milliseconds, for when the event was created or last updated.
        For newly created events, update_timestamp == source_timestamp.
        """
        return self._update_timestamp

    @update_timestamp.setter
    def update_timestamp(self: Self, new_update_timestamp: int) -> None:
        self._update_timestamp = new_update_timestamp
