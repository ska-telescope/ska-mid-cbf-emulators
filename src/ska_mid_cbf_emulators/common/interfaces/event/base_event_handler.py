"""Base class and related models for IP block emulator API definitions."""

from typing import Self, override

from ska_mid_cbf_emulators.common.interfaces.logging.logging_base import LoggingBase
from ska_mid_cbf_emulators.common.models.event.manual_event import ManualEvent
from ska_mid_cbf_emulators.common.models.event.pulse_event import PulseEvent
from ska_mid_cbf_emulators.common.models.event.signal_update_event_list import SignalUpdateEventList
from ska_mid_cbf_emulators.common.services.subcontroller import emulator_subcontroller


class BaseEventHandler(LoggingBase):
    """Base class for IP block emulator event handler definitions.

    Args:
        subcontroller (:obj:`EmulatorSubcontroller`): The subcontroller to associate with this IP block emulator.

    Attributes:
        subcontroller (:obj:`EmulatorSubcontroller`): The subcontroller associated with this IP block emulator.
        ip_block (:obj:`EmulatorIPBlock`): The simulated IP block component used by this emulator.
    """
    def __init__(self: Self, subcontroller: emulator_subcontroller.EmulatorSubcontroller) -> None:
        super().__init__(log_prefix=f'{subcontroller.display_name} Event Handler', extra_stacklevel=1)
        self.subcontroller = subcontroller
        self.ip_block = subcontroller.ip_block

    def handle_pulse_event(self: Self, event: PulseEvent, **kwargs) -> None:
        """Handle an incoming pulse event.

        Args:
            event (:obj:`PulseEvent`): The event to handle.
            **kwargs: Arbitrary keyword arguments.
        """
        self.log_trace(f'{self.subcontroller.display_name} pulse event handler not implemented. (This is probably expected.)')
        return

    def handle_signal_update_events(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
        """Handle an incoming Signal Update event list.

        Args:
            event_list (:obj:`SignalUpdateEventList`): The signal update event list to handle.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            :obj:`SignalUpdateEventList` The signal update event list to send to the next block.
        """
        self.log_trace(f'{self.subcontroller.display_name} signal update event handler not implemented. '
                       '(This may or may not be expected.)')
        return event_list

    def handle_manual_event(self: Self, event: ManualEvent, **kwargs) -> None | list[ManualEvent]:
        """Handle an incoming manual event.

        Args:
            event (:obj:`ManualEvent`): The manual event to handle.
            **kwargs: Arbitrary keyword arguments.

        Returns:
            :obj:`None | list[ManualEvent]` Optionally, a list of one or more new manual events \
                to automatically forward downstream.
        """
        self.log_trace(f'{self.subcontroller.display_name} manual event handler not implemented. (Something is probably wrong.)')
        return


class UnimplementedEventHandler(BaseEventHandler):
    """Class for an unimplemented IP block emulator event handler."""
    def __init__(self: Self, *args, **kwargs) -> None:
        super(BaseEventHandler, self).__init__(extra_stacklevel=1)

    @override
    def handle_pulse_event(self: Self, event: PulseEvent, **kwargs) -> None:
        self.log_debug('UnimplementedEventHandler was called. Check that your event handlers are correctly defined.')
        return

    @override
    def handle_signal_update_events(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
        self.log_debug('UnimplementedEventHandler was called. Check that your event handlers are correctly defined.')
        return event_list

    @override
    def handle_manual_event(self: Self, event: ManualEvent, **kwargs) -> None | list[ManualEvent]:
        self.log_debug('UnimplementedEventHandler was called. Check that your event handlers are correctly defined.')
        return
