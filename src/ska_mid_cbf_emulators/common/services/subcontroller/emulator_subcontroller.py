"""Subcontroller for an IP block emulator."""

from __future__ import annotations

from io import BytesIO
from typing import Any, Iterable, Self

from ska_mid_cbf_emulators.common.interfaces.event.base_event import BaseEvent
from ska_mid_cbf_emulators.common.interfaces.fsm.finite_state_machine import BaseState, BaseTransitionTrigger, FiniteStateMachine
from ska_mid_cbf_emulators.common.interfaces.logging.logging_base import LoggingBase
from ska_mid_cbf_emulators.common.interfaces.messaging.base_message_service import BaseMessageService
from ska_mid_cbf_emulators.common.models.config.emulator_config_ip_block import EmulatorConfigIPBlock
from ska_mid_cbf_emulators.common.models.event.manual_event import ManualEvent
from ska_mid_cbf_emulators.common.services.logging.logger_factory import EmulatorLogger, LoggerFactory


class EmulatorSubcontroller(LoggingBase):
    """Subcontroller for an IP block emulator.

    Args:
        event_service (:obj:`BaseMessageService`): instance of a message service\
            to use for communication between IP block emulators.
        api_service (:obj:`BaseMessageService`): instance of a message service\
            to use for API calls.
        state_machine (:obj:`FiniteStateMachine`): A state machine to use for the IP block emulator.\
            Default is `None`.
        ip_block_config (:obj:`EmulatorConfigIPBlock`, optional): If specified, an object\
            from which the `id`, `display_name`, `type`, `downstream_block_ids` and `state_machine`\
            fields will be pulled (and their respective arguments ignored). Default is `None`.
        ip_block_id (:obj:`str`, optional): The unique ID of the IP block to be associated with this subcontroller.\
            Default is `'unknown'`.
        display_name (:obj:`str`, optional): A (not necessarily unique) human-readable\
            name for the IP block emulator used for user-facing display purposes.\
            Default is `'Unknown IP'`.
        type (:obj:`str`, optional): The type of IP Block this IP block emulator wraps.\
            Default is `None`.
        downstream_block_ids (:obj:`list[str]`, optional): A list of IDs of downstream IP block emulators\
            which this IP block emulator is directly connected to. Default is an empty list.
        logger (:obj:`EmulatorLogger`, optional): A logger instance to use for this subcontroller. \
            Default is a new unnamed logger.

    Attributes:
        ip_block_id (:obj:`str`): The unique ID of the IP block associated with this subcontroller.
        display_name (:obj:`str`): The human-readable name for the IP block emulator\
            used for user-facing display purposes.
        type (:obj:`str`): The type of IP Block this IP block emulator wraps.
        downstream_block_ids (:obj:`list[str]`): A list of the IDs of downstream IP block emulators\
            which this IP block emulator is directly connected to.
        ip_interface (:obj:`Any`): The interface/API of the emulator's simulated IP block.
        event_service (:obj:`BaseMessageService`): The message service\
            used for communication between IP block emulators.
        api_service (:obj:`BaseMessageService`): The message service\
            used for API calls.
        state_machine (:obj:`FiniteStateMachine`): The IP block emulator's state machine.
    """

    def __init__(
            self: Self,
            bitstream_emulator_id: str,
            event_service: BaseMessageService,
            api_service: BaseMessageService,
            state_machine: FiniteStateMachine | None = None,
            ip_block_config: EmulatorConfigIPBlock | None = None,
            ip_block_id: str = 'unknown',
            display_name: str = 'Unknown IP',
            type: str | None = None,
            downstream_block_ids: list[str] = [],
            logger: EmulatorLogger | None = None,
            **kwargs
    ) -> None:
        super().__init__(logger=(LoggerFactory.get_logger() if logger is None else logger))

        self.bitstream_emulator_id = bitstream_emulator_id

        if ip_block_config is not None:
            self._ip_block_id: str = ip_block_config.id
            self._display_name: str = ip_block_config.display_name
            self._type: str = ip_block_config.type
            self._downstream_block_ids = list(ip_block_config.downstream_block_ids)
        else:
            self._ip_block_id: str = ip_block_id
            self._display_name: str = display_name
            self._type: str = type
            self._downstream_block_ids = downstream_block_ids

        self._logger_name = self._display_name

        self._ip_block: Any = None
        self._event_service = event_service
        self._api_service = api_service
        self._state_machine: FiniteStateMachine = state_machine

        self.subcontroller_init(**kwargs)

    def __repr__(self: Self) -> str:
        return f'<Subcontroller for IP block "{self._display_name}" with ID {self._ip_block_id} of type {self._type}>'

    @property
    def ip_block_id(self: Self) -> str:
        """:obj:`str`: The unique ID of the IP block associated with this subcontroller."""
        return self._ip_block_id

    @ip_block_id.setter
    def ip_block_id(self: Self, new_ip_block_id: str) -> None:
        self._ip_block_id = new_ip_block_id

    @property
    def display_name(self: Self) -> str:
        """:obj:`str`: A (not necessarily unique) name for the IP block emulator
            used for user-facing display purposes.
        """
        return self._display_name

    @display_name.setter
    def display_name(self: Self, new_display_name: str) -> None:
        self._display_name = new_display_name
        self._logger_name = new_display_name

    @property
    def type(self: Self) -> str:
        """:obj:`str`: The type of IP Block this IP block emulator wraps."""
        return self._type

    @type.setter
    def type(self: Self, new_type: str) -> None:
        self._type = new_type

    @property
    def downstream_block_ids(self: Self) -> list[str]:
        """:obj:`list[str]`: A list of IDs of downstream IP block emulators
            which this IP block emulator is directly connected to.
        """
        return self._downstream_block_ids

    @downstream_block_ids.setter
    def downstream_block_ids(self: Self, new_downstream_block_ids: Iterable[str]) -> None:
        self._downstream_block_ids = list(new_downstream_block_ids)

    @property
    def ip_block(self: Self) -> Any:
        """:obj:`Any`: The interface to the emulator's simulated IP block."""
        return self._ip_block

    @ip_block.setter
    def ip_block(self: Self, new_ip_block: Any) -> None:
        self._ip_block = new_ip_block

    @property
    def event_service(self: Self) -> BaseMessageService:
        """:obj:`BaseMessageService`: The message service
            used for communication between IP block emulators."""
        return self._event_service

    @event_service.setter
    def event_service(self: Self, new_event_service: BaseMessageService) -> None:
        self._event_service = new_event_service

    @property
    def api_service(self: Self) -> BaseMessageService:
        """:obj:`BaseMessageService`: The message service
            used for API calls."""
        return self._api_service

    @api_service.setter
    def api_service(self: Self, new_api_service: BaseMessageService) -> None:
        self._api_service = new_api_service

    @property
    def state_machine(self: Self) -> FiniteStateMachine | None:
        """:obj:`FiniteStateMachine`: The IP block emulator's state machine."""
        return self._state_machine

    @state_machine.setter
    def state_machine(self: Self, new_state_machine: FiniteStateMachine) -> None:
        self._state_machine = new_state_machine

    def subcontroller_init(self: Self) -> None:
        """Initialize subcontroller-specific properties."""
        return

    def may_trigger(self: Self, trigger_name: BaseTransitionTrigger, *args, **kwargs) -> bool:
        """Return whether it is currently possible to execute the given trigger.

        Args:
            trigger_name (:obj:`BaseTransitionTrigger`): The state transition trigger to check.
            *args: Arbitrary positional arguments to be passed down to the state machine's trigger method(s).
            **kwargs: Arbitrary keyword arguments to be passed down to the state machine's trigger method(s).
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return False
        try:
            may_fn = getattr(self.state_machine, f'may_{trigger_name}')
            return may_fn(*args, **kwargs)
        except Exception as e:
            self.log_error(f'Trigger {trigger_name} could not be checked: {str(e)}.')
            return False

    def trigger(self: Self, trigger_name: BaseTransitionTrigger, *args, **kwargs) -> None:
        """Trigger a state transition by name.

        Will throw a MachineError if the transition is not possible;
        use `trigger_if_allowed` to run a check first and avoid the error.

        Args:
            trigger_name (:obj:`BaseTransitionTrigger`): The state transition trigger to execute.
            *args: Arbitrary positional arguments to be passed down to the state machine's trigger method(s).
            **kwargs: Arbitrary keyword arguments to be passed down to the state machine's trigger method(s).
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return
        try:
            curr = self.get_state()
            self.state_machine.trigger(trigger_name, *args, **kwargs)
            self.log_debug(f'{self.display_name} | Triggered {trigger_name}; new state is {self.get_state()}.')
            if curr != self.get_state():
                self.log_info(f'{self.display_name} | State changed from {curr} to {self.get_state()}.')
        except Exception as e:
            self.log_error(e)
            raise e

    def trigger_if_allowed(self: Self, trigger_name: BaseTransitionTrigger, *args, **kwargs) -> bool:
        """Trigger a state transition by name only if it is allowed from the current state.

        Args:
            trigger_name (:obj:`BaseTransitionTrigger`): The state transition trigger to execute.
            *args: Arbitrary positional arguments to be passed down to the state machine's trigger method(s).
            **kwargs: Arbitrary keyword arguments to be passed down to the state machine's trigger method(s).

        Returns:
            :obj:`bool` `True` if the transition was successfully triggered/executed; `False` otherwise.
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return False
        try:
            if self.may_trigger(trigger_name, *args, **kwargs):
                curr = self.get_state()
                self.state_machine.trigger(trigger_name, *args, **kwargs)
                self.log_debug(f'{self.display_name} | Triggered {trigger_name}; new state is {self.get_state()}.')
                if curr != self.get_state():
                    self.log_info(f'{self.display_name} | State changed from {curr} to {self.get_state()}.')
                return True
            else:
                self.log_debug(f'{self.display_name} | Not allowed to trigger {trigger_name} from state {self.get_state()}.')
                return False
        except Exception as e:
            self.log_error(f'An unexpected error has occurred: {e}')
            return False

    def get_state(self: Self) -> BaseState:
        """Return the current state of the IP block emulator's state machine.

        Returns:
            :obj:`BaseState` The current state.
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return None
        return self.state_machine.state

    def force_state(self: Self, state: BaseState) -> None:
        """Force a transition to a specific state regardless of defined transitions.

        This should probably never be used, but it is available just in case.

        Args:
            state (:obj:`BaseState`): The state to force a transition to.
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return
        try:
            curr = self.get_state()
            self.state_machine.trigger(f'to_{state}')
            self.log_debug(f'{self.display_name} | Forced state to {self.get_state()}.')
            if curr != self.get_state():
                self.log_info(f'{self.display_name} | State changed from {curr} to {self.get_state()}.')
        except Exception as e:
            self.log_error(e)

    def export_state_diagram_full(self: Self, file: str | BytesIO, title: str = '', format: str = 'png') -> None:
        """Export the full state machine context to a state-transition diagram image.

        The context includes all states and transitions in the entire machine model,
        highlighting both the current state and the previous state (i.e. the most recent transition).

        Args:
            file (:obj:`str | BytesIO`): Either a filename (e.g. `'diagram.png'`),
                or a `BytesIO` stream, to export the image to.
            title (:obj:`str`, optional): A custom title to display at the bottom of the image, if desired.
                Defaults to `"<IP Block Emulator Display Name> - Full State Machine Context"`.
                To export with no title, set this explicitly to `None`.
            format (:obj:`str | None`, optional): The format of the image export.
                Default is `'png'`.
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return
        self.state_machine.export_diagram_full(
            file,
            title=(None if title is None else title or f'{self.display_name} - Full State Machine Context'),
            format=format
        )

    def export_state_diagram_current_context(self: Self, file: str | BytesIO, title: str = '', format: str = 'png') -> None:
        """Export the current state context to a state-transition diagram image.

        The state context includes the current state, the previous state,
        and all other states to which the current state may transition.

        Args:
            file (:obj:`str | BytesIO`): Either a filename (e.g. `'diagram.png'`),
                or a `BytesIO` stream, to export the image to.
            title (:obj:`str`, optional): A custom title to display at the bottom of the image, if desired.
                Defaults to `"<IP Block Emulator Display Name> - Current State Context"`.
                To export with no title, set this explicitly to `None`.
            format (:obj:`str | None`, optional): The format of the image export.
                Default is `'png'`.
        """
        if self.state_machine is None:
            self.log_error(f'IP block emulator {self.display_name} does not have a state machine.')
            return
        self.state_machine.export_diagram_current_context(
            file,
            title=(None if title is None else title or f'{self.display_name} - Current State Context'),
            format=format
        )

    def subscribe(self: Self, **kwargs) -> Any:
        """Subscribe to events and API calls.

        Args:
            **kwargs: Arbitrary keyword arguments.
        """
        self.event_service.subscribe(**kwargs)
        self.api_service.subscribe(**kwargs)

    def unsubscribe(self: Self, **kwargs) -> Any:
        """Unsubscribe from events and API calls.

        Args:
            **kwargs: Arbitrary keyword arguments.
        """
        self.event_service.unsubscribe(**kwargs)
        self.api_service.unsubscribe(**kwargs)

    def publish(self: Self, event: BaseEvent, *args, **kwargs) -> Any:
        """Publish an event via the event service.

        Args:
            event (:obj:`BaseEvent`): The event to publish.
            **kwargs: Arbitrary keyword arguments.
        """
        self.event_service.publish(event, *args, **kwargs)

    def publish_manual_event(self: Self, event: ManualEvent, *args, **kwargs) -> Any:
        """Publish a manual event via the event service.

        Args:
            event (:obj:`ManualEvent`): The manual event to publish.
            **kwargs: Arbitrary keyword arguments.
        """
        self.event_service.publish(event, exchange=self.event_service.manual_exchange, *args, **kwargs)

    def force_signal_update(self: Self) -> None:
        """Force a signal update to occur when the next pulse is processed."""
        self.event_service.force_signal_update = True
