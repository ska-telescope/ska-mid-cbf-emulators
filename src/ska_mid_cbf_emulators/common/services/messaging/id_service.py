class IdService():

    @staticmethod
    def pulse_queue_id(bitstream_emulator_id: str, ip_block_id: str):
        """Get a standardized pulse queue identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The bitstream emulator ID for this queue identifier.
            ip_block_id (:obj:`str`): The IP block ID for this queue identifier.

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{ip_block_id}_pulse_queue'

    @staticmethod
    def signal_update_queue_id(bitstream_emulator_id: str, ip_block_id: str = ''):
        """Get a standardized signal update queue identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this queue identifier.
            ip_block_id (:obj:`str`, optional): The IP block ID for this queue identifier. \
                Default is the empty string (i.e. only the emulator ID will be used).

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{(ip_block_id + "_") if len(ip_block_id) else ""}signal_update_queue'

    @staticmethod
    def manual_queue_id(bitstream_emulator_id: str, ip_block_id: str = ''):
        """Get a standardized manual queue identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this queue identifier.
            ip_block_id (:obj:`str`, optional): The IP block ID for this queue identifier. \
                Default is the empty string (i.e. only the emulator ID will be used).

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{(ip_block_id + "_") if len(ip_block_id) else ""}manual_queue'

    @staticmethod
    def api_request_queue_id(bitstream_emulator_id: str, ip_block_id: str = ''):
        """Get a standardized API request queue identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this queue identifier.
            ip_block_id (:obj:`str`, optional): The IP block ID for this queue identifier. \
                Default is the empty string (i.e. only the emulator ID will be used).

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{(ip_block_id + "_") if len(ip_block_id) else ""}api_request_queue'

    @staticmethod
    def api_callback_queue_id(bitstream_emulator_id: str, ip_block_id: str = ''):
        """Get a standardized API callback queue identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this queue identifier.
            ip_block_id (:obj:`str`, optional): The IP block ID for this queue identifier. \
                Default is the empty string (i.e. only the emulator ID will be used).

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{(ip_block_id + "_") if len(ip_block_id) else ""}api_callback_queue'

    @staticmethod
    def pulse_exchange_id(bitstream_emulator_id: str, ip_block_id: str):
        """Get a standardized pulse exchange identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this exchange identifier.
            ip_block_id (:obj:`str`): The IP block ID for this exchange identifier.

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{ip_block_id}_pulse_exchange'

    @staticmethod
    def signal_update_exchange_id(bitstream_emulator_id: str, ip_block_id: str):
        """Get a standardized signal update exchange identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this exchange identifier.
            ip_block_id (:obj:`str`): The IP block ID for this exchange identifier.

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{ip_block_id}_signal_update_exchange'

    @staticmethod
    def manual_exchange_id(bitstream_emulator_id: str, ip_block_id: str):
        """Get a standardized manual exchange identifier for the given bitstream emulator and IP block IDs.

        Args:
            bitstream_emulator_id (:obj:`str`): The emulator ID for this exchange identifier.
            ip_block_id (:obj:`str`): The IP block ID for this exchange identifier.

        Returns:
            :obj:`str` The new standardized identifier.
        """
        return f'{bitstream_emulator_id}_{ip_block_id}_manual_exchange'
