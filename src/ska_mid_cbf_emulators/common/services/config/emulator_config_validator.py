"""Validator which handles emulator configuration files."""

from typing import Any, Self, override

import jsonschema

from ska_mid_cbf_emulators.common.interfaces.validation.base_validator import (
    BaseValidator,
    BaseValidatorResult,
    ValidatorMessage,
    ValidatorStatus,
)
from ska_mid_cbf_emulators.common.models.config.emulator_config import EmulatorConfig
from ska_mid_cbf_emulators.common.models.config.emulator_config_ip_block import EmulatorConfigIPBlock
from ska_mid_cbf_emulators.common.schemas.emulator_config import emulator_config_schema


class EmulatorConfigValidatorResult(BaseValidatorResult):
    """Message object for a validation of a particular emulator configuration.

    Args:
        initial_status (:obj:`ValidatorStatus`, optional): The initial status to assume for this validation result.\
            Default is `ValidatorStatus.OK`.

    Attributes:
        overall_status (:obj:`ValidatorStatus`): The overall status of this validation result.
        result (:obj:`list[ValidatorMessage]`):\
            A dictionary containing all messages generated from this validation, organized by IP block ID.
    """

    def __init__(self: Self, initial_status: ValidatorStatus = ValidatorStatus.OK) -> None:
        super().__init__(initial_status)
        self.result: list[ValidatorMessage] = []

    @override
    @property
    def json_dict(self: Self) -> dict[str, Any]:
        """A JSON-serializable dictionary representing this result object."""
        return {
            'overall_status': self.overall_status.name,
            'result': [msg.json_dict for msg in self.result]
        }

    @override
    def insert_msg(self: Self, msg: ValidatorMessage) -> None:
        """Insert a message into the result."""
        self.result.append(msg)
        self.overall_status = max(self.overall_status, msg.status)


class EmulatorConfigValidator(BaseValidator):
    """Validator which handles emulator configuration files.

    Implements :obj:`BaseValidator`.
    """
    @override
    @staticmethod
    def validate(
        config_dict: dict[Any, Any]
    ) -> EmulatorConfigValidatorResult:
        """Validate a loaded emulator configuration.

        Args:
            config_dict (:obj:`str`): The loaded JSON dict to validate.

        Returns:
            :obj:`EmulatorConfigValidatorResult` The result of the validation.
        """
        partial_result = EmulatorConfigValidatorResult()
        EmulatorConfigValidator._validate(config_dict, partial_result)
        return partial_result

    @staticmethod
    def validate_and_get_config(
        config_dict: dict[Any, Any]
    ) -> tuple[EmulatorConfigValidatorResult, EmulatorConfig | None]:
        """Validate a loaded emulator configuration and return the contructed EmulatorConfig object.

        Args:
            config_dict (:obj:`str`): The loaded JSON dict to validate.

        Returns:
            :obj:`EmulatorConfigValidatorResult` The result of the validation.
        """
        partial_result = EmulatorConfigValidatorResult()
        config = EmulatorConfigValidator._validate(config_dict, partial_result)
        return partial_result, config

    @staticmethod
    def _validate(
        config_dict: dict[Any, Any],
        partial_result: EmulatorConfigValidatorResult
    ) -> EmulatorConfig | None:
        """Validate a loaded emulator configuration."""
        ok: bool = True

        try:
            jsonschema.validate(config_dict, emulator_config_schema)
        except jsonschema.ValidationError as e:
            partial_result.insert_msg(ValidatorMessage(
                ValidatorStatus.ERROR,
                {
                    'error_message': e.message,
                    'failed_instance': e.instance,
                    'against_schema': e.schema,
                    'schema_path': '.'.join(e.schema_path)
                }
            ))
            return None

        ip_block_definitions: dict[str, EmulatorConfigIPBlock] = {}

        for ip_block in config_dict.get('ip_blocks'):
            ip_block_id: str = ip_block.get('id')

            if ip_block_definitions.get(ip_block_id, None) is not None:
                partial_result.insert_msg(ValidatorMessage(
                    ValidatorStatus.ERROR,
                    f'Duplicate IP block ID `{ip_block_id}` was found. '
                    'All IP block IDs must be unique. '
                    'Please double check your configuration file.'
                ))
                ok = False
            else:
                ip_block_definitions[ip_block_id] = EmulatorConfigIPBlock(
                    id=ip_block_id,
                    display_name=ip_block.get('display_name'),
                    type=ip_block.get('type'),
                    downstream_block_ids=ip_block.get('downstream_block_ids'),
                    constants=ip_block.get('constants', {})
                )

        first_block_ids: str = config_dict.get('first')
        for first_block_id in first_block_ids:
            if ip_block_definitions.get(first_block_id, None) is None:
                partial_result.insert_msg(ValidatorMessage(
                    ValidatorStatus.ERROR,
                    'The `first` property must contain a list of one or more IP block IDs. '
                    f'No IP block with ID `{first_block_id}` was found. '
                    'Please double check your configuration file.'
                ))
                ok = False

        for ip_block_definition in ip_block_definitions.values():
            for ds_block_id in ip_block_definition.downstream_block_ids:
                if ip_block_definitions.get(ds_block_id, None) is None:
                    partial_result.insert_msg(ValidatorMessage(
                        ValidatorStatus.ERROR,
                        f'IP Block ID `{ds_block_id}` was specified '
                        f'as a downstream block of `{ip_block_definition.id}`, '
                        f'however no IP block with ID `{ds_block_id}` was found. '
                        'Please double check your configuration file.'
                    ))
                    ok = False
                else:
                    ip_block_definitions.get(ds_block_id).upstream_block_ids.add(ip_block_definition.id)

        if not ok:
            return None

        config_result = EmulatorConfig(
            id=config_dict.get('id'),
            version=config_dict.get('version'),
            ip_blocks=ip_block_definitions.values(),
            first=first_block_ids
        )

        allow_excess_root_warning = True
        if not config_result.graph.is_connected(mode='weak'):
            partial_result.insert_msg(ValidatorMessage(
                ValidatorStatus.WARNING,
                'The graph defined by this configuration file is not connected, '
                'i.e. there are some IP blocks or subset of IP blocks which are not connected in any way '
                'to the initial block(s), and will receive no data from any pulses flowing through the system. '
                'Please double check your configuration file.'
            ))
            allow_excess_root_warning = False

        root_names = [v['name'] for v in config_result.graph.vs if v.indegree() == 0]
        for first_block_id in first_block_ids:
            first_neighbors = config_result.graph.neighbors(first_block_id, mode='in')
            if len(first_neighbors):
                partial_result.insert_msg(ValidatorMessage(
                    ValidatorStatus.WARNING,
                    f'The `first` property contains `{first_block_id}`, however in the graph defined by this configuration, '
                    'this IP block is not a root node. Only blocks connected downstream '
                    'from those in the `first` property  will receive pulses. '
                    f'The current root nodes are: {root_names}. '
                    'The emulator\'s root nodes should be exactly specified in the `first` property. '
                    'Please double check your configuration file.'
                ))
            elif (excess_roots := (set(root_names) - set(first_block_ids))) and allow_excess_root_warning:
                partial_result.insert_msg(ValidatorMessage(
                    ValidatorStatus.WARNING,
                    f'The graph defined by this configuration has excess root nodes {excess_roots}, '
                    'which will receive no data from any pulses flowing through the system. '
                    'The emulator\'s root nodes should be exactly specified in the `first` property. '
                    'Please double check your configuration file.'
                ))

        return config_result
