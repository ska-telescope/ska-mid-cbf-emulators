"""Emulator controller configuration parser/loader."""

import json

from ska_mid_cbf_emulators.common.interfaces.validation.base_validator import ValidationError, ValidatorStatus
from ska_mid_cbf_emulators.common.models.config.emulator_config import EmulatorConfig
from ska_mid_cbf_emulators.common.services.config.emulator_config_validator import EmulatorConfigValidator
from ska_mid_cbf_emulators.common.services.logging.logger_factory import LoggerFactory


class EmulatorConfigParser():
    """Configuration parser/loader for the emulator controller.

    A static class providing functionality to extract config data
    from a JSON string into an EmulatorConfig object.
    """

    @staticmethod
    def decode(config_str: str) -> EmulatorConfig:
        """Convert JSON configuration into an EmulatorConfig object.

        Takes in a JSON string containing the emulator configuration
        and extracts the data into an EmulatorConfig object.

        Args:
            config_str (:obj:`str`): The configuration JSON string to parse.

        Returns:
            :obj:`EmulatorConfig`: The generated configuration object.
        """
        logger = LoggerFactory.get_logger()
        json_config = json.loads(config_str)

        validation_result, config = EmulatorConfigValidator.validate_and_get_config(json_config)
        match validation_result.overall_status:
            case ValidatorStatus.OK:
                logger.info(f'Config validation was successful. Result: \n{str(validation_result)}')
                return config
            case ValidatorStatus.WARNING:
                logger.warning(f'Config validation passed with warnings. Result: \n{str(validation_result)}')
                return config
            case ValidatorStatus.ERROR:
                raise ValidationError(f'Config validation failed with errors. Result: \n{str(validation_result)}')
