"""Factory for creating logger instances."""

import logging
import logging.config
import os
from typing import Self

import yaml

from ska_mid_cbf_emulators.common.models.config.emulator_log_level import EmulatorLogLevel


class EmulatorLogger(logging.Logger):
    """Extension of the Logger class with TRACE level support."""
    NONE = 1000
    TRACE = 5

    def trace(self: Self, msg: str, *args, **kwargs):
        """
        Log 'msg % args' with severity 'TRACE'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.trace("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        self._log(self.TRACE, msg, args, stacklevel=kwargs.pop('stacklevel', 1) + 1, **kwargs)


class LoggerFactory:
    """Factory class for creating logger instances.

    Example:
        logger = LoggerFactory.get_logger()
    """

    _LOGGING_CONFIG_NAME = 'logging.yaml'
    _logger: EmulatorLogger | None = None
    _overridden_console_log_level: EmulatorLogLevel | None = None

    @classmethod
    def get_logger(cls) -> EmulatorLogger:
        """Get the logger instance.

        Returns:
            :obj:`logging.Logger` The retrieved logger instance.
        """
        if cls._logger is None:
            # get the environment variable if set, else default to development
            logger_key = os.getenv('LOGGING_MODE', 'dev')
            try:
                cls._load_config(logger_key)
                cls._logger.info(f'Logger mode configured to: {logger_key}')
            except FileNotFoundError:
                cls._fallback_config('Logging configuration file not found. Using default logger.')
        return cls._logger

    @classmethod
    def override_console_log_level(cls, level: EmulatorLogLevel) -> None:
        """Override the default console log level."""
        cls._overridden_console_log_level = level

    @classmethod
    def _load_config(cls, logger_key: str) -> None:
        """Loads the logging configuration from a yaml configuration file."""
        logging.setLoggerClass(EmulatorLogger)
        setattr(logging, 'TRACE', EmulatorLogger.TRACE)
        logging._levelToName[EmulatorLogger.TRACE] = 'TRACE'
        logging._nameToLevel['TRACE'] = EmulatorLogger.TRACE

        setattr(logging, 'NONE', EmulatorLogger.NONE)
        logging._levelToName[EmulatorLogger.NONE] = 'NONE'
        logging._nameToLevel['NONE'] = EmulatorLogger.NONE

        logging.getLogger('pika').setLevel(logging.WARNING)

        module_dir = os.path.dirname(os.path.abspath(__file__))
        config_file = os.path.join(module_dir, cls._LOGGING_CONFIG_NAME)

        # read config file
        with open(config_file) as f:
            config = yaml.safe_load(f.read())

        if cls._overridden_console_log_level is not None:
            config['handlers']['console']['level'] = cls._overridden_console_log_level.name

        # load config file
        logging.config.dictConfig(config)

        # check if key exists
        if logger_key not in EmulatorLogger.manager.loggerDict:
            # key doesn't exist, set logger to root configuration
            cls._logger = logging.getLogger()
            cls._logger.error(f'Logger with key {logger_key} not found in configuration.')
        else:
            # key exists, set logger to key
            cls._logger = logging.getLogger(logger_key)

    @classmethod
    def _fallback_config(cls, error_message: str) -> None:
        """Loads a fallback logging configuration when loading from a configuration file fails."""
        # set to root logger
        cls._logger = logging.getLogger()
        cls._logger.error(error_message)
