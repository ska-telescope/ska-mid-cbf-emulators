emulator_config_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Emulator Configuration",
    "description": "Configuration object for one full emulator.",
    "type": "object",
    "properties": {
        "id": {
            "description": "ID for this configuration. This should remain the same between different versions.",
            "type": "string"
        },
        "version": {
            "description": "Semantic version of this configuration, for the specified ID.",
            "type": "string",
            "pattern": (
                r"^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)"
                r"(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))"
                r"?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"
            )
        },
        "ip_blocks": {
            "description": "List of IP blocks captured by this emulator.",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "description": "Unique ID of this IP block.",
                        "type": "string"
                    },
                    "display_name": {
                        "description": "Display name of this IP block (used in logging, diagrams, etc).",
                        "type": "string"
                    },
                    "type": {
                        "description": "Type of this IP block (e.g. \"wideband_input_buffer\").",
                        "type": "string"
                    },
                    "downstream_block_ids": {
                        "description": "List of IP block IDs directly connected downstream from this block.",
                        "type": "array",
                        "items": {
                            "description": "An IP block ID. This should be an ID defined somewhere in this configuration.",
                            "type": "string"
                        },
                        "minItems": 0,
                        "uniqueItems": True
                    },
                    "constants": {
                        "description": "Dict of any constant 'register values' to set on the simulated IP block.",
                        "type": "object"
                    }
                },
                "required": [
                    "id",
                    "display_name",
                    "type",
                    "downstream_block_ids"
                ]
            },
            "minItems": 1,
            "uniqueItems": True
        },
        "first": {
            "description": "A list of IDs of the first IP blocks in the chain, in parallel, immediately after the DISH.",
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 1,
            "uniqueItems": True
        }
    },
    "required": [
        "id",
        "version",
        "ip_blocks",
        "first"
    ]
}
