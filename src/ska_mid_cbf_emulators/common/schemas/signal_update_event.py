from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity

signal_update_event_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Emulator Signal Update Event",
    "description": "A signal update event sent by/to the emulator.",
    "type": "object",
    "properties": {
        "type": {
            "description": "Type of this event. Must be 'signal_update' (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(signal_update)$"
        },
        "value": {
            "description": "Value of this event. Can be anything as long as it is an object (i.e. an empty value is {}).",
            "type": "object"
        },
        "severity": {
            "description": "Severity of this event. Must be a valid EventSeverity value (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([s for s in EventSeverity]) + r")$"
        },
        "source_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was originally created.",
            "type": "number",
            "minimum": 0
        },
        "update_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was created or last updated.",
            "type": "number",
            "minimum": 0
        },
    },
    "required": [
        "value",
        "severity",
        "source_timestamp",
    ]
}
