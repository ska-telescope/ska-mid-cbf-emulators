from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity
from ska_mid_cbf_emulators.common.models.event.event_type import EventType
from ska_mid_cbf_emulators.common.models.event.manual_event_subtype import ManualEventSubType

emulator_event_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Emulator Event",
    "description": "An event sent by/to the emulator.",
    "type": "object",
    "properties": {
        "type": {
            "description": "Type of this event. Must be a valid EventType value (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([t for t in EventType]) + r")$"
        },
        "subtype": {
            "description": ("Subtype of this event, if available. Currently only used for Manual Events. "
                            "Must be a valid ManualEventSubType value if provided (case-insensitive)."),
            "type": "string",
            "pattern": (
                r"(?i)^("
                + r"|".join([t for t in ManualEventSubType])
                + r")$"
            )
        },
        "value": {
            "description": "Value of this event. Can be anything as long as it is an object (i.e. an empty value is {}).",
            "type": "object"
        },
        "severity": {
            "description": "Severity of this event. Must be a valid EventSeverity value.",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([s for s in EventSeverity]) + r")$"
        },
        "source_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was originally created.",
            "type": "number",
            "minimum": 0
        },
        "update_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was created or last updated.",
            "type": "number",
            "minimum": 0
        },
    },
    "required": [
        "type",
        "value",
        "severity",
        "source_timestamp",
    ]
}
