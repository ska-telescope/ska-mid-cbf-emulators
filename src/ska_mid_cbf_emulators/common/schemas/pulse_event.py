from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity

pulse_event_schema = {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Emulator Pulse Event",
    "description": "A pulse event sent by/to the emulator.",
    "type": "object",
    "properties": {
        "type": {
            "description": "Type of this event. Must be 'pulse' (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(pulse)$"
        },
        "value": {
            "description": (
                "Value of this event. Must contain an \"id\" property, "
                "but any other arbitrary properties may also be provided."
            ),
            "type": "object",
            "properties": {
                "id": {
                    "description": (
                        "Integer ID for this pulse. Should generally represent the pulse count "
                        "and be unique for a given timestamp."
                    ),
                    "type": "integer",
                    "minimum": 0
                },
            },
            "additionalProperties": True,
            "required": [
                "id",
            ]
        },
        "severity": {
            "description": "Severity of this event. Must be a valid EventSeverity value (case-insensitive).",
            "type": "string",
            "pattern": r"(?i)^(" + r"|".join([s for s in EventSeverity]) + r")$"
        },
        "source_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was originally created.",
            "type": "number",
            "minimum": 0
        },
        "update_timestamp": {
            "description": "Timestamp, in milliseconds, for when this event was created or last updated.",
            "type": "number",
            "minimum": 0
        },
    },
    "required": [
        "value",
        "severity",
        "source_timestamp",
    ]
}
