
from enum import StrEnum, auto


class EventType(StrEnum):
    """Event type enum."""
    PULSE = auto()
    MANUAL = auto()
    SIGNAL_UPDATE = auto()
