"""Enum containing event severity levels."""

from enum import StrEnum, auto


class EventSeverity(StrEnum):
    """Enum containing event severity levels."""
    GENERAL = auto()
    WARNING = auto()
    FATAL_ERROR = auto()
