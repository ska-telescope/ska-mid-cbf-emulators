"""Model for a list of Signal Update events with some additional metadata."""

from typing import Self

from ska_mid_cbf_emulators.common.models.event.signal_update_event import SignalUpdateEvent


class SignalUpdateEventList:
    """A list of Signal Update events with some additional metadata.

    Args:
        events (:obj:`list[SignalUpdateEvent]`): The list of events.
        source_timestamp (:obj:`int`): The source timestamp of all events in the list.

    Attributes:
        events (:obj:`list[SignalUpdateEvent]`): The list of events.
        source_timestamp (:obj:`int`): The source timestamp of all events in the list.
    """

    def __init__(
        self: Self,
        events: list[SignalUpdateEvent] = [],
        source_timestamp: int = -1
    ) -> None:
        self.events = events
        self.source_timestamp = source_timestamp if source_timestamp >= 0 else events[0].source_timestamp if len(events) else 0

    def __len__(self: Self) -> int:
        return len(self.events)

    def __repr__(self: Self) -> str:
        return f'<Signal Update event list with source timestamp {self.source_timestamp} and events {self.events}>'
