"""Models for Signal Update Events."""

from __future__ import annotations

import json
from typing import Any, Self, override

import jsonschema

from ska_mid_cbf_emulators.common.interfaces.event.base_event import BaseEvent
from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity
from ska_mid_cbf_emulators.common.models.event.event_type import EventType
from ska_mid_cbf_emulators.common.schemas.signal_update_event import signal_update_event_schema
from ska_mid_cbf_emulators.common.services.logging.logger_factory import LoggerFactory


class SignalUpdateEvent(BaseEvent):
    """Model for Signal Update Events.

    Implements :obj:`BaseEvent`.

    These events will be sent to IP block emulators' Signal Update Queues/Exchanges to be processed.

    Args:
        value (:obj:`dict[Any, Any]`, optional): The value of this event, \
            which can be any arbitrary dictionary. Default is `{}`.
        severity (:obj:`EventSeverity`, optional): The severity of this event. \
            Defaults to :obj:`EventSeverity.GENERAL`.
        source_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was originally created. \
            Defaults to the current system time.
        update_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was created or last updated. \
            Defaults to the value of source_timestamp.

    Attributes:
        value (:obj:`dict[Any, Any]`): The value of the event.
        severity (:obj:`EventSeverity`): The severity of the event.
        source_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was originally created.
        update_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was created or last updated. \
            For newly created events, update_timestamp == source_timestamp.
    """

    def __init__(
        self: Self,
        value: dict[Any, Any] = {},
        severity: EventSeverity = EventSeverity.GENERAL,
        source_timestamp: int = None,
        update_timestamp: int = None
    ) -> None:
        super().__init__(
            type=EventType.SIGNAL_UPDATE,
            value=value,
            severity=severity,
            source_timestamp=source_timestamp,
            update_timestamp=update_timestamp
        )

    @override
    @staticmethod
    def decode(json_event_str: str) -> SignalUpdateEvent:
        """Decode a JSON-encoded signal update event.

        Args:
            encoded_event (:obj:`str`): The JSON string containing the event to decode

        Returns:
            :obj:`SignalUpdateEvent`: The decoded event
        """
        logger = LoggerFactory.get_logger()
        json_event = json.loads(json_event_str)
        try:
            jsonschema.validate(json_event, signal_update_event_schema)
        except Exception as e:
            logger.error(str(e))
            raise e
        logger.trace('Signal Update event schema validation was successful.')

        return SignalUpdateEvent(
            value=json_event.get('value'),
            severity=EventSeverity(json_event.get('severity').lower()),
            source_timestamp=json_event.get('source_timestamp'),
            update_timestamp=json_event.get('update_timestamp')
        )
