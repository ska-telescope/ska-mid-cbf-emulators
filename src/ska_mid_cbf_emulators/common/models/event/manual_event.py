"""Models for Manual Events."""

from __future__ import annotations

import json
from typing import Any, Self, override

import jsonschema

from ska_mid_cbf_emulators.common.interfaces.event.base_event import BaseEvent
from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity
from ska_mid_cbf_emulators.common.models.event.event_type import EventType
from ska_mid_cbf_emulators.common.models.event.manual_event_subtype import ManualEventSubType
from ska_mid_cbf_emulators.common.schemas.manual_event import manual_event_schema
from ska_mid_cbf_emulators.common.services.logging.logger_factory import LoggerFactory


class ManualEvent(BaseEvent):
    """Model for Manual Events.

    Implements :obj:`BaseEvent`.

    These events will be sent to IP block emulators' Manual Queues/Exchanges to be processed.

    Args:
        subtype (:obj:`ManualEventSubType`, optional): The subtype of this event. \
            Defaults to `ManualEventSubType.GENERAL`.
        value (:obj:`dict[Any, Any]`, optional): The value of this event, \
            which can be any arbitrary dictionary. Default is `{}`.
        severity (:obj:`EventSeverity`, optional): The severity of this event. \
            Defaults to :obj:`EventSeverity.GENERAL`.
        source_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was originally created. \
            Defaults to the current system time.
        update_timestamp (:obj:`int`, optional): The timestamp, in milliseconds, when the event was created or last updated. \
            Defaults to the value of source_timestamp.

    Attributes:
        subtype (:obj:`ManualEventSubType`): The subtype of the event.
        value (:obj:`dict[Any, Any]`): The value of the event.
        severity (:obj:`EventSeverity`): The severity of the event.
        source_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was originally created.
        update_timestamp (:obj:`int`): The timestamp, in milliseconds, when the event was created or last updated. \
            For newly created events, update_timestamp == source_timestamp.
    """

    def __init__(
        self: Self,
        subtype: ManualEventSubType = ManualEventSubType.GENERAL,
        value: dict[Any, Any] = {},
        severity: EventSeverity = EventSeverity.GENERAL,
        source_timestamp: int = None,
        update_timestamp: int = None
    ) -> None:
        super().__init__(
            type=EventType.MANUAL,
            subtype=subtype,
            value=value,
            severity=severity,
            source_timestamp=source_timestamp,
            update_timestamp=update_timestamp
        )

    def __repr__(self: Self) -> str:
        return (f'<Manual event with severity {self._severity}, subtype {self._subtype}, '
                f'source timestamp {self._source_timestamp}, update timestamp {self._update_timestamp}, value {self._value}>')

    @override
    @staticmethod
    def decode(json_event_str: str) -> ManualEvent:
        """Decode a JSON-encoded manual event.

        Args:
            encoded_event (:obj:`str`): The JSON string containing the event to decode

        Returns:
            :obj:`ManualEvent`: The decoded event
        """
        logger = LoggerFactory.get_logger()
        json_event = json.loads(json_event_str)
        try:
            jsonschema.validate(json_event, manual_event_schema)
        except Exception as e:
            logger.error(str(e))
            raise e
        logger.trace('Manual event schema validation was successful.')

        return ManualEvent(
            subtype=ManualEventSubType(json_event.get('subtype').lower()),
            value=json_event.get('value'),
            severity=EventSeverity(json_event.get('severity').lower()),
            source_timestamp=json_event.get('source_timestamp'),
            update_timestamp=json_event.get('update_timestamp')
        )
