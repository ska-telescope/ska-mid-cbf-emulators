"""Enum containing Manual Event subtypes."""
from enum import auto

from ska_mid_cbf_emulators.common.interfaces.event.base_event import BaseEventSubType


class ManualEventSubType(BaseEventSubType):
    """Enum containing Manual Event subtypes."""
    GENERAL = auto()
    INJECTION = auto()
