"""Model containing internal RPC request data for API calls."""

from __future__ import annotations

import json
from typing import Any, Self


class InternalRestRequest():
    """Model containing internal RPC request data for API calls.

    Args:
        method_name (:obj:`str`): Name of the implementation method to call internally.
        http_method (:obj:`str`): HTTP method type for the request.
        kwargs (:obj:`dict[str, Any]`): Any keyword arguments (e.g. path/query parameters, etc)
            to pass to the implentation method.

    Attributes:
        method_name (:obj:`str`): Name of the implementation method to call internally.
        http_method (:obj:`str`): HTTP method type for the request.
        kwargs (:obj:`dict[str, Any]`): Keyword arguments to pass to the implentation method.
    """
    def __init__(
            self: Self,
            method_name: str = '',
            http_method: str = 'GET',
            kwargs: dict[str, Any] = {}
    ) -> None:
        self.method_name: str = method_name
        self.http_method: str = http_method
        self.kwargs: dict[str, Any] = kwargs

    @staticmethod
    def from_json(json_str: str) -> InternalRestRequest:
        """Deserialize a JSON config string into an :obj:`InternalRestRequest` object.

        Args:
            json_str (:obj:`str`): JSON config string to deserialize.

        Returns:
            :obj:`InternalRestRequest` the generated object.
        """
        loaded = json.loads(json_str)
        return InternalRestRequest(**loaded)

    @staticmethod
    def to_json(obj: InternalRestRequest) -> str:
        """Serialize an :obj:`InternalRestRequest` object to a JSON string.

        Args:
            obj (:obj:`InternalRestRequest`): The object to serialize.

        Returns:
            :obj:`str` the generated JSON string.
        """
        return json.dumps(vars(obj))
