"""Model containing internal response data for API calls."""

from __future__ import annotations

import json
import pickle
from typing import Any, Self

from fastapi import status


class InternalRestResponse():
    """Model containing internal response data for API calls.

    Args:
        status (:obj:`fastapi.status | int`, optional): HTTP status of the response. \
            Default is :obj:`status.HTTP_200_OK`.
        body (:obj:`dict[str, Any] | bytes`, optional): Body of the response. \
            Default is an empty dict.
        headers (:obj:`dict[str, str]`, optional): Headers of the response. \
            Default is None.
        media_type (:obj:`str`, optional): Media type of the response. \
            Default is None.

    Attributes:
        status (:obj:`fastapi.status | int`): HTTP status of the response.
        body (:obj:`dict[str, Any] | bytes`): Body of the response.
    """
    def __init__(
            self: Self,
            status: status = status.HTTP_200_OK,
            body: dict[str, Any] | bytes = {},
            headers: dict[str, str] = None,
            media_type: str = None
    ) -> None:
        self.status = status
        self.body = body
        self.headers = headers
        self.media_type = media_type

    @staticmethod
    def ok(
        body: dict[str, Any] | bytes = {},
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 200 (OK) and the given body.

        Args:
            body (:obj:`dict[str, Any]`, optional): Body of the response. Default is `{}`.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_200_OK,
            body=body,
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def accepted(
        body: dict[str, Any] | bytes = {},
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 202 (Accepted) and the given body.

        Args:
            body (:obj:`dict[str, Any]`, optional): Body of the response. Default is `{}`.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_202_ACCEPTED,
            body=body,
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def no_content(
        body: dict[str, Any] | bytes = {},
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 204 (No Content) and the given body.

        Args:
            body (:obj:`dict[str, Any]`, optional): Body of the response. Default is `{}`.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_204_NO_CONTENT,
            body=body,
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def bad_request(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 400 (Bad Request) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_400_BAD_REQUEST,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def not_found(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 404 (Not Found) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_404_NOT_FOUND,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def request_timeout(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 408 (Request Timeout) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_408_REQUEST_TIMEOUT,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def conflict(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 409 (Conflict) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_409_CONFLICT,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def unprocessable_entity(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 422 (Unprocessable Entity) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def internal_server_error(
        error_message: str = '',
        headers: dict[str, str] = None,
        media_type: str = None
    ) -> InternalRestResponse:
        """Generate a response with HTTP status 500 (Internal Server Error) and a body containing the given error message.

        Args:
            error_message (:obj:`str`, optional): Error message to include in the response body.
                Default is the empty string.

        Returns:
            (:obj:`InternalRestResponse`) the generated response object.
        """
        return InternalRestResponse(
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            body={
                'error_message': error_message
            },
            headers=headers,
            media_type=media_type
        )

    @staticmethod
    def from_json(json_str: str) -> InternalRestResponse:
        """Deserialize a JSON config string into an :obj:`InternalRestResponse` object.

        Args:
            json_str (:obj:`str`): JSON config string to deserialize.

        Returns:
            :obj:`InternalRestResponse` the generated object.
        """
        loaded = json.loads(json_str)
        return InternalRestResponse(**loaded)

    @staticmethod
    def to_json(obj: InternalRestResponse) -> str:
        """Serialize an :obj:`InternalRestResponse` object to a JSON string.

        Args:
            obj (:obj:`InternalRestResponse`): The object to serialize.

        Returns:
            :obj:`str` the generated JSON string.
        """
        return json.dumps(vars(obj))

    @staticmethod
    def deserialize(serialized_obj: bytes) -> InternalRestResponse:
        """Deserialize a serialized :obj:`bytes` object into an :obj:`InternalRestResponse` object.

        Args:
            serialized_obj (:obj:`bytes`): Serialized :obj:`bytes` object to deserialize.

        Returns:
            :obj:`InternalRestResponse` the generated object.
        """
        return pickle.loads(serialized_obj)

    @staticmethod
    def serialize(obj: InternalRestResponse) -> bytes:
        """Serialize an :obj:`InternalRestResponse` object to a :obj:`bytes` object.

        Args:
            obj (:obj:`InternalRestResponse`): The object to serialize.

        Returns:
            :obj:`bytes` the generated :obj:`bytes` object.
        """
        return pickle.dumps(obj)
