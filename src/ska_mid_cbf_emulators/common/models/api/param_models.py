"""Models related to API function parameters."""

from enum import Enum
from inspect import Parameter
from typing import Annotated, Self, Type, TypeVar

T = TypeVar('T')


class ApiParamType(Enum):
    """Enum containing internal API parameter types."""
    PATH = 'path'
    QUERY = 'query'
    BODY = 'body'


type PathParam[T] = Annotated[T, ApiParamType.PATH]
"""A path parameter of type `T`. A variable typed with this alias can be used identically as if it were type `T`."""


type QueryParam[T] = Annotated[T, ApiParamType.QUERY]
"""A query parameter of type `T`. A variable typed with this alias can be used identically as if it were type `T`."""


type BodyParam[T] = Annotated[T, ApiParamType.BODY]
"""A body parameter of type `T`. A variable typed with this alias can be used identically as if it were type `T`."""


class ApiParam[T]():
    """Model containing information about a single API implementation function parameter.

    Args:
        name (:obj:`str`): The name of the parameter.
        type (:obj:`Type[T]`): The type of the parameter.
        default (:obj:`T | Parameter.empty`, optional): The default value of the parameter, if any.\
            If there is no default value, set to `Parameter.empty` (not `None`). Default is `Parameter.empty`.

    Attributes:
        name (:obj:`str`): The name of the parameter.
        type (:obj:`Type[T]`): The type of the parameter.
        default (:obj:`T | Parameter.empty`): The default value of the parameter, if any.
    """
    def __init__(self: Self, name: str, type: Type[T], default: T | Parameter.empty = Parameter.empty) -> None:
        self.name: str = name
        self.type: Type[T] = type
        self.default: T | Parameter.empty = default

    def __repr__(self: Self) -> str:
        return f'<ApiParam: name={self.name}, type={self.type}, default={self.default}>'
