
from enum import IntEnum, auto


class EmulatorLogLevel(IntEnum):
    NONE = 0
    ERROR = auto()
    WARNING = auto()
    INFO = auto()
    DEBUG = auto()
    TRACE = auto()
