"""Model for IP blocks in emulator configurations."""

from __future__ import annotations

from typing import Any, Iterable, Self


class EmulatorConfigIPBlock():
    """Emulator configuration sub-model for an individual IP block definition.

    This data is used to configure IP block emulators.

    Args:
        id (:obj:`str`): A unique identifier for the IP Block.
        display_name (:obj:`str`): A (not necessarily unique) name for the IP Block \
            used for user-facing display purposes.
        type (:obj:`str`): The type of IP Block.
        downstream_block_ids (:obj:`Iterable[str]`): A list of IDs of downstream IP blocks \
            which the IP block is directly connected to.
        constants (:obj:`dict[str, Any]`, optional): A dict of constant register values to set on the simulated IP block. \
            Default is an empty dict.

    Attributes:
        id (:obj:`str`): The unique identifier for this IP Block.
        display_name (:obj:`str`): The human-readable name for this IP Block \
            used for user-facing display purposes.
        type (:obj:`str`): The type of this IP Block.
        downstream_block_ids (:obj:`set[str]`): A list of the IDs of downstream IP blocks \
            which this IP block is directly connected to.
        upstream_block_ids (:obj:`set[str]`): A list of the IDs of upstream IP blocks \
            which this IP block is directly connected to.
        constants (:obj:`dict[str, Any]`): The dict of constant register values to set on the simulated IP block.
    """

    def __init__(
            self: Self,
            id: str,
            display_name: str,
            type: str,
            downstream_block_ids: Iterable[str],
            constants: dict[str, Any] = {}
    ) -> None:
        self._id: str = None
        self._display_name: str = None
        self._type: str = None
        self._downstream_block_ids: set[str] = None
        self._constants: dict[str, Any] = None
        self._upstream_block_ids: set[str] = set()

        self.id = id
        self.display_name = display_name
        self.type = type
        self.downstream_block_ids = downstream_block_ids
        self.constants: dict[str, Any] = constants

    def __repr__(self: Self) -> str:
        return (
            f'IP Block configuration: id={self.id}, display_name={self.display_name}, type={self.type}, '
            f'ds={self.downstream_block_ids}, us={self.upstream_block_ids}'
        )

    @property
    def id(self: Self) -> str:
        """:obj:`str`: The unique identifier for this IP Block."""
        return self._id

    @id.setter
    def id(self: Self, new_id: str) -> None:
        self._id = new_id

    @property
    def display_name(self: Self) -> str:
        """:obj:`str`: The human-readable name for this IP Block
            used for user-facing display purposes.
        """
        return self._display_name

    @display_name.setter
    def display_name(self: Self, new_display_name: str) -> None:
        self._display_name = new_display_name

    @property
    def type(self: Self) -> str:
        """:obj:`str`: The type of this IP Block."""
        return self._type

    @type.setter
    def type(self: Self, new_type: str) -> None:
        self._type = new_type

    @property
    def downstream_block_ids(self: Self) -> set[str]:
        """:obj:`set[str]`: A list of the IDs of downstream IP blocks
            which this IP block is directly connected to.
        """
        return self._downstream_block_ids

    @downstream_block_ids.setter
    def downstream_block_ids(self: Self, new_downstream_block_ids: Iterable[str]) -> None:
        self._downstream_block_ids = set(new_downstream_block_ids)

    @property
    def upstream_block_ids(self: Self) -> set[str]:
        """:obj:`set[str]`: A list of the IDs of upstream IP blocks
            which this IP block is directly connected to.
        """
        return self._upstream_block_ids

    @upstream_block_ids.setter
    def upstream_block_ids(self: Self, new_upstream_block_ids: Iterable[str]) -> None:
        self._upstream_block_ids = set(new_upstream_block_ids)

    @property
    def constants(self: Self) -> dict[str, Any]:
        """:obj:`dict[str, Any]`: A dict of constant register values to set on the simulated IP block."""
        return self._constants

    @constants.setter
    def constants(self: Self, new_constants: dict[str, Any]) -> None:
        self._constants = new_constants
