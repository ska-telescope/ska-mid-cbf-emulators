"""Model for top-level emulator configurations."""

from __future__ import annotations

import math
from io import BytesIO
from typing import Iterable, Self

from igraph import Graph
from igraph import plot as plot_graph
from matplotlib import pyplot as plt
from matplotlib.axes import Axes

from ska_mid_cbf_emulators.common.models.config.emulator_config_ip_block import EmulatorConfigIPBlock


class EmulatorConfig():
    """Top-level emulator configuration model.

    Contains a list of known IP blocks and their details, plus some metadata.

    Args:
        id (:obj:`str`): A unique ID for the emulator being configured.
        version (:obj:`str`): A semantic version, relative to the ID, for the emulator being configured.
        ip_blocks (:obj:`Iterable[EmulatorConfigIPBlock]`): A list of individual
            IP block configurations to use for this emulator.
        first (:obj:`list[str]`): The IDs of the IP blocks to put at the start of the chain, in parallel, \
            immediately after the DISH. May be just one or several IDs.

    Attributes:
        id (:obj:`str`): The ID of the emulator this configuration applies to.
        version (:obj:`str`): The semantic version, relative to the ID, of this configuration.
        ip_blocks (:obj:`list[EmulatorConfigIPBlock]`): A list of the individual
            IP block configurations used for this emulator.
        first (:obj:`list[str]`): The IDs of the IP blocks at the start of the chain, in parallel, \
            immediately after the DISH. May be just one or several IDs.
        graph (:obj:`igraph.Graph`): A directed graph of the connections between IP blocks.
    """

    def __init__(
            self: Self,
            id: str,
            version: str,
            ip_blocks: Iterable[EmulatorConfigIPBlock],
            first: list[str]
    ) -> None:
        self._id: str = None
        self._version: str = None
        self._ip_blocks: list[EmulatorConfigIPBlock] = None
        self._first: list[str] = None
        self._graph = None

        self.id = id
        self.version = version
        self.ip_blocks = ip_blocks
        self.first = first

    @property
    def id(self: Self) -> str:
        """:obj:`str`: The ID of the emulator this configuration applies to."""
        return self._id

    @id.setter
    def id(self: Self, new_id: str) -> None:
        self._id = new_id
        return

    @property
    def version(self: Self) -> str:
        """:obj:`str`: The semantic version, relative to the ID, of this configuration."""
        return self._version

    @version.setter
    def version(self: Self, new_version: str) -> None:
        self._version = new_version
        return

    @property
    def ip_blocks(self: Self) -> list[EmulatorConfigIPBlock]:
        """:obj:`Iterable[EmulatorConfigIPBlock]`: A list of the individual
            IP block configurations used for this emulator.
        """
        return self._ip_blocks

    @ip_blocks.setter
    def ip_blocks(self: Self, new_defs: list[EmulatorConfigIPBlock]) -> None:
        self._ip_blocks = new_defs
        self._graph = self._generate_graph()
        return

    @property
    def first(self: Self) -> list[str]:
        """:obj:`str`: The IDs of the IP blocks at the start of the chain, in parallel,
        immediately after the DISH. May be just one or several IDs.
        """
        return self._first

    @first.setter
    def first(self: Self, new_first: list[str]) -> None:
        self._first = new_first

    @property
    def graph(self: Self) -> Graph:
        """:obj:`igraph.Graph`: A directed graph of the connections between IP blocks."""
        return self._graph

    def get_graph_img(self: Self) -> bytes:
        """Get a plot of the IP block graph as a PNG image.

        Returns:
            :obj:`bytes`: The raw PNG data.
        """
        strm = BytesIO()
        _, ax = plt.subplots(figsize=(
            min(6.0 + (6.0 * len(self.graph.connected_components(mode='weak'))), 30.0),
            min(5.0 + (1.0 * len(self.ip_blocks)), 20.0)
        ), dpi=100)
        ax: Axes
        plot_graph(
            self.graph,
            target=ax,
            layout='sugiyama',
            vertex_shape='circle',
            vertex_color='deepskyblue',
            vertex_label_angle=(1.5 * math.pi),
            vertex_label_dist=1.5,
            vertex_size=25,
            vertex_label_size=12,
            edge_width=2,
            edge_color='lightsteelblue'
        )
        ax.invert_yaxis()
        ax.set_xmargin(0.5)
        ax.set_ymargin(0.1)
        plt.savefig(strm, format='png', dpi=100)
        return strm.getvalue()

    def _generate_graph(self: Self) -> Graph:
        g: Graph = Graph.DictDict(
            {block.id: {ds_id: {} for ds_id in block.downstream_block_ids} for block in self._ip_blocks},
            directed=True
        )
        g.vs['label'] = g.vs['name']
        return g
