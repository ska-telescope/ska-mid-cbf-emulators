###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

UNRELEASED CHANGES
******************

0.7.1
******
* CIP-3095 Allow overriding ``initial_signal.json`` from Helm chart.
* CIP-3273, CIP-3339 Various FW dev image fixes & updates.

0.7.0
******
* CIP-3143 Signal Updates refactor. See: `https://confluence.skatelescope.org/display/SE/FHS+Emulator+-+Control+Flow+Design`_.
  * Added a glossary to the documentation for common emulator terminology.
  * The term "module" as previously used has now been renamed to "Subcontroller" or "IP block emulator" as appropriate.
  * The generic term "emulator" has been changed in many places to "bitstream emulator" or "IP block emulator" as appropriate, to reduce confusion.
  * "Processing" events, queues, etc. have been renamed to "Manual".
  * Added new "Signal Update" event type, with its own RabbitMQ queues and exchanges for each IP block emulator. See the doc linked above for more information.
  * The "timestamp" field on all events has been split into "source_timestamp" and "update_timestamp".
  * Event Handlers in IP block emulators are now classes and split their functionality into event type-specific methods.
  * Pulse Events now have a mandatory ID as part of their value.
  * Convergence support has been added. A block with multiple input pulses will now wait for all inputs to send a pulse, then process the first received and discard the rest.
  * The "first" property in config.json is now a list, and expects the first block(s) immediately after the DISH rather than the DISH itself.
  * DISH information has been eliminated from config.json, and its components now live inside the emulator engine.
  * Added initial_signal.json config file which allows the user to change the signal the DISH sends on the first pulse.
  * Added DISH injector support which allows the user to make changes to the DISH signal during runtime.
  * Options to override the config.json and/or initial_signal.json files have been added to the emulator app CLI, as well as a pulse interval option.
  * The injector app CLI now uses the same framework as the emulator app, and now supports a verbosity option.
  * Renamed and/or moved certain other classes and files, including several "Base<Thing>" classes which were originally intended as base classes but are no longer used as such.
  * Added RabbitMQ heartbeat to the pulse generator to protect against spontaneous death.
  * The IP block emulator factory will no longer duplicate validation or IP block emulator component classes when multiple blocks of the same type are provided.
  * Added a LoggingBase class which many of the common interfaces and other classes now extend,
    providing class logger methods without needing to manually instantiate a new logger in various places.
  * Added and updated many tests to align with the refactor.
  * Tests now all have type hinting where appropriate.
  * Removed all traces of the EFK (logging) stack for Docker Compose builds.
  * The distutils dependency in the development image has been removed as it is now fully removed from Python 3.12.
  * Added several new Makefile commands for managing Docker images.
  * Documentation has been updated.

0.6.0
******
* CIP-3109 Update deployment to support multiple bitstreams

0.5.4
******
* CIP-2975 Update Emulators to use volumes for bitstream
* CIP-2948 FSS/WIB default config updates
* CIP-2954 Add bitstream volumes and downloading
* CIP-2888 Fix emulator app label and use loadbalancer to expose emulator pod DNS
* CIP-2929 Fixes for H1 deployment
* Split global emulator service into one service per emulator
* Remove deployment.yaml
* Remove temp IP block emulators (now live in bitstream repo)
* Add minikube eval check on install chart
* Add k8s-destroy make script to uninstall and wait for all pods to terminate
* Add gitlab override option for custom bitstreams

0.5.3
******
* CIP-2929 Update emulator paths

0.5.2
******
* CIP-2606 Add unit tests to emulator
* Add support for rabbitmq tests
* Update code to fix issues caught by testing where necessary

0.5.1
******
* CIP-2837 Update deployments to pull down bitstream emulators from CAR instead of storing them locally, and update emulator/injector paths to support this change.
* Add ``deployment.yaml`` file which controls the spin-up of emulator pods when using k8s.
* Add initContainers to all emulator/injector pods such that they wait nicely for RabbitMQ to be ready, instead of crashing several times.
* Update docker configurations to support these changes when using Docker Compose.
* Add ``/config`` API route to emulators, which will return the contents of the config.json file they have pulled from CAR.
* Remove outdated docs.
* Add custom base runner image used for pylint and pytest in the CI/CD pipeline.
* Move the FastAPI app from a global variable to an attribute of the EmulatorController.
* Add some error checks in certain parts of the code.
* Extend container support in received API request bodies.
* Fix a bug in IP block emulator validation where non-route private functions would still expect parameters to be passed as ``APIParam``s.
* Minor bug fixes.
* Add initial configurations for pytest (actual tests to come later).

0.5.0
******
* CIP-2687 Update emulator RTD with README contents
* Move all README contents into ReadTheDocs, making RTD the primary source of documentation for this project.
* Remove as much outdated code from the repo as possible.
* Remove all references to Tango as the emulators explicitly do not use or need it.
* Update Helm charts such that emulator pods are exposed and identifiable and usable in conjunction with other pods, e.g. Tango device servers.
  This release is the first definitively Helm-compatible (and Helm-preferred) one for this repository.
* Minor bug fixes.

0.4.7
******
* CIP-2629 Add time delayed actions and IP block emulator validation to the emulator
* Add delay_action() general function which wraps Python Timers for time delayed async actions
* Add top-level controller API with start, stop, terminate routes
* Move API handling into separate service from event handling
* Add generic validator classes and move existing validation to leverage them
* Add new validation functionality for IP block emulators
* Add validation CLI utility script to check IP block emulators and config files are OK without running the emulator
* Streamline Makefile commands & update READMEs
* Cleanup logging & add verbosity CLI option to emulator script
* Fix excessive injector CPU usage

0.4.6
******
* CIP-2601 Refactor injector to use shared library and function more similarly to emulator
* Emulator event "type" has been renamed to "subtype", with the "type" field now being used for "PROCESSING" or "PULSE" to simplify handling serialized events.
* Injector web GUI has been removed as it complicated the code and could not be used anyway on systems without a visual browser.
* A new "constants" field has been added to the emulator configuration, which contains values that will be set on the simulated IP block on initialization.
* The emulator and injector pods now have healthcheck probes, and the APIs healthcheck endpoints; previously there was no defined way to check that the API server has been started except to view the emulator/injector logs.

0.4.5
******
* Fix missing dependencies in poetry config/lockfile

0.4.4
******
* CIP-2590 Implement method to supply constant register values to emulated IP block on instantiation
* Add API call to draw graph from IP block connections
* Add validation on emulator config
* Remove internal IP block type enum

0.4.3
******
* CIP-2585 Create initial IP Block emulators for FHS-VCC

0.4.2
******
* Added initial Helm charts and updated the CI pipeline steps

0.4.1
******
* Restructured emulator into new generic engine design
