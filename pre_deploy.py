import yaml
import click

@click.command()
@click.option('--dev', type=bool, default=False, help='Run in development mode.')
@click.option('--tag', type=str, default='latest', help='Tag for local emulator images. Ignored if not in development mode.')
@click.option('--enable-bitstream-download', type=bool, default=False, help='Tag for disabling pv creation and bitstream downloading.')
@click.option('--enable-bitstream-pvc', type=bool, default=False, help='Tag for disabling pv of the bitstream.')
@click.option('--k3d', type=bool, default=False, help='Tag for when using k3d as a cluster.')
def pre_deploy(dev: bool, tag: str, enable_bitstream_download: bool, enable_bitstream_pvc: bool, k3d: bool):

    click.echo('Loading `charts/ska-mid-cbf-emulators/values.yaml`...')
    with open('charts/ska-mid-cbf-emulators/values.yaml', 'r') as values_yaml:
        values_yaml_data: dict = yaml.safe_load(values_yaml)

    click.echo(f'Saving loaded deployment(s) to `charts/ska-mid-cbf-emulators/generated_values.yaml`...')
    if dev:
        values_yaml_data['emulator']['image']['repository'] = 'harbor.skao.int/staging/ska-mid-cbf-emulators-emulator'
        values_yaml_data['emulator']['image']['tag'] = tag
        values_yaml_data['injector']['image']['repository'] = 'harbor.skao.int/staging/ska-mid-cbf-emulators-injector'
        values_yaml_data['injector']['image']['tag'] = tag
        
    if enable_bitstream_download:
        values_yaml_data['bitstreamDownloadJob']['enabled'] = True
        
    if enable_bitstream_pvc:
        values_yaml_data['bitstreamPv']['enabled'] = True
        
    if k3d:
        values_yaml_data['global']['k3d'] = True
        
    with open('charts/ska-mid-cbf-emulators/generated_values.yaml', 'w') as generated_values_yaml:
        yaml.safe_dump(values_yaml_data, generated_values_yaml)

    click.echo(f'Pre-deploy script completed successfully.')


if __name__ == '__main__':
    pre_deploy()
