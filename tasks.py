"""Tasks that can be run to interact with the bitstream emulator when running in Docker for development."""
from invoke import task, Collection
import requests
import json as pyjson
import rich
import subprocess
import os


DEFAULT_EMULATOR_HOSTNAME = "http://localhost:5001/"
DEFAULT_INJECTOR_HOSTNAME = "http://injector:5002/"

ns = Collection()


# ---------------
# MISC COMMANDS
# ---------------
@task()
def start_emulator(c):
    """
    Start the emulator server engine located in images/ska-mid-cbf-emulators-emulator.
    """
    app = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "images",
        "ska-mid-cbf-emulators-emulator",
        "app.py",
    )
    subprocess.run(["python", app])


@task()
def start_injector(c):
    """
    Start the injector located in images/ska-mid-cbf-emulators-injector.
    """
    app = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "images",
        "ska-mid-cbf-emulators-injector",
        "app.py",
    )
    subprocess.run(["python", app])


ns.add_task(start_emulator, "start_emulator")
ns.add_task(start_emulator, "start_injector")


# ---------------
# GLOBAL REQUESTS
# ---------------
@task(
    help={
        "endpoint": "command endpoint",
        "hostname": "hostname url of emulator",
        "params": "query parameters",
    }
)
def get(c, endpoint, params="{}", hostname=DEFAULT_EMULATOR_HOSTNAME):
    """
    Perform a GET request to the emulator controller.

    Usage: invoke get <endpoint> [params_dict]
    """
    r = requests.get(hostname + endpoint, params=pyjson.loads(params))
    rich.print_json(pyjson.dumps(r.json()))


@task(
    help={
        "endpoint": "command endpoint",
        "hostname": "hostname url of emulator",
        "json": "json payload, or @path/to/file.json",
    }
)
def post(c, endpoint, json="{}", params="{}", hostname=DEFAULT_EMULATOR_HOSTNAME):
    """
    Perform a POST request to the emulator controller.

    Usage: invoke post <endpoint> [json_to_post] [params_dict]
    """
    if json.startswith("@"):
        path = json[1:]
        with open(path, "r") as f:
            json = f.read()
        print(f"Reading JSON from {path}")
    if json != "{}":
        print("POST request with JSON:")
        rich.print_json(json)
        print("RESPONSE:")
    r = requests.post(hostname + endpoint, json=pyjson.loads(json), params=pyjson.loads(params))
    rich.print_json(pyjson.dumps(r.json()))


ns.add_task(get)
ns.add_task(post)


# ---------------
# EMULATOR CONTROLLER API
# ---------------
controller = Collection("controller")


@task
def controller_graph(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Plot the IP block graph as a PNG image and store in output.png.

    Usage: invoke controller.graph
    """
    r = requests.get(hostname + "graph")
    with open("output.png", "wb") as f:
        f.write(r.content)
    print("Wrote output.png")


@task
def controller_server_healthcheck(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Verify the bitstream emulator API is accepting requests. For now, always returns 200 OK and an empty body.

    Usage: invoke controller.server-healthcheck
    """
    get(c, "server_healthcheck", hostname=hostname)


@task
def controller_state(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Get the current state of the emulator controller.

    Usage: invoke controller.state
    """
    get(c, "state", hostname=hostname)


@task
def controller_config(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Get the emulator configuration from this emulator's bitstream.

    Usage: invoke controller.config
    """
    get(c, "config", hostname=hostname)


@task
def controller_initial_signal(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Get the initial dish signal used to instantiate this bitstream emulator.

    Usage: invoke controller.initial_signal
    """
    get(c, "initial_signal", hostname=hostname)


@task
def controller_start(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Start the emulator controller if it is stopped. Pulses and event listeners will reactivate.

    Usage: invoke controller.start
    """
    post(c, "start", hostname=hostname)


@task
def controller_stop(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Stop the emulator controller if it is started. The API server will remain active but pulses and event listeners will be halted.

    Usage: invoke controller.stop
    """
    post(c, "stop", hostname=hostname)


@task
def controller_terminate(c, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Terminate the emulator controller.

    Stops all pulses and listeners, and tears down the API server, completely terminating execution.
    If used, the emulator script will have to be re-run to start again.

    Usage: invoke controller.terminate
    """
    post(c, "terminate", hostname=hostname)


controller.add_task(controller_graph, "graph")
controller.add_task(controller_server_healthcheck, "server_healthcheck")
controller.add_task(controller_state, "state")
controller.add_task(controller_config, "config")
controller.add_task(controller_start, "start")
controller.add_task(controller_stop, "stop")
controller.add_task(controller_terminate, "terminate")
ns.add_collection(controller)

# ---------------
# INJECTOR CONTROLLER API
# ---------------
injector = Collection("injector")


@task
def injector_inject(c, json, hostname=DEFAULT_INJECTOR_HOSTNAME):
    """Injects a JSON InjectorEvents request.

    Takes a posted JSON InjectorEvents request and injects them into the
    appropriate emulators.

    Usage: invoke injector.inject <json_to_inject>
    """
    post(c, "inject", json=json, hostname=hostname)


injector.add_task(injector_inject, "inject")
ns.add_collection(injector)

# ---------------
# DEVICE CONTROLLER API
# ---------------
ip = Collection("ip")


@task(
    help={
        "ip": "IP block name",
        "endpoint": "command endpoint",
        "hostname": "hostname url of emulator",
        "params": "query parameters",
    }
)
def ip_get(c, ip, endpoint, params="{}", hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Perform a GET request to an IP block.

    Usage: invoke ip.get <ip_block_id> <endpoint> [params_dict]
    """
    get(c, f"{ip}/{endpoint}", params=params, hostname=hostname)


@task(
    help={
        "ip": "IP block name",
        "endpoint": "command endpoint",
        "hostname": "hostname url of emulator",
        "json": "json payload, or @path/to/file.json",
    }
)
def ip_post(c, ip, endpoint, json="{}", params="{}", hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Perform a POST request to an IP block.

    Usage: invoke ip.get <ip_block_id> <endpoint> [json_to_post] [params_dict]
    """
    post(c, f"{ip}/{endpoint}", json=json, params=params, hostname=hostname)


ip.add_task(ip_get, "get")
ip.add_task(ip_post, "post")


# ---------------
# EMULATOR CONTROLLER API
# ---------------
@task
def ip_configure(c, ip, json, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Configure an IP block.

    Usage: invoke ip.configure <ip_block_id> <config_json>
    """
    ip_post(c, ip, "configure", json=json, hostname=hostname)


@task
def ip_deconfigure(c, ip, json, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Deconfigure an IP block.

    Usage: invoke ip.deconfigure <ip_block_id> <config_json>
    """
    ip_post(c, ip, "deconfigure", json=json, hostname=hostname)


@task
def ip_recover(c, ip, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Recover an IP block.

    Usage: invoke ip.recover <ip_block_id>
    """
    ip_post(c, ip, "recover", hostname=hostname)


@task
def ip_start(c, ip, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Start an IP block.

    Usage: invoke ip.start <ip_block_id>
    """
    ip_post(c, ip, "start", hostname=hostname)


@task
def ip_stop(c, ip, hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Stop an IP block.

    Usage: invoke ip.stop <ip_block_id>
    """
    ip_post(c, ip, "stop", hostname=hostname)


@task
def ip_status(c, ip, params="{}", hostname=DEFAULT_EMULATOR_HOSTNAME):
    """Get the status of an IP block.

    Usage: invoke ip.status <ip_block_id> [params_dict]
    """
    ip_get(
        c, ip, "status", params=params, hostname=hostname
    )


ip.add_task(ip_configure, "configure")
ip.add_task(ip_deconfigure, "deconfigure")
ip.add_task(ip_recover, "recover")
ip.add_task(ip_start, "start")
ip.add_task(ip_stop, "stop")
ip.add_task(ip_status, "status")
ns.add_collection(ip)
