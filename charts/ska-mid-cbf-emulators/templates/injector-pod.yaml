apiVersion: v1
kind: Pod
metadata:
  name: {{ .Values.injector.name }}
  labels:
    app: {{ .Values.injector.app_label }}
    internal_app: injector
spec:
  hostname: {{ .Values.injector.name }}
  containers:
    - name: {{ .Values.injector.name }}
      image: "{{ .Values.injector.image.repository }}:{{ .Values.injector.image.tag }}"
      imagePullPolicy: {{ .Values.injector.image.pullPolicy | default "IfNotPresent" }}
      ports:
      {{- range $port := .Values.injector.ports }}
        - name: {{ $port.name }} 
          containerPort: {{ $port.containerPort }} 
      {{- end }}
      env:
        - name: CONFIG_FILE
          value: /app/images/ska-mid-cbf-emulators-injector/configmap/config.json
      readinessProbe:
        httpGet:
          path: /server_healthcheck
          # use the port specified under name "api". if multiple are defined (which should never be the case), use the first one
          port: {{ range $port := .Values.injector.ports -}} {{- if eq $port.name "api" -}} {{- $port.containerPort -}} {{- break -}} {{- end -}} {{- end }}
        initialDelaySeconds: 10
        periodSeconds: 10
      volumeMounts:
        - name: injector-configmap-volume
          mountPath: /app/images/ska-mid-cbf-emulators-injector/configmap
  initContainers:
  - name: "init-{{.Values.injector.name}}"
    image: "{{ .Values.injector.image.repository }}:{{ .Values.injector.image.tag }}"
    imagePullPolicy: {{ .Values.injector.image.pullPolicy | default "IfNotPresent" }}
    env:
      - name: RABBITMQ_HOST
        value: "{{.Values.rabbitmq.host}}"
    command: ['sh', '-c', "until poetry run python .init_container; do echo \"Retrying...\"; sleep 1; done"]
  volumes:
  - name: injector-configmap-volume
    configMap:
      name: injector-configmap
---
# configmap used to pull in local json config that injector uses
apiVersion: v1
kind: ConfigMap
metadata:
  name: injector-configmap
  labels:
    app: {{ .Values.injector.app_label }}
    internal_app: injector
data:
  config.json: |-
    {
      "rabbitmq_host": "{{.Values.rabbitmq.host}}",
      "rabbitmq_port": 5672,
      "bitstream_emulators": [
        {{- $idx := 1 -}}
        {{- $total := len .Values.instances -}}
        {{- range $index, $instance := $.Values.instances -}}
        {{- $deviceId := $instance.deviceId | int }}
        {{- $scope := dict "deviceId" $deviceId "deviceId000" (printf "%03d" $deviceId) "receptorId" (mod (sub $deviceId 1) 3) }}
        {{- $emulatorId := tpl (default $instance.emulatorId | default $.Values.properties.emulatorId) $scope }}
        {{- $bitstreamId := tpl (default $instance.bitstreamId | default $.Values.properties.bitstreamId) $scope }}
        {{- $bitstreamVersion := tpl (default $instance.bitstreamVersion | default $.Values.properties.bitstreamVersion) $scope }}
        {
          "bitstream_emulator_id": "{{ $emulatorId }}",
          "bitstream_id": "{{ $bitstreamId }}",
          "bitstream_ver": "{{ $bitstreamVersion }}"
        }{{- if ne $idx $total -}},{{- end -}}
        {{- $idx = add $idx 1 -}}
        {{- end }}
      ]
    }
