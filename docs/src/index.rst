SKA Mid CBF Emulators
===============================
.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

.. GLOSSARY ======================================================
.. toctree::
  :maxdepth: 2
  :caption: Glossary

  glossary

.. EMULATORS =====================================================
.. toctree::
  :maxdepth: 2
  :caption: Emulators

  emulator_subpages/emulator_usage
  emulator_subpages/emulator_configuration
  emulator_subpages/ip_block_emulators
  emulator_subpages/emulator_api
  emulator_subpages/injector
  emulator_subpages/sequence_diagrams

.. AUTO-GENERATED ================================================
.. toctree::
  :maxdepth: 2
  :caption: Emulator Development

  emulator_dev_subpages/fw_dev_container_setup

.. TEST =============================================================
.. toctree::
  :maxdepth: 2
  :caption: Auto-Generated Pages

  generated_diagrams
  generated_code

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`