Glossary
********

.. glossary:: :sorted:

   emulator
      A software system which mocks hardware functionality. There are two levels
      of emulator for SKA: a :term:`bitstream emulator`, which represents a full
      FPGA bitstream, and :term:`IP block emulators<IP block emulator>`, which represent individual
      IP blocks on an FPGA card. The bitstream emulator drives the connection
      between all of the IP block emulators.

   bitstream emulator
   top-level emulator
      An :term:`emulator` for a full FPGA bitstream. It is the container for the
      :term:`emulator engine`, which constructs and creates the connections
      between :term:`IP block emulators<IP block emulator>`, as well as the emulator :term:`controller`, which exposes
      the APIs for each IP block emulator, plus some top-level endpoints.

   IP block emulator
      An :term:`emulator` for a single IP block on an FPGA card. Its functionality is written
      via a generic and very simplified interface (see: :ref:`IP Block Emulators`),
      which is stored along with the full FPGA bitstream, and is then pulled in at runtime
      by the :term:`emulator engine` to construct the full IP block emulator.
      It mocks only at a high level some of the state changes, signal updates,
      and driver API methods of the original IP block.

   engine
   emulator engine
      The set of services which construct, connect and drive :term:`IP block emulators<IP block emulator>`,
      based on the IP block emulator definitions in the FPGA bitstream.

   controller
   emulator controller
      The top-level interface and service which controls and drives the :term:`emulator engine`
      and its :term:`IP block emulators<IP block emulator>` for a single :term:`bitstream emulator`.

   module
      The full set of data for an :term:`IP block emulator`, including emulator components,
      the subcontroller, and all associated :term:`RabbitMQ` :term:`queues<queue>` and :term:`exchanges<exchange>`.

   RabbitMQ
      The message broker used by the :term:`emulator`. It uses :term:`queues<queue>` and :term:`exchanges<exchange>`
      to receive and distribute messages within and across many services at scale, with the theoretical capability
      to handle millions of messages with minimal to no loss of performance. (This capability is yet to be seen
      from an emulator perspective.) See: `RabbitMQ Documentation <https://www.rabbitmq.com/tutorials>`_.

   queue
   pulse queue
   manual queue
   signal update queue
      In the context of an :term:`emulator`, a queue is a RabbitMQ mechanism
      which receives and stores messages from an :term:`exchange`, until they are consumed.
      The emulator :term:`subcontrollers<subcontroller>` use queues to receive
      :term:`pulses<pulse>`, :term:`signal updates<signal update>`, :term:`manual events<manual event>`, and API requests.

   exchange
   pulse exchange
   manual exchange
   signal update exchange
      In the context of an :term:`emulator`, an exchange is a RabbitMQ mechanism
      which distributes published messages to one or more :term:`queues<queue>`.
      The emulator :term:`subcontrollers<subcontroller>` use exchanges to send
      messages to downstream :term:`IP block emulators<IP block emulator>`.

   subcontroller
      The interface and service which wraps and exposes the components
      of an :term:`IP block emulator` as well as provides some common functionality
      which can be leveraged within the components. It is responsible for sending
      and receiving messages via :term:`RabbitMQ` under the hood, and distributing
      the content of those messages to the components as required.

   event
      A message sent from one :term:`IP block emulator` to another, containing
      some data for the recipient to handle as well as some metadata.
      Event types include :term:`pulses<pulse>`, :term:`signal updates<signal update>`, and :term:`manual events<manual event>`.

   components
   emulator components
      The simplified building blocks of an IP block emulator
      which are stored within the FPGA bitstream.
      These include the :term:`API<API (component)>`, the :term:`event handler<event handler (component)>`, the :term:`state machine<state machine (component)>`,
      and the :term:`simulated IP block<IP block (component)>` (see: :ref:`IP Block Emulators`).

   API (component)
      The :term:`emulator component<components>` which defines the outward-facing REST API for an :term:`IP block emulator`.
      The IP block emulator API always implements a (non-strict) superset of the respective firmware driver API,
      such that the two interfaces may be loaded arbitrarily and interchangeably by a higher-level device for interaction.
      See: :ref:`IPAPI`

   event handler (component)
      The :term:`emulator component<components>` which takes in :term:`events<event>`,
      processes them, potentially updates them, and returns new or updated events
      to send to the next downstream blocks. See: :ref:`IPEventHandler`

   state machine (component)
      The :term:`emulator component<components>` which maintains the state of an :term:`IP block emulator`
      and handles any transitions between those states. The component defines a set of possible states,
      a set of possible triggers which may cause state transitions, and a set of all possible state transitions.
      See: :ref:`IPStateMachine`

   IP block (component)
   simulated IP block (component)
      The :term:`emulator component<components>` which contains IP block-specific data,
      such as variables representing register values on an IP block, or simple methods to mutate such values.
      It is intended to be used primarily within other components, such as the :term:`API<API (component)>`,
      e.g. for operations where registers or other data should be changed.
      See: :ref:`IPIPBlock`

   pulse
   pulse event
      An :term:`event` used as the primary synchronization and timing mechanism
      for a :term:`bitstream emulator` and its :term:`IP block emulators<IP block emulator>`.
      It contains a timestamp (generated by the DISH) which remains static throughout the signal chain,
      and when received by an IP block emulator, informs its :term:`subcontroller` to pull the
      latest :term:`signal updates<signal update>` and :term:`manual events<manual event>`
      from their respective :term:`queues<queue>` for processing.
      Pulse events should generally contain no data of their own except for the DISH timestamp.
      They also have a strict multiplicity of 1, i.e. any given IP block emulator
      may only ever output 1 pulse event with a given timestamp, and any such IP block emulator
      which receives pulse events from multiple inputs with the same timestamp
      may only ever process 1 of those events (in the current model, the first received pulse event is kept,
      and all others with the same timestamp are discarded).

   signal update
   signal update event
      An :term:`event` which provides data updates about the emulated signal from the DISH.
      It follows the same path as :term:`pulse events<pulse>`, however it is only generated from the DISH
      on the first pulse and then only by request in any block when data changes (for example if some values
      in an :term:`IP block emulator` are configured that will change the downstream state of the signal).
      Signal updates from the previous IP block emulator(s) with a certain DISH timestamp
      are processed upon receipt of a pulse event with the same DISH timestamp.
      Additionally, signal update events, unlike pulse events, have unrestricted multiplicity,
      i.e. for any given pulse, an IP block emulator may process an unlimited number
      of input signal update events with the same timestamp,
      and may output an unlimited number of new or updated events to downstream blocks.
 
   manual event
      An :term:`event` which can only be triggered via manual request, either within
      a :term:`component<components>` or through the :term:`event injector`.
      Manual events, as opposed to :term:`pulse events<pulse>` and :term:`signal updates<signal update>`,
      contain a timestamp separate from the DISH time. Manual events are processed
      upon receipt of a pulse event with a DISH timestamp greater than or equal to their own timestamps
      (e.g. a manual event with timestamp 1234 will not be processed on a pulse event with timestamp 1000,
      but will be processed on the next pulse event with timestamp 2000).
      Manuel events have unlimited multiplicity, but are always processed individually,
      as opposed to signal updates which are processed as an entire array at once.
      Formerly known as a "processing event" (this term is now deprecated).

   injector
   event injector
      A global service (relative to any set of :term:`bitstream emulators<bitstream emulator>`) with the ability to inject arbitrary :term:`manual events<manual event>`
      into the manual event :term:`queue` of any :term:`IP block emulator` of any bitstream emulator at an arbitrary specified time.
      It lives as a separate process from all emulators and exposes a single API endpoint, which takes in a JSON file
      specifying a list of event groups, each containing events to send to a particular IP block emulator.
      See: :ref:`Injector`

