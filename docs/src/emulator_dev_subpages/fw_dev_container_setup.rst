Emulator Development Using the FW-Dev Container
***********************************************

To help facilitate development of the fw emulators a development container has been created that contains all the tools necessary in order to successfully build a new emulator block.

The dev container comes with the following:

    * GCC / G++ 13
    * Python 3.12
    * Poetry
    * Cloned ska-mid-cbf-bitstreams project located inside the /emulator_dev directory

Prerequisites
=============
    * VS Code
        * Extensions:
            * Dev Containers


Setup and Configuration
=======================

SSH into Remote Dev Server
--------------------------

First using VS Code we need to connect to the server that will run the FW development container.

.. note:: 

    This will only need to be done once or when using a new remote server.

#. Open VS Code 

#. Check that the ``Dev Containers`` extension is installed by clicking the ``Extensions`` icon from the side menu bar.

    .. image:: images/vscode_extensions_1.png
        :width: 600

    .. note::
        
        If it's not installed, search for it using the ``Extensions`` search and click install.

    .. image:: images/vscode_dev_containers_ext.png 
        :width: 600

#. Open the ``Remote Explorer`` from the side menu.

    .. image:: images/remote_explorer.png
        :width: 600

#. Select ``Remotes (Tunnels/SSH)`` from the top right dropdown.

    .. image:: images/remote_explorer_ssh_menu.png
        :width: 600

#. Add a new SSH Connection using the ``+`` icon next to ``SSH``.

    .. image:: images/add_ssh_connection_1.png
        :width: 600

#. Enter username and remote address for SSH connection, e.g.: ``<username>@rmdskadevdu002.mda.ca``

    .. image:: images/add_ssh_connection_2.png
        :width: 600

#. Next, select the SSH config file in your users directory to update.

    .. image:: images/add_ssh_connection_3.png
        :width: 600

#. Connect to the server by clicking the ``Connect in Current Window`` or ``Connect in New Window`` option.

    .. image:: images/add_ssh_connection_4.png
        :width: 600

#. Once inside, open the explorer from the side menu and ``Open Folder`` on the server.

    .. image:: images/remote_server_1.png
        :width: 600 


Starting the FW Dev Container
----------------------------------

Initially after you have SSH'd into the remote server, you have to clone the project and start up the FW Dev Container.

.. note:: 
    
    If you have already cloned and started the FW dev container, skip to the next section: :ref:`Attaching to the FW Dev Container`.

#. Run the following to clone the ``ska-mid-cbf-emulators`` project:

    .. code-block:: bash

        git clone https://gitlab.com/ska-telescope/ska-mid-cbf-emulators
    
#. Navigate inside the ``ska-mid-cbf-emulators`` project and run the following to initiate the makefile:

    .. code-block:: bash
        
        git submodule init
        git submodule update

#. Run the following to start the FW dev container:

    .. code-block:: bash

        make run-fw-dev WORKSPACE=<path to your workspace>

    .. note:: 
        
        If WORKSPACE is omitted the default workspace location is ~/workspace

#. If successful, the following should be displayed in the terminal:

    .. image:: images/fw_dev_container_running.png
        :width: 600


Attaching to the FW Dev Container
---------------------------------
#. Press ``Ctrl-Shift-P`` or ``F1`` from within VS Code to open the command search prompt.

#. Search for the following and run it from the opened prompt:

    .. code-block:: text

        Dev Containers: Attach to running container...

    .. image:: images/attach_to_dev_container_1.png
        :width: 600

#. Attach to the container named ``/emulator-dev-<your user name>``

    .. image:: images/attach_to_dev_container_2.png
        :width: 600


.. note::

    The following steps from 4 - 6 only need to be performed once, on the initial run of the FW dev container.

4. If this is the first time attaching to the container, one extra step must be completed. Press ``Ctrl-Shift-P`` or ``F1`` to open the command search prompt.

#. Search for and run the following command:

    .. code-block:: text

        Dev Containers: Open Container Configuration File

    .. image:: images/attach_to_dev_container_3.png
        :width: 600

#. In the configuration file, add the following extra item to the list and save.
   This will make it so any files you create / save will match your user on the host machine,
   as the ``fwdev`` user is set up to match your user's UID and GID:

    .. code-block:: text

        "remoteUser": "fwdev"

    .. image:: images/attach_to_dev_container_4.png
        :width: 600

#.  To finish enabling the ``fwdev`` user, close the VS Code window that's attached to the container, and then re-attach it following from step 1.


Running an Interactive Shell
----------------------------
To run an interactive shell inside the dev container, from the dev server running the container, run:

    .. code-block:: bash

        docker exec -it emulator-dev-<your username> bash

OR if within the emulators repository, you can run:

    .. code-block:: bash

        make enter-fw-dev


Running the Emulator
---------------------
Once any changes to the IP block emulators have been made and you would like to test them,
from within the FW dev container you can start the bitstream emulator by running the following:

    .. code-block:: text

        run-emulator [options]

This will spin up the emulator engine and automatically pull in your changes from the /workspace directory.
See :ref:`Run the emulator in the dev environment` or use `--help` for the list of allowed options.

    .. note::

        For changes to be applied, you must terminate and restart the bitstream emulator.

Once the bitstream emulator is running, please see the following
:ref:`list of emulator commands <Interact with the Emulator and Injector APIs>`
for some commands you can send to the emulator and/or injector APIs from within the FW dev container.


Teardown
--------

.. note::

    This is to be performed from the server you are SSH'd into, not the FW dev container.

1. If open, close the attached dev container VS Code window, then from the server you're SSH'd into, navigate into the ``ska-mid-cbf-emulators`` project.

2. Run the following to shut down the FW dev container:

    .. code-block:: bash

        make remove-fw-dev WORKSPACE=<path to your workspace>

3. On successful shutdown, the output to the terminal should be as follows:

    .. image:: images/container_shutdown.png
        :width: 600