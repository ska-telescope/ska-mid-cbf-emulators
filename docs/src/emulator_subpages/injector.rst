Injector
********

The injector is used to inject events directly into specific IP block emulators at provided timestamps/offsets.

Injector Configuration
======================
If the injector is being run through kubernetes/minikube,
the injector config is passed automatically as part of the Helm chart (see :ref:`Helm Configuration`).
Otherwise, the injector will fall back to
`default_config.json <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-injector/default_config.json>`_.
This configuration specifies RabbitMQ connection info as well as a list of emulators and their configuration versions.
Example configuration:

.. code-block:: javascript

  {
    "rabbitmq_host": "rabbitmq",
    "rabbitmq_port": 5672,
    "bitstream_emulators": [
      {
        "bitstream_emulator_id": "fhs-vcc-emulator-1",
        "bitstream_id": "agilex-vcc",
        "bitstream_ver": "0.0.16"
      },
      {
        "bitstream_emulator_id": "fhs-vcc-emulator-2",
        "bitstream_id": "agilex-vcc",
        "bitstream_ver": "0.0.16"
      }
    ]
  }


Using the Injector / API
========================
Injecting events into an emulator is as simple as sending an API request.
This may contain one or several events at a time, so long as the JSON follows the above specification.

If running via minikube/k8s (the preferred way), the hostname for the injector is

.. code-block:: text

  injector-service.ska-mid-cbf-emulators.svc.cluster.local:5002

which can be accessed via any pod in the cluster, e.g.

.. code-block:: bash

  curl -v -d @events_body.json -H 'Content-Type: application/json' injector-service.ska-mid-cbf-emulators.svc.cluster.local:5002/inject

Otherwise, for Docker, it is ``http://injector:5002`` (preferred by Docker) or ``http://localhost:5002``
and is only accessible from within the same container as the running injector.

Inject events
+++++++++++++

.. http:post:: /inject

  Inject events into an emulator.

  **Example request**:

  .. code-block:: bash

    curl -v -d @events_body.json -H 'Content-Type: application/json' injector-service.ska-mid-cbf-emulators.svc.cluster.local:5002/inject

  ``events_body.json`` follows
  `this schema <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-injector/emulator_injector/schemas/injector_event_group_list.py>`_,
  of which an example looks like:

  .. code-block:: javascript

    {
      "injector_event_groups": [
        {
          "bitstream_emulator_id": "fhs-vcc-emulator-1",
          "ip_block_emulator_id": "ethernet_200g",
          "events": [
            {
              "severity": "WARNING",
              "value": {
                "id": "inj01234",
                "injection_type": "update_link_badness",
                "message": "Degrade the link.",
                "badness": 0.75
              },
              "offset": 10000
            },
            {
              "severity": "GENERAL",
              "value": {
                "id": "inj01235",
                "injection_type": "update_link_badness",
                "message": "Fix the link.",
                "badness": 0.2
              },
              "offset": 20000
            }
          ]
        },
        {
          "bitstream_emulator_id": "fhs-vcc-emulator-2",
          "ip_block_emulator_id": "wideband_input_buffer",
          "events": [
            {
              "severity": "FATAL_ERROR",
              "value": {
                "id": "inj01236",
                "injection_type": "fault",
                "message": "The Wideband Input Buffer has been hit by a meteorite."
              },
              "timestamp": 1727740800000
            }
          ]
        }
      ]
    }

  **Example response**:

  .. code-block:: javascript

    {}

  :reqjson list[object] injector_event_groups: a list of event groups, in which each event group specifies
                                               a list of events for a specific IP block in a specific bitstream emulator.
  :reqjson string bitstream_emulator_id: the bitstream emulator ID to inject this group of events into.
  :reqjson string ip_block_emulator_id: the IP block emulator ID within the specified bitstream emulator to inject this group of events into.
  :reqjson list[object] events: a list of event objects to inject.
  :reqjson string severity: the severity of this event. Must be a member of the
                            `EventSeverity <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/src/ska_mid_cbf_emulators/common/models/event/event_severity.py>`_ enum.
  :reqjson string offset: an offset, in milliseconds from the start of injection, by which to delay this event's injection.
                          May not be used in conjunction with ``timestamp``.
  :reqjson string timestamp: a specific timestamp, in milliseconds from the emulator's epoch,
                             at which to trigger this event's injection. May not be used in conjunction with ``offset``.
  :reqjson object value: an arbitrary value object to inject.
                         Must contain an ``injection_type`` property, but is otherwise unrestricted.
    :reqjson string injection_type: an arbitrary string representing the type of injection. 

  :reqheader Content-Type: application/json
  :resheader Content-Type: application/json

  :statuscode 200: Injections successfully queued.
  :statuscode 400: The provided body encountered validation errors.


DISH Injection
++++++++++++++

The DISH also supports certain injected events in order to change signal values during runtime.
The ``update_signal_properties`` injection type allows you to update specific signal values,
and the ``update_signal`` injection type allows you to update the entire signal all at once.
See `the DISH injection schema <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/emulator_engine/schemas/dish_injection.py>`_ for more details.

For example:

  .. code-block:: javascript

    {
      "injector_event_groups": [
        {
          "bitstream_emulator_id": "fhs-vcc-emulator-1",
          "ip_block_emulator_id": "dish",
          "events": [
            {
              "severity": "GENERAL",
              "value": {
                "id": "inj1000",
                "injection_type": "update_signal_properties",
                "message": "Update the sample rate.",
                "updated_properties": {
                  "sample_rate": 3_900_123_456
                }
              },
              "offset": 10000
            },
            {
              "severity": "GENERAL",
              "value": {
                  "id": "inj1001",
                  "injection_type": "update_signal",
                  "message": "Change the DISH signal.",
                  "signal": {
                      "packet_rate": 1_500_123.5,
                      "sample_rate": 3_900_456_789,
                      "dish_id": "MKT004",
                      "band_id": "4",
                      "regular_sky_power": [0.7, 0.9],
                      "noise_diode_on_power": [0.8, 0.95],
                      "duty_cycle": 0.79
                  }
              }
              "offset": 20000
            }
          ]
        }
      ]
    }
