Emulator APIs
*************

Each bitstream emulator will have a number of API routes defined in individual IP block emulators,
as well as a number of standardized routes across all emulators.

If running via minikube/k8s, the hostname for each bitstream emulator is

.. code-block:: text

  <bitstream_emulator_id>.<namespace>.svc.cluster.local:5001

which can be accessed via any pod in the cluster, e.g.

.. code-block:: bash

  curl -v fhs-vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/ethernet_200g/status

Otherwise, for Docker, it is ``http://emulator:5001`` (preferred by Docker) or ``http://localhost:5001``
and is only accessible from within the same container as the running emulator.

Standardized Routes
===================
Controller API
--------------
Each bitstream emulator has the following top-level API routes:

Stop
++++

.. http:post:: /stop

  Stop the emulator, but do not shutdown the API server. Useful to temporarily stop generating logs.
  Will allow the most recent pulse to finish flowing through the system.

  **Example request**:

  .. code-block:: bash

    curl -v -X POST vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/stop

  :statuscode 202: Request to stop has been accepted and should complete shortly.
  :statuscode 409: The emulator is not currently in a state in which it is allowed to stop.

Start
+++++

.. http:post:: /start

  Start the emulator from a stopped state.

  **Example request**:

  .. code-block:: bash

    curl -v -X POST vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/start

  :statuscode 202: Request to start has been accepted and should complete shortly.
  :statuscode 409: The emulator is not currently in a state in which it is allowed to start.

Terminate
+++++++++

.. http:post:: /terminate

  Terminate/teardown the emulator entirely. Will gracefully end all subscriptions and then tear down the API server,
  ending the script (note that it may take a few seconds for all subscriptions to finish). If running in a pod, can be used to force a restart.

  **Example request**:

  .. code-block:: bash

    curl -v -X POST vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/terminate

  :statuscode 202: Request to terminate has been accepted and should complete shortly.
  :statuscode 409: The emulator is not currently in a state in which it is allowed to terminate.

State
+++++

.. http:get:: /state

  Get the current state of the emulator controller.
  This does not necessarily represent the state of any underlying IP block emulators.

  **Example request**:

  .. code-block:: bash

    curl -v vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/state

  **Example response**:

  .. code-block:: javascript

    {
      "current_state": "IDLE"
    }

  :resjson string current_state: the current state of the emulator controller.

  :resheader Content-Type: application/json

  :statuscode 200: Retrieved successfully.

Config
++++++

.. http:get:: /config

  Get the contents of the config.json file being used by the bitstream emulator.
  Useful to verify that the correct configuration was loaded when debugging/testing.

  **Example request**:

  .. code-block:: bash

    curl -v vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/config

  **Example response**:

  .. code-block:: javascript

    {
      "id": "vcc",
      "version": "0.0.1",
      "ip_blocks": [
        {
          "id": "ethernet_200g",
          "display_name": "200Gb Ethernet MAC",
          "type": "ethernet_200g_r4",
          "downstream_block_ids": [],
          "constants": {
            "num_fibres": 4,
            "num_lanes": 4
          }
        }
      ],
      "first": ["ethernet_200g"]
    }

  :resheader Content-Type: application/json

  :statuscode 200: Retrieved successfully.

Initial Signal
++++++++++++++

.. http:get:: /initial_signal

  Get the contents of the initial_signal.json file that was used to instantiate the bitstream emulator.
  Useful to verify that the correct initial signal was loaded when debugging/testing.

  **Example request**:

  .. code-block:: bash

    curl -v vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/initial_signal

  **Example response**:

  .. code-block:: javascript

    {
      "packet_rate": 1500000,
      "sample_rate": 3900000000,
      "dish_id": "MKT001",
      "band_id": "1",
      "regular_sky_power": [0.4, 0.5],
      "noise_diode_on_power": [0.5, 0.6],
      "duty_cycle": 0.50
    }

  :resheader Content-Type: application/json

  :statuscode 200: Retrieved successfully.

Server Healthcheck
++++++++++++++++++

.. http:get:: /server_healthcheck

  Get True if the API server is running and the emulator controller is not in an error state.
  Used to determine the health state of a pod.

  **Example request**:

  .. code-block:: bash

    curl -v vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/server_healthcheck

  **Example response**:

  .. code-block:: javascript

    {}

  :resheader Content-Type: application/json

  :statuscode 200: Server is running and emulator is not in an error state.
  :statuscode 404: Server is not running or is not ready to accept requests.
  :statuscode 500: Emulator is in an error state.

Graph
+++++

.. http:get:: /graph

  Get a PNG image of a graph defined by the currently loaded config file,
  representing all of the connections between IP blocks.

  **Example request**:

  .. code-block:: bash

    curl -v -o test_graph.png vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/graph

  **Example response**:

  .. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: image/png

  :resheader Content-Type: image/png

  :statuscode 200: Retrieved successfully.

IP Block Emulator API
---------------------
Each IP block emulator also has the following standard API routes (in addition to their individual API implementations):

State
+++++

.. http:get:: /(string:ip_block_id)/state

  Get the current state of the IP block emulator. States will vary between blocks depending on their implementations.

  **Example request**:

  .. code-block:: bash

    curl -v vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/ethernet_200g/state

  **Example response**:

  .. code-block:: javascript

    {
      "current_state": "RESET"
    }

  :param string ip_block_id: the IP block emulator ID to send the request to.
  :resjson string current_state: the current state of the specified IP block emulator.

  :resheader Content-Type: application/json

  :statuscode 200: Retrieved successfully.

State Diagram
+++++++++++++

.. http:get:: /(string:ip_block_id)/state_diagram

  Get a PNG image of a state-transition diagram representing the state machine of the IP block emulator,
  including the current/previous state.

  **Example request**:

  .. code-block:: bash

    curl -v -o test_diagram.png vcc-emulator-1.ska-mid-cbf-emulators.svc.cluster.local:5001/ethernet_200g/state_diagram

  **Example response**:

  .. sourcecode:: http

    HTTP/1.1 200 OK
    Content-Type: image/png

  :param string ip_block_id: the IP block emulator ID to send the request to.
  :query boolean contextOnly: Generate only a small section of the state machine representing the current state,
                              previous state, and currently available transitions.

  :resheader Content-Type: image/png

  :statuscode 200: Retrieved successfully.
