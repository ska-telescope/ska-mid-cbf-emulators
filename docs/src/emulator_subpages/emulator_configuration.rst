Bitstream Emulator Configuration
********************************

.. note::
  
  All configuration changes require terminating and re-running the emulator in order to take effect.

A bitstream emulator is configured via a set of JSON configuration files.

Bitstream emulator configuration file
-------------------------------------
A bitstream emulator configuration file specifies which IP blocks are included as part of that emulator,
and how they are ordered/connected. This is expected to be included with the bitstream package
pulled from CAR. An example file looks like this:

.. code-block:: javascript
  
  {
    "id": "vcc_processing",
    "version": "0.0.1",
    "ip_blocks": [
      {
        "id": "ethernet_200g",
        "display_name": "200Gb Ethernet",
        "type": "ftile_ethernet",
        "downstream_block_ids": [
          "packet_validation"
        ],
        "constants": {
          "num_fibres": 4,
          "num_lanes": 4
        }
      },
      {
        "id": "packet_validation",
        "display_name": "Packet Validation",
        "type": "packet_validation",
        "downstream_block_ids": [
          "wideband_input_buffer"
        ],
        "constants": {
          "expected_ethertype": 65261
        }
      }
    ],
    "first": ["ethernet_200g"]
  }

* ``id`` is the ID of this configuration, used for semantic versioning.
* ``version`` is the semantic version of this configuration file, relative to the ID.
  For example, the configuration with ID ``vcc`` may have a version ``0.0.1``, a version ``0.0.2``, a version ``0.1.0``, etc.
* ``ip_blocks`` is the list of IP blocks included in this bitstream emulator:

  * ``id`` is the ID to use throughout the bitstream emulator for this IP block and its IP block emulator.
  * ``display_name`` is a human-readable name used for user-facing display purposes.
  * ``type`` is the type of IP block, used to load the correct IP block emulator. 
    This must match the name of an available IP block emulator (which in turn must be the name of a valid IP block repository).
  * ``downstream_block_ids`` is a list of IDs of other IP blocks which are directly connected to this one downstream
    (i.e. if ``block_1`` passes data directly to ``block_2``, then ``block_2`` should be included in ``block_1``'s ``downstream_block_ids`` list).
  * ``constants`` is a dictionary of arbitrary constants to set on the simulated IP block at creation time.
    For example, if you set ``"my_const": 1234`` here, then ``ip_block.my_const`` will be accessible within the emulator, with value ``1234``.

* ``first`` is a list of the first IP block(s) in the chain, i.e. those which should receive a pulse directly from the DISH.
  If multiple IP blocks are specified here, they will all be sent the same pulse from the DISH in parallel.


Validation
==========
Any set of bitstream emulator configurations may be validated using the helper script 
`validate_bitstream_files.py <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/validate_bitstream_files.py>`_.
Use the ``-c <config_file>`` to check a provided config file (this option can be provided multiple times to check multiple files at once).
This script uses the same validation functionality as the full emulator, so if validation passes here, it should pass in the real thing too.
See the script file or run ``poetry run python validate_bitstream_files.py --help`` for more details.

Initial signal configuration file
---------------------------------
The initial signal configuration file, located by default at
`initial_signal.json <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/initial_signal.json>`_,
specifies the contents of the signal that will be sent from the DISH alongside the first pulse
(for example, the sample rate, dish ID, band ID, etc).

To override the default, an alternative file can be specified via the ``-s/--signal-config-file`` option when running the emulator,
or a custom set of values can be provided via Helm (see :ref:`Helm Configuration` for details).

The exact specification of this file is subject to change, so for the most up to date information,
check the schema here: `dish_signal.py <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/emulator_engine/schemas/dish_signal.py>`_

To make changes to the specification (adding or removing properties, or changing property names or types),
many or all of the following locations need to be updated:

* `DishSignal schema <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/emulator_engine/schemas/dish_signal.py>`_,
  to update the specification itself.
* `initial_signal.json <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/initial_signal.json>`_,
  to align with the updated specification.
* `DishSignal.__init__ <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/emulator_engine/features/dish/signal.py>`_,
  to update the Python class associated with the specification.
* `DishEventHandler._generate_signal_update <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/images/ska-mid-cbf-emulators-emulator/emulator_engine/features/dish/event_handler.py>`_,
  to align with the updated Python class associated with the specification.
* The integration test `test_inject_to_dish <https://gitlab.com/ska-telescope/ska-mid-cbf-emulators/-/blob/main/tests/integration/injector/with_engine/injector_api_test.py>`_,
  possibly in several places depending on the change, to align the test injector events with the updated signal spec.
