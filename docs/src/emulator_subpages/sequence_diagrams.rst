Sequence Diagrams
*****************

Opening the images in a new tab is recommended to view them at higher resolution.

Previous Design Example Flow
============================
.. image:: /diagrams/pulse_proc_sequence.svg

New Design Example Flow
=======================
.. image:: /diagrams/pulse_signal_manual_sequence.svg

Divergence/Convergence Example
==============================
.. image:: /diagrams/convergence.svg
