"""Script for generating custom auto-generated documentation pages."""
from __future__ import annotations

from abc import ABC, abstractmethod
import re
from typing import override
from ska_mid_cbf_emulators.common import FiniteStateMachine

class PageGenerator(ABC):
    """Base generator class for generating custom rST pages.

    Do not use this class directly; create a new subclass for any new pages.

    Attributes:
        PAGE_HEADER (:obj:`str`): The header to be written at the start of the file (before the body).
            Most or all subclasses should override this field.
        PAGE_FOOTER (:obj:`str`): The footer to be written at the end of the file (after the body).
            Most or all subclasses should override this field.
        PAGE_FILENAME (:obj:`str`): The filename to write to (e.g. `'my_page.rst'`).
            All subclasses must override this field.
        DIR_PATH (:obj:`str`): The parent directory for the generated page and any other data/images/etc.
            Subclasses should not override this field.
    """
    PAGE_HEADER: str = ''
    PAGE_FOOTER: str = '\n'
    PAGE_FILENAME: str = None

    DIR_PATH: str = 'src/_generated_pages'

    @classmethod
    def generate(cls: PageGenerator) -> None:
        """Generate this page."""
        with open(f'{cls.DIR_PATH}/{cls.PAGE_FILENAME}', 'wt') as docpage:
            docpage.write(cls.PAGE_HEADER)
            docpage.write(cls._generate_body())
            docpage.write(cls.PAGE_FOOTER)

    @classmethod
    @abstractmethod
    def _generate_body(cls: PageGenerator, *args, **kwargs) -> str:
        """Generate the body for this page."""
        return ''


class STDPageGenerator(PageGenerator):
    """Generator class for the auto-generated state-transition diagram page.

    Implements :obj:`PageGenerator`.

    Attributes:
        PAGE_HEADER (:obj:`str`): The header that will be written at the start of the file (before the body).
        PAGE_FOOTER (:obj:`str`): The footer that will be written at the end of the file (after the body).
        PAGE_FILENAME (:obj:`str`): The filename to which the page will be written.
        DIR_PATH (:obj:`str`): The parent directory for the generated page and any other data/images/etc.
            This is passed from the base class and should not be overwritten.
        FSMS (:obj:`list[tuple[str, FiniteStateMachine]]`): The list of state machines for which diagrams will be generated.
            Each entry is of the form `(display_name, state_machine)`.
    """
    PAGE_HEADER = """\
Emulator State-Transition Diagrams
**********************************
Auto-generated state-transition diagrams for various IP block emulators. For readability reasons, these are not intended \
as finalized/production diagrams, but more as a quick reference to the current implementations \
of the various emulators' state machines.

"""

    PAGE_FOOTER = """
"""

    PAGE_FILENAME = 'stds.rst'

    FSMS: list[tuple[str, FiniteStateMachine]] = [
        # ('100Gb Ethernet MAC', MacStateMachine()),
        # ('Wideband Input Buffer', WIBStateMachine()),
        # ('Very Coarse Channelizer (20 Channel Version)', VccCh20StateMachine())
    ]

    @override
    @classmethod
    def _generate_body(cls: STDPageGenerator, *args, **kwargs) -> str:
        body = ''

        for dname, fsm in cls.FSMS:
            iname = f'img/std_{re.sub("[^A-Za-z0-9]", "", dname)}.png'
            fsm.export_diagram_full(f'{cls.DIR_PATH}/{iname}', title=dname, format='png')
            body = body + (
                f'{dname.strip()}\n'
                f'{'=' * len(dname.strip())}\n'
                '\n'
                f'.. figure:: ./{iname}\n'
                '  :align: center'
                '\n\n'
            )

        return body


if __name__ == '__main__':
    # Add generators here if desired (e.g. [STDPageGenerator, MyNewGenerator, ...])
    generators_to_execute: list[PageGenerator] = []

    for generator in generators_to_execute:
        generator.generate()
