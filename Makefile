# define private overrides for above variables in here
-include PrivateRules.mak

# W503: "Line break before binary operator." Disabled to work around a bug in flake8 where currently both "before" and "after" are disallowed.
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=DAR201,W503,E731

# F0002, F0010: Astroid errors. Not our problem.
# E0401: Import errors. Ignore for now until we figure out our actual project structure.
# E0202: Method hidden. Trips false positives when using certain class inheritance structures and is unlikely to catch any real problems.
PYTHON_SWITCHES_FOR_PYLINT = --disable=E0401,F0002,F0010,E0202
PYTHON_SWITCHES_FOR_PYLINT_LOCAL = --disable=E0401,F0002,F0010,E0202

# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/*.mk

PYTHON_LINT_TARGET = ./src/ ./images/ska-mid-cbf-emulators-emulator/ ./images/ska-mid-cbf-emulators-injector/
PYTHON_LINE_LENGTH = 130
POETRY_PYTHON_RUNNER = poetry run python3.12 -m

#
# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
CAR_OCI_USE_HARBOR = true
CAR_OCI_REGISTRY_HOST = harbor.skao.int/staging
PROJECT = ska-mid-cbf-emulators


# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE = ska-mid-cbf-emulators
CLUSTER_DOMAIN ?= cluster.local

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME = ska-mid-cbf-emulators

OCI_IMAGES = ska-mid-cbf-emulators-emulator ska-mid-cbf-emulators-injector ska-mid-cbf-emulators-fw-dev ska-mid-cbf-emulators-base
OCI_IMAGES_TO_PUBLISH = ska-mid-cbf-emulators-emulator ska-mid-cbf-emulators-injector
OCI_IMAGE_BUILD_CONTEXT = $(PWD)
#OCI_BUILD_ADDITIONAL_ARGS = --no-cache
##export DEBUG_DIRTY=true

HELM_CHART ?= ska-mid-cbf-emulators## HELM_CHART the chart name
K8S_CHART ?= $(HELM_CHART)
K8S_UMBRELLA_CHART_PATH ?= ./charts/ska-mid-cbf-emulators-umbrella

#CI_REGISTRY ?= gitlab.com/ska-telescope/ska-mid-cbf-emulators

CI_PROJECT_DIR ?= .

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

ifneq ($(strip $(CI_JOB_ID)),)
K8S_TEST_IMAGE_TO_TEST = $(CI_REGISTRY)/ska-telescope/ska-mid-cbf-emulators/ska-mid-cbf-emulators:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
PYTHON_RUNNER = python3 -m
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-mid-cbf-emulators:$(VERSION)
endif
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=$(CURDIR)/$(PYTHON_SRC):/app/$(PYTHON_SRC):$(PYTHONPATH)
PYTHON_TEST_FILE = tests/

CI_PROJECT_PATH_SLUG ?= ska-mid-cbf-emulators
CI_ENVIRONMENT_SLUG ?= ska-mid-cbf-emulators

HOST_UID := $(shell id -u)
HOST_GID := $(shell id -g)
WORKSPACE := $(shell realpath ~/workspace)
PERSONA := ska_vcc_persona


check-minikube-eval:
	@minikube status | grep -iE '^.*(docker-env: in-use).*$$'; \
	if [ $$? -ne 0 ]; then \
		echo "Minikube docker-env not active. Please run: 'eval \$$(minikube docker-env)'."; \
		exit 1; \
	else echo "Minikube docker-env is active."; \
	fi

k8s-pre-install-chart:
	@if [ "$(MINIKUBE)" = "true" ]; then make check-minikube-eval; fi;

k8s-deploy: export K8S_CHART_PARAMS = -f ./charts/ska-mid-cbf-emulators/generated_values.yaml
k8s-deploy: DEV = false
k8s-deploy: SKIP_BUILD = false
k8s-deploy: ENABLE_BITSTREAM_DOWNLOAD = true
k8s-deploy: ENABLE_BITSTREAM_PVC = true
k8s-deploy: K3D = false
k8s-deploy:
	@if [ "$(MINIKUBE)" = "true" ]; then make check-minikube-eval; fi;
	poetry run python pre_deploy.py --dev=$(DEV) --tag=$(VERSION) --enable-bitstream-download=$(ENABLE_BITSTREAM_DOWNLOAD) --enable-bitstream-pvc=$(ENABLE_BITSTREAM_PVC) --k3d=$(K3D)
	@if [ "$(DEV)" = "true" ]; then \
		if [ "$(SKIP_BUILD)" != "true" ]; then \
			make oci-build-all OCI_IMAGES="$(OCI_IMAGES_TO_PUBLISH)"; \
		fi; \
	fi;
	make k8s-install-chart

k8s-destroy:
	make k8s-uninstall-chart
	@echo "Waiting for all pods in namespace $(KUBE_NAMESPACE) to terminate..."
	@time kubectl wait pod --all --for=delete --timeout=5m0s --namespace $(KUBE_NAMESPACE)

run:
	docker compose -f ./images/ska-mid-cbf-emulators-emulator/docker-compose.yaml up --build -d

run-dev:
	docker compose -f ./images/ska-mid-cbf-emulators-dev/docker-compose.yaml up --build -d

run-dev-no-injector:
	docker compose -f ./images/ska-mid-cbf-emulators-dev/docker-compose-no-injector.yaml up --build -d

run-fw-dev:
	export HOST_UID=$(HOST_UID); \
	export HOST_GID=$(HOST_GID); \
	export WORKSPACE=$(WORKSPACE); \
	export PERSONA=$(PERSONA); \
	docker compose -p $(USER) -f ./images/ska-mid-cbf-emulators-fw-dev/docker-compose.yaml up --build -d;

remove:
	docker compose -f ./images/ska-mid-cbf-emulators-emulator/docker-compose.yaml down

remove-dev:
	docker compose -f ./images/ska-mid-cbf-emulators-dev/docker-compose.yaml down

remove-dev-no-injector:
	docker compose -f ./images/ska-mid-cbf-emulators-dev/docker-compose-no-injector.yaml down

remove-fw-dev:
	export HOST_UID=$(HOST_UID); \
	export HOST_GID=$(HOST_GID); \
	export WORKSPACE=$(WORKSPACE); \
	export PERSONA=$(PERSONA); \
	docker compose -p $(USER) -f ./images/ska-mid-cbf-emulators-fw-dev/docker-compose.yaml down;

enter:
	docker exec -it emulator bash

enter-injector:
	docker exec -it injector bash

enter-dev:
	docker exec -it emulator-dev bash

enter-fw-dev:
	docker exec -it emulator-dev-$(USER) bash

view-logs:
	docker logs --follow emulator

view-logs-injector:
	docker logs --follow injector

view-logs-dev:
	docker logs --follow emulator-dev

view-logs-fw-dev:
	docker logs --follow emulator-dev-$(USER)

documentation:  ## Re-generate documentation
	cd docs && make clean && make html

help: ## show this help
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

requirements:
	poetry install

build-docs-local:
	make requirements
	@echo "Cleaning up old build(s)..."
	-@rm -rf docs/src/_code/*.rst
	-@rm -rf docs/src/_generated_pages/*.rst
	-@rm -rf docs/src/_generated_pages/img/*.png
	-@rm -rf ./build/sphinx_test/
	@echo "Generating pages..."
	-@(cd docs; poetry run python generate_pages.py)
	@echo "Generating API docs..."
	-@poetry run sphinx-apidoc -t docs/src/_templates -o docs/src/_code/emulator_engine -M -f -d 2 images/ska-mid-cbf-emulators-emulator/emulator_engine/ images/ska-mid-cbf-emulators-emulator/emulator_engine/tests/*
	-@poetry run sphinx-apidoc -t docs/src/_templates -o docs/src/_code/ska_mid_cbf_emulators -M -f -d 2 src/ska_mid_cbf_emulators/
	@echo "Building docs..."
	-@$(POETRY_PYTHON_RUNNER) sphinx -T -b html -d ./build/sphinx_test/emu_cache/doctrees -D language=en ./docs/src ./build/sphinx_test/emu_output
	@echo "Done. Open build/sphinx_test/emu_output/index.html to view the generated docs."

fix-python-imports:
	$(POETRY_PYTHON_RUNNER) isort --profile black --line-length $(PYTHON_LINE_LENGTH) $(PYTHON_SWITCHES_FOR_ISORT) $(PYTHON_LINT_TARGET)

lint-python-local:
	make requirements
	mkdir -p build/lint-output
	@echo "Linting..."
	-@ISORT_ERROR=0 BLACK_ERROR=0 FLAKE_ERROR=0 PYLINT_ERROR=0; \
	$(POETRY_PYTHON_RUNNER) isort --check-only --profile black --line-length $(PYTHON_LINE_LENGTH) $(PYTHON_SWITCHES_FOR_ISORT) $(PYTHON_LINT_TARGET) &> build/lint-output/1-isort-output.txt; \
	if [ $$? -ne 0 ]; then ISORT_ERROR=1; fi; \
	$(POETRY_PYTHON_RUNNER) black --exclude .+\.ipynb --check --line-length $(PYTHON_LINE_LENGTH) $(PYTHON_SWITCHES_FOR_BLACK) $(PYTHON_LINT_TARGET) &> build/lint-output/2-black-output.txt; \
	if [ $$? -ne 0 ]; then BLACK_ERROR=1; fi; \
	$(POETRY_PYTHON_RUNNER) flake8 --show-source --statistics --max-line-length $(PYTHON_LINE_LENGTH) $(PYTHON_SWITCHES_FOR_FLAKE8) $(PYTHON_LINT_TARGET) &> build/lint-output/3-flake8-output.txt; \
	if [ $$? -ne 0 ]; then FLAKE_ERROR=1; fi; \
	$(POETRY_PYTHON_RUNNER) pylint --output-format=parseable --max-line-length $(PYTHON_LINE_LENGTH) $(PYTHON_SWITCHES_FOR_PYLINT_LOCAL) $(PYTHON_LINT_TARGET) &> build/lint-output/4-pylint-output.txt; \
	if [ $$? -ne 0 ]; then PYLINT_ERROR=1; fi; \
	if [ $$ISORT_ERROR -ne 0 ]; then echo "Isort lint errors were found. Check build/lint-output/1-isort-output.txt for details."; fi; \
	if [ $$BLACK_ERROR -ne 0 ]; then echo "Black lint errors were found. Check build/lint-output/2-black-output.txt for details."; fi; \
	if [ $$FLAKE_ERROR -ne 0 ]; then echo "Flake8 lint errors were found. Check build/lint-output/3-flake8-output.txt for details."; fi; \
	if [ $$PYLINT_ERROR -ne 0 ]; then echo "Pylint lint errors were found. Check build/lint-output/4-pylint-output.txt for details."; fi; \
	if [ $$ISORT_ERROR -eq 0 ] && [ $$BLACK_ERROR -eq 0 ] && [ $$FLAKE_ERROR -eq 0 ] && [ $$PYLINT_ERROR -eq 0 ]; then echo "Lint was successful. Check build/lint-output for any additional details."; fi;

.PHONY: all documentation help
