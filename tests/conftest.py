from unittest import mock
import pytest

import logging
from typing import Generator

import pytest
from pika import BlockingConnection, ConnectionParameters
from pika.credentials import PlainCredentials
from pytest import FixtureRequest

from pytest_rabbitmq.factories.executor import RabbitMqExecutor

@pytest.fixture(scope='session', autouse=True)
def setup_teardown():
    logging.getLogger("pika").setLevel(logging.WARNING)
    yield

def safe_clear_rabbitmq(process: RabbitMqExecutor, rabbitmq_connection: BlockingConnection) -> None:
    """Modified from: https://github.com/ClearcodeHQ/pytest-rabbitmq/blob/main/pytest_rabbitmq/factories/client.py"""
    try:
        channel = rabbitmq_connection.channel()

        for exchange in process.list_exchanges():
            if exchange.startswith("amq."):
                continue
            channel.exchange_delete(exchange)

        for queue_name in process.list_queues():
            if queue_name.startswith("amq."):
                continue
            channel.queue_delete(queue_name)
    except Exception:
        return

@pytest.fixture(scope='function', autouse=True)
def rabbitmq_mock(request: FixtureRequest) -> Generator[BlockingConnection, None, None]:
    """Modified from: https://github.com/ClearcodeHQ/pytest-rabbitmq/blob/main/pytest_rabbitmq/factories/client.py"""
    process = request.getfixturevalue('rabbitmq_proc')

    credentials = PlainCredentials("guest", "guest")
    parameters = ConnectionParameters(
        host=process.host, port=process.port, virtual_host="/", credentials=credentials
    )

    connections = []
    def connection_factory(*args, **kwargs):
        connection = BlockingConnection(parameters)
        connections.append(connection)
        return connection

    with (
        mock.patch('emulator_engine.services.messaging.api_queue_service.BlockingConnection', side_effect=connection_factory),
        mock.patch('emulator_engine.services.messaging.event_queue_service.BlockingConnection', side_effect=connection_factory),
        mock.patch('emulator_engine.services.controller.controller_router_client.BlockingConnection', side_effect=connection_factory),
        mock.patch('emulator_engine.services.api.router_client.BlockingConnection', side_effect=connection_factory),
        mock.patch('emulator_engine.features.controller.emulator_controller.BlockingConnection', side_effect=connection_factory),
        mock.patch('emulator_injector.services.messaging.message_service.BlockingConnection', side_effect=connection_factory)
    ):
        yield

    for connection in connections:
        safe_clear_rabbitmq(process, connection)
        try:
            connection.close()
        except Exception as e:
            pass
