from enum import auto
from typing import Any, Self, override

from ska_mid_cbf_emulators.common import BaseState, BaseTransitionTrigger, FiniteStateMachine


class FooState(BaseState):
    OFF = 'OFF'
    ON = 'ON'


class FooTransitionTrigger(BaseTransitionTrigger):
    START = auto()
    STOP = auto()


class EmulatorStateMachine(BaseState):
    @override
    @property
    def _states(self: Self) -> list[FooState]:
        return [
            FooState.OFF,
            FooState.ON
        ]

    @override
    @property
    def _initial_state(self: Self) -> FooState:
        return FooState.OFF

    @override
    @property
    def _transitions(self) -> list[dict[str, Any]]:
        return [
            {
                'source': FooState.OFF,
                'dest': FooState.ON,
                'trigger': FooTransitionTrigger.START
            },
            {
                'source': FooState.ON,
                'dest': FooState.OFF,
                'trigger': FooTransitionTrigger.STOP
            },
        ]
