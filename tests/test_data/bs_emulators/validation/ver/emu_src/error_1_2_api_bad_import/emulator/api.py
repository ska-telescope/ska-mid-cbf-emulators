from typing import Self

from ska_mid_cbf_emulators.common import BaseEmulatorApi, HttpMethod, InternalRestResponse
from .ok_1_simple_example.ip_block import EmulatorIPBlock


class EmulatorApi(BaseEmulatorApi):

    @BaseEmulatorApi.route(http_method=HttpMethod.GET)
    def get_ok(self: Self) -> InternalRestResponse:
        return InternalRestResponse.ok({'value': self._priv(1)})

    def _priv(self: Self, x: int) -> int:
        return x + 1
