from typing import Self

from ska_mid_cbf_emulators.common import BaseEmulatorApi, HttpMethod, InternalRestResponse


class EmulatorApi(BaseEmulatorApi):

    @BaseEmulatorApi.route(http_method=HttpMethod.GET)
    def get_ok(self: Self, y: int) -> InternalRestResponse:
        return InternalRestResponse.ok({'value': self._priv(y)})

    def _priv(self: Self, x: int) -> int:
        return x + 1
