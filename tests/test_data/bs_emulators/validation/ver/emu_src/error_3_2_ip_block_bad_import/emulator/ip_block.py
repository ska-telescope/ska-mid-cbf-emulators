from typing import Self
from .ok_1_simple_example.state_machine import FooState


class EmulatorIPBlock():

    def __init__(self: Self) -> None:
        # const defaults
        self.x = 1

        # variables
        self.y = 2
