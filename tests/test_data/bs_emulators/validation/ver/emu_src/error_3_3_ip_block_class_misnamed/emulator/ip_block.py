from typing import Self


class WrongEmulatorIPBlock():

    def __init__(self: Self) -> None:
        # const defaults
        self.x = 1

        # variables
        self.y = 2
