from typing import Self


class EmulatorIPBlock():

    def __init__(self: Self, y, x=1) -> None:
        # const defaults
        self.x = x

        # variables
        self.y = y
