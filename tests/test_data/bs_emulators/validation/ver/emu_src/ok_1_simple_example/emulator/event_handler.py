from typing import Self, override
from ska_mid_cbf_emulators.common import BaseEventHandler, ManualEvent, PulseEvent, SignalUpdateEventList


class EmulatorEventHandler(BaseEventHandler):

    @override
    def handle_pulse_event(self: Self, event: PulseEvent, **kwargs) -> None:
        return None

    @override
    def handle_signal_update_events(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
        return event_list

    @override
    def handle_manual_event(self: Self, event: ManualEvent, **kwargs) -> None:
        return None
