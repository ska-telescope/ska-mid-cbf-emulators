from typing import Self


class EmulatorIPBlock():

    def setup(self: Self) -> None:
        # const defaults
        self.x = 1

        # variables
        self.y = 2
