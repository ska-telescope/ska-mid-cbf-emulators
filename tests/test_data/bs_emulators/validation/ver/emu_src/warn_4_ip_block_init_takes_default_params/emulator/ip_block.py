from typing import Self


class EmulatorIPBlock():

    def __init__(self: Self, x=1) -> None:
        # const defaults
        self.x = x

        # variables
        self.y = 2
