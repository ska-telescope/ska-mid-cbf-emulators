from enum import auto
from typing import Any, Self, override

from ska_mid_cbf_emulators.common import BaseState, BaseTransitionTrigger, FiniteStateMachine


class FooState(BaseState):
    OFF = 'OFF'
    ON = 'ON'


class FooTransitionTrigger(BaseTransitionTrigger):
    START = auto()
    STOP = auto()


class EmulatorStateMachine(FiniteStateMachine):
    @override
    @property
    def _states(self: Self) -> list[FooState]:
        return [
            FooState.OFF,
            FooState.ON
        ]
