from typing import Self, override
from ska_mid_cbf_emulators.common import BaseEvent, ManualEvent, SignalUpdateEventList


class EmulatorEventHandler(BaseEvent):

    @override
    def handle_pulse_event(self: Self, event: BaseEvent, data: str, **kwargs) -> None:
        return None

    @override
    def handle_signal_update_events(self: Self, event_list: SignalUpdateEventList, **kwargs) -> SignalUpdateEventList:
        return event_list

    @override
    def handle_manual_event(self: Self, event: ManualEvent, **kwargs) -> None:
        return None
