from typing import Self
from emulator_injector.models.config.injector_config import InjectorConfigEmulator

class TestInjectorConfigEmulator:
    
    def test_init_success(self: Self):
        _ = InjectorConfigEmulator('', '', '')

    def test_property_setters_getters_correct(self: Self):
        config = InjectorConfigEmulator('test_emu', 'test_bs', '0.0.1')
        config.bitstream_emulator_id = 'new_emu'
        config.bitstream_id = 'new_bs'
        config.bitstream_ver = '0.0.2'
        assert config.bitstream_emulator_id == config._bitstream_emulator_id == 'new_emu'
        assert config.bitstream_id == config._bitstream_id == 'new_bs'
        assert config.bitstream_ver == config._bitstream_ver == '0.0.2'
