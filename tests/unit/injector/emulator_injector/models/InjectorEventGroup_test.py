from typing import Self
import pytest
from emulator_injector.models.event.injector_event_group import InjectorEventGroup
from ska_mid_cbf_emulators.common import ManualEvent

class TestInjectorEventGroup:

    @pytest.fixture()
    def event_group(self):
        return InjectorEventGroup('test_emu', 'test_ip', [ManualEvent(value={'test_key': 'test_value'}, source_timestamp=1_000_000_000)])
    
    def test_init_success(self: Self):
        _ = InjectorEventGroup('', '', [])
    
    def test_repr_success(self: Self, event_group: InjectorEventGroup):
        assert isinstance(repr(event_group), str)

    def test_property_setters_getters_correct(self: Self, event_group: InjectorEventGroup):
        event_group.bitstream_emulator_id = 'new_emu'
        event_group.ip_block_emulator_id = 'new_ip'
        event_group.events = [ManualEvent(value={'new_key': 'new_value'}, source_timestamp=999_999_999)]
        assert event_group.bitstream_emulator_id == event_group._bitstream_emulator_id == 'new_emu'
        assert event_group.ip_block_emulator_id == event_group._ip_block_emulator_id == 'new_ip'
        assert event_group.events == event_group._events and event_group._events[0].value.get('new_key') == 'new_value'
