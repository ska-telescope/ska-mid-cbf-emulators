from typing import Self
from emulator_injector.models.config.injector_config import InjectorConfig, InjectorConfigEmulator

class TestInjectorConfig:
    
    def test_init_success(self: Self):
        _ = InjectorConfig('', '', {})

    def test_property_setters_getters_correct(self: Self):
        config = InjectorConfig('test_host', 1234, {'emu': InjectorConfigEmulator('test_emu', 'test_bs', '0.0.1')})
        config.rabbitmq_host = 'new_host'
        config.rabbitmq_port = 5678
        config.bitstream_emulators = {'new_emu': InjectorConfigEmulator('new_emu', 'new_bs', '0.0.2')}
        assert config.rabbitmq_host == config._rabbitmq_host == 'new_host'
        assert config.rabbitmq_port == config._rabbitmq_port == 5678
        assert config.bitstream_emulators == config._bitstream_emulators and config._bitstream_emulators.get('new_emu').bitstream_emulator_id == 'new_emu'
