import time
from typing import Self
import pytest
from emulator_injector.services.time.time_service import TimeService

class TestTimeService:
    
    def test_get_timestamp_success(self: Self):
        current_time = time.time() * 1000
        got_time = TimeService.get_timestamp(offset_milliseconds=150_000)
        assert got_time - 150_000 == pytest.approx(current_time, abs=500)
