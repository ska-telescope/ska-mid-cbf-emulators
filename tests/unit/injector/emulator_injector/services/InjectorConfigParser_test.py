from typing import Self
import pytest

from emulator_injector.models.config.injector_config import InjectorConfig, InjectorConfigEmulator
from emulator_injector.services.parsing.injector_config_parser import InjectorConfigParser
from jsonschema import ValidationError
from contextlib import AbstractContextManager, nullcontext as does_not_raise

class TestInjectorConfigParser:

    @pytest.mark.parametrize(
        ('json_config', 'expected_config', 'exception_expectation'),
        [
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                InjectorConfig(
                    'abcd',
                    12345,
                    {
                        'test_emu': InjectorConfigEmulator(
                            'test_emu',
                            'test_bs',
                            '0.0.1'
                        )
                    }
                ),
                does_not_raise(),
                id='simple_config_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "rabbitmq-service.ska-mid-cbf-emulators.svc.cluster.local",'
                    r'  "rabbitmq_port": 50051,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "emu1",'
                    r'      "bitstream_id": "bs1",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    },'
                    r'    {'
                    r'      "bitstream_emulator_id": "emu2",'
                    r'      "bitstream_id": "bs1",'
                    r'      "bitstream_ver": "2.3.1-beta3.0.104-SNAPSHOT"'
                    r'    },'
                    r'    {'
                    r'      "bitstream_emulator_id": "emu3",'
                    r'      "bitstream_id": "bs2",'
                    r'      "bitstream_ver": "1.0.0"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                InjectorConfig(
                    'rabbitmq-service.ska-mid-cbf-emulators.svc.cluster.local',
                    50051,
                    {
                        'emu1': InjectorConfigEmulator(
                            'emu1',
                            'bs1',
                            '0.0.1'
                        ),
                        'emu2': InjectorConfigEmulator(
                            'emu2',
                            'bs1',
                            '2.3.1-beta3.0.104-SNAPSHOT'
                        ),
                        'emu3': InjectorConfigEmulator(
                            'emu3',
                            'bs2',
                            '1.0.0'
                        )
                    }
                ),
                does_not_raise(),
                id='complex_config_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_host_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_port_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_emulators_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_bitstream_emulator_id_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_bitstream_id_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_bitstream_ver_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": 3.14159,'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_host_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": "12345",'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_port_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": -1,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='port_too_low_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 123456,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='port_too_high_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": 1234,'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_bitstream_emulator_id_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": 1234,'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_bitstream_id_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": 0.01'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_bitstream_ver_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.invalid.1.0"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='bitstream_ver_invalid_semantic_version_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "rabbitmq_host": "abcd",'
                    r'  "rabbitmq_port": 12345,'
                    r'  "bitstream_emulators": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    },'
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "bitstream_id": "test_bs",'
                    r'      "bitstream_ver": "0.0.1"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='duplicate_emulator_error'
            ),
        ]
    )
    def test_decode(self: Self, json_config: str, expected_config: InjectorConfig, exception_expectation: AbstractContextManager):
        with exception_expectation:
            result = InjectorConfigParser.decode(json_config)
            if expected_config is not None:
                assert result.rabbitmq_host == expected_config.rabbitmq_host
                assert result.rabbitmq_port == expected_config.rabbitmq_port
                assert len(result.bitstream_emulators) == len(expected_config.bitstream_emulators)
                for emulator_key in result.bitstream_emulators:
                    expected_emu = expected_config.bitstream_emulators.get(emulator_key)
                    assert expected_emu is not None
                    result_emu = result.bitstream_emulators.get(emulator_key)
                    assert result_emu.bitstream_emulator_id == expected_emu.bitstream_emulator_id
                    assert result_emu.bitstream_id == expected_emu.bitstream_id
                    assert result_emu.bitstream_ver == expected_emu.bitstream_ver
