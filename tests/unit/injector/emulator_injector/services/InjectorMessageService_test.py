from typing import Self
from unittest import mock
from emulator_injector.services.messaging.message_service import InjectorMessageService

class TestInjectorMessageService:

    def test_publish_success(self: Self):
        with (
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.basic_publish') as mock_publish,
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.exchange_declare'),
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_declare')
        ):
            InjectorMessageService.publish('', 'my_queue', 'some_body', 'localhost', 5001)
            mock_publish.assert_called_once_with(exchange='', routing_key='my_queue', body='some_body')

    def test_publish_declares_exchange_only_if_not_empty(self: Self):
        with (
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.basic_publish'),
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.exchange_declare') as mock_exchange_declare,
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_declare')
        ):
            InjectorMessageService.publish('', '', '', '', 0)
            mock_exchange_declare.assert_not_called()
            InjectorMessageService.publish('asdf', '', '', '', 0)
            mock_exchange_declare.assert_called_once_with(exchange='asdf')

    def test_publish_declares_queue_only_if_not_empty(self: Self):
        with (
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.basic_publish'),
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.exchange_declare'),
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_declare') as mock_queue_declare
        ):
            InjectorMessageService.publish('', '', '', '', 0)
            mock_queue_declare.assert_not_called()
            InjectorMessageService.publish('', 'asdf', '', '', 0)
            mock_queue_declare.assert_called_once_with(queue='asdf')
