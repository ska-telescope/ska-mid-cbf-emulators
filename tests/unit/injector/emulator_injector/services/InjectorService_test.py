from typing import Self
from unittest import mock

import pytest
from emulator_injector.models.event.injector_event_group import InjectorEventGroup
from emulator_injector.services.messaging.injector_service import InjectorService
from ska_mid_cbf_emulators.common.models.event.event_severity import EventSeverity
from ska_mid_cbf_emulators.common.models.event.manual_event import ManualEvent
from ska_mid_cbf_emulators.common.models.event.manual_event_subtype import ManualEventSubType
from ska_mid_cbf_emulators.common.services.messaging.id_service import IdService

class TestInjectorService:

    @pytest.fixture()
    def service(self: Self):
        return InjectorService('localhost', 5672)
    
    def test_init_success(self: Self):
        _ = InjectorService('', 1)

    def test_inject_event_group_list_success(self: Self, service: InjectorService):
        event_group_list = [
            InjectorEventGroup(
                'test_emu',
                'test_ip',
                [
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_000_000
                    ),
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_001_000
                    )
                ]
            ),
            InjectorEventGroup(
                'test_emu2',
                'test_ip2',
                [
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_005_000
                    ),
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_006_000
                    )
                ]
            )
        ]
        with (
            mock.patch('emulator_injector.services.parsing.injector_event_group_list_parser.InjectorEventGroupListParser.decode', return_value=event_group_list),
            mock.patch.object(service, 'inject_event_group') as mock_inject_group
        ):
            service.inject_event_group_list('mock_json_str')
            mock_inject_group.assert_has_calls(
                [
                    mock.call(event_group_list[0]),
                    mock.call(event_group_list[1])
                ],
                any_order=True
            )

    def test_inject_event_group_success(self: Self, service: InjectorService):
        with mock.patch('emulator_injector.services.messaging.message_service.InjectorMessageService.publish') as mock_publish:
            event_group = InjectorEventGroup(
                'test_emu',
                'test_ip',
                [
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_000_000
                    ),
                    ManualEvent(
                        subtype=ManualEventSubType.INJECTION,
                        value={
                            'injection_type': 'general_injection'
                        },
                        severity=EventSeverity.GENERAL,
                        source_timestamp=1_001_000
                    )
                ]
            )
            expected_id = IdService.manual_queue_id('test_emu', 'test_ip')
            service.inject_event_group(event_group)
            mock_publish.assert_has_calls(
                [
                    mock.call(mock.ANY, expected_id, ManualEvent.encode(event_group.events[0]), host=service._host, port=service._port),
                    mock.call(mock.ANY, expected_id, ManualEvent.encode(event_group.events[1]), host=service._host, port=service._port),
                ],
                any_order=True
            )
