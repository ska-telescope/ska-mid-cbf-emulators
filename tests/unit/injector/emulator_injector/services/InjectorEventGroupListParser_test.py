from typing import Self
from unittest import mock
import pytest

from emulator_injector.models.config.injector_config import InjectorConfig, InjectorConfigEmulator
from emulator_injector.models.event.injector_event_group import InjectorEventGroup
from emulator_injector.services.parsing.injector_config_parser import InjectorConfigParser
from jsonschema import ValidationError
from contextlib import AbstractContextManager, nullcontext as does_not_raise

from emulator_injector.services.parsing.injector_event_group_list_parser import InjectorEventGroupListParser
from ska_mid_cbf_emulators.common import ManualEvent, ManualEventSubType, EventSeverity

class TestInjectorEventGroupListParser:

    @pytest.mark.parametrize(
        ('json_list', 'expected_list', 'exception_expectation'),
        [
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                [
                    InjectorEventGroup(
                        'test_emu',
                        'test_ip',
                        [
                            ManualEvent(
                                subtype=ManualEventSubType.INJECTION,
                                value={
                                    'injection_type': 'general_injection'
                                },
                                severity=EventSeverity.GENERAL,
                                source_timestamp=1_000_001
                            )
                        ]
                    )
                ],
                does_not_raise(),
                id='simple_list_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "emu1",'
                    r'      "ip_block_emulator_id": "ip1",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "test_key": "test_value",'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "general",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    },'
                    r'    {'
                    r'      "bitstream_emulator_id": "emu2",'
                    r'      "ip_block_emulator_id": "ip2",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "abcde": {'
                    r'              "fghjk": 123.45,'
                    r'              "lmnop": [1, 2, 3]'
                    r'            },'
                    r'            "ok": true,'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GenEraL",'
                    r'          "offset": 2'
                    r'        }'
                    r'      ]'
                    r'    },'
                    r'    {'
                    r'      "bitstream_emulator_id": "emu1",'
                    r'      "ip_block_emulator_id": "ip3",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "other_key": "other_value",'
                    r'            "injection_type": "temperature_warning"'
                    r'          },'
                    r'          "severity": "WARNING",'
                    r'          "offset": 2000'
                    r'        },'
                    r'        {'
                    r'          "value": {'
                    r'            "other_key2": "other_value2",'
                    r'            "injection_type": "fault"'
                    r'          },'
                    r'          "severity": "FATAL_ERROR",'
                    r'          "offset": 2500'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                [
                    InjectorEventGroup(
                        'emu1',
                        'ip1',
                        [
                            ManualEvent(
                                subtype=ManualEventSubType.INJECTION,
                                value={
                                    'test_key': 'test_value',
                                    'injection_type': 'general_injection'
                                },
                                severity=EventSeverity.GENERAL,
                                source_timestamp=1_000_001
                            )
                        ]
                    ),
                    InjectorEventGroup(
                        'emu2',
                        'ip2',
                        [
                            ManualEvent(
                                subtype=ManualEventSubType.INJECTION,
                                value={
                                    'abcde': {
                                        'fghjk': 123.45,
                                        'lmnop': [1, 2, 3]
                                    },
                                    'ok': True,
                                    'injection_type': 'general_injection'
                                },
                                severity=EventSeverity.GENERAL,
                                source_timestamp=1_000_002
                            )
                        ]
                    ),
                    InjectorEventGroup(
                        'emu1',
                        'ip3',
                        [
                            ManualEvent(
                                subtype=ManualEventSubType.INJECTION,
                                value={
                                    'other_key': 'other_value',
                                    'injection_type': 'temperature_warning'
                                },
                                severity=EventSeverity.WARNING,
                                source_timestamp=1_002_000
                            ),
                            ManualEvent(
                                subtype=ManualEventSubType.INJECTION,
                                value={
                                    'other_key2': 'other_value2',
                                    'injection_type': 'fault'
                                },
                                severity=EventSeverity.FATAL_ERROR,
                                source_timestamp=1_002_500
                            )
                        ]
                    )
                ],
                does_not_raise(),
                id='complex_list_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_list_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_bitstream_emulator_id_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_ip_block_id_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip"'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_events_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_value_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_severity_missing_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "a": "null"'
                    r'          },'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_value_injection_type_missing_error'
            ),
            pytest.param(
                (
                    r'['
                    r'  ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r']'
                ),
                None,
                pytest.raises(ValidationError),
                id='outer_object_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": {'
                    r'    "null": {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  }'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_list_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": 12345,'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_bitstream_emulator_id_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": 12345,'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_ip_block_id_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": {'
                    r'        "null": {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      }'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='group_events_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": [1, 2, 3],'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_value_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": 12345,'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_severity_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1.25'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='timestamp_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": 12345'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_value_injection_type_wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        },'
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='duplicate_event_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "invalid_severity",'
                    r'          "timestamp": 1000001'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_invalid_severity_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": -1'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_invalid_timestamp_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "offset": -1'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_invalid_offset_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "injector_event_groups": ['
                    r'    {'
                    r'      "bitstream_emulator_id": "test_emu",'
                    r'      "ip_block_emulator_id": "test_ip",'
                    r'      "events": ['
                    r'        {'
                    r'          "value": {'
                    r'            "injection_type": "general_injection"'
                    r'          },'
                    r'          "severity": "GENERAL",'
                    r'          "timestamp": 1000001,'
                    r'          "offset": 1000'
                    r'        }'
                    r'      ]'
                    r'    }'
                    r'  ]'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='event_timestamp_offset_both_provided_error'
            ),
        ]
    )
    def test_decode(self: Self, json_list: str, expected_list: list[InjectorEventGroup], exception_expectation: AbstractContextManager):
        with (
            mock.patch('time.time', return_value=1_000),
            exception_expectation
        ):
            result = InjectorEventGroupListParser.decode(json_list)
            if expected_list is not None:
                assert len(result) == len(expected_list)
                for (group, expected_group) in zip(result, expected_list):
                    assert group.bitstream_emulator_id == expected_group.bitstream_emulator_id
                    assert group.ip_block_emulator_id == expected_group.ip_block_emulator_id
                    for (event, expected_event) in zip(group.events, expected_group.events):
                        assert event.type == expected_event.type
                        assert event.subtype == expected_event.subtype
                        assert event.value == expected_event.value
                        assert event.severity == expected_event.severity
                        assert event.source_timestamp == expected_event.source_timestamp
                        assert event.update_timestamp == expected_event.update_timestamp
