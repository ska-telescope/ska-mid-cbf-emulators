from typing import Self
from emulator_injector.features.injector.injector_controller import InjectorController

class TestInjectorController:
    
    def test_init_success(self: Self):
        with open('tests/test_data/injector/config.json', 'r') as config_file:
            config_str = config_file.read()
        _ = InjectorController(config_str)
