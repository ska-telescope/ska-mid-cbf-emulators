import time
from typing import Self
import pytest
from unittest import mock
from ska_mid_cbf_emulators.common import delay_action

class TestDelayAction:

    @pytest.fixture(autouse=True)
    def example_action(self: Self):
        self._action = lambda l: l.append(1)

    def test_action_completes(self: Self):
        l = [5, 4, 3, 2]
        timer = delay_action(500, self._action, l)
        timer.join(10)
        assert not timer.is_alive()
        assert len(l) == 5 and l[-1] == 1

    def test_action_is_delayed(self: Self):
        l = [5, 4, 3, 2]
        delay_action(1000, self._action, l)
        assert len(l) == 4 and l[-1] == 2

    def test_action_delay_is_ok(self: Self):
        l = []
        t1 = time.time()
        timer = delay_action(1000, self._action, l)
        timer.join(10)
        t2 = time.time()
        assert not timer.is_alive()
        assert t2 - t1 == pytest.approx(1.0, abs=0.3)

    def test_action_error(self: Self):
        l = 'not_a_list'
        with mock.patch('ska_mid_cbf_emulators.common.services.logging.logger_factory.EmulatorLogger.error') as mock_logger_error:
            timer = delay_action(1000, self._action, l)
            timer.join(10)
            assert not timer.is_alive()
            mock_logger_error.assert_called_once()
