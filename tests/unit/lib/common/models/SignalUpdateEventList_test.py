from typing import Self
from unittest import mock
from ska_mid_cbf_emulators.common import SignalUpdateEvent, SignalUpdateEventList

class TestSignalUpdateEventList:

    @mock.patch('ska_mid_cbf_emulators.common.SignalUpdateEvent')
    def test_init_success(self: Self, mock_event: mock.MagicMock):
        _ = SignalUpdateEventList([mock_event(), mock_event(), mock_event()], 0)

    def test_init_no_timestamp_success(self: Self):
        src = 123
        su_list = SignalUpdateEventList([SignalUpdateEvent({'a': 1}, source_timestamp=src), SignalUpdateEvent({'b': 2}, source_timestamp=src)])
        assert su_list.source_timestamp == src

    def test_len_success(self: Self):
        src = 123
        su_list = SignalUpdateEventList([SignalUpdateEvent({'a': 1}, source_timestamp=src), SignalUpdateEvent({'b': 2}, source_timestamp=src)], src)
        assert len(su_list) == 2
