from typing import Self
from ska_mid_cbf_emulators.common import InternalRestRequest

class TestInternalRestRequest:

    def test_init_success(self: Self):
        _ = InternalRestRequest()

    def test_encode_decode_methods_are_inverses(self: Self):
        req = InternalRestRequest('test_method', 'GET', {'x': 1})
        enc = InternalRestRequest.to_json(req)
        dec = InternalRestRequest.from_json(enc)
        assert isinstance(dec, InternalRestRequest)
        assert dec.method_name == 'test_method'
        assert dec.http_method == 'GET'
        assert dec.kwargs == {'x': 1}
