from typing import Self
from unittest import mock
import pytest
from ska_mid_cbf_emulators.common import EmulatorConfig, EmulatorConfigIPBlock

class TestEmulatorConfig:

    @pytest.fixture()
    def config(self: Self):
        return EmulatorConfig(
            'test_conf_id',
            '1.2.3',
            [
                EmulatorConfigIPBlock('test_ip_1', 'IP 1', 'type_1', ['test_ip_2', 'test_ip_3'], {'x': 3}),
                EmulatorConfigIPBlock('test_ip_2', 'IP 2', 'type_2', [], {'y': 4}),
                EmulatorConfigIPBlock('test_ip_3', 'IP 3', 'type_3', [], {'z': 5})
            ],
            'test_ip_1'
        )
    
    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.EmulatorConfig._generate_graph'):
            _ = EmulatorConfig('', '', '', [])

    def test_id_setter_getter_correct(self: Self, config: EmulatorConfig):
        config.id = 'new_conf_id'
        assert config.id == config._id == 'new_conf_id'

    def test_version_setter_getter_correct(self: Self, config: EmulatorConfig):
        config.version = '3.2.1'
        assert config.version == config._version == '3.2.1'

    def test_first_setter_getter_correct(self: Self, config: EmulatorConfig):
        config.first = 'test_ip_2'
        assert config.first == config._first == 'test_ip_2'

    def test_ip_blocks_setter_updates_graph_correctly(self: Self, config: EmulatorConfig):
        config.ip_blocks = [
            EmulatorConfigIPBlock('test_ip_4', 'IP 4', 'type_4', [], {'x': -1}),
            EmulatorConfigIPBlock('test_ip_5', 'IP 5', 'type_5', ['test_ip_6'], {'y': 0}),
            EmulatorConfigIPBlock('test_ip_6', 'IP 6', 'type_6', ['test_ip_4'], {'z': 1})
        ]
        g = config._graph
        assert set(g.vs['name']) == set(g.vs['label']) == {'test_ip_4', 'test_ip_5', 'test_ip_6'}
        assert g.are_adjacent('test_ip_5', 'test_ip_6')
        assert g.are_adjacent('test_ip_6', 'test_ip_4')
        assert not g.are_adjacent('test_ip_4', 'test_ip_5')
        assert not g.are_adjacent('test_ip_4', 'test_ip_6')
        assert not g.are_adjacent('test_ip_5', 'test_ip_4')
        assert not g.are_adjacent('test_ip_6', 'test_ip_5')

    def test_ip_blocks_getter_correct(self: Self, config: EmulatorConfig):
        assert config.ip_blocks == config._ip_blocks

    def test_graph_getter_correct(self: Self, config: EmulatorConfig):
        assert config.graph == config._graph

    def test_get_graph_img_success(self: Self, config: EmulatorConfig):
        with (
            mock.patch('ska_mid_cbf_emulators.common.models.config.emulator_config.plot_graph') as mock_plot_graph,
            mock.patch('matplotlib.pyplot.savefig') as mock_savefig
        ):
            b = config.get_graph_img()
            mock_plot_graph.assert_called_once()
            mock_savefig.assert_called_once()
            assert isinstance(b, bytes)
