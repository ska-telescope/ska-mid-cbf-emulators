from typing import Any, Callable, Self
import pytest
from ska_mid_cbf_emulators.common import InternalRestResponse
from fastapi import status

class TestInternalRestResponse:

    @pytest.fixture()
    def resp_params(self: Self):
        return {
            'body': {'x': 1},
            'headers': {'Header': 'dummy'},
            'media_type': 'text/plain'
        }

    def test_init_success(self: Self):
        _ = InternalRestResponse()

    @pytest.mark.parametrize(
        ('method', 'expected_status'),
        [
            (InternalRestResponse.ok, status.HTTP_200_OK),
            (InternalRestResponse.accepted, status.HTTP_202_ACCEPTED),
            (InternalRestResponse.no_content, status.HTTP_204_NO_CONTENT)
        ]
    )
    def test_2xx_response_generation_returns_correct_status_and_params(self: Self, method: Callable[[Any], InternalRestResponse], expected_status: status, resp_params: dict[str, Any]):
        resp: InternalRestResponse = method(**resp_params)
        assert resp.status == expected_status
        assert resp.body == resp_params.get('body')
        assert resp.headers == resp_params.get('headers')
        assert resp.media_type == resp_params.get('media_type')

    @pytest.mark.parametrize(
        ('method', 'expected_status'),
        [
            (InternalRestResponse.bad_request, status.HTTP_400_BAD_REQUEST),
            (InternalRestResponse.not_found, status.HTTP_404_NOT_FOUND),
            (InternalRestResponse.request_timeout, status.HTTP_408_REQUEST_TIMEOUT),
            (InternalRestResponse.conflict, status.HTTP_409_CONFLICT),
            (InternalRestResponse.unprocessable_entity, status.HTTP_422_UNPROCESSABLE_ENTITY),
            (InternalRestResponse.internal_server_error, status.HTTP_500_INTERNAL_SERVER_ERROR)
        ]
    )
    def test_4xx_5xx_response_generation_returns_correct_status_and_params(self: Self, method: Callable[[Any], InternalRestResponse], expected_status: status, resp_params: dict[str, Any]):
        resp: InternalRestResponse = method(error_message='test_error_message', headers=resp_params.get('headers'), media_type=resp_params.get('media_type'))
        assert resp.status == expected_status
        assert resp.body.get('error_message', None) is not None and resp.body['error_message'] == 'test_error_message'
        assert resp.headers == resp_params.get('headers')
        assert resp.media_type == resp_params.get('media_type')

    @pytest.mark.parametrize(
        ('encode_method', 'decode_method'),
        [
            (InternalRestResponse.to_json, InternalRestResponse.from_json),
            (InternalRestResponse.serialize, InternalRestResponse.deserialize)
        ]
    )
    def test_encode_decode_methods_are_inverses(self: Self, encode_method: Callable[[InternalRestResponse], Any], decode_method: Callable[[Any], InternalRestResponse], resp_params: dict[str, Any]):
        resp = InternalRestResponse(status.HTTP_200_OK, **resp_params)
        enc = encode_method(resp)
        dec = decode_method(enc)
        assert isinstance(dec, InternalRestResponse)
        assert dec.status == status.HTTP_200_OK
        assert dec.body == resp_params.get('body')
        assert dec.headers == resp_params.get('headers')
        assert dec.media_type == resp_params.get('media_type')
