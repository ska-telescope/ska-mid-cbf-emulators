from typing import Self
import pytest
from ska_mid_cbf_emulators.common import EmulatorConfigIPBlock

class TestEmulatorConfigIPBlock:

    @pytest.fixture()
    def ip_block_config(self: Self):
        return EmulatorConfigIPBlock('test_id', 'test_display_name', 'test_type', ['test_id_2'], {'x': 3})
    
    def test_init_success(self: Self):
        _ = EmulatorConfigIPBlock('', '', '', [])
    
    def test_repr_success(self: Self, ip_block_config: EmulatorConfigIPBlock):
        assert isinstance(repr(ip_block_config), str)

    def test_property_setters_getters_correct(self: Self, ip_block_config: EmulatorConfigIPBlock):
        ip_block_config.id = 'new_id'
        ip_block_config.display_name = 'new_display_name'
        ip_block_config.type = 'new_type'
        ip_block_config.downstream_block_ids = ['new_ds_id_2']
        ip_block_config.upstream_block_ids = ['new_us_id_2']
        ip_block_config.constants = {'y': -1}
        assert ip_block_config.id == ip_block_config._id == 'new_id'
        assert ip_block_config.display_name == ip_block_config._display_name == 'new_display_name'
        assert ip_block_config.type == ip_block_config._type == 'new_type'
        assert ip_block_config.downstream_block_ids == ip_block_config._downstream_block_ids == {'new_ds_id_2'}
        assert ip_block_config.upstream_block_ids == ip_block_config._upstream_block_ids == {'new_us_id_2'}
        assert ip_block_config.constants == ip_block_config._constants == {'y': -1}
