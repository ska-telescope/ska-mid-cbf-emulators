from typing import Self
from unittest import mock
from jsonschema import ValidationError
import pytest
from contextlib import AbstractContextManager, nullcontext as does_not_raise
from ska_mid_cbf_emulators.common import PulseEvent, EventType, EventSeverity

class TestPulseEvent:
    
    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.BaseEvent.__init__') as mock_superclass_init:
            src = 123
            upd = 456
            _ = PulseEvent(source_timestamp=src, update_timestamp=upd)
            mock_superclass_init.assert_called_once()
            assert mock_superclass_init.call_args.kwargs.get('type') == EventType.PULSE
            assert mock_superclass_init.call_args.kwargs.get('source_timestamp') == src
            assert mock_superclass_init.call_args.kwargs.get('update_timestamp') == upd

    @pytest.mark.parametrize(
        ('json_event', 'expected_event', 'exception_expectation'),
        [
            pytest.param(
                (
                    r'{'
                    r'  "type": "PULSE",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "GENERAL",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                PulseEvent(
                    {"id": 0},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='all_uppercase_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "warning",'
                    r'  "source_timestamp": 12345,'
                    r'  "update_timestamp": 12543'
                    r'}'
                ),
                PulseEvent(
                    {"id": 0},
                    EventSeverity.WARNING,
                    12345,
                    12543
                ),
                does_not_raise(),
                id='all_lowercase_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 0'
                    r'}'
                ),
                PulseEvent(
                    {"id": 0},
                    EventSeverity.GENERAL,
                    0
                ),
                does_not_raise(),
                id='timestamp_0_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0, "x": [1, 2, 3, 4, 5]},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                PulseEvent(
                    {'id': 0, 'x': [1, 2, 3, 4, 5]},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='simple_value_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0, "x": [1, 2, 3, 4.7, {"x1": [6, 7], "x2": {"x3": true}}], "y": [[[[[[[[{}],{}],{}],{}],{}],{}],{}],{}]},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                PulseEvent(
                    {"id": 0, "x": [1, 2, 3, pytest.approx(4.7), {"x1": [6, 7], "x2": {"x3": True}}], "y": [[[[[[[[{}],{}],{}],{}],{}],{}],{}],{}]},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='complex_value_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "value": {"id": 0},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                PulseEvent(
                    {"id": 0},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='type_not_specified_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "MANUAL",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "GENERAL",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": -1'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='negative_timestamp_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='value_missing_id_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": "asdf",'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='value_not_a_dict_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "wrong_severity",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='invalid_severity_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_value_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_severity_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "pulse",'
                    r'  "value": {"id": 0},'
                    r'  "severity": "general"'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_timestamp_error'
            ),
        ]
    )
    def test_decode(self: Self, json_event: str, expected_event: PulseEvent, exception_expectation: AbstractContextManager):
        with exception_expectation:
            result = PulseEvent.decode(json_event)
            if expected_event is not None:
                assert result.type == expected_event.type
                assert result.subtype == expected_event.subtype == None
                assert result.value == expected_event.value
                assert result.severity == expected_event.severity
                assert result.source_timestamp == expected_event.source_timestamp
                assert result.update_timestamp == expected_event.update_timestamp
