from typing import Self
from ska_mid_cbf_emulators.common import ApiParam

class TestApiParam:
    
    def test_init_success(self: Self):
        _ = ApiParam[str]('test_name', str, 'default')
    
    def test_repr_success(self: Self):
        assert isinstance(repr(ApiParam[str]('test_name', str, 'default')), str)
