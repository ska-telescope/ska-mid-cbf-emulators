from enum import auto
from io import BytesIO
from typing import Any, Self, override
from unittest import mock
from contextlib import AbstractContextManager, nullcontext as does_not_raise

import pytest
from transitions import MachineError
from ska_mid_cbf_emulators.common import EmulatorSubcontroller, EmulatorConfigIPBlock, BaseState, BaseTransitionTrigger, FiniteStateMachine, TransitionCondition, RoutingState

class TestEmulatorSubcontroller:

    @pytest.fixture()
    def id(self: Self) -> str:
        return 'test_id'

    @pytest.fixture()
    def display_name(self: Self) -> str:
        return 'test_display_name'

    @pytest.fixture()
    def type(self: Self) -> str:
        return 'test_type'

    @pytest.fixture()
    def downstream_block_ids(self: Self) -> list[str]:
        return ['test_id_2']

    @pytest.fixture()
    def ip_block_config(self: Self, id: str, display_name: str, type: str, downstream_block_ids: list[str]):
        return EmulatorConfigIPBlock(id, display_name, type, downstream_block_ids)

    @pytest.fixture()
    def diagram_params(self: Self):
        return {
            'file': BytesIO(),
            'title': 'test_title',
            'format': 'png'
        }

    @pytest.fixture(scope='class')
    def state(self: Self):
        class MockState(BaseState):
            STATE_ONE = 'STATE_ONE'
            STATE_TWO = 'STATE_TWO'
            STATE_THREE = 'STATE_THREE'
            STATE_FOUR = 'STATE_FOUR'
        return MockState

    @pytest.fixture(scope='class')
    def transition_trigger(self: Self):
        class MockTransitionTrigger(BaseTransitionTrigger):
            TRIG_ONE = auto()
            TRIG_TWO = auto()
            TRIG_THREE = auto()
            TRIG_FOUR = auto()
        return MockTransitionTrigger

    @pytest.fixture(scope='class')
    def state_machine(self: Self, state: BaseState, transition_trigger: BaseTransitionTrigger):
        class MockStateMachine(FiniteStateMachine):
            @override
            @property
            def _states(self):
                return [
                    state.STATE_ONE,
                    state.STATE_TWO,
                    state.STATE_THREE,
                    state.STATE_FOUR
                ]

            @override
            @property
            def _initial_state(self):
                return state.STATE_ONE

            @override
            @property
            def _transitions(self):
                return [
                    {
                        'source': state.STATE_ONE,
                        'dest': state.STATE_TWO,
                        'trigger': transition_trigger.TRIG_ONE
                    },
                    {
                        'source': state.STATE_ONE,
                        'dest': state.STATE_TWO,
                        'trigger': transition_trigger.TRIG_TWO,
                        'conditions': TransitionCondition(
                            'x > 0',
                            lambda x: x > 0
                        )
                    },
                    {
                        'source': state.STATE_ONE,
                        'dest': state.STATE_THREE,
                        'trigger': transition_trigger.TRIG_TWO,
                        'conditions': TransitionCondition(
                            'x < 0',
                            lambda x: x < 0
                        )
                    },
                    {
                        'source': RoutingState.FROM_ANY,
                        'dest': RoutingState.TO_SAME,
                        'trigger': transition_trigger.TRIG_THREE
                    },
                    {
                        'source': [state.STATE_TWO, state.STATE_THREE],
                        'dest': state.STATE_FOUR,
                        'trigger': transition_trigger.TRIG_FOUR
                    }
                ]
        return MockStateMachine

    @pytest.fixture()
    def mock_msg_svc(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.BaseMessageService') as mock_svc:
            yield mock_svc

    @pytest.fixture()
    def default_subcontroller(self: Self, mock_msg_svc: mock.MagicMock, ip_block_config: EmulatorConfigIPBlock):
        return EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc(), ip_block_config=ip_block_config)

    @pytest.fixture()
    def fsm_subcontroller(self: Self, mock_msg_svc: mock.MagicMock, state_machine: FiniteStateMachine, ip_block_config: EmulatorConfigIPBlock):
        return EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc(), state_machine=state_machine(), ip_block_config=ip_block_config)

    def test_init_success(self: Self, mock_msg_svc: mock.MagicMock):
        _ = EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc())

    def test_init_ip_block_config_overrides_other_arguments(self: Self, mock_msg_svc: mock.MagicMock, ip_block_config: EmulatorConfigIPBlock):
        m = EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc(), None, ip_block_config, 'overridden', 'overridden', 'overridden', ['overridden'], None)
        assert m._ip_block_id == ip_block_config.id
        assert m._display_name == ip_block_config.display_name
        assert m._type == ip_block_config.type
        assert m._downstream_block_ids == list(ip_block_config.downstream_block_ids)

    def test_init_ip_block_config_not_provided_uses_other_arguments(self: Self, mock_msg_svc: mock.MagicMock, id: str, display_name: str, type: str, downstream_block_ids: list[str]):
        m = EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc(), None, None, id, display_name, type, downstream_block_ids, None)
        assert m._ip_block_id == id
        assert m._display_name == display_name
        assert m._type == type
        assert m._downstream_block_ids == downstream_block_ids

    def test_subcontroller_init_success(self: Self, default_subcontroller: EmulatorSubcontroller):
        default_subcontroller.subcontroller_init()

    def test_repr_success(self: Self, mock_msg_svc: mock.MagicMock):
        assert isinstance(repr(EmulatorSubcontroller('default-emulator', mock_msg_svc(), mock_msg_svc())), str)

    def test_property_setters_getters_correct(self: Self, default_subcontroller: EmulatorSubcontroller):
        default_subcontroller.ip_block_id = 'new_id'
        default_subcontroller.display_name = 'new_display_name'
        default_subcontroller.type = 'new_type'
        default_subcontroller.downstream_block_ids = ['new_id_2']
        default_subcontroller.ip_block = object()
        default_subcontroller.event_service = object()
        default_subcontroller.api_service = object()
        default_subcontroller.state_machine = object()
        assert default_subcontroller.ip_block_id == default_subcontroller._ip_block_id == 'new_id'
        assert default_subcontroller.display_name == default_subcontroller._display_name == 'new_display_name'
        assert default_subcontroller.type == default_subcontroller._type == 'new_type'
        assert default_subcontroller.downstream_block_ids == default_subcontroller._downstream_block_ids == ['new_id_2']
        assert default_subcontroller.ip_block == default_subcontroller._ip_block is not None
        assert default_subcontroller.event_service == default_subcontroller._event_service is not None
        assert default_subcontroller.api_service == default_subcontroller._api_service is not None
        assert default_subcontroller.state_machine == default_subcontroller._state_machine is not None

    def test_may_trigger_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            result = default_subcontroller.may_trigger('trig_one')
            assert result is False
            mock_logger_error.assert_called_once()

    @pytest.mark.parametrize(
        ('trigger_name', 'condition_kwargs', 'expected_result', 'exception_expected'),
        [
            ('trig_one', {}, True, False),
            ('trig_two', {'x': 1}, True, False),
            ('trig_two', {'x': -1}, True, False),
            ('trig_two', {'x': 0}, False, False),
            ('trig_three', {}, True, False),
            ('trig_four', {}, False, False),
            ('trig_error', {}, False, True),
        ]
    )
    def test_may_trigger(self: Self, trigger_name: str, condition_kwargs: dict[str, Any], expected_result: bool, exception_expected: bool, fsm_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(fsm_subcontroller._logger, 'error') as mock_logger_error:
            result = fsm_subcontroller.may_trigger(trigger_name, **condition_kwargs)
            assert result == expected_result
            if exception_expected:
                mock_logger_error.assert_called_once()

    def test_trigger_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            default_subcontroller.trigger('trig_one')
            mock_logger_error.assert_called_once()

    @pytest.mark.parametrize(
        ('initial_state', 'trigger_name', 'condition_kwargs', 'expected_state', 'exception_expectation'),
        [
            ('STATE_ONE', 'trig_one', {}, 'STATE_TWO', does_not_raise()),
            ('STATE_ONE', 'trig_two', {'x': 1}, 'STATE_TWO', does_not_raise()),
            ('STATE_ONE', 'trig_two', {'x': 0}, 'STATE_ONE', does_not_raise()),
            ('STATE_ONE', 'trig_three', {}, 'STATE_ONE', does_not_raise()),
            ('STATE_ONE', 'trig_four', {}, 'STATE_ONE', pytest.raises(MachineError)),
            ('STATE_ONE', 'trig_error', {}, 'STATE_ONE', pytest.raises((KeyError, AttributeError))),
            ('STATE_TWO', 'trig_three', {}, 'STATE_TWO', does_not_raise()),
            ('STATE_TWO', 'trig_four', {}, 'STATE_FOUR', does_not_raise()),
            ('STATE_TWO', 'trig_two', {'x': 1}, 'STATE_TWO', pytest.raises(MachineError)),
            ('STATE_THREE', 'trig_four', {}, 'STATE_FOUR', does_not_raise()),
        ]
    )
    def test_trigger(self: Self, initial_state: str, trigger_name: str, condition_kwargs: dict[str, Any], expected_state: str, exception_expectation: AbstractContextManager, fsm_subcontroller: EmulatorSubcontroller):
        with exception_expectation:
            fsm_subcontroller.state_machine.trigger(f'to_{initial_state}')
            fsm_subcontroller.trigger(trigger_name, **condition_kwargs)
            assert fsm_subcontroller.state_machine.state == expected_state

    def test_trigger_if_allowed_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            default_subcontroller.trigger_if_allowed('trig_one')
            mock_logger_error.assert_called_once()

    @pytest.mark.parametrize(
        ('initial_state', 'trigger_name', 'condition_kwargs', 'expected_state', 'expected_result', 'exception_expected'),
        [
            ('STATE_ONE', 'trig_one', {}, 'STATE_TWO', True, False),
            ('STATE_ONE', 'trig_two', {'x': 1}, 'STATE_TWO', True, False),
            ('STATE_ONE', 'trig_two', {'x': 0}, 'STATE_ONE', False, False),
            ('STATE_ONE', 'trig_three', {}, 'STATE_ONE', True, False),
            ('STATE_ONE', 'trig_four', {}, 'STATE_ONE', False, False),
            ('STATE_ONE', 'trig_error', {}, 'STATE_ONE', False, True),
            ('STATE_TWO', 'trig_three', {}, 'STATE_TWO', True, False),
            ('STATE_TWO', 'trig_four', {}, 'STATE_FOUR', True, False),
            ('STATE_TWO', 'trig_two', {'x': 1}, 'STATE_TWO', False, False),
            ('STATE_THREE', 'trig_four', {}, 'STATE_FOUR', True, False),
        ]
    )
    def test_trigger_if_allowed(self: Self, initial_state: str, trigger_name: str, condition_kwargs: dict[str, Any], expected_state: str, expected_result: bool, exception_expected: bool, fsm_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(fsm_subcontroller._logger, 'error') as mock_logger_error:
            fsm_subcontroller.state_machine.trigger(f'to_{initial_state}')
            result = fsm_subcontroller.trigger_if_allowed(trigger_name, **condition_kwargs)
            assert fsm_subcontroller.state_machine.state == expected_state
            assert result == expected_result
            if exception_expected:
                mock_logger_error.assert_called_once()

    def test_trigger_if_allowed_unexpected_exception(self: Self, fsm_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(EmulatorSubcontroller, 'may_trigger', side_effect=Exception('mock_exception')):
            result = fsm_subcontroller.trigger_if_allowed('trig_one')
            assert result is False

    def test_get_state_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            result = default_subcontroller.get_state()
            assert result is None
            mock_logger_error.assert_called_once()

    def test_get_state_success(self: Self, fsm_subcontroller: EmulatorSubcontroller, state: BaseState):
        assert fsm_subcontroller.get_state() == fsm_subcontroller.state_machine._initial_state
        fsm_subcontroller.state_machine.trigger(f'to_{state.STATE_TWO}')
        assert fsm_subcontroller.get_state() == state.STATE_TWO

    def test_force_state_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller, state: BaseState):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            result = default_subcontroller.force_state(state.STATE_TWO)
            assert result is None
            mock_logger_error.assert_called_once()

    def test_force_state_success(self: Self, fsm_subcontroller: EmulatorSubcontroller, state: BaseState):
        fsm_subcontroller.force_state(state.STATE_TWO)
        assert fsm_subcontroller.state_machine.state == state.STATE_TWO

    def test_force_state_unexpected_exception(self: Self, fsm_subcontroller: EmulatorSubcontroller, state: BaseState):
        with mock.patch.object(fsm_subcontroller, 'get_state', side_effect=Exception('mock_exception')):
            with mock.patch.object(fsm_subcontroller._logger, 'error') as mock_logger_error:
                fsm_subcontroller.force_state(state.STATE_TWO)
                mock_logger_error.assert_called_once()

    def test_export_state_diagram_full_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller, diagram_params: dict[str, Any]):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            default_subcontroller.export_state_diagram_full(**diagram_params)
            mock_logger_error.assert_called_once()

    def test_export_state_diagram_full_success(self: Self, fsm_subcontroller: EmulatorSubcontroller, diagram_params: dict[str, Any]):
        with mock.patch.object(fsm_subcontroller.state_machine, 'export_diagram_full') as mock_export_diagram_full:
            fsm_subcontroller.export_state_diagram_full(**diagram_params)
            mock_export_diagram_full.assert_called_once_with(diagram_params['file'], title=diagram_params['title'], format=diagram_params['format'])

    def test_export_state_diagram_current_context_no_state_machine_error(self: Self, default_subcontroller: EmulatorSubcontroller, diagram_params):
        with mock.patch.object(default_subcontroller._logger, 'error') as mock_logger_error:
            default_subcontroller.export_state_diagram_current_context(**diagram_params)
            mock_logger_error.assert_called_once()

    def test_export_state_diagram_current_context_success(self: Self, fsm_subcontroller: EmulatorSubcontroller, diagram_params: dict[str, Any]):
        with mock.patch.object(fsm_subcontroller.state_machine, 'export_diagram_current_context') as mock_export_diagram_current_context:
            fsm_subcontroller.export_state_diagram_current_context(**diagram_params)
            mock_export_diagram_current_context.assert_called_once_with(diagram_params['file'], title=diagram_params['title'], format=diagram_params['format'])

    def test_subscribe_success(self: Self, default_subcontroller: EmulatorSubcontroller):
        default_subcontroller.subscribe(x=1, y=2)
        default_subcontroller.event_service.subscribe.assert_has_calls([mock.ANY, mock.call(x=1, y=2)])

    def test_unsubscribe_success(self: Self, default_subcontroller: EmulatorSubcontroller):
        default_subcontroller.unsubscribe(x=1, y=2)
        default_subcontroller.event_service.unsubscribe.assert_has_calls([mock.call(x=1, y=2), mock.call(x=1, y=2)])

    def test_publish_success(self: Self, default_subcontroller: EmulatorSubcontroller):
        with (
            mock.patch('ska_mid_cbf_emulators.common.BaseEvent') as mock_event,
            mock.patch.object(default_subcontroller.event_service, 'publish') as mock_event_publish
        ):
            event = mock_event()
            default_subcontroller.publish(event, 1, y=2)
            mock_event_publish.assert_called_once_with(event, 1, y=2)

    def test_publish_manual_event_success(self: Self, default_subcontroller: EmulatorSubcontroller):
        with (
            mock.patch('ska_mid_cbf_emulators.common.ManualEvent') as mock_event,
            mock.patch.object(default_subcontroller.event_service, 'publish') as mock_event_publish
        ):
            default_subcontroller.event_service.manual_exchange = 'test_exchange'
            event = mock_event()
            default_subcontroller.publish_manual_event(event, 1, y=2)
            mock_event_publish.assert_called_once_with(event, 1, exchange='test_exchange', y=2)
