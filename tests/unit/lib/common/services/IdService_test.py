from typing import Self
from ska_mid_cbf_emulators.common.services.messaging.id_service import IdService

class TestIdService:

    def test_pulse_queue_id_success(self: Self):
        result = IdService.pulse_queue_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_pulse_queue'

    def test_manual_queue_id_all_params_success(self: Self):
        result = IdService.manual_queue_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_manual_queue'

    def test_manual_queue_emulator_only_success(self: Self):
        result = IdService.manual_queue_id('emu_asdfghjkl_12345')
        assert result == 'emu_asdfghjkl_12345_manual_queue'

    def test_api_request_queue_id_all_params_success(self: Self):
        result = IdService.api_request_queue_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_api_request_queue'

    def test_api_request_queue_emulator_only_success(self: Self):
        result = IdService.api_request_queue_id('emu_asdfghjkl_12345')
        assert result == 'emu_asdfghjkl_12345_api_request_queue'

    def test_api_callback_queue_id_all_params_success(self: Self):
        result = IdService.api_callback_queue_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_api_callback_queue'

    def test_api_callback_queue_emulator_only_success(self: Self):
        result = IdService.api_callback_queue_id('emu_asdfghjkl_12345')
        assert result == 'emu_asdfghjkl_12345_api_callback_queue'

    def test_pulse_exchange_id_success(self: Self):
        result = IdService.pulse_exchange_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_pulse_exchange'

    def test_manual_exchange_id_success(self: Self):
        result = IdService.manual_exchange_id('emu_asdfghjkl_12345', 'ip_zxcvbnm_67890')
        assert result == 'emu_asdfghjkl_12345_ip_zxcvbnm_67890_manual_exchange'
