from typing import Any, Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import EmulatorConfigValidator, EmulatorConfigValidatorResult, ValidatorStatus, EmulatorConfig, EmulatorConfigIPBlock

class TestEmulatorConfigValidator:

    def test_validate_only_returns_validation_result(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.EmulatorConfigValidator._validate') as mock_intl_validate:
            result = EmulatorConfigValidator.validate({'x': 17})
            mock_intl_validate.assert_called_once_with({'x': 17}, mock.ANY)
            assert isinstance(result, EmulatorConfigValidatorResult)

    @pytest.mark.parametrize(
        ('config_dict', 'expected_overall', 'expected_num_ok', 'expected_num_warn', 'expected_num_err', 'expected_config'),
        [
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.OK, 0, 0, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[]
                        ),
                    ],
                    first=['ip_A']
                ),
                id='single_ip_block_ok'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.OK, 0, 0, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_B'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[
                                'ip_C'
                            ],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_A']
                ),
                id='linear_ip_blocks_ok'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B',
                                'ip_C'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.OK, 0, 0, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_B',
                                'ip_C'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_A']
                ),
                id='branching_ip_blocks_ok'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_C'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A', 'ip_B']
                },
                ValidatorStatus.OK, 0, 0, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_C'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[
                                'ip_C'
                            ],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_A', 'ip_B']
                ),
                id='parallel_first_blocks_ok'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.WARNING, 0, 1, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_B'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_A']
                ),
                id='disconnected_ip_blocks_warning'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_B']
                },
                ValidatorStatus.WARNING, 0, 1, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_B'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[
                                'ip_C'
                            ],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_B']
                ),
                id='first_is_not_root_warning'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_C'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.WARNING, 0, 1, 0,
                EmulatorConfig(
                    id='test_id',
                    version='0.0.1',
                    ip_blocks=[
                        EmulatorConfigIPBlock(
                            id='ip_A',
                            display_name='IP Block A',
                            type='type1',
                            downstream_block_ids=[
                                'ip_C'
                            ]
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_B',
                            display_name='IP Block B',
                            type='type2',
                            downstream_block_ids=[
                                'ip_C'
                            ],
                            constants={
                                'B1': 1,
                                'B2': 2
                            }
                        ),
                        EmulatorConfigIPBlock(
                            id='ip_C',
                            display_name='IP Block C',
                            type='type3',
                            downstream_block_ids=[],
                            constants={
                                'C1': 1
                            }
                        ),
                    ],
                    first=['ip_A']
                ),
                id='excess_root_ip_blocks_warning'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_C'
                            ]
                        },
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='duplicate_ip_block_id_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_D']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='invalid_first_ip_block_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_B',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [
                                'ip_C'
                            ],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_C',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [
                                'ip_D'
                            ],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='invalid_downstream_block_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B'
                            ]
                        },
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block B',
                            'type': 'type2',
                            'downstream_block_ids': [],
                            'constants': {
                                'B1': 1,
                                'B2': 2
                            }
                        },
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block C',
                            'type': 'type3',
                            'downstream_block_ids': [],
                            'constants': {
                                'C1': 1
                            }
                        },
                    ],
                    'first': ['ip_B']
                },
                ValidatorStatus.ERROR, 0, 0, 4,
                None,
                id='multiple_errors_error'
            ),
            pytest.param(
                {
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_config_id_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_config_version_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_config_ip_blocks_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_config_first_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': 'abc123',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='invalid_semantic_version_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [],
                    'first': ['']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='empty_ip_blocks_list_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        },
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='duplicate_ip_block_config_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_ip_block_id_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'type': 'type1',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_ip_block_display_name_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'downstream_block_ids': []
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_ip_block_type_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1'
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='missing_ip_block_downstream_block_ids_error'
            ),
            pytest.param(
                {
                    'id': 'test_id',
                    'version': '0.0.1',
                    'ip_blocks': [
                        {
                            'id': 'ip_A',
                            'display_name': 'IP Block A',
                            'type': 'type1',
                            'downstream_block_ids': [
                                'ip_B',
                                'ip_B'
                            ]
                        }
                    ],
                    'first': ['ip_A']
                },
                ValidatorStatus.ERROR, 0, 0, 1,
                None,
                id='duplicate_downstream_block_id_error'
            ),
        ]
    )
    def test_validate_and_get_config(self: Self, config_dict: dict[Any, Any], expected_overall: ValidatorStatus, expected_num_ok: int, expected_num_warn: int, expected_num_err: int, expected_config: EmulatorConfig):
        result, config = EmulatorConfigValidator.validate_and_get_config(config_dict)
        assert result.overall_status == expected_overall
        assert sum(map(lambda msg: msg.status == ValidatorStatus.OK, result.result)) == expected_num_ok
        assert sum(map(lambda msg: msg.status == ValidatorStatus.WARNING, result.result)) == expected_num_warn
        assert sum(map(lambda msg: msg.status == ValidatorStatus.ERROR, result.result)) == expected_num_err
        if expected_config is None:
            assert config is None
            return
        assert config.id == expected_config.id
        assert config.version == expected_config.version
        assert config.first == expected_config.first
        for ip_block, exp_ip_block in zip(config.ip_blocks, expected_config.ip_blocks):
            assert ip_block.id == exp_ip_block.id
            assert ip_block.display_name == exp_ip_block.display_name
            assert ip_block.type == exp_ip_block.type
            assert ip_block.downstream_block_ids == exp_ip_block.downstream_block_ids
            assert ip_block.constants == exp_ip_block.constants
