from typing import Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import EmulatorConfigParser, EmulatorConfigValidator, EmulatorConfigValidatorResult, ValidatorStatus, EmulatorConfig, ValidationError

class TestEmulatorConfigParser:

    def test_decode_validation_ok_success(self: Self):
        with (
            mock.patch.object(EmulatorConfig, '_generate_graph'),
            mock.patch.object(EmulatorConfigValidator, 'validate_and_get_config', return_value=(EmulatorConfigValidatorResult(ValidatorStatus.OK), EmulatorConfig('c_id', '', [], ''))) as mock_validate
        ):
            result = EmulatorConfigParser.decode(r'{"dummy_key": "dummy_value"}')
            mock_validate.assert_called_once_with({'dummy_key': 'dummy_value'})
            assert result is not None and result.id == 'c_id'

    def test_decode_validation_warning_success(self: Self):
        with (
            mock.patch.object(EmulatorConfig, '_generate_graph'),
            mock.patch.object(EmulatorConfigValidator, 'validate_and_get_config', return_value=(EmulatorConfigValidatorResult(ValidatorStatus.WARNING), EmulatorConfig('c_id', '', [], ''))) as mock_validate
        ):
                result = EmulatorConfigParser.decode(r'{"dummy_key": "dummy_value"}')
                mock_validate.assert_called_once_with({'dummy_key': 'dummy_value'})
                assert result is not None and result.id == 'c_id'

    def test_decode_validation_error_error(self: Self):
        with (
            mock.patch.object(EmulatorConfigValidator, 'validate_and_get_config', return_value=(EmulatorConfigValidatorResult(ValidatorStatus.ERROR), None)) as mock_validate,
            pytest.raises(ValidationError)
        ):
            result = EmulatorConfigParser.decode(r'{"dummy_key": "dummy_value"}')
            mock_validate.assert_called_once_with({'dummy_key': 'dummy_value'})
            assert result is None
