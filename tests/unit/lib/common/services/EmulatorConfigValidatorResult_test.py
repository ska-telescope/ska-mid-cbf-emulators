from typing import Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import EmulatorConfigValidatorResult, ValidatorStatus, ValidatorMessage

class TestEmulatorConfigValidatorResult:

    @pytest.fixture()
    def validator_result(self: Self):
        result = EmulatorConfigValidatorResult(ValidatorStatus.WARNING)
        result.result = [
            ValidatorMessage(ValidatorStatus.OK, 'ok_msg_1'),
            ValidatorMessage(ValidatorStatus.WARNING, 'warn_msg_1'),
            ValidatorMessage(ValidatorStatus.OK, 'ok_msg_2')
        ]
        return result

    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.BaseValidatorResult.__init__') as mock_superclass_init:
            r = EmulatorConfigValidatorResult(ValidatorStatus.WARNING)
            mock_superclass_init.assert_called_once()
            assert r.result == []

    def test_json_dict_property_is_correct(self: Self, validator_result: EmulatorConfigValidatorResult):
        d = validator_result.json_dict
        assert set(d.keys()) == {'overall_status', 'result'}
        assert d.get('overall_status') == 'WARNING'
        result = d.get('result')
        assert result[0].get('status') == 'OK'
        assert result[1].get('status') == 'WARNING'
        assert result[2].get('status') == 'OK'
        assert result[0].get('message') == 'ok_msg_1'
        assert result[1].get('message') == 'warn_msg_1'
        assert result[2].get('message') == 'ok_msg_2'

    @pytest.mark.parametrize(
        ('msg_status', 'msg_msg', 'expected_overall_status'),
        [
            pytest.param(ValidatorStatus.OK, 'ok_msg_3', ValidatorStatus.WARNING, id='new_status_lower_severity_does_not_override_success'),
            pytest.param(ValidatorStatus.WARNING, 'warn_msg_2', ValidatorStatus.WARNING, id='new_status_equal_severity_does_not_override_success'),
            pytest.param(ValidatorStatus.ERROR, 'err_msg_1', ValidatorStatus.ERROR, id='new_status_higher_severity_does_override_success'),
        ]
    )
    def test_insert_msg(self: Self, msg_status: ValidatorStatus, msg_msg: str, expected_overall_status: ValidatorStatus, validator_result: EmulatorConfigValidatorResult):
        validator_result.insert_msg(ValidatorMessage(msg_status, msg_msg))
        assert len(validator_result.result) == 4 and validator_result.result[-1].message == msg_msg
        assert validator_result.overall_status == expected_overall_status
