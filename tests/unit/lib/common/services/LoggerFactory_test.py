from typing import Self
from unittest import mock

from ska_mid_cbf_emulators.common import LoggerFactory, EmulatorLogLevel

class TestLoggerFactory:

    def test_get_logger_key_found_success(self: Self):
        with (
            mock.patch('logging.getLogger') as mock_get_logger,
            mock.patch('logging.config.dictConfig') as mock_logging_config
        ):
            LoggerFactory._logger = None
            with mock.patch('os.getenv', return_value='dev'):
                LoggerFactory.get_logger()
            assert LoggerFactory._logger is not None
            mock_logging_config.assert_called_once()
            mock_get_logger.assert_called_with('dev')

    def test_get_logger_key_not_found_success(self: Self):
        with (
            mock.patch('logging.getLogger') as mock_get_logger,
            mock.patch('logging.config.dictConfig') as mock_logging_config
        ):
            LoggerFactory._logger = None
            with mock.patch('os.getenv', return_value='this_is_not_a_valid_logger_key'):
                LoggerFactory.get_logger()
            assert LoggerFactory._logger is not None
            mock_logging_config.assert_called_once()
            mock_get_logger.assert_called_with()

    def test_get_logger_file_not_found_success(self: Self):
        with (
            mock.patch('yaml.safe_load', side_effect=FileNotFoundError('mock_error_msg')),
            mock.patch('logging.config.dictConfig') as mock_logging_config
        ):
            LoggerFactory._logger = None
            LoggerFactory.get_logger()
            assert LoggerFactory._logger is not None
            mock_logging_config.assert_not_called()

    def test_override_console_log_level_success(self: Self):
        with (
            mock.patch('logging.config.dictConfig') as mock_logging_config
        ):
            LoggerFactory._logger = None
            LoggerFactory._overridden_console_log_level = None
            LoggerFactory.override_console_log_level(EmulatorLogLevel.TRACE)
            LoggerFactory.get_logger()
            assert mock_logging_config.call_args is not None and isinstance(mock_logging_config.call_args.args[0], dict)
            assert mock_logging_config.call_args.args[0].get('handlers', {}).get('console', {}).get('level', None) == EmulatorLogLevel.TRACE.name
