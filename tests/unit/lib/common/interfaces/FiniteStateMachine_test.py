from __future__ import annotations

from enum import auto
from io import BytesIO
from typing import Self, override
from unittest import mock

import pytest
from ska_mid_cbf_emulators.common import FiniteStateMachine, BaseState, BaseTransitionTrigger, TransitionCondition, RoutingState
from transitions.extensions.diagrams import GraphMachine

class TestFiniteStateMachine:

    @pytest.fixture(scope='class')
    def state(self: Self):
        class MockState(BaseState):
            STATE_ONE = 'STATE_ONE'
            STATE_TWO = 'STATE_TWO'
            STATE_THREE = 'STATE_THREE'
        return MockState

    @pytest.fixture(scope='class')
    def transition_trigger(self: Self):
        class MockTransitionTrigger(BaseTransitionTrigger):
            TRIG_ONE = auto()
            TRIG_TWO = auto()
            TRIG_THREE = auto()
        return MockTransitionTrigger

    @pytest.fixture(scope='class')
    def state_machine(self: Self, state: BaseState, transition_trigger: BaseTransitionTrigger):
        class MockStateMachine(FiniteStateMachine):
            @override
            @property
            def _states(self):
                return [
                    state.STATE_ONE,
                    state.STATE_TWO,
                    state.STATE_THREE
                ]

            @override
            @property
            def _initial_state(self):
                return state.STATE_ONE

            @override
            @property
            def _transitions(self):
                return [
                    {
                        'source': state.STATE_ONE,
                        'dest': state.STATE_TWO,
                        'trigger': transition_trigger.TRIG_ONE
                    },
                    {
                        'source': state.STATE_TWO,
                        'dest': state.STATE_ONE,
                        'trigger': transition_trigger.TRIG_ONE,
                        'conditions': TransitionCondition(
                            'x > 0',
                            lambda x: x > 0
                        )
                    },
                    {
                        'source': state.STATE_TWO,
                        'dest': state.STATE_THREE,
                        'trigger': transition_trigger.TRIG_ONE,
                        'conditions': TransitionCondition(
                            'x <= 0',
                            lambda x: x <= 0
                        )
                    },
                    {
                        'source': [state.STATE_ONE, state.STATE_TWO],
                        'dest': state.STATE_THREE,
                        'trigger': transition_trigger.TRIG_TWO,
                        'conditions': lambda: True
                    },
                    {
                        'source': RoutingState.FROM_ANY,
                        'dest': RoutingState.TO_SAME,
                        'trigger': transition_trigger.TRIG_THREE
                    }
                ]
        return MockStateMachine

    @mock.patch.multiple(FiniteStateMachine, __abstractmethods__=set())
    def test_init_success(self: Self, state_machine: FiniteStateMachine):
        _ = state_machine()

    def test_export_diagram_full_success(self: Self, state_machine: FiniteStateMachine):
        with mock.patch.object(GraphMachine, 'get_combined_graph') as mock_get_combined_graph:
            fsm: FiniteStateMachine = state_machine()
            file = BytesIO()
            title = 'some_title'
            format = 'png'
            fsm.export_diagram_full(file, title, format)
            mock_get_combined_graph.assert_called_once_with(title=title)

    def test_export_diagram_current_context_success(self: Self, state_machine: FiniteStateMachine):
        with mock.patch.object(GraphMachine, 'get_combined_graph') as mock_get_combined_graph:
            fsm: FiniteStateMachine = state_machine()
            file = BytesIO()
            title = 'some_title'
            format = 'png'
            fsm.export_diagram_current_context(file, title, format)
            mock_get_combined_graph.assert_called_once_with(show_roi=True, title=title)
