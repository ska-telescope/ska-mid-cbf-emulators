from typing import Self
from unittest import mock
from ska_mid_cbf_emulators.common import TransitionCondition


class TestTransitionCondition:

    def test_init_success(self: Self):
        _ = TransitionCondition('test', lambda: True)

    def test_inner_fn_called(self: Self):
        tc = TransitionCondition('test', lambda x: x > 0)
        with mock.patch.object(tc, 'fn') as mock_fn:
            tc(x=1)
            mock_fn.assert_called_once_with(x=1)
