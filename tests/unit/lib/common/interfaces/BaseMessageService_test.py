from typing import Self
from unittest import mock

import pytest
from ska_mid_cbf_emulators.common import BaseMessageService, LoggerFactory

class TestBaseMessageService:

    @pytest.fixture(autouse=True)
    @mock.patch.multiple(BaseMessageService, __abstractmethods__=set())
    def create_service(self: Self):
        self.ms = BaseMessageService()

    @mock.patch.multiple(BaseMessageService, __abstractmethods__=set())
    def test_init_success(self: Self):
        _ = BaseMessageService()

    def test_unsubscribe_success(self: Self):
        self.ms.unsubscribe()
