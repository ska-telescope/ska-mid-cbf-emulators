from typing import Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import BaseEvent, EventType, ManualEventSubType, EventSeverity

class TestBaseEvent:

    @pytest.fixture()
    @mock.patch.multiple(BaseEvent, __abstractmethods__=set())
    def event(self: Self):
        return BaseEvent(type=EventType.PULSE, value={'x': -1}, severity=EventSeverity.GENERAL, source_timestamp=12345)
    
    @mock.patch.multiple(BaseEvent, __abstractmethods__=set())
    def test_init_success(self: Self):
        e = BaseEvent()
        assert e._source_timestamp is not None
        assert e._update_timestamp is not None

    def test_repr_success(self: Self, event: BaseEvent):
        assert isinstance(repr(event), str)

    def test_encode_success(self: Self, event: BaseEvent):
        with mock.patch('json.dumps') as mock_dumps:
            BaseEvent.encode(event)
            mock_dumps.assert_called_once_with({
                'type': event.type,
                'subtype': event.subtype,
                'severity': event.severity,
                'source_timestamp': event.source_timestamp,
                'update_timestamp': event.update_timestamp,
                'value': event.value
            })

    def test_property_setters_getters_correct(self: Self, event: BaseEvent):
        event.type = EventType.MANUAL
        event.subtype = ManualEventSubType.GENERAL
        event.value = {'x': 1, 'y': 2}
        event.severity = EventSeverity.WARNING
        event.source_timestamp = 54321
        event.update_timestamp = 12345
        assert event.type == event._type == EventType.MANUAL
        assert event.subtype == event._subtype == ManualEventSubType.GENERAL
        assert event.value == event._value == {'x': 1, 'y': 2}
        assert event.severity == event._severity == EventSeverity.WARNING
        assert event.source_timestamp == event._source_timestamp == 54321
        assert event.update_timestamp == event._update_timestamp == 12345
