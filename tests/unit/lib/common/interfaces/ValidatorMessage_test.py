from typing import Self
import pytest
from ska_mid_cbf_emulators.common import ValidatorStatus, ValidatorMessage

class TestValidatorMessage:

    @pytest.fixture()
    def validator_msg(self: Self):
        return ValidatorMessage(ValidatorStatus.OK, 'test_message')

    def test_init_success(self: Self):
        _ = ValidatorMessage(ValidatorStatus.WARNING, 'asdfghjkl')

    def test_repr_success(self: Self, validator_msg: ValidatorMessage):
        assert isinstance(repr(validator_msg), str)

    def test_json_dict_property_is_correct(self: Self, validator_msg: ValidatorMessage):
        assert validator_msg.json_dict == {
            'status': 'OK',
            'message': 'test_message'
        }
