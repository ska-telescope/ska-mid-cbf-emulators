from typing import Annotated, Self, TypeVar
from unittest import mock
from ska_mid_cbf_emulators.common import BaseEmulatorApi, ApiRouteMetadata, HttpMethod, PathParam, QueryParam, BodyParam, ApiParam

class TestBaseEmulatorApi:

    @BaseEmulatorApi.route(HttpMethod.GET)
    def route_fn_no_params(self: Self):
        return

    @BaseEmulatorApi.route(HttpMethod.GET)
    def route_fn_params(self: Self, some_path_param: PathParam[str], some_body_param: BodyParam[dict[str, str]], some_query_param: QueryParam[int] = 0):
        some_body_param['some_key'] = 'some_value'
        return some_path_param * some_query_param

    _T = TypeVar('_T')
    type bad_type[_T] = Annotated[_T, 'wrong']
    @BaseEmulatorApi.route(HttpMethod.GET)
    def route_fn_invalid_params(self: Self, no_annotation, bad_annotation: float, bad_param_type: bad_type[str]):
        return

    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.services.subcontroller.emulator_subcontroller.EmulatorSubcontroller') as mock_subcontroller:
            _ = BaseEmulatorApi(mock_subcontroller())

    def test_route_creates_correct_metadata_with_no_params(self: Self):
        md = getattr(self.route_fn_no_params, 'metadata', None)
        assert md is not None
        assert isinstance(md, ApiRouteMetadata)
        assert md.path_param is None
        assert md.query_params == []
        assert md.body_param is None

    def test_route_creates_correct_metadata_with_params(self: Self):
        md = getattr(self.route_fn_params, 'metadata', None)
        assert md is not None
        assert isinstance(md, ApiRouteMetadata)
        assert isinstance(md.path_param, ApiParam)
        assert md.path_param.name == 'some_path_param'
        assert md.path_param.type == str
        assert len(md.query_params) == 1 and isinstance(md.query_params[0], ApiParam)
        assert md.query_params[0].name == 'some_query_param'
        assert md.query_params[0].type == int
        assert md.query_params[0].default == 0
        assert isinstance(md.body_param, ApiParam)
        assert md.body_param.name == 'some_body_param'
        assert md.body_param.type == dict[str, str]

    def test_route_ignores_invalid_parameters(self: Self):
        md = getattr(self.route_fn_invalid_params, 'metadata', None)
        assert md is not None
        assert isinstance(md, ApiRouteMetadata)
        assert md.path_param is None
        assert md.query_params == []
        assert md.body_param is None

    def test_route_inner_method_runs_normally(self: Self):
        b = {}
        r = self.route_fn_params(self, 'asdf', b, 3)
        assert b.get('some_key', None) == 'some_value'
        assert r == 'asdfasdfasdf'
