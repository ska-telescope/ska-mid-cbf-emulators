from typing import Self
from ska_mid_cbf_emulators.common import ApiRouteMetadata, HttpMethod

class TestApiRouteMetadata:

    def test_init_success(self: Self):
        _ = ApiRouteMetadata(HttpMethod.GET)
