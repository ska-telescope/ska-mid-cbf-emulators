from typing import Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import BaseEventHandler, ManualEvent, PulseEvent, SignalUpdateEvent, UnimplementedEventHandler, SignalUpdateEventList, EmulatorSubcontroller


class BaseEventHandlerTestHelper(BaseEventHandler):
    """"""


class TestBaseEventHandler:

    @pytest.fixture()
    @mock.patch('ska_mid_cbf_emulators.common.EmulatorSubcontroller')
    def subcontroller(self: Self, mock_subcontroller: mock.MagicMock):
        return mock_subcontroller()

    def test_handle_pulse_event_logs_message_and_returns_none_if_unimplemented_success(self: Self, subcontroller: EmulatorSubcontroller):
        eh = BaseEventHandlerTestHelper(subcontroller)
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            resp = eh.handle_pulse_event(PulseEvent())
            mock_log.assert_called()
            assert resp is None

    def test_handle_signal_update_events_logs_message_and_returns_same_events_if_unimplemented_success(self: Self, subcontroller: EmulatorSubcontroller):
        eh = BaseEventHandlerTestHelper(subcontroller)
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            su = SignalUpdateEventList([SignalUpdateEvent()])
            resp = eh.handle_signal_update_events(su)
            mock_log.assert_called()
            assert resp == su

    def test_handle_manual_event_logs_message_and_returns_none_if_unimplemented_success(self: Self, subcontroller: EmulatorSubcontroller):
        eh = BaseEventHandlerTestHelper(subcontroller)
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            resp = eh.handle_manual_event(ManualEvent())
            mock_log.assert_called()
            assert resp is None


class TestUnimplementedEventHandler:

    def test_handle_pulse_event_logs_message_and_returns_none_success(self: Self):
        eh = UnimplementedEventHandler()
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            resp = eh.handle_pulse_event(PulseEvent())
            mock_log.assert_called()
            assert resp is None

    def test_handle_signal_update_events_logs_message_and_returns_same_events_success(self: Self):
        eh = UnimplementedEventHandler()
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            su = SignalUpdateEventList([SignalUpdateEvent()])
            resp = eh.handle_signal_update_events(su)
            mock_log.assert_called()
            assert resp == su

    def test_handle_manual_event_logs_message_and_returns_none_success(self: Self):
        eh = UnimplementedEventHandler()
        with mock.patch.object(eh._logger, '_log') as mock_log:
            mock_log.reset_mock()
            resp = eh.handle_manual_event(ManualEvent())
            mock_log.assert_called()
            assert resp is None
