from typing import Self
from unittest import mock

import pytest

from ska_mid_cbf_emulators.common import LoggingBase


class TestLoggingBase:

    @pytest.fixture()
    def base_logger(self: Self) -> LoggingBase:
        return LoggingBase()
    
    def test_log_critical_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'critical') as mock_critical
        ):
            base_logger.log_critical('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_critical.assert_called_once()
    
    def test_log_error_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'error') as mock_error
        ):
            base_logger.log_error('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_error.assert_called_once()

    def test_log_warning_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'warning') as mock_warning
        ):
            base_logger.log_warning('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_warning.assert_called_once()

    def test_log_info_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'info') as mock_info
        ):
            base_logger.log_info('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_info.assert_called_once()

    def test_log_debug_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'debug') as mock_debug
        ):
            base_logger.log_debug('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_debug.assert_called_once()

    def test_log_trace_success(self: Self, base_logger: LoggingBase):
        with (
            mock.patch.object(LoggingBase, '_msg_with_prefix') as mock_msg_with_prefix,
            mock.patch.object(base_logger._logger, 'trace') as mock_trace
        ):
            base_logger.log_trace('message')
            mock_msg_with_prefix.assert_called_once_with('message')
            mock_trace.assert_called_once()

    def test_logging_with_name_success(self: Self, base_logger: LoggingBase):
        base_logger._log_prefix = 'some_name'
        with mock.patch.object(base_logger._logger, 'error') as mock_error:
            base_logger.log_error('message')
            mock_error.assert_called_once_with('some_name | message', stacklevel=mock.ANY)

    def test_logging_without_name_success(self: Self, base_logger: LoggingBase):
        base_logger._log_prefix = None
        with mock.patch.object(base_logger._logger, 'error') as mock_error:
            base_logger.log_error('message')
            mock_error.assert_called_once_with('message', stacklevel=mock.ANY)