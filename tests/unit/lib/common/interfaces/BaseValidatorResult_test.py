from typing import Self
from unittest import mock
from ska_mid_cbf_emulators.common import ValidatorStatus, BaseValidatorResult

class TestBaseValidatorResult:

    @mock.patch.multiple(BaseValidatorResult, __abstractmethods__=set())
    def test_init_success(self: Self):
        r = BaseValidatorResult(ValidatorStatus.WARNING)
        assert r.result is not None

    @mock.patch.multiple(BaseValidatorResult, __abstractmethods__=set())
    def test_repr_success(self: Self):
        assert isinstance(repr(BaseValidatorResult(ValidatorStatus.WARNING)), str)
