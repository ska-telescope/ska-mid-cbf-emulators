from typing import Self
from unittest import mock
from jsonschema import ValidationError
import pytest
from contextlib import AbstractContextManager, nullcontext as does_not_raise
from emulator_engine.models.controller_command import ControllerCommand
from ska_mid_cbf_emulators.common import ManualEventSubType, EventSeverity
from emulator_engine.models.controller_command_event import ControllerCommandEvent

class TestControllerCommandEvent:
    
    @mock.patch('ska_mid_cbf_emulators.common.ManualEvent.__init__')
    def test_init_success(self: Self, mock_superclass_init: mock.MagicMock):
        src = 123
        upd = 456
        _ = ControllerCommandEvent(ControllerCommand.START, source_timestamp=src, update_timestamp=upd)
        mock_superclass_init.assert_called_once()
        assert mock_superclass_init.call_args.kwargs.get('subtype') == ManualEventSubType.GENERAL
        assert mock_superclass_init.call_args.kwargs.get('source_timestamp') == src
        assert mock_superclass_init.call_args.kwargs.get('update_timestamp') == upd

    @pytest.mark.parametrize(
        ('json_event', 'expected_event', 'exception_expectation'),
        [
            pytest.param(
                (
                    r'{'
                    r'  "type": "MANUAL",'
                    r'  "subtype": "GENERAL",'
                    r'  "command": "START",'
                    r'  "value": {},'
                    r'  "severity": "GENERAL",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.START,
                    {},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='all_uppercase_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "stop",'
                    r'  "value": {},'
                    r'  "severity": "warning",'
                    r'  "source_timestamp": 12345,'
                    r'  "update_timestamp": 12543'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.STOP,
                    {},
                    EventSeverity.WARNING,
                    12345,
                    12543
                ),
                does_not_raise(),
                id='all_lowercase_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 0'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.START,
                    {},
                    EventSeverity.GENERAL,
                    0
                ),
                does_not_raise(),
                id='timestamp_0_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {"x": [1, 2, 3, 4, 5]},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.START,
                    {'x': [1, 2, 3, 4, 5]},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='simple_value_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {"x": [1, 2, 3, 4.7, {"x1": [6, 7], "x2": {"x3": true}}], "y": [[[[[[[[{}],{}],{}],{}],{}],{}],{}],{}]},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.START,
                    {"x": [1, 2, 3, pytest.approx(4.7), {"x1": [6, 7], "x2": {"x3": True}}], "y": [[[[[[[[{}],{}],{}],{}],{}],{}],{}],{}]},
                    EventSeverity.GENERAL,
                    12345
                ),
                does_not_raise(),
                id='complex_value_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "subtype": "general",'
                    r'  "command": "terminate",'
                    r'  "value": {},'
                    r'  "severity": "fatal_error",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.TERMINATE,
                    {},
                    EventSeverity.FATAL_ERROR,
                    12345
                ),
                does_not_raise(),
                id='type_not_specified_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "command": "terminate",'
                    r'  "value": {},'
                    r'  "severity": "fatal_error",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                ControllerCommandEvent(
                    ControllerCommand.TERMINATE,
                    {},
                    EventSeverity.FATAL_ERROR,
                    12345
                ),
                does_not_raise(),
                id='subtype_not_specified_success'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "PULSE",'
                    r'  "subtype": "GENERAL",'
                    r'  "command": "START",'
                    r'  "value": {},'
                    r'  "severity": "GENERAL",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='wrong_type_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": -1'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='negative_timestamp_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": "asdf",'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='value_not_a_dict_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "injection",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='invalid_subtype_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "take_over_the_world",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='invalid_command_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "severity": "wrong_severity",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='invalid_severity_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "value": {},'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_command_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "severity": "general",'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_value_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "source_timestamp": 12345'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_severity_error'
            ),
            pytest.param(
                (
                    r'{'
                    r'  "type": "manual",'
                    r'  "subtype": "general",'
                    r'  "command": "start",'
                    r'  "value": {},'
                    r'  "severity": "general"'
                    r'}'
                ),
                None,
                pytest.raises(ValidationError),
                id='missing_timestamp_error'
            ),
        ]
    )
    def test_decode(self: Self, json_event: str, expected_event: ControllerCommandEvent, exception_expectation: AbstractContextManager):
        with exception_expectation:
            result = ControllerCommandEvent.decode(json_event)
            if expected_event is not None:
                assert result.type == expected_event.type
                assert result.subtype == expected_event.subtype
                assert result.command == expected_event.command
                assert result.value == expected_event.value
                assert result.severity == expected_event.severity
                assert result.source_timestamp == expected_event.source_timestamp
                assert result.update_timestamp == expected_event.update_timestamp

    def test_encode_decode_are_inverse(self: Self):
        event = ControllerCommandEvent(
            ControllerCommand.START,
            {'x': [1, 2, 3, 4, 5]},
            EventSeverity.GENERAL,
            12345
        )
        enc = ControllerCommandEvent.encode(event)
        dec = ControllerCommandEvent.decode(enc)
        assert dec.type == event.type
        assert dec.subtype == event.subtype
        assert dec.command == event.command
        assert dec.value == event.value
        assert dec.severity == event.severity
        assert dec.source_timestamp == event.source_timestamp
        assert dec.update_timestamp == event.update_timestamp

    def test_command_getter_setter_success(self: Self):
        event = ControllerCommandEvent(
            ControllerCommand.START,
            {'x': [1, 2, 3, 4, 5]},
            EventSeverity.GENERAL,
            12345
        )
        event.command = ControllerCommand.TERMINATE
        assert event.command == ControllerCommand.TERMINATE
