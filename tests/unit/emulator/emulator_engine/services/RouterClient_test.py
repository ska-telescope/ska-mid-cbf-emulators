from typing import Self
from unittest import mock

from fastapi import APIRouter
import pytest

from emulator_engine.services.api.router_client import RouterClient
from ska_mid_cbf_emulators.common import EmulatorError

class TestRouterClient:

    def test_init_has_subcontroller_success(self: Self):
        cli = RouterClient('bitstream_emulator_id', 'ip_block_id')
        assert cli.router is not None

    def test_init_has_no_subcontroller_success(self: Self):
        cli = RouterClient('bitstream_emulator_id', '')
        assert cli.router is not None

    def test_init_cannot_connect_error(self: Self):
        with (
            mock.patch('emulator_engine.services.api.router_client.BlockingConnection', side_effect=Exception('mock_connection_error')),
            pytest.raises(EmulatorError)
        ):
            _ = RouterClient('bitstream_emulator_id', 'ip_block_id')

    def test_router_getter_setter_success(self: Self):
        cli = RouterClient('bitstream_emulator_id', 'ip_block_id')
        cli.router = APIRouter(prefix='/new', tags=['new'])
        assert cli.router.prefix == '/new'
