from typing import Self
from unittest import mock

import pytest

from emulator_engine.services.ip_block_emulator.ip_block_emulator_validator import IPBlockEmulatorValidatorResult
from ska_mid_cbf_emulators.common import ValidatorStatus, ValidatorMessage

class TestIPBlockEmulatorValidatorResult:

    @pytest.fixture()
    def validator_result(self: Self) -> IPBlockEmulatorValidatorResult:
        result = IPBlockEmulatorValidatorResult(ValidatorStatus.WARNING)
        result.result = {
            IPBlockEmulatorValidatorResult.API_KEY: [
                ValidatorMessage(ValidatorStatus.OK, 'ok_msg_1')
            ],
            IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY: [
                ValidatorMessage(ValidatorStatus.WARNING, 'warn_msg_1')
            ],
            IPBlockEmulatorValidatorResult.IP_BLOCK_KEY: [
                ValidatorMessage(ValidatorStatus.OK, 'ok_msg_2'),
                ValidatorMessage(ValidatorStatus.OK, 'ok_msg_3')
            ],
            IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY: [
                ValidatorMessage(ValidatorStatus.WARNING, 'warn_msg_2'),
                ValidatorMessage(ValidatorStatus.OK, 'ok_msg_4')
            ]
        }
        return result

    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.BaseValidatorResult.__init__') as mock_superclass_init:
            r = IPBlockEmulatorValidatorResult(ValidatorStatus.WARNING)
            mock_superclass_init.assert_called_once()
            assert r.result[IPBlockEmulatorValidatorResult.API_KEY] == []
            assert r.result[IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY] == []
            assert r.result[IPBlockEmulatorValidatorResult.IP_BLOCK_KEY] == []
            assert r.result[IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY] == []

    def test_json_dict_property_is_correct(self: Self, validator_result: IPBlockEmulatorValidatorResult):
        d = validator_result.json_dict
        assert set(d.keys()) == {'overall_status', 'result'}
        assert d.get('overall_status') == 'WARNING'
        result = d.get('result')
        assert result[IPBlockEmulatorValidatorResult.API_KEY][0].get('status') == 'OK'
        assert result[IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY][0].get('status') == 'WARNING'
        assert result[IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY][1].get('status') == 'OK'
        assert result[IPBlockEmulatorValidatorResult.API_KEY][0].get('message') == 'ok_msg_1'
        assert result[IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY][0].get('message') == 'warn_msg_1'
        assert result[IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY][1].get('message') == 'ok_msg_4'

    @pytest.mark.parametrize(
        ('key', 'msg_status', 'msg_msg', 'expected_overall_status'),
        [
            pytest.param(IPBlockEmulatorValidatorResult.API_KEY,
                         ValidatorStatus.OK, 'ok_msg_5', ValidatorStatus.WARNING, id='new_status_lower_severity_does_not_override_success'),
            pytest.param(IPBlockEmulatorValidatorResult.EVENT_HANDLER_KEY,
                         ValidatorStatus.WARNING, 'warn_msg_3', ValidatorStatus.WARNING, id='new_status_equal_severity_does_not_override_success'),
            pytest.param(IPBlockEmulatorValidatorResult.STATE_MACHINE_KEY,
                         ValidatorStatus.ERROR, 'err_msg_1', ValidatorStatus.ERROR, id='new_status_higher_severity_does_override_success'),
        ]
    )
    def test_insert_msg(self: Self, key: str, msg_status: ValidatorStatus, msg_msg: str, expected_overall_status: ValidatorStatus, validator_result: IPBlockEmulatorValidatorResult):
        orig_len = len(validator_result.result[key])
        validator_result.insert_msg(key, ValidatorMessage(msg_status, msg_msg))
        assert len(validator_result.result[key]) == orig_len + 1 and validator_result.result[key][-1].message == msg_msg
        assert validator_result.overall_status == expected_overall_status
