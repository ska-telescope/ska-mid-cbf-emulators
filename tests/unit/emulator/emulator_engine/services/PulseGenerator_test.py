from typing import Self
from emulator_engine.services.controller.pulse_generator import PulseGenerator

class TestPulseGenerator:

    def test_init_success(self: Self):
        _ = PulseGenerator(5)

    def test_del_success(self: Self):
        gen = PulseGenerator(5)
        del gen
