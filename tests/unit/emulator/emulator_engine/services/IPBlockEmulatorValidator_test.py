from typing import Self
from unittest import mock
import os
import sys

import pytest

from ska_mid_cbf_emulators.common import ValidatorStatus
from emulator_engine.services.ip_block_emulator.ip_block_emulator_validator import IPBlockEmulatorValidatorResult, IPBlockEmulatorValidator

class TestIPBlockEmulatorValidator:

    def test_validate_only_returns_validation_result(self: Self):
        with mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator._process_ip_block_emulator'):
            result = IPBlockEmulatorValidator.validate('', '')
            assert isinstance(result, IPBlockEmulatorValidatorResult)

    def test_validate_and_get_components_returns_result_and_components(self: Self):
        with mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator._process_ip_block_emulator', return_value=(None, None, None, None)):
            result = IPBlockEmulatorValidator.validate_and_get_components('', '')
            assert isinstance(result, tuple) and len(result) >= 2 and isinstance(result[0], IPBlockEmulatorValidatorResult)

    @pytest.mark.parametrize(
        ('test_emulator_name', 'expected_overall', 'expected_components_not_none_bitwise'),
        [
            pytest.param('ok_1_simple_example', ValidatorStatus.OK, 0b1111, id='simple_example_ok'),
            pytest.param('ok_2_complex_example', ValidatorStatus.OK, 0b1111, id='complex_example_ok'),
            pytest.param('warn_1_api_has_undecorated_public_method', ValidatorStatus.WARNING, 0b1111, id='api_has_undecorated_public_method_warning'),
            pytest.param('warn_2_api_has_decorated_private_method', ValidatorStatus.WARNING, 0b1111, id='api_has_decorated_private_method_warning'),
            pytest.param('warn_3_api_route_has_unannotated_param', ValidatorStatus.WARNING, 0b1111, id='api_route_has_unannotated_param_warning'),
            pytest.param('warn_4_ip_block_init_takes_default_params', ValidatorStatus.WARNING, 0b1111, id='ip_block_init_takes_default_params_warning'),
            pytest.param('error_1_1_api_file_missing', ValidatorStatus.ERROR, 0b0111, id='api_file_missing_error'),
            pytest.param('error_1_2_api_bad_import', ValidatorStatus.ERROR, 0b0111, id='api_bad_import_error'),
            pytest.param('error_1_3_api_class_misnamed', ValidatorStatus.ERROR, 0b0111, id='api_class_misnamed_error'),
            pytest.param('error_1_4_api_wrong_superclass', ValidatorStatus.ERROR, 0b0111, id='api_wrong_superclass_error'),
            pytest.param('error_2_1_event_handler_file_missing', ValidatorStatus.ERROR, 0b1011, id='event_handler_file_missing_error'),
            pytest.param('error_2_2_event_handler_bad_import', ValidatorStatus.ERROR, 0b1011, id='event_handler_bad_import_error'),
            pytest.param('error_2_3_event_handler_class_misnamed', ValidatorStatus.ERROR, 0b1011, id='event_handler_class_misnamed_error'),
            pytest.param('error_2_4_event_handler_wrong_superclass', ValidatorStatus.ERROR, 0b1011, id='event_handler_wrong_superclass_error'),
            pytest.param('error_2_5_event_handler_pulse_wrong_signature', ValidatorStatus.ERROR, 0b1011, id='event_handler_pulse_wrong_signature_error'),
            pytest.param('error_2_6_event_handler_signal_wrong_signature', ValidatorStatus.ERROR, 0b1011, id='event_handler_signal_wrong_signature_error'),
            pytest.param('error_2_7_event_handler_manual_wrong_signature', ValidatorStatus.ERROR, 0b1011, id='event_handler_manual_wrong_signature_error'),
            pytest.param('error_3_1_ip_block_file_missing', ValidatorStatus.ERROR, 0b1101, id='ip_block_file_missing_error'),
            pytest.param('error_3_2_ip_block_bad_import', ValidatorStatus.ERROR, 0b1101, id='ip_block_bad_import_error'),
            pytest.param('error_3_3_ip_block_class_misnamed', ValidatorStatus.ERROR, 0b1101, id='ip_block_class_misnamed_error'),
            pytest.param('error_3_4_ip_block_init_missing', ValidatorStatus.ERROR, 0b1101, id='ip_block_init_missing_error'),
            pytest.param('error_3_5_ip_block_init_takes_nondefault_params', ValidatorStatus.ERROR, 0b1101, id='ip_block_init_takes_nondefault_params_error'),
            pytest.param('error_4_10_state_machine_transitions_not_a_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_transitions_not_a_list_error'),
            pytest.param('error_4_11_state_machine_transition_not_a_dict', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_not_a_dict_error'),
            pytest.param('error_4_12_state_machine_transition_from_any_in_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_from_any_in_list_error'),
            pytest.param('error_4_13_state_machine_transition_to_same_in_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_to_same_in_list_error'),
            pytest.param('error_4_14_state_machine_transition_source_states_list_not_subset_of_states', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_source_states_list_not_subset_of_states_error'),
            pytest.param('error_4_15_state_machine_transition_to_same_as_source', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_to_same_as_source_error'),
            pytest.param('error_4_16_state_machine_transition_source_state_not_in_states', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_source_state_not_in_states_error'),
            pytest.param('error_4_17_state_machine_transition_dest_is_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_dest_is_list_error'),
            pytest.param('error_4_18_state_machine_transition_from_any_in_dest', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_from_any_in_dest_error'),
            pytest.param('error_4_19_state_machine_transition_dest_not_in_states', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_dest_not_in_states_error'),
            pytest.param('error_4_1_state_machine_file_missing', ValidatorStatus.ERROR, 0b1110, id='state_machine_file_missing_error'),
            pytest.param('error_4_20_state_machine_transition_trigger_is_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_trigger_is_list_error'),
            pytest.param('error_4_21_state_machine_transition_wrong_trigger_superclass', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_wrong_trigger_superclass_error'),
            pytest.param('error_4_22_state_machine_transition_condition_wrong_class', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_condition_wrong_class_error'),
            pytest.param('error_4_23_state_machine_transition_condition_in_list_wrong_class', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_condition_in_list_wrong_class_error'),
            pytest.param('error_4_24_state_machine_transition_missing_properties', ValidatorStatus.ERROR, 0b1110, id='state_machine_transition_missing_properties_error'),
            pytest.param('error_4_25_state_machine_misc_error', ValidatorStatus.ERROR, 0b1110, id='state_machine_misc_error_error'),
            pytest.param('error_4_2_state_machine_bad_import', ValidatorStatus.ERROR, 0b1110, id='state_machine_bad_import_error'),
            pytest.param('error_4_3_state_machine_class_misnamed', ValidatorStatus.ERROR, 0b1110, id='state_machine_class_misnamed_error'),
            pytest.param('error_4_4_state_machine_wrong_superclass', ValidatorStatus.ERROR, 0b1110, id='state_machine_wrong_superclass_error'),
            pytest.param('error_4_5_state_machine_missing_abstract_property_implementations', ValidatorStatus.ERROR, 0b1110, id='state_machine_missing_abstract_property_implementations_error'),
            pytest.param('error_4_6_state_machine_states_not_a_list', ValidatorStatus.ERROR, 0b1110, id='state_machine_states_not_a_list_error'),
            pytest.param('error_4_7_state_machine_has_no_states', ValidatorStatus.ERROR, 0b1110, id='state_machine_has_no_states_error'),
            pytest.param('error_4_8_state_machine_wrong_state_superclass', ValidatorStatus.ERROR, 0b1110, id='state_machine_wrong_state_superclass_error'),
            pytest.param('error_4_9_state_machine_initial_state_not_in_states', ValidatorStatus.ERROR, 0b1110, id='state_machine_initial_state_not_in_states_error'),
            pytest.param('error_5_misc_load_error', ValidatorStatus.ERROR, 0b0111, id='misc_load_error_error')
        ]
    )
    def test_validate_and_get_components(self: Self, test_emulator_name: str, expected_overall: ValidatorStatus, expected_components_not_none_bitwise: int):
        bitstream_path = os.path.join(os.getcwd(), 'tests/test_data/bs_emulators/validation/ver/emu_src')
        sys.path.append(bitstream_path)
        result, api, eh, ip, fsm = IPBlockEmulatorValidator.validate_and_get_components(bitstream_path, test_emulator_name)
        assert result.overall_status == expected_overall
        assert ((api is not None) << 3) + ((eh is not None) << 2) + ((ip is not None) << 1) + (fsm is not None) == expected_components_not_none_bitwise
