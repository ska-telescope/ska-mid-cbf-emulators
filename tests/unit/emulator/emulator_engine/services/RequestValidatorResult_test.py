from typing import Self
from unittest import mock

import pytest

from emulator_engine.services.ip_block_emulator.request_validator import RequestValidatorResult
from ska_mid_cbf_emulators.common import ValidatorStatus, ValidatorMessage

class TestRequestValidatorResult:

    @pytest.fixture()
    def validator_result(self: Self) -> RequestValidatorResult:
        result = RequestValidatorResult(ValidatorStatus.WARNING)
        result.result = {
            RequestValidatorResult.PATH_KEY: {
                'ok_path': ValidatorMessage(ValidatorStatus.OK, 'ok_msg_1')
            },
            RequestValidatorResult.QUERY_KEY: {
                'ok_query': ValidatorMessage(ValidatorStatus.OK, 'ok_msg_2'),
                'warn_query': ValidatorMessage(ValidatorStatus.WARNING, 'warn_msg_1')
            },
            RequestValidatorResult.BODY_KEY: {
                'ok_body': ValidatorMessage(ValidatorStatus.OK, 'ok_msg_3')
            },
            RequestValidatorResult.OTHER_KEY: [
                ValidatorMessage(ValidatorStatus.WARNING, 'warn_msg_2'),
                ValidatorMessage(ValidatorStatus.OK, 'ok_msg_4')
            ]
        }
        result.combined_args = {'ok_path': 1, 'ok_query': 'q', 'warn_query': -1, 'ok_body': {'a': 0}}
        return result

    def test_init_success(self: Self):
        with mock.patch('ska_mid_cbf_emulators.common.BaseValidatorResult.__init__') as mock_superclass_init:
            r = RequestValidatorResult(ValidatorStatus.WARNING)
            mock_superclass_init.assert_called_once()
            assert r.result[RequestValidatorResult.PATH_KEY] == {}
            assert r.result[RequestValidatorResult.QUERY_KEY] == {}
            assert r.result[RequestValidatorResult.BODY_KEY] == {}
            assert r.result[RequestValidatorResult.OTHER_KEY] == []

    def test_json_dict_property_is_correct(self: Self, validator_result: RequestValidatorResult):
        d = validator_result.json_dict
        assert set(d.keys()) == {'overall_status', 'result', 'combined_args'}
        assert d.get('overall_status') == 'WARNING'
        result = d.get('result')
        assert result[RequestValidatorResult.PATH_KEY].get('ok_path').get('status') == 'OK'
        assert result[RequestValidatorResult.QUERY_KEY].get('warn_query').get('status') == 'WARNING'
        assert result[RequestValidatorResult.BODY_KEY].get('ok_body').get('status') == 'OK'
        assert result[RequestValidatorResult.OTHER_KEY][0].get('status') == 'WARNING'
        assert result[RequestValidatorResult.PATH_KEY].get('ok_path').get('message') == 'ok_msg_1'
        assert result[RequestValidatorResult.QUERY_KEY].get('warn_query').get('message') == 'warn_msg_1'
        assert result[RequestValidatorResult.BODY_KEY].get('ok_body').get('message') == 'ok_msg_3'
        assert result[RequestValidatorResult.OTHER_KEY][0].get('message') == 'warn_msg_2'

    @pytest.mark.parametrize(
        ('key', 'param_name', 'msg_status', 'msg_msg', 'expected_overall_status'),
        [
            pytest.param(RequestValidatorResult.PATH_KEY, 'new_path',
                         ValidatorStatus.OK, 'ok_msg_5', ValidatorStatus.WARNING, id='new_status_lower_severity_does_not_override_success'),
            pytest.param(RequestValidatorResult.QUERY_KEY, 'new_query',
                         ValidatorStatus.WARNING, 'warn_msg_3', ValidatorStatus.WARNING, id='new_status_equal_severity_does_not_override_success'),
            pytest.param(RequestValidatorResult.BODY_KEY, 'new_body',
                         ValidatorStatus.ERROR, 'err_msg_1', ValidatorStatus.ERROR, id='new_status_higher_severity_does_override_success'),
        ]
    )
    def test_insert_msg(self: Self, key: str, param_name: str, msg_status: ValidatorStatus, msg_msg: str, expected_overall_status: ValidatorStatus, validator_result: RequestValidatorResult):
        orig_len = len(validator_result.result[key].keys())
        validator_result.insert_msg(key, param_name, ValidatorMessage(msg_status, msg_msg))
        assert len(validator_result.result[key].keys()) == orig_len + 1 and validator_result.result[key][param_name].message == msg_msg
        assert validator_result.overall_status == expected_overall_status

    @pytest.mark.parametrize(
        ('msg_status', 'msg_msg', 'expected_overall_status'),
        [
            pytest.param(ValidatorStatus.OK, 'ok_msg_6', ValidatorStatus.WARNING, id='new_status_lower_severity_does_not_override_success'),
            pytest.param(ValidatorStatus.WARNING, 'warn_msg_4', ValidatorStatus.WARNING, id='new_status_equal_severity_does_not_override_success'),
            pytest.param(ValidatorStatus.ERROR, 'err_msg_2', ValidatorStatus.ERROR, id='new_status_higher_severity_does_override_success'),
        ]
    )
    def test_insert_other(self: Self, msg_status: ValidatorStatus, msg_msg: str, expected_overall_status: ValidatorStatus, validator_result: RequestValidatorResult):
        orig_len = len(validator_result.result[RequestValidatorResult.OTHER_KEY])
        validator_result.insert_other(ValidatorMessage(msg_status, msg_msg))
        assert len(validator_result.result[RequestValidatorResult.OTHER_KEY]) == orig_len + 1
        assert validator_result.result[RequestValidatorResult.OTHER_KEY][-1].message == msg_msg
        assert validator_result.overall_status == expected_overall_status

    def test_insert_arg(self: Self, validator_result: RequestValidatorResult):
        orig_len = len(validator_result.combined_args.keys())
        validator_result.insert_arg('new_arg', 12345)
        assert len(validator_result.combined_args.keys()) == orig_len + 1 and validator_result.combined_args['new_arg'] == 12345
