from typing import Self
from unittest import mock

import pytest
from emulator_engine.services.messaging.event_queue_service import EventQueueService
from ska_mid_cbf_emulators.common import EmulatorError, ManualEvent


class TestEventQueueService:

    @pytest.fixture()
    def service(self: Self):
        return EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', ['test_ds_ip_block_id'], 1.0)

    @pytest.fixture()
    def subscribed_service(self: Self, service: EventQueueService):
        service.subscribe(event_handler=lambda x: x+1)
        yield service
        print('EQ SERVICE TEST COMPLETED')
        if service._listener_t is not None and service._listener_t.is_alive():
            print('EQ SERVICE TEST UNSUBSCRIBING')
            service.unsubscribe()
            print('EQ SERVICE TEST UNSUBSCRIBED')

    def test_init_no_args_success(self: Self):
        _ = EventQueueService(1.0)

    def test_init_with_queues_success(self: Self):
        svc = EventQueueService(
            pulse_interval=1.0,
            pulse_queue='pulse123',
            manual_queue='manual123',
            publishable_pulse_queues=['pulse456'],
            publishable_manual_queues=['manual456'],
            pulse_exchange='pulse_ex123',
            manual_exchange='manual_ex123',
        )
        assert svc.pulse_queue == 'pulse123'
        assert svc.manual_queue == 'manual123'
        assert svc.publishable_pulse_queues == ['pulse456']
        assert svc.publishable_manual_queues == ['manual456']
        assert svc.pulse_exchange == 'pulse_ex123'
        assert svc.manual_exchange == 'manual_ex123'

    def test_init_connection_error_error(self: Self):
        with (
            mock.patch('emulator_engine.services.messaging.event_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error')),
            pytest.raises(EmulatorError)
        ):
            _ = EventQueueService(1.0)

    def test_init_for_subcontroller_success(self: Self):
        svc = EventQueueService.init_for_subcontroller('1test_emu1', '2test_subcontroller2', ['3test_ds3'], 1.0)
        assert '1test_emu1' in svc.pulse_queue
        assert '2test_subcontroller2' in svc.pulse_queue
        assert '1test_emu1' in svc.manual_queue
        assert '2test_subcontroller2' in svc.manual_queue
        assert '1test_emu1' in svc.publishable_pulse_queues[0]
        assert '3test_ds3' in svc.publishable_pulse_queues[0]
        assert '1test_emu1' in svc.publishable_manual_queues[0]
        assert '3test_ds3' in svc.publishable_manual_queues[0]
        assert '1test_emu1' in svc.pulse_exchange
        assert '2test_subcontroller2' in svc.pulse_exchange
        assert '1test_emu1' in svc.manual_exchange
        assert '2test_subcontroller2' in svc.manual_exchange

    def test_subscribe_unsubscribe_success(self: Self, service: EventQueueService):
        service.subscribe(event_handler=lambda _: None)
        assert service._listener_t is not None and service._listener_t.is_alive()
        service.unsubscribe()
        assert service._listener_t is None

    def test_subscribe_connection_error_is_caught_success(self: Self, service: EventQueueService):
        with (
            mock.patch('emulator_engine.services.messaging.event_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('threading.excepthook') as mock_thread_exc
        ):
            service.subscribe()
            service._listener_t.join(10)
            assert service._listener_t is not None and not service._listener_t.is_alive()
            mock_thread_exc.assert_called_once()

    def test_subscribe_no_pulse_queue_cancels_listener_success(self: Self, service: EventQueueService):
        with mock.patch('threading.excepthook') as mock_thread_exc:
            service.pulse_queue = ''
            service.subscribe()
            service._listener_t.join(10)
            assert service._listener_t is not None and not service._listener_t.is_alive()
            mock_thread_exc.assert_not_called()

    def test_subscribe_no_signal_update_queue_cancels_listener_success(self: Self, service: EventQueueService):
        with mock.patch('threading.excepthook') as mock_thread_exc:
            service.signal_update_queue = ''
            service.subscribe()
            service._listener_t.join(10)
            assert service._listener_t is not None and not service._listener_t.is_alive()
            mock_thread_exc.assert_not_called()

    def test_subscribe_ignore_if_already_subscribed_success(self: Self, subscribed_service: EventQueueService):
        assert subscribed_service._listener_t is not None and subscribed_service._listener_t.is_alive()
        with mock.patch.object(subscribed_service._listener_t, 'start') as mock_thread_start:
            subscribed_service.subscribe()
            mock_thread_start.assert_not_called()

    def test_unsubscribe_ignore_if_not_subscribed_success(self: Self, service: EventQueueService):
        with mock.patch('threading.Thread.join') as mock_thread_join:
            service.unsubscribe()
            mock_thread_join.assert_not_called()

    def test_publish_success(self: Self, service: EventQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.basic_publish') as mock_rmq_publish:
            service.publish(ManualEvent(), 'test_exchange', 'test_queue')
            mock_rmq_publish.assert_called_once_with(exchange='test_exchange', routing_key='test_queue', body=mock.ANY)

    def test_publish_connection_error_is_caught_success(self: Self, service: EventQueueService):
        with (
            mock.patch('emulator_engine.services.messaging.event_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.basic_publish') as mock_rmq_publish
        ):
            service.publish(ManualEvent(), 'test_exchange', 'test_queue')
            mock_rmq_publish.assert_not_called()

    def test_flush_success(self: Self, service: EventQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge') as mock_queue_purge:
            service.flush()
            mock_queue_purge.assert_has_calls([
                mock.call(service.pulse_queue),
                mock.call(service.manual_queue),
                mock.call(service.publishable_pulse_queues[0]),
                mock.call(service.publishable_manual_queues[0])
            ], any_order=True)

    def test_flush_connection_error_is_caught_success(self: Self, service: EventQueueService):
        with (
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge') as mock_queue_purge,
            mock.patch('emulator_engine.services.messaging.event_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error'))
        ):
            service.flush()
            mock_queue_purge.assert_not_called()

    def test_flush_purge_error_is_ignored_success(self: Self, service: EventQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge', side_effect=[Exception('mock_queue_error'), None, None, None]):
            service.flush()
