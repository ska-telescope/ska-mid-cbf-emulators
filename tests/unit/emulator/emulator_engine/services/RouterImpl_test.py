from typing import Self
import pytest

from emulator_engine.services.api.router_impl import RouterImpl
from ska_mid_cbf_emulators.common import InternalRestResponse
from fastapi import status

class TestRouterImpl:

    @pytest.fixture(scope='class')
    def router_impl(self: Self):
        impl = RouterImpl()
        def test_callable(a: int, b: str) -> InternalRestResponse:
            return InternalRestResponse.ok({'result': b * a})
        setattr(impl, 'test_callable', test_callable)
        yield impl

    def test_init_success(self: Self):
        _ = RouterImpl()

    def test_call_success(self: Self, router_impl: RouterImpl):
        result = router_impl.call('test_callable', a=5, b='c')
        assert isinstance(result, InternalRestResponse)
        assert result.body.get('result') == 'ccccc'

    def test_call_name_does_not_exist_error(self: Self, router_impl: RouterImpl):
        result = router_impl.call('invalid_callable', a=5, b='c')
        assert result is None

    def test_error_catcher_no_error_caught_success(self: Self, router_impl: RouterImpl):
        wrapped = RouterImpl.error_catcher(lambda self, x, y=1, z=2: InternalRestResponse.ok({'result': (x + y) / z}))
        result = wrapped(router_impl, 3, z=2)
        assert isinstance(result, InternalRestResponse)
        assert result.status == status.HTTP_200_OK
        assert result.body.get('result') == 2

    def test_error_catcher_error_caught_success(self: Self, router_impl: RouterImpl):
        wrapped = RouterImpl.error_catcher(lambda self, x, y=1, z=2: InternalRestResponse.ok({'result': (x + y) / z}))
        result = wrapped(router_impl, 3, z=0)
        assert isinstance(result, InternalRestResponse)
        assert result.status == status.HTTP_500_INTERNAL_SERVER_ERROR
        assert result.body.get('result', None) is None
