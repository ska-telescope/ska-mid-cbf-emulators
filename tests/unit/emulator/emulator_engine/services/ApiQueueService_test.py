from typing import Self
from unittest import mock

import pytest
from emulator_engine.services.messaging.api_queue_service import ApiQueueService
from ska_mid_cbf_emulators.common import InternalRestRequest, EmulatorError
from fastapi import status

class TestApiQueueService:

    @pytest.fixture()
    def service(self: Self):
        return ApiQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id')

    @pytest.fixture()
    def subscribed_service(self: Self, service: ApiQueueService):
        service.subscribe()
        yield service
        if service._api_listener_t is not None and service._api_listener_t.is_alive():
            service.unsubscribe()

    def test_init_no_args_success(self: Self):
        _ = ApiQueueService()

    def test_init_with_queues_success(self: Self):
        svc = ApiQueueService(
            api_request_queue='abc123',
            api_callback_queue='def456'
        )
        assert svc.api_request_queue == 'abc123'
        assert svc.api_callback_queue == 'def456'

    def test_init_connection_error_error(self: Self):
        with (
            mock.patch('emulator_engine.services.messaging.api_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error')),
            pytest.raises(EmulatorError)
        ):
            _ = ApiQueueService()

    def test_init_for_subcontroller_success(self: Self):
        svc = ApiQueueService.init_for_subcontroller('1test_emu1', '2test_subcontroller2')
        assert '1test_emu1' in svc.api_callback_queue
        assert '2test_subcontroller2' in svc.api_callback_queue
        assert '1test_emu1' in svc.api_request_queue
        assert '2test_subcontroller2' in svc.api_request_queue

    def test_subscribe_unsubscribe_success(self: Self, service: ApiQueueService):
        service.subscribe()
        assert service._api_listener_t is not None and service._api_listener_t.is_alive()
        service.unsubscribe()
        assert service._api_listener_t is None

    def test_subscribe_connection_error_is_caught_success(self: Self, service: ApiQueueService):
        with (
            mock.patch('emulator_engine.services.messaging.api_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('threading.excepthook') as mock_thread_exc
        ):
            service.subscribe()
            service._api_listener_t.join(10)
            assert not service._api_listener_t.is_alive()
            mock_thread_exc.assert_called_once()
            assert service._api_listener_t is not None and not service._api_listener_t.is_alive()

    def test_subscribe_ignore_if_already_subscribed_success(self: Self, subscribed_service: ApiQueueService):
        assert subscribed_service._api_listener_t is not None and subscribed_service._api_listener_t.is_alive()
        subscribed_service.subscribe()  # failure will throw an error here

    def test_unsubscribe_ignore_if_not_subscribed_success(self: Self, service: ApiQueueService):
        with mock.patch('threading.Thread.join') as mock_thread_join:
            service.unsubscribe()
            mock_thread_join.assert_not_called()

    def test_publish_does_nothing_success(self: Self, service: ApiQueueService):
        assert service.publish() is None

    def test_flush_success(self: Self, service: ApiQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge') as mock_queue_purge:
            service.flush()
            mock_queue_purge.assert_has_calls([mock.call(service.api_callback_queue), mock.call(service.api_request_queue)], any_order=True)

    def test_flush_connection_error_is_caught_success(self: Self, service: ApiQueueService):
        with (
            mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge') as mock_queue_purge,
            mock.patch('emulator_engine.services.messaging.api_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error'))
        ):
            service.flush()
            mock_queue_purge.assert_not_called()

    def test_flush_purge_error_is_ignored_success(self: Self, service: ApiQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.queue_purge', side_effect=[Exception('mock_queue_error'), None]):
            service.flush()

    def test_get_rpc_response_connection_error_is_caught_success(self: Self, service: ApiQueueService):
        with (
            mock.patch('pika.BlockingConnection.channel') as mock_channel_get,
            mock.patch('emulator_engine.services.messaging.api_queue_service.BlockingConnection', side_effect=Exception('mock_connection_error'))
        ):
            resp = service.get_rpc_response('null', 'null2', InternalRestRequest(method_name='null3'))
            assert resp.status == status.HTTP_500_INTERNAL_SERVER_ERROR
            mock_channel_get.assert_not_called()
