from typing import Self
from unittest import mock
from emulator_engine.models.controller_command import ControllerCommand
from emulator_engine.services.controller.controller_router_client import ControllerRouterClient

class TestControllerRouterClient:

    def test_init_success(self: Self):
        _ = ControllerRouterClient('bitstream_emulator_id', 1.0)

    def test_send_command_connection_error_is_caught_success(self: Self):
        cli = ControllerRouterClient('bitstream_emulator_id', 1.0)
        with (
            mock.patch('emulator_engine.services.controller.controller_router_client.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('pika.BlockingConnection.channel') as mock_channel_init
        ):
            cli._send_command(ControllerCommand.START, {})
            mock_channel_init.assert_not_called()
