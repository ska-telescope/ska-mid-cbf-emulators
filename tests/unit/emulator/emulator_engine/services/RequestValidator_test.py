from typing import Any, Self, Type, get_origin

import pytest

from ska_mid_cbf_emulators.common import ValidatorStatus, ApiRouteMetadata, HttpMethod, ApiParam
from emulator_engine.services.ip_block_emulator.request_validator import RequestValidator, RequestValidatorResult

class TestRequestValidator:

    def root_type(self: Self, t: Type[Any]) -> Type[Any]:
        return get_origin(t) or t

    @pytest.mark.parametrize(
        ('path_param', 'query_params', 'body', 'metadata', 'expected_overall'),
        [
            pytest.param(
                {'my_param': 'test'},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('my_param', str),
                    route_name='my_route'
                ),
                ValidatorStatus.OK,
                id='simple_example_ok'
            ),
            pytest.param(
                {'my_param': '1'},
                {'my_query': 'abc', 'my_query2': 'true', 'my_query3': '5'},
                {'anything': {'anything_else': ['something']}},
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('my_param', int),
                    query_params=[ApiParam('my_query', str), ApiParam('my_query2', bool), ApiParam('my_query3', float)],
                    body_param=ApiParam('my_body', dict[str, dict[str, list[str]]]),
                    route_name='my_route'
                ),
                ValidatorStatus.OK,
                id='complex_example_ok'
            ),
            pytest.param(
                {},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('my_param', int, 0),
                    query_params=[ApiParam('my_query', str, 'default')],
                    body_param=ApiParam('my_body', dict[str, str], {'default': 'value'}),
                    route_name='my_route'
                ),
                ValidatorStatus.OK,
                id='default_values_ok'
            ),
            pytest.param(
                {},
                {'list': ['a', 'b'], 'list[str]': ['a', 'b'], 'set[str]': ['a', 'a', 'b'], 'tuple[str]': ['a', 'b'], 'list[list[str]]': [['a', 'b'], ['c']]},
                {'dict[str, list[dict[str, list[str]]]]': [{'a': ['b', 'c'], 'd': ['e']}, {'f': ['g']}]},
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[
                        ApiParam('list', list),
                        ApiParam('list[str]', list[str]),
                        ApiParam('set[str]', set[str]),
                        ApiParam('tuple[str]', tuple[str]),
                        ApiParam('list[list[str]]', list[list[str]])
                    ],
                    body_param=ApiParam('some_body', dict[str, list[dict[str, list[str]]]]),
                    route_name='my_route'
                ),
                ValidatorStatus.OK,
                id='various_container_types_ok'
            ),
            pytest.param(
                {},
                {'set': ['a', 'b'], 'dict[str, int]': [('a', 1), ('b', 2)], 'list[str]': {'a', 'a', 'b'}, 'tuple': {'a': None, 'b': None}},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[
                        ApiParam('set', set),
                        ApiParam('dict[str, int]', dict[str, int]),
                        ApiParam('list[str]', list[str]),
                        ApiParam('tuple', tuple)
                    ],
                    route_name='my_route'
                ),
                ValidatorStatus.OK,
                id='container_conversion_ok'
            ),
            pytest.param(
                {'unknown_path': 'test'},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    route_name='my_route'
                ),
                ValidatorStatus.WARNING,
                id='unexpected_path_param_warning'
            ),
            pytest.param(
                {},
                {'unknown_query': 'test'},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('query', str, 'default'),
                    route_name='my_route'
                ),
                ValidatorStatus.WARNING,
                id='unknown_query_param_warning'
            ),
            pytest.param(
                {},
                {},
                {'body': 'data'},
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('path', str, 'default'),
                    route_name='my_route'
                ),
                ValidatorStatus.WARNING,
                id='unexpected_body_warning'
            ),
            pytest.param(
                {},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    path_param=ApiParam('required_path', str),
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='required_path_missing_error'
            ),
            pytest.param(
                {},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[ApiParam('required_query', str)],
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='required_query_missing_error'
            ),
            pytest.param(
                {},
                {},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    body_param=ApiParam('required_body', dict),
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='required_body_missing_error'
            ),
            pytest.param(
                {},
                {'int': '1.5'},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[ApiParam('int', int)],
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='unparseable_int_type_error'
            ),
            pytest.param(
                {},
                {'float': 'not_a_float'},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[ApiParam('float', float)],
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='unparseable_float_type_error'
            ),
            pytest.param(
                {},
                {'bool': '-1'},
                None,
                ApiRouteMetadata(
                    HttpMethod.POST,
                    query_params=[ApiParam('bool', bool)],
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='unparseable_bool_type_error'
            ),
            pytest.param(
                {},
                {},
                [1, 2],
                ApiRouteMetadata(
                    HttpMethod.POST,
                    body_param=ApiParam('dict', dict[int, int]),
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='inconvertible_container_parametrized_type_error'
            ),
            pytest.param(
                {},
                {},
                [1, 2],
                ApiRouteMetadata(
                    HttpMethod.POST,
                    body_param=ApiParam('dict', dict),
                    route_name='my_route'
                ),
                ValidatorStatus.ERROR,
                id='inconvertible_container_origin_type_error'
            ),
        ]
    )
    def test_validate(self: Self, path_param: dict[str, Any], query_params: dict[str, str], body: dict[str, Any], metadata: ApiRouteMetadata, expected_overall: ValidatorStatus):
        result = RequestValidator.validate(path_param, query_params, body, metadata)
        assert result.overall_status == expected_overall
        if len(path_param) and metadata.path_param is not None:
            pk = list(path_param)[0]
            if (pm:=result.result[RequestValidatorResult.PATH_KEY].get(pk, None)) is not None and pm.status == ValidatorStatus.OK:
                assert (a:=result.combined_args.get(pk, None)) is not None and isinstance(a, self.root_type(metadata.path_param.type))
        for qk in query_params.keys():
            if (qm:=result.result[RequestValidatorResult.QUERY_KEY].get(qk, None)) is not None and qm.status == ValidatorStatus.OK:
                f = filter(lambda p: p.name == qk, metadata.query_params)
                try:
                    param = next(f)
                    assert (a:=result.combined_args.get(qk, None)) is not None and isinstance(a, self.root_type(param.type))
                except StopIteration:
                    continue
        if body is not None and metadata.body_param is not None:
            if (bm:=result.result[RequestValidatorResult.BODY_KEY].get(metadata.body_param.name, None)) is not None and bm.status == ValidatorStatus.OK:
                assert (a:=result.combined_args.get(metadata.body_param.name, None)) is not None and isinstance(a, self.root_type(metadata.body_param.type))
