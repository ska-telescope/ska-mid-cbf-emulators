from typing import Self
from unittest import mock

from emulator_engine.services.api.subcontroller_router_impl import SubcontrollerRouterImpl

class TestSubcontrollerRouterImpl:

    @mock.patch('emulator_engine.services.api.router_impl.RouterImpl.__init__')
    @mock.patch('ska_mid_cbf_emulators.common.EmulatorSubcontroller')
    def test_init_success(self: Self, mock_subcontroller: mock.MagicMock, mock_superclass_init: mock.MagicMock):
        _ = SubcontrollerRouterImpl(mock_subcontroller())
        mock_superclass_init.assert_called_once_with()
