import asyncio
from typing import Self
from unittest import mock

from fastapi import Request, Response
from fastapi.datastructures import QueryParams
import pytest
from emulator_engine.services.api.router_client import RouterClient
from emulator_engine.services.api.router_impl import RouterImpl
from emulator_engine.services.api.subcontroller_router_impl import SubcontrollerRouterImpl
from emulator_engine.services.ip_block_emulator.request_validator import RequestValidatorResult
from emulator_engine.services.ip_block_emulator.router_factory import RouterFactory
from ska_mid_cbf_emulators.common import BaseEmulatorApi, HttpMethod, InternalRestResponse, PathParam, QueryParam, BodyParam, ValidatorStatus, EmulatorSubcontroller

class RouterFactoryTestHelper(BaseEmulatorApi):

    @BaseEmulatorApi.route(http_method=HttpMethod.GET)
    def method_one(self: Self, value: QueryParam[int]) -> InternalRestResponse:
        return InternalRestResponse.ok({'value': value + 1})

    @BaseEmulatorApi.route(http_method=HttpMethod.POST)
    def method_two(self: Self, value: PathParam[int]) -> InternalRestResponse:
        if value > 0:
            return InternalRestResponse.ok({'value': self._method_four(value)})
        return InternalRestResponse.bad_request()

    @BaseEmulatorApi.route(http_method=HttpMethod.POST)
    def method_three(self: Self, value: BodyParam[dict[str, str]]) -> InternalRestResponse:
        if len(value) > 0:
            if value.get('mock_error') is not None:
                value = value / 0
            return InternalRestResponse.ok()
        return InternalRestResponse.bad_request()

    def _method_four(self: Self, value: int) -> int:
        return 100 // value


class TestRouterFactory:

    @pytest.fixture()
    @mock.patch('ska_mid_cbf_emulators.common.EmulatorSubcontroller')
    def subcontroller(self: Self, mock_subcontroller: mock.MagicMock):
        return mock_subcontroller()

    @pytest.fixture()
    def router(self: Self, subcontroller: EmulatorSubcontroller):
        return RouterFactory.create('a', subcontroller, RouterFactoryTestHelper(subcontroller))

    def test_create_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        client, impl = router
        assert isinstance(client, RouterClient)
        assert isinstance(impl, RouterImpl)
        assert hasattr(client, 'method_one')
        assert hasattr(client, 'method_two')
        assert hasattr(client, 'method_three')
        assert not hasattr(client, '_method_four')
        assert hasattr(impl, 'method_one')
        assert hasattr(impl, 'method_two')
        assert hasattr(impl, 'method_three')
        assert hasattr(impl, '_method_four')
        resp1 = impl.method_one(19)
        resp2 = impl.method_two(5)
        resp3 = impl.method_three({'data1': 'value1'})
        resp4 = impl._method_four(5)
        assert resp1.status == 200
        assert resp1.body.get('value') == 20
        assert resp2.status == 200
        assert resp2.body.get('value') == 20
        assert resp3.status == 200
        assert resp3.body == {}
        assert resp4 == 20

    def test_client_fn_validation_ok_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        client, _ = router
        with (
            mock.patch.object(client, 'get_rpc_response', return_value=InternalRestResponse.ok()),
            mock.patch('emulator_engine.services.ip_block_emulator.request_validator.RequestValidator.validate', return_value=RequestValidatorResult(ValidatorStatus.OK))
        ):
            async def body():
                return b'{}'
            req = mock.MagicMock(Request, path_params={}, query_params=QueryParams({}), body=body)
            resp = Response()
            asyncio.run(client.method_three(req, resp))
            assert resp.status_code == 200

    def test_client_fn_bad_body_returns_bad_request_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        client, _ = router
        with mock.patch.object(client, 'get_rpc_response'):
            async def body():
                return b'{'
            req = mock.MagicMock(Request, path_params={}, query_params=QueryParams({}), body=body)
            resp = Response()
            asyncio.run(client.method_three(req, resp))
            assert resp.status_code == 400

    def test_client_fn_validation_error_returns_bad_request_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        client, _ = router
        with (
            mock.patch.object(client, 'get_rpc_response'),
            mock.patch('emulator_engine.services.ip_block_emulator.request_validator.RequestValidator.validate', return_value=RequestValidatorResult(ValidatorStatus.ERROR))
        ):
            async def body():
                return b'{}'
            req = mock.MagicMock(Request, path_params={}, query_params=QueryParams({}), body=body)
            resp = Response()
            asyncio.run(client.method_three(req, resp))
            assert resp.status_code == 400

    def test_impl_fn_error_returns_internal_server_error_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        _, impl = router
        assert impl.method_three({'mock_error': 'true'}).status == 500
