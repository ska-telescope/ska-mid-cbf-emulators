from typing import Self
from unittest import mock

import pytest
from emulator_engine.services.api.router_client import RouterClient
from emulator_engine.services.api.subcontroller_router_impl import SubcontrollerRouterImpl
from emulator_engine.services.ip_block_emulator.ip_block_emulator_validator import IPBlockEmulatorValidatorResult
from emulator_engine.services.ip_block_emulator.ip_block_emulator_factory import IPBlockEmulatorFactory
from ska_mid_cbf_emulators.common import ValidatorStatus, EmulatorConfigIPBlock, ValidationError

class IPBlockEmulatorFactoryTestHelper():
    """"""

class TestIPBlockEmulatorFactory:
    @pytest.fixture()
    @mock.patch('emulator_engine.services.api.router_impl.RouterImpl')
    @mock.patch('emulator_engine.services.api.router_client.RouterClient')
    def router(self: Self, client: mock.MagicMock, impl: mock.MagicMock):
        return client(), impl()

    def test_create_validation_ok_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.OK), str, str, str, str)),
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router)
        ):
            _ = IPBlockEmulatorFactory.create('a', 'b', EmulatorConfigIPBlock('c', 'd', 'e', []), 1.0)

    def test_create_validation_warn_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.WARNING), str, str, str, str)),
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router)
        ):
            _ = IPBlockEmulatorFactory.create('a', 'b', EmulatorConfigIPBlock('c', 'd', 'e', []), 1.0)

    def test_create_validation_error_error(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.ERROR), str, str, IPBlockEmulatorFactoryTestHelper, str)),
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router),
            pytest.raises(ValidationError)
        ):
            _ = IPBlockEmulatorFactory.create('a', 'b', EmulatorConfigIPBlock('c', 'd', 'e', []), 1.0)

    def test_create_sets_constants_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.OK), str, str, IPBlockEmulatorFactoryTestHelper, str)),
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router)
        ):
            subcontroller, _ = IPBlockEmulatorFactory.create('a', 'b', EmulatorConfigIPBlock('c', 'd', 'e', [], constants={
                'test_const_1': 1234,
                'test_const_2': 'abcd',
                'test_const_3': {'value': ['data1', 'data2']}
            }), 1.0)
            assert hasattr(subcontroller.ip_block, 'test_const_1') and subcontroller.ip_block.test_const_1 == 1234
            assert hasattr(subcontroller.ip_block, 'test_const_2') and subcontroller.ip_block.test_const_2 == 'abcd'
            assert hasattr(subcontroller.ip_block, 'test_const_3') and subcontroller.ip_block.test_const_3 == {'value': ['data1', 'data2']}

    def test_create_many_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.OK), str, str, str, str)),
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router)
        ):
            _ = IPBlockEmulatorFactory.create_many('a', 'b', [EmulatorConfigIPBlock('c', 'd', 'e', []), EmulatorConfigIPBlock('f', 'g', 'h', [])], 1.0)

    def test_create_many_does_not_duplicate_success(self: Self, router: tuple[RouterClient, SubcontrollerRouterImpl]):
        with (
            mock.patch('emulator_engine.services.ip_block_emulator.ip_block_emulator_validator.IPBlockEmulatorValidator.validate_and_get_components', return_value=(IPBlockEmulatorValidatorResult(ValidatorStatus.OK), str, str, str, str)) as mock_validate,
            mock.patch('emulator_engine.services.ip_block_emulator.router_factory.RouterFactory.create', return_value=router)
        ):
            mock_validate.reset_mock()
            _ = IPBlockEmulatorFactory.create_many('a', 'b', [EmulatorConfigIPBlock('c', 'd', 'e', []), EmulatorConfigIPBlock('f', 'g', 'h', []), EmulatorConfigIPBlock('i', 'j', 'e', [])], 1.0)
            assert mock_validate.call_count == 2
