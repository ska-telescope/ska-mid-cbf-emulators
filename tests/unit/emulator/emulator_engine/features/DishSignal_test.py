from typing import Self

from jsonschema import ValidationError
import pytest
from emulator_engine.features.dish.signal import DishSignal

class TestDishSignal:

    def test_decode_success(self: Self):
        with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
            signal_str = signal_file.read()
        _ = DishSignal.decode(signal_str)

    def test_decode_fails_validation_error(self: Self):
        with pytest.raises(ValidationError):
            _ = DishSignal.decode('{"invalid_field": 9999999}')
