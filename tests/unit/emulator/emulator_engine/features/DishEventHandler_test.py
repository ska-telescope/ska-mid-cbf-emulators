from typing import Self
from unittest import mock

import pytest
from emulator_engine.features.dish.event_handler import DishEventHandler
from ska_mid_cbf_emulators.common import PulseEvent, ManualEvent, ManualEventSubType

class TestDishEventHandler:

    @pytest.fixture()
    @mock.patch('emulator_engine.features.dish.subcontroller.DishSubcontroller')
    def subcontroller(self: Self, mock_subcontroller: mock.MagicMock):
        return mock_subcontroller()

    def test_init_success(self: Self, subcontroller: mock.MagicMock):
        _ = DishEventHandler(subcontroller)

    def test_second_pulse_does_not_force_update_success(self: Self, subcontroller: mock.MagicMock):
        eh = DishEventHandler(subcontroller)
        with mock.patch.object(eh, '_generate_signal_update') as mock_gen_su:
            mock_gen_su.reset_mock()
            eh.handle_pulse_event(PulseEvent(source_timestamp=0))
            assert mock_gen_su.call_count == 1
            eh.handle_pulse_event(PulseEvent(source_timestamp=10))
            assert mock_gen_su.call_count == 1

    def test_unhandled_manual_event_subtype_returns_none_success(self: Self, subcontroller: mock.MagicMock):
        eh = DishEventHandler(subcontroller)
        resp = eh.handle_manual_event(ManualEvent(subtype=ManualEventSubType.GENERAL, source_timestamp=0))
        assert resp is None

    def test_injection_without_injection_type_is_not_handled_success(self: Self, subcontroller: mock.MagicMock):
        eh = DishEventHandler(subcontroller)
        with mock.patch('jsonschema.validate') as mock_validate:
            mock_validate.reset_mock()
            resp = eh.handle_manual_event(ManualEvent(subtype=ManualEventSubType.INJECTION, value={}, source_timestamp=0))
            assert resp is None
            mock_validate.assert_not_called()
