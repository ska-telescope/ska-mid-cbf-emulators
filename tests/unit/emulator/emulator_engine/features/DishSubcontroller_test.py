from typing import Self
from unittest import mock
from emulator_engine.features.dish.signal import DishSignal
from emulator_engine.features.dish.subcontroller import DishSubcontroller

class TestDishSubcontroller:

    def test_init_success(self: Self):
        with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
            signal_str = signal_file.read()
        signal = DishSignal.decode(signal_str)
        
        with mock.patch('ska_mid_cbf_emulators.common.BaseMessageService') as mock_msg_svc:
            _ = DishSubcontroller('test-emulator', mock_msg_svc(), mock_msg_svc(), None, None, '', '', 'dish', [], None, initial_signal=signal)
