import time
from typing import Self

from fastapi.testclient import TestClient
from httpx import Response

class TestInjectorAPI:

    def test_server_healthcheck_ok(self: Self, api_clients: tuple[TestClient, TestClient]):
        _, i_client = api_clients
        response: Response = i_client.get('/server_healthcheck')
        assert response.status_code == 200

    def test_inject_to_ip_block_success(self: Self, api_clients: tuple[TestClient, TestClient]):
        e_client, i_client = api_clients

        response = e_client.post('/ethernet_200g/start')
        assert response.status_code == 200

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'ethernet_200g',
                    'events': [
                        {
                            'severity': 'WARNING',
                            'value': {
                                'id': 'inj0001',
                                'injection_type': 'link_degrade',
                                'message': 'Degrade the link.',
                                'badness': 0.75
                            }
                        },
                        {
                            'severity': 'WARNING',
                            'value': {
                                'id': 'inj0002',
                                'injection_type': 'link_fix',
                                'message': 'Fix the link.',
                                'badness': 0.02
                            },
                            'offset': 5000
                        }
                    ]
                },
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'packet_validation',
                    'events': [
                        {
                            'severity': 'FATAL_ERROR',
                            'value': {
                                'id': 'inj0003',
                                'injection_type': 'fault',
                                'message': 'Packet validation fault'
                            },
                            'offset': 5000
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'DEGRADED'}

        time.sleep(5)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response: Response = e_client.get('/packet_validation/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'FAULT'}

        response = e_client.post('/ethernet_200g/recover')
        assert response.status_code == 200

        response = e_client.post('/packet_validation/recover')
        assert response.status_code == 200

    def test_inject_to_dish(self: Self, api_clients: tuple[TestClient, TestClient]):
        e_client, i_client = api_clients

        time.sleep(2)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        assert response.json().get('rx_sample_rate') == 3_900_000_000

        # Update properties (OK)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1000',
                                'injection_type': 'update_signal_properties',
                                'message': 'Change the DISH sample rate.',
                                'updated_properties': {
                                    'sample_rate': 3_900_123_456
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status: dict = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_123_456

        # Update properties (bad properties ignored OK)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1001',
                                'injection_type': 'update_signal_properties',
                                'message': 'Change the DISH sample rate.',
                                'updated_properties': {
                                    'invalid_property': -1,
                                    'sample_rate': 3_900_654_321
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status: dict = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_654_321

        # Update entire signal (OK)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1002',
                                'injection_type': 'update_signal',
                                'message': 'Change the DISH signal.',
                                'signal': {
                                    'packet_rate': 1_500_123.5,
                                    'sample_rate': 3_900_456_789,
                                    'dish_id': 'MKT004',
                                    'band_id': '4',
                                    'regular_sky_power': [0.7, 0.9],
                                    'noise_diode_on_power': [0.8, 0.95],
                                    'duty_cycle': 0.79
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_456_789
        assert wib_status.get('meta_dish_id') == 'MKT004'
        assert wib_status.get('meta_band_id') == '4'

        # Unsupported injection type (does not update values, OK)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1003',
                                'injection_type': 'dont_update_signal_properties',
                                'message': 'Change the DISH sample rate.',
                                'updated_properties': {
                                    'sample_rate': 3_999_999_999
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_456_789

        # Missing injection type (ERROR)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1004',
                                'message': 'Change the DISH sample rate.',
                                'updated_properties': {
                                    'sample_rate': 3_999_999_999
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 400

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_456_789

        # Schema mismatch for specific injection type (request OK, but should exit event handler without processing)
        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'dish',
                    'events': [
                        {
                            'severity': 'GENERAL',
                            'value': {
                                'id': 'inj1004',
                                'injection_type': 'update_signal_properties',
                                'message': 'Change the DISH sample rate.',
                                'invalid_properties': {
                                    'sample_rate': 3_999_999_999
                                }
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 200

        time.sleep(3)

        response: Response = e_client.get('/wideband_input_buffer/status')
        assert response.status_code == 200
        wib_status = response.json()
        assert wib_status.get('rx_sample_rate') == 3_900_456_789

    def test_inject_error_caught_internal_server_error(self: Self, api_clients: tuple[TestClient, TestClient]):
        e_client, i_client = api_clients

        response = e_client.post('/ethernet_200g/start')
        assert response.status_code == 200

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response: Response = i_client.post('/inject', content=b'[')
        assert response.status_code == 500

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response = e_client.post('/ethernet_200g/recover')
        assert response.status_code == 200

    def test_inject_validation_error_bad_request(self: Self, api_clients: tuple[TestClient, TestClient]):
        e_client, i_client = api_clients

        response = e_client.post('/ethernet_200g/start')
        assert response.status_code == 200

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response: Response = i_client.post('/inject', json={
            'injector_event_groups': [
                {
                    'bitstream_emulator_id': 'integration-injector-emulator',
                    'ip_block_emulator_id': 'ethernet_200g',
                    'event': [
                        {
                            'severity': 'WARNING',
                            'value': {
                                'id': 'inj0001',
                                'injection_type': 'link_degrade',
                                'message': 'Degrade the link.',
                                'badness': 0.75
                            }
                        }
                    ]
                }
            ]
        })
        assert response.status_code == 400

        time.sleep(2)

        response: Response = e_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'LINK'}

        response = e_client.post('/ethernet_200g/recover')
        assert response.status_code == 200
