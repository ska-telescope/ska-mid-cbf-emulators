from fastapi.testclient import TestClient
import pytest
from emulator_engine.features.controller.emulator_controller import EmulatorController
from emulator_injector.features.injector.injector_controller import InjectorController

@pytest.fixture(scope='function')
def api_clients():
    with open('tests/test_data/bs_emulators/api/ver/emu_src/config.json', 'r') as e_config_file:
        e_config_str = e_config_file.read()
    with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
        signal_str = signal_file.read()
    with open('tests/test_data/injector/integration_config.json', 'r') as i_config_file:
        i_config_str = i_config_file.read()
    e_controller = EmulatorController(e_config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'integration-injector-emulator', 1)
    i_controller = InjectorController(i_config_str)
    e_testclient = TestClient(e_controller.fastapi_app)
    i_testclient = TestClient(i_controller.fastapi_app)
    e_controller.flush_all()
    e_controller.start()
    i_controller.start()
    yield e_testclient, i_testclient
    e_controller.terminate()
    e_controller.await_api_cleanup()
    i_controller.stop()
