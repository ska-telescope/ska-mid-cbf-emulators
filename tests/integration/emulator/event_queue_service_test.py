import time
from typing import Self
from unittest import mock

import pika
import pika.adapters.blocking_connection
import pika.channel
import pytest
from emulator_engine.services.messaging.event_queue_service import EventQueueService
from ska_mid_cbf_emulators.common import ManualEvent, PulseEvent, SignalUpdateEvent, SignalUpdateEventList
from ska_mid_cbf_emulators.common.models.event.manual_event_subtype import ManualEventSubType


class TestEventQueueService:

    @pytest.fixture()
    def subscribed_service(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', ['test_ds_ip_block_id'], 1.0)
        svc.subscribe()
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    @pytest.fixture()
    def subscribed_service_end_of_chain(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', [], 1.0)
        svc.subscribe()
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    @pytest.fixture()
    def subscribed_service_multi_input(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', ['test_ds_ip_block_id'], 1.0, expected_input_pulses=2)
        svc.subscribe()
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    @pytest.fixture()
    def publisher_service(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id_publisher', ['test_ip_block_id'], 1.0)
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    @pytest.fixture()
    def subscribed_service_timeout(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', ['test_ds_ip_block_id'], 1.0, subscriber_timeout=3)
        svc.subscribe()
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    @pytest.fixture()
    def subscribed_service_no_manual_queue(self: Self):
        svc = EventQueueService.init_for_subcontroller('test_bitstream_emulator_id', 'test_ip_block_id', ['test_ds_ip_block_id'], 1.0)
        svc.manual_queue = ''
        svc.subscribe()
        yield svc
        if svc._listener_t is not None and svc._listener_t.is_alive():
            svc.unsubscribe()

    def test_pulse_timeout_success(self: Self, subscribed_service_timeout: EventQueueService):
        time.sleep(5)
        subscribed_service_timeout._listener_t.join(10)
        assert subscribed_service_timeout._listener_t is not None and not subscribed_service_timeout._listener_t.is_alive()

    def test_no_manual_queue_success(self: Self, subscribed_service_no_manual_queue: EventQueueService):
        with mock.patch('pika.adapters.blocking_connection.BlockingChannel.consume') as mock_consume:
            subscribed_service_no_manual_queue.publish(PulseEvent(), '', subscribed_service_no_manual_queue.pulse_queue)
            subscribed_service_no_manual_queue.unsubscribe()
            assert mock.call(subscribed_service_no_manual_queue.pulse_queue, inactivity_timeout=mock.ANY, auto_ack=mock.ANY) in mock_consume.mock_calls
            assert mock.call(subscribed_service_no_manual_queue.manual_queue, inactivity_timeout=mock.ANY) not in mock_consume.mock_calls

    def test_pulse_signal_manual_heartbeat_success(self: Self, subscribed_service: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service.flush()
        for t in range(11):
            subscribed_service.publish(SignalUpdateEvent(source_timestamp=20), '', subscribed_service.signal_update_queue)
        for t in range(11):
            subscribed_service.publish(ManualEvent(source_timestamp=t), '', subscribed_service.manual_queue)
        subscribed_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service.pulse_queue)
        time.sleep(1)
        for t in range(11):
            subscribed_service.publish(PulseEvent(source_timestamp=100+t), '', subscribed_service.pulse_queue)

    def test_expected_input_pulses_not_met_logs_error_and_continues_success(self: Self, subscribed_service_multi_input: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service_multi_input.flush()
        with (
            mock.patch.object(subscribed_service_multi_input, 'event_handler') as mock_event_handler,
            mock.patch.object(subscribed_service_multi_input, 'log_error') as mock_log_error,
        ):
            mock_event_handler.reset_mock()
            publisher_service.publish(PulseEvent(value={'id': 0}, source_timestamp=20), '', subscribed_service_multi_input.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 0
            mock_log_error.reset_mock()
            publisher_service.publish(PulseEvent(value={'id': 1}, source_timestamp=20), '', subscribed_service_multi_input.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 1
            mock_log_error.assert_called()
            mock_log_error.reset_mock()
            publisher_service.publish(PulseEvent(value={'id': 1}, source_timestamp=20), '', subscribed_service_multi_input.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 2
            mock_log_error.assert_not_called()

    def test_signal_updates_returned_are_published_success(self: Self, subscribed_service: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service.flush()
        with mock.patch.object(subscribed_service, 'publish') as mock_publish:
            mock_publish.reset_mock()
            su_event = SignalUpdateEvent(value={'a': 1}, source_timestamp=20)
            publisher_service.publish(su_event, '', subscribed_service.signal_update_queue)
            with mock.patch.object(subscribed_service.event_handler, 'handle_signal_update_events', return_value=SignalUpdateEventList([su_event], source_timestamp=20)) as mock_su_handler:
                mock_su_handler.reset_mock()
                publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service.pulse_queue)
                time.sleep(1)
                assert mock.call(event=su_event, exchange=subscribed_service.signal_update_exchange, channel=mock.ANY) in mock_publish.mock_calls

    def test_signal_updates_not_published_if_nothing_downstream_success(self: Self, subscribed_service_end_of_chain: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service_end_of_chain.flush()
        with mock.patch.object(subscribed_service_end_of_chain, 'publish') as mock_publish:
            mock_publish.reset_mock()
            su_event = SignalUpdateEvent(value={'a': 1}, source_timestamp=20)
            publisher_service.publish(su_event, '', subscribed_service_end_of_chain.signal_update_queue)
            with mock.patch.object(subscribed_service_end_of_chain.event_handler, 'handle_signal_update_events', return_value=SignalUpdateEventList([su_event], source_timestamp=20)) as mock_su_handler:
                mock_su_handler.reset_mock()
                publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service_end_of_chain.pulse_queue)
                time.sleep(1)
                assert mock.call(event=mock.ANY, exchange=subscribed_service_end_of_chain.signal_update_exchange, channel=mock.ANY) not in mock_publish.mock_calls

    def test_manual_events_returned_are_published_success(self: Self, subscribed_service: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service.flush()
        with mock.patch.object(subscribed_service, 'publish') as mock_publish:
            mock_publish.reset_mock()
            manual_event = ManualEvent(subtype=ManualEventSubType.GENERAL, value={'a': 1}, source_timestamp=15)
            publisher_service.publish(manual_event, '', subscribed_service.manual_queue)
            with mock.patch.object(subscribed_service.event_handler, 'handle_manual_event', return_value=[manual_event]) as mock_manual_handler:
                mock_manual_handler.reset_mock()
                publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service.pulse_queue)
                time.sleep(1)
                assert mock.call(event=manual_event, exchange=subscribed_service.manual_exchange, channel=mock.ANY) in mock_publish.mock_calls

    def test_manual_events_none_returned_does_not_publish_success(self: Self, subscribed_service: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service.flush()
        with mock.patch.object(subscribed_service, 'publish') as mock_publish:
            mock_publish.reset_mock()
            manual_event = ManualEvent(subtype=ManualEventSubType.GENERAL, value={'a': 1}, source_timestamp=15)
            publisher_service.publish(manual_event, '', subscribed_service.manual_queue)
            with mock.patch.object(subscribed_service.event_handler, 'handle_manual_event', return_value=None) as mock_manual_handler:
                mock_manual_handler.reset_mock()
                publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service.pulse_queue)
                time.sleep(1)
                assert mock.call(event=mock.ANY, exchange=subscribed_service.manual_exchange, channel=mock.ANY) not in mock_publish.mock_calls

    def test_manual_events_not_published_if_nothing_downstream_success(self: Self, subscribed_service_end_of_chain: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service_end_of_chain.flush()
        with mock.patch.object(subscribed_service_end_of_chain, 'publish') as mock_publish:
            mock_publish.reset_mock()
            manual_event = ManualEvent(subtype=ManualEventSubType.GENERAL, value={'a': 1}, source_timestamp=15)
            publisher_service.publish(manual_event, '', subscribed_service_end_of_chain.manual_queue)
            with mock.patch.object(subscribed_service_end_of_chain.event_handler, 'handle_manual_event', return_value=[manual_event]) as mock_manual_handler:
                mock_manual_handler.reset_mock()
                publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service_end_of_chain.pulse_queue)
                time.sleep(1)
                assert mock.call(event=mock.ANY, exchange=subscribed_service_end_of_chain.manual_exchange, channel=mock.ANY) not in mock_publish.mock_calls

    def test_pulse_signal_manual_sequence_success(self: Self, subscribed_service: EventQueueService, publisher_service: EventQueueService):
        publisher_service.flush()
        subscribed_service.flush()
        with mock.patch.object(subscribed_service, 'event_handler') as mock_event_handler:
            mock_event_handler.reset_mock()
            for t in range(3):
                publisher_service.publish(ManualEvent(source_timestamp=t), '', subscribed_service.manual_queue)
            publisher_service.publish(PulseEvent(source_timestamp=20), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 1
            assert mock_event_handler.handle_manual_event.call_count == 3
            assert mock_event_handler.handle_signal_update_events.call_count == 0
            for t in range(3):
                publisher_service.publish(PulseEvent(source_timestamp=100+t), '', subscribed_service.pulse_queue)
            time.sleep(4)
            assert mock_event_handler.handle_pulse_event.call_count == 4
            assert mock_event_handler.handle_manual_event.call_count == 3
            assert mock_event_handler.handle_signal_update_events.call_count == 0
            publisher_service.publish(ManualEvent(source_timestamp=1000), '', subscribed_service.manual_queue)
            publisher_service.publish(PulseEvent(source_timestamp=800), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 5
            assert mock_event_handler.handle_manual_event.call_count == 3
            assert mock_event_handler.handle_signal_update_events.call_count == 0
            publisher_service.publish(PulseEvent(source_timestamp=1200), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 6
            assert mock_event_handler.handle_manual_event.call_count == 4
            assert mock_event_handler.handle_signal_update_events.call_count == 0
            publisher_service.publish(SignalUpdateEvent(source_timestamp=1600), '', subscribed_service.signal_update_queue)
            publisher_service.publish(SignalUpdateEvent(source_timestamp=1800), '', subscribed_service.signal_update_queue)
            publisher_service.publish(SignalUpdateEvent(source_timestamp=2000), '', subscribed_service.signal_update_queue)
            publisher_service.publish(SignalUpdateEvent(source_timestamp=2000), '', subscribed_service.signal_update_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 6
            assert mock_event_handler.handle_manual_event.call_count == 4
            assert mock_event_handler.handle_signal_update_events.call_count == 0
            publisher_service.publish(PulseEvent(source_timestamp=1600), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 7
            assert mock_event_handler.handle_manual_event.call_count == 4
            assert mock_event_handler.handle_signal_update_events.call_count == 1
            publisher_service.publish(PulseEvent(source_timestamp=1800), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 8
            assert mock_event_handler.handle_manual_event.call_count == 4
            assert mock_event_handler.handle_signal_update_events.call_count == 2
            publisher_service.publish(PulseEvent(source_timestamp=2000), '', subscribed_service.pulse_queue)
            time.sleep(1)
            assert mock_event_handler.handle_pulse_event.call_count == 9
            assert mock_event_handler.handle_manual_event.call_count == 4
            assert mock_event_handler.handle_signal_update_events.call_count == 3
