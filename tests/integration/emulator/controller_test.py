from threading import Thread
import time
from typing import Self
from unittest import mock

from psutil import Process
import pytest
from emulator_engine.features.controller.emulator_controller import EmulatorController
from emulator_engine.services.controller.controller_state_machine import ControllerState
from ska_mid_cbf_emulators.common import EmulatorError


class TestEmulatorController:

    @pytest.fixture()
    def config_str(self: Self):
        with open('tests/test_data/bs_emulators/api/ver/emu_src/config.json', 'r') as config_file:
            config_str = config_file.read()
        return config_str

    @pytest.fixture()
    def signal_str(self: Self):
        with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
            signal_str = signal_file.read()
        return signal_str

    @pytest.fixture()
    def started_controller(self: Self, config_str: str, signal_str: str):
        controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)
        controller.flush_all()
        controller.start()
        time.sleep(3)
        yield controller
        if controller.state_machine.state != ControllerState.TERMINATED:
            controller.terminate()
            controller.await_api_cleanup()

    def test_init_connection_error_error(self: Self, config_str: str, signal_str: str):
        with (
            mock.patch('emulator_engine.features.controller.emulator_controller.BlockingConnection', side_effect=Exception('mock_connection_error')),
            pytest.raises(EmulatorError)
        ):
            _ = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)

    def test_command_connection_error_error(self: Self, config_str: str, signal_str: str):
        with (
            mock.patch('emulator_engine.features.controller.emulator_controller.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('emulator_engine.features.controller.emulator_controller.EmulatorController._init_connection'),
            mock.patch('emulator_engine.features.controller.emulator_controller.EmulatorController._start_controller_api'),
            mock.patch('emulator_engine.features.controller.emulator_controller.EmulatorController._kill_uvicorn'),
        ):
            controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)
            time.sleep(3)
            assert not controller._command_listener_t.is_alive()
            controller.terminate()
            controller.await_api_cleanup()

    def test_pulse_connection_error_error(self: Self, config_str: str, signal_str: str):
        controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)
        with (
            mock.patch('emulator_engine.features.controller.emulator_controller.BlockingConnection', side_effect=Exception('mock_connection_error')),
            mock.patch('ska_mid_cbf_emulators.common.EmulatorSubcontroller.subscribe'),
            pytest.raises(EmulatorError)
        ):
            controller.start()
        time.sleep(3)
        assert controller._pulse_t is None or not controller._pulse_t.is_alive()
        controller.await_api_cleanup()

    def test_no_command_queue_error(self: Self, config_str: str, signal_str: str):
        controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)
        controller._command_queue = ''
        controller.start()
        time.sleep(3)
        assert not controller._command_listener_t.is_alive()
        controller.terminate()
        controller.await_api_cleanup()

    def test_controller_in_subthread_join_success(self: Self, config_str: str, signal_str: str):
        def _intl():
            controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'controller-test-emulator', 1)
            controller.flush_all()
            controller.start()
            time.sleep(3)
            controller.terminate()
            controller.await_api_cleanup()
        t = Thread(target=_intl)
        t.start()
        t.join(timeout=20)
        assert not t.is_alive()

    def test_server_workers_killed_success(self: Self, started_controller: EmulatorController):
        mock_worker = mock.MagicMock(Process)
        with mock.patch('psutil.Process.children', return_value=[mock_worker]):
            started_controller.terminate()
            mock_worker.kill.assert_called_once()
            started_controller.await_api_cleanup()

    def test_illegal_state_changes_handled_success(self: Self, started_controller: EmulatorController):
        with mock.patch.object(started_controller, '_start_internal') as mock_start_internal:
            started_controller.start()
            mock_start_internal.assert_not_called()
        started_controller.stop()
        time.sleep(5)
        with mock.patch.object(started_controller, '_stop_internal') as mock_stop_internal:
            started_controller.stop()
            mock_stop_internal.assert_not_called()
        with mock.patch.object(started_controller.state_machine, 'may_terminate', return_value=False):
            with mock.patch.object(started_controller, '_terminate_internal') as mock_terminate_internal:
                started_controller.terminate()
                mock_terminate_internal.assert_not_called()
