from typing import Self
from emulator_engine.features.controller.emulator_controller import EmulatorController

from fastapi.testclient import TestClient
import time
from unittest import mock

from emulator_engine.services.controller.controller_router_impl import ControllerRouterImpl

class TestApiQueueServiceWithEngine:

    def test_force_stop_success(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        _, controller = api_client_and_controller
        controller._api_service.force_stop()
        controller._api_service._api_listener_t.join(10)
        assert not controller._api_service._api_listener_t.is_alive()

    def test_get_rpc_response_timeout_handled_success(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, _ = api_client_and_controller
        with mock.patch.object(ControllerRouterImpl, '_get_state', lambda _: time.sleep(35)):
            resp = api_client.get('/state')
            assert resp.status_code == 408

    def test_get_rpc_response_internal_call_error_handled_success(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, _ = api_client_and_controller
        with mock.patch.object(ControllerRouterImpl, '_get_state', lambda _: 1 / 0):
            resp = api_client.get('/state')
            assert resp.status_code == 500
