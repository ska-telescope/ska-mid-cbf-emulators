from typing import Self
from emulator_engine.features.controller.emulator_controller import EmulatorController

from fastapi.testclient import TestClient
from httpx import Response

class TestIPBlockEmulatorAPI:

    def test_get_state_ok(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, _ = api_client_and_controller
        response: Response = api_client.get('/ethernet_200g/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'RESET'}

    def test_get_state_diagram_ok(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, _ = api_client_and_controller
        response: Response = api_client.get('/ethernet_200g/state_diagram')
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'image/png'
        assert len(response.content) > 0

    def test_get_state_diagram_context_only_ok(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, _ = api_client_and_controller
        response: Response = api_client.get('/ethernet_200g/state_diagram?contextOnly=True')
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'image/png'
        assert len(response.content) > 0
