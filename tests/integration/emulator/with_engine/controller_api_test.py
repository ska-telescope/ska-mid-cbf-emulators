import time
from typing import Self
from emulator_engine.features.controller.emulator_controller import EmulatorController

from fastapi.testclient import TestClient
from httpx import Response

from emulator_engine.services.controller.controller_state_machine import ControllerState

class TestControllerAPI:

    def test_simple_request_flow_success(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, controller = api_client_and_controller
        response: Response = api_client.get('/server_healthcheck')
        assert response.status_code == 200

        response = api_client.get('/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'RUNNING'}

        response = api_client.post('/start')
        assert response.status_code == 409

        response = api_client.post('/stop')
        assert response.status_code == 202

        allowed_stop_time_remaining = 20
        while controller.state_machine.state != ControllerState.IDLE and allowed_stop_time_remaining > 0:
            time.sleep(1)
            allowed_stop_time_remaining -= 1
        assert allowed_stop_time_remaining > 0
        
        response = api_client.get('/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'IDLE'}
        
        response = api_client.get('/config')
        assert response.status_code == 200
        assert response.json().get('id', None) is not None
        
        response = api_client.get('/initial_signal')
        assert response.status_code == 200
        assert response.json().get('sample_rate', None) is not None

        response = api_client.post('/stop')
        assert response.status_code == 409

        response = api_client.post('/start')
        assert response.status_code == 202

        allowed_start_time_remaining = 20
        while controller.state_machine.state != ControllerState.RUNNING and allowed_start_time_remaining > 0:
            time.sleep(1)
            allowed_start_time_remaining -= 1
        assert allowed_start_time_remaining > 0
        
        response = api_client.get('/state')
        assert response.status_code == 200
        assert response.json() == {'current_state': 'RUNNING'}
        
        response = api_client.get('/graph')
        assert response.status_code == 200
        assert response.headers.get('content-type') == 'image/png'
        assert len(response.content) > 0

        response = api_client.post('/terminate')
        assert response.status_code == 202
        time.sleep(1)

        response = api_client.post('/terminate')
        assert response.status_code == 409

        allowed_terminate_time_remaining = 20
        while controller.state_machine.state != ControllerState.TERMINATED and allowed_terminate_time_remaining > 0:
            time.sleep(1)
            allowed_terminate_time_remaining -= 1
        assert allowed_terminate_time_remaining > 0
        time.sleep(3)

    def test_server_healthcheck_error_when_controller_in_error_state_success(self: Self, api_client_and_controller: tuple[TestClient, EmulatorController]):
        api_client, controller = api_client_and_controller
        controller.state_machine.error()
        response: Response = api_client.get('/server_healthcheck')
        assert response.status_code == 500
