import time
from typing import Self
from unittest import mock
from emulator_engine.features.controller.emulator_controller import EmulatorController

from fastapi.testclient import TestClient
from httpx import Response


class TestSignalUpdate:

    def test_initial_signal_flows_to_bottom_success(self: Self, api_client_and_controller_not_started: tuple[TestClient, EmulatorController]):
        api_client, controller = api_client_and_controller_not_started
        with mock.patch.object(controller._subcontrollers['packetizer'].event_service.event_handler, 'handle_signal_update_events') as mock_packetizer_su_handler:
            mock_packetizer_su_handler.reset_mock()
            controller.start()

            response: Response = api_client.get('/server_healthcheck')
            assert response.status_code == 200

            response = api_client.get('/state')
            assert response.status_code == 200
            assert response.json() == {'current_state': 'RUNNING'}

            time.sleep(5)

            mock_packetizer_su_handler.assert_called_once()

    def test_forced_signal_update_success(self: Self, api_client_and_controller_not_started: tuple[TestClient, EmulatorController]):
        api_client, controller = api_client_and_controller_not_started
        with mock.patch.object(controller._subcontrollers['packetizer'].event_service.event_handler, 'handle_signal_update_events') as mock_packetizer_su_handler:
            mock_packetizer_su_handler.reset_mock()
            controller.start()

            response: Response = api_client.get('/server_healthcheck')
            assert response.status_code == 200

            response = api_client.get('/state')
            assert response.status_code == 200
            assert response.json() == {'current_state': 'RUNNING'}

            time.sleep(5)

            controller._subcontrollers['wideband_input_buffer'].force_signal_update()

            time.sleep(5)

            assert mock_packetizer_su_handler.call_count == 2
