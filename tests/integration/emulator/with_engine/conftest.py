from fastapi.testclient import TestClient
import pytest
from emulator_engine.features.controller.emulator_controller import EmulatorController
from emulator_engine.services.controller.controller_state_machine import ControllerState

@pytest.fixture(scope='function')
def api_client_and_controller():
    with open('tests/test_data/bs_emulators/api/ver/emu_src/config.json', 'r') as config_file:
        config_str = config_file.read()
    with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
        signal_str = signal_file.read()
    controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'integration-test-emulator', 1)
    testclient = TestClient(controller.fastapi_app)
    controller.flush_all()
    controller.start()
    yield testclient, controller
    if controller.state_machine.state != ControllerState.TERMINATED:
        controller.terminate()
        controller.await_api_cleanup()

@pytest.fixture(scope='function')
def api_client_and_controller_not_started():
    with open('tests/test_data/bs_emulators/api/ver/emu_src/config.json', 'r') as config_file:
        config_str = config_file.read()
    with open('images/ska-mid-cbf-emulators-emulator/initial_signal.json', 'r') as signal_file:
        signal_str = signal_file.read()
    controller = EmulatorController(config_str, signal_str, 'tests/test_data/bs_emulators/api/ver/emu_src', 'integration-test-emulator', 1)
    testclient = TestClient(controller.fastapi_app)
    controller.flush_all()
    yield testclient, controller
    if controller.state_machine.state != ControllerState.TERMINATED:
        controller.terminate()
        controller.await_api_cleanup()
