from threading import Thread
import time
from typing import Self

from emulator_engine.services.controller.pulse_generator import PulseGenerator

class TestPulseGenerator:

    def test_pulse_generator_start_stop_success(self: Self):
        gen = PulseGenerator(1)
        x = []
        t = Thread(target=lambda: [x.append(True) for pulse in gen.start()])
        t.start()
        time.sleep(6)
        gen.stop()
        t.join(10)
        assert not t.is_alive()
        assert len(x) >= 5
